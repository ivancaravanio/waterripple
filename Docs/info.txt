-> http://www.roxlu.com/2013/024/height-field-simulation-on-gpu
-> http://www.roxlu.com/downloads/scholar/001.fluid.height_field_simulation.pdf
-> http://www.cs.ubc.ca/~rbridson/fluidsimulation/fluids_notes.pdf
-> http://matthias-mueller-fischer.ch/talks/siggraph2006.pdf
-> http://www.cs.ubc.ca/~rbridson/fluidsimulation/GameFluids2007.pdf
-> http://www.balintmiklos.com/layered_water.pdf
-> http://www.cs.umd.edu/class/fall2009/cmsc828v/presentations/Fluid_Sim_Overview.pdf
-> http://freespace.virgin.net/hugo.elias/graphics/x_water.htm
-> http://madebyevan.com/webgl-water/

ROXLU:
-> https://gist.github.com/roxlu?page=3 :
    -> https://gist.github.com/roxlu/8100466
    -> https://gist.github.com/roxlu/8106237
-> https://gist.github.com/roxlu?page=4 :
    -> https://gist.github.com/roxlu/8097039

-> http://www.roxlu.com/downloads/scholar/
-> https://github.com/roxlu/tinylib

-> http://http.download.nvidia.com/developer/presentations/GDC_2004/HLSL_WaterVTF.pdf
-> http://www.bfilipek.com/2014/01/simple-water-simulation.html#fea
-> https://www.youtube.com/watch?v=mh4ouyoMlL8
       For week 4 of Google Summer of Code 2013, I have implemented various height field water simulation algorithms.

       Tessendorf iWave: This is based on Jerry Tessendorf's "Interactive Water Surfaces", Game Programming Gems 4.

       Muller GDC2008: This is based on Matthias Müller-Fischer's "Fast Water Simulation for Games Using Height Fields", Games Developers Conference 2008.

       Muller GDC2008 HelloWorld: This is based on the the same article above but is the introductory "hello world" simulation. Amazing how a simple equation like this can work so well.

       X Water: This is based on a popular webpage online that a lot of folks quote as a reference: http://freespace.virgin.net/hugo.elia...

       The water sim resolution is a bit low at the moment (64x64). It is running serial codes at the moment on the CPU. I will make a GPU version in future weeks.

       [ Source Codes ]
       A snapshot of the source codes at the end of week 4 is available at GitHub: https://github.com/skeelogy/ifc-ar-fl...

       [ Background info ]
       For Google Summer of Code 2013, I am working with Iowa Flood Center to create a web-based augmented reality application using HTML5 technologies for an interactive flood simulation.

Ultra important !!!!!!!!!!!!!!!
-> http://www.cs.umd.edu/class/fall2009/cmsc828v/
-> http://www.cs.umd.edu/class/fall2009/cmsc828v/lectures.html
-> http://www.cs.umd.edu/class/spring2005/cmsc828v/

Procedural water:
-> http://graphics.ucsd.edu/courses/rendering/2005/jdewall/tessendorf.pdf
-> http://www-evasion.imag.fr/~Fabrice.Neyret/images/fluids-nuages/waves/Jonathan/articlesCG/simulating-ocean-water-01.pdf
-> https://www.youtube.com/watch?v=2AVh1x-Uqjs
-> https://www.sfdm.scad.edu/faculty/mkesson/vsfx319/wip/best_winter2004/ca301.3/palmer_sean/water/water.html

Full Navier-Stokes 3D water:
-> http://www.cs.columbia.edu/~keenan/Projects/GPUFluid/paper.pdf
-> http://www.cs.columbia.edu/~keenan/Projects/GPUFluid/
-> https://www.youtube.com/watch?v=D4FY75GwA00

-> https://www.opengl.org/discussion_boards/showthread.php/185120-Implement-Water-Ripple-effect
-> https://github.com/fenbf/simpleWater
-> http://vterrain.org/Water/
-> http://cg.skeelogy.com/skunamijs/
-> http://adrianboeing.blogspot.com/2011/02/ripple-effect-in-webgl.html
-> https://www.youtube.com/watch?v=-GMNAMBV3dQ
-> https://www.youtube.com/watch?v=SKbIhWD6apA
-> http://en.wikipedia.org/wiki/Shallow_water_equations
-> http://en.wikipedia.org/wiki/Navier%E2%80%93Stokes_equations:
       In physics, the Navier–Stokes equations [navˈjeː stəʊks], named after Claude-Louis Navier and George Gabriel Stokes,
       describe the motion of fluid substances.
       These equations arise from applying Newton's second law to fluid motion ...
-> http://en.wikipedia.org/wiki/Fluid_dynamics
-> http://users.ices.utexas.edu/~arbogast/cam397/dawson_v2.pdf
-> http://www.cs.cmu.edu/afs/cs/academic/class/15462-s12/www/project/p1.pdf
-> http://joachimcoppens.com/downloads/WaterByJoachimCoppens.pdf
-> https://www.opengl.org/discussion_boards/showthread.php/156031-Water-coords-in-heightmap
-> https://classes.soe.ucsc.edu/cmps162/Spring12/proj/klew3/proj/final%20report.pdf
-> http://tihlde.org/~pedjoha/prosjekter/Cg/waterreport.pdf
-> http://panoramix.ift.uni.wroc.pl/~maq/eng/waves.php
-> http://stackoverflow.com/questions/5012905/glsl-for-simple-water-surface-effects
-> http://http.developer.nvidia.com/GPUGems/gpugems_ch01.html
-> https://www.opengl.org/discussion_boards/showthread.php/171755-water-ripple
-> http://www.sulaco.co.za/opengl_project_water_ripples.htm
-> http://stackoverflow.com/questions/665076/how-to-implement-water-ripples

-> http://www.cs.virginia.edu/~gfx/Courses/2003/ImageSynthesis/papers/Acceleration/Fast%20MinimumStorage%20RayTriangle%20Intersection.pdf

How to parallelize the calculation of the water heightfield using GPGPU:
-> http://www.seas.upenn.edu/~cis565/Lectures2011/Lecture12.pdf
-> https://www.opengl.org/wiki/Sampler_%28GLSL%29
-> http://www.mathematik.uni-dortmund.de/~goeddeke/gpgpu/tutorial.html#arrays4
-> http://perso.limsi.fr/jacquemi/OGL-5
-> https://www.opengl.org/discussion_boards/showthread.php/168643-Is-sampler2DRect-really-necessary
-> https://www.packtpub.com/books/content/introduction-modern-opengl

OpenGL Programming Guide, 8th edition
    -> Color, Pixels, and Framebuffers
        -> Framebuffer Objects:
            Framebuffer objects are quite useful for performing off-screen-rendering,
            updating texture maps, and engaging in buffer ping-ponging (a data-transfer
            techniques used in GPGPU).
        -> Writing to Multiple Renderbuffers Simultaneously ( MRT (for multiple-render target) rendering )
            While this technique is used often in GPGPU, it can also be used when
            generating geometry and other information (like textures or normal map)
            which is written to different buffers during the same rendering pass.

-> https://www.opengl.org/wiki/Framebuffer_Object#Multiple_Render_Targets
-> https://www.opengl.org/wiki/GLSL_Object
-> https://www.opengl.org/wiki/OpenGL_Object
-> https://www.opengl.org/wiki/Sampler_Object

-> https://www.opengl.org/wiki/Uniform_(GLSL)
    Explicit uniform location
    (Core in version	4.5)
    (Core since version	4.3)

    Uniforms defined outside of interface block have a location. This location can be directly assigned in the shader, using this syntax:
    layout(location = 2) uniform mat4 modelToWorldMatrix;
    Calling glGetUniformLocation(prog, "modelToWorldMatrix");​ with is guaranteed to return 2.
    It is illegal to assign the same uniform location to two uniforms in the same shader or the same program.
    Even if those two uniforms have the same name and type in different shader stages, it is not allowed and will result in a linker error.
    Me: so, not mentioning location explicitly for the uniforms in different stages where they have exactly the same type and name
    will result in that OpenGL would automatically assign different locations for both "clonings" of the same variable?

    ...

    uniform vec3 initialUniform = vec3(1.0, 0.0, 0.0);
    This will cause the uniform to have this vector as its value until the user changes it.

    Driver bug note: Some drivers do not implement uniform initializers correctly.
    Me: this happens with mat3/4 uniforms.

    ...

    Uniform management
    Like regular OpenGL Objects, program objects encapsulate certain state.
    In this case, this state is a set of uniforms that will be used when rendering with that program.
    All of the stages within a program object use the same set of uniforms.
    Thus, if you have a uniform named "projectionMatrix" defined as a mat4 in both the vertex and the fragment stages,
    then there will be only one uniform of that name exposed by the program object.
    Changing this uniform will affect both the vertex and fragment stages.
    Therefore, it is a program linker error to have a uniform in a vertex and fragment shader that uses the same name, but has a different type in each shader.

    ...

    Changing uniform values
    However, in order to change a uniform value, you must first bind the program to the context with glUseProgram​. The glUniform*​ functions do not take a program object name; they use the currently bound program.

normals:
-> http://www.flipcode.com/archives/Calculating_Vertex_Normals_for_Height_Maps.shtml
-> http://stackoverflow.com/questions/5281261/generating-a-normal-map-from-a-height-map

VAO, state keeping, which function calls modify what state, when it is safe to unbind a bound buffer, etc.:
-> http://www.arcsynthesis.org/gltut/Positioning/Tutorial%2005.html
-> https://www.opengl.org/wiki/Vertex_Specification#Vertex_Array_Object

Line/Ray-Triangle intersection: GPU-based
-> https://www.iam.unibe.ch/de/forschung/publikationen/techreports/2004/iam-04-004/at_download/file

-> http://people.clemson.edu/~jtessen/papers_files/Interactive_Water_Surfaces.pdf

-> http://stackoverflow.com/questions/17650219/when-is-z-axis-flipped-opengl

Same as the point of the thesis - CPU to GPGPU comparison:
-> http://www.arcsynthesis.org/gltut/positioning/Tutorial%2003.html
-> http://www.arcsynthesis.org/gltut/
-> https://www.youtube.com/watch?v=-P28LKWTzrI

-> http://oglplus.org/

-> http://perso.limsi.fr/jacquemi/OGL-5/OGL-5-slides.pdf

-> https://www.youtube.com/watch?v=-P28LKWTzrI
-> http://www.ibiblio.org/e-notes/webgl/gpu/contents.htm
-> http://www.ibiblio.org/e-notes/webgl/gpu/
-> http://www.ibiblio.org/e-notes/webgl/gpu/flat_wave.htm
-> http://www.ibiblio.org/e-notes/webgl/gpu/flat_wave2.htm
-> http://www.ibiblio.org/e-notes/webgl/gpu/flat_wave21.htm
-> http://www.ibiblio.org/e-notes/webgl/gpu/big_wave.htm
-> http://www.ibiblio.org/e-notes/webgl/gpu/water.htm
-> http://www.ibiblio.org/e-notes/webgl/gpu/slit2.htm
-> http://www.ibiblio.org/e-notes/webcl/webcl.htm
-> http://www.ibiblio.org/e-notes/webgl/gpu/fluid.htm
-> http://en.wikipedia.org/wiki/Gaussian_function
-> http://ixacta.com/heat_wave/heat_wave/wave_equation.html
-> http://meatfighter.com/fluiddynamics/GPU_Gems_Chapter_38.pdf
-> http://dice.se/wp-content/uploads/water-interaction-ottosson_bjorn.pdf
-> http://www.researchgate.net/profile/Jianbin_Fang/publication/221084751_A_Comprehensive_Performance_Comparison_of_CUDA_and_OpenCL/links/0c96051c2bd67d9896000000.pdf
-> http://www.cs.cmu.edu/afs/cs/academic/class/16823-f06/appearance-modeling-22.ppt

-> http://www.dgp.toronto.edu/people/stam/reality/Research/pdf/ns.pdf
-> http://www.dgp.toronto.edu/people/stam/reality/Research/pdf/GDC03.pdf
-> http://physbam.stanford.edu/~fedkiw/papers/stanford2002-02.pdf
-> http://physbam.stanford.edu/~fedkiw/papers/stanford2001-01.pdf
-> https://books.google.bg/books?id=9OHvUz8CQRIC&pg=PA1&lpg=PA1&dq=GPU+and+Multicore+Programming+%E2%80%93+History+and+General+Principles&source=bl&ots=JuBLodrgJ-&sig=hNJjlHT1H0SiUP01pXhIJniDjIY&hl=en&sa=X&ei=8RzRVJi4GsH9ULv1g9AP&ved=0CEAQ6AEwBQ#v=onepage&q=GPU%20and%20Multicore%20Programming%20%E2%80%93%20History%20and%20General%20Principles&f=false
-> http://math.stackexchange.com/questions/22064/calculating-a-point-that-lies-on-an-ellipse-given-an-angle


Theory:
=======================================================================
-> http://www.balintmiklos.com/layered_water.pdf
-> http://graphics.ucsd.edu/courses/rendering/2005/jdewall/tessendorf.pdf
-> https://graphics.ethz.ch/~sobarbar/papers/Sol11b/Sol11b.pdf
-> http://conf.uni-ruse.bg/bg/docs/cp11/2/2-3.pdf
-> https://www.sfdm.scad.edu/faculty/mkesson/vsfx319/wip/best_winter2004/ca301.3/palmer_sean/water/water.html
-> http://rnd-zimmer.de/sph
-> http://gamasutra.com/blogs/FrankKane/20140122/209052/3D_Water_Effects_Heres_Whats_Involved.php?print=1
-> http://www.geforce.com/whats-new/articles/borderlands-2-physx
-> http://blogs.nvidia.com/blog/2014/03/20/directx-12/
-> http://www.cs.unc.edu/~lin/COMP768-S09/LEC/fluid.ppt
-> http://matthias-mueller-fischer.ch/realtimephysics/
-> http://www.pse.ice.bas.bg/www_systems_engineerig_laboratory/Distance_learning_systmeng/Distance_Course_7/Dict%20-%20viskozitet.html
-> http://www.pse.ice.bas.bg/www_systems_engineerig_laboratory/Distance_learning_systmeng/Distance_Course_7/

Navier-Stokes 3D:
-> http://www.cs.columbia.edu/~keenan/Projects/GPUFluid/

CFD:
-> http://magazines.elmedia.net/energia/eng-2011-1/images/pdf/eng-2011-1.pdf
-> http://chat.moto-zone.bg/moto-glory/64-motosport-legendi/1321-word-speed-record-1600kmh


OpenCL - rectangular mesh 1000 x 1000 better performance:
-> http://l2program.co.uk/tag/opengl

Word best practices:
====================
http://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/nrcs141p2_015431.pdf