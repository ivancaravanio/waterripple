#ifndef WATERRIPPLESIMULATOR_DEPLOYQT_LOGGER_H
#define WATERRIPPLESIMULATOR_DEPLOYQT_LOGGER_H

#include <QString>

namespace WaterRippleSimulator {
namespace DeployQt {

class Logger
{
private:
    Logger();

public:
    static void log( const QString& message );
};

}
}

#endif // WATERRIPPLESIMULATOR_DEPLOYQT_LOGGER_H
