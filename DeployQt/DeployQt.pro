QT += core
QT -= gui

CONFIG += console
CONFIG -= app_bundle

CONFIG += c++11

macx {
    QMAKE_CXXFLAGS += -std=c++11 -stdlib=libc++ -mmacosx-version-min=10.7
    LIBS += -stdlib=libc++ -mmacosx-version-min=10.7
}

CONFIG *= precompile_header

PRECOMPILED_HEADER = DeployQtPrecompiled.h

TARGET = DeployQt
TEMPLATE = app

SOURCES += \
    Logger.cpp \
    ModuleInfo.cpp \
    main.cpp \
    Version.cpp \
    Deployer.cpp

HEADERS += \
    DeployQtPrecompiled.h \
    Logger.h \
    ModuleInfo.h \
    Version.h \
    Deployer.h

include(../Build/Global.pri)

VERSION_MAJ = 1
VERSION_MIN = 0
VERSION_PAT = 2

include(../Build/Build.pri)

win32 {
    LIBS += -lVersion
}

win32 {
    DEPENDS_FILE_NAME = depends.exe
    DEFINES *= DEPENDS_FILE_NAME=\\\"$${DEPENDS_FILE_NAME}\\\"

    QMAKE_POST_LINK += $$copyFile( $${PWD}/../ThirdParty/depends/64/$${DEPENDS_FILE_NAME}, $${OUT_PWD}/$${BUILD} ) $$CRLF
    QMAKE_CLEAN     += $$fixedPath( $${OUT_PWD}/$${BUILD}/$${DEPENDS_FILE_NAME} )

    QMAKE_POST_LINK += $$copyFile( $${PWD}/../ThirdParty/depends/64/$${DEPENDS_FILE_NAME}, $${DEPLOYDIR} ) $$CRLF
    QMAKE_CLEAN     += $$fixedPath( $${DEPLOY}/$${BUILD}/$${DEPENDS_FILE_NAME} )
}
