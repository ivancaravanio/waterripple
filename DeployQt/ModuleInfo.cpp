#include "ModuleInfo.h"

#ifdef Q_OS_WIN

#include <Windows.h>

#include <QFile>
#include <QScopedArrayPointer>

using namespace WaterRippleSimulator::DeployQt;

ModuleInfo::ModuleInfo()
    : m_type( TypeInvalid ),
      m_build( BuildInvalid )
{
}

ModuleInfo::ModuleInfo( const QString& inFilePath )
    : m_filePath( inFilePath ),
      m_type( TypeInvalid ),
      m_build( BuildInvalid )
{
    ModuleInfo::moduleInfo( inFilePath, m_type, m_build, m_version );
}

QString ModuleInfo::filePath() const
{
    return m_filePath;
}

bool ModuleInfo::isValid() const
{
    return ( m_type == TypeApplication
             || m_type == TypeStaticLinkLibrary
             || m_type == TypeDynamicLinkLibrary )
           && ( m_build == BuildDebug
                || m_build == BuildRelease )
           && m_version.isValid();
}

ModuleInfo::Type ModuleInfo::type() const
{
    return m_type;
}

ModuleInfo::Build ModuleInfo::build() const
{
    return m_build;
}

Version ModuleInfo::version() const
{
    return m_version;
}

bool ModuleInfo::moduleInfo(
        const QString& inFilePath,
        ModuleInfo::Type& outType,
        ModuleInfo::Build& outBuild,
        Version& outVersion )
{
    if ( ! QFile::exists( inFilePath ) )
    {
        return false;
    }

    LPCTSTR szVersionFile = reinterpret_cast< LPCTSTR > ( inFilePath.utf16() );
    DWORD  verHandle = 0;
    UINT   size      = 0;
    LPBYTE lpBuffer  = NULL;
    DWORD  verSize   = GetFileVersionInfoSize( szVersionFile, &verHandle );

    if ( verSize == 0 || GetLastError() != ERROR_SUCCESS )
    {
        return false;
    }

    QScopedArrayPointer< CHAR > verData( new char[ verSize ] );
    static const QString SUB_BLOCK = "\\";
    if ( GetFileVersionInfo( szVersionFile, verHandle, verSize, verData.data() ) == FALSE )
    {
        return false;
    }

    if ( VerQueryValue( verData.data(), reinterpret_cast< LPCTSTR > ( SUB_BLOCK.utf16() ), (VOID FAR* FAR*) &lpBuffer , &size ) == FALSE )
    {
        return false;
    }

    if ( size == 0 )
    {
        return false;
    }

    VS_FIXEDFILEINFO* const verInfo = reinterpret_cast< VS_FIXEDFILEINFO* > ( lpBuffer );
    if ( verInfo->dwSignature != 0xfeef04bd )
    {
        return false;
    }

    if ( verInfo->dwFileType == VFT_APP )
    {
        outType = TypeApplication;
    }
    else if ( verInfo->dwFileType == VFT_STATIC_LIB )
    {
        outType = TypeStaticLinkLibrary;
    }
    else if ( verInfo->dwFileType == VFT_DLL )
    {
        outType = TypeDynamicLinkLibrary;
    }
    else
    {
        outType = TypeInvalid;
    }

    const DWORD fileFlags = verInfo->dwFileFlags & verInfo->dwFileFlagsMask;
    outBuild = ( fileFlags & 0x00000001L ) == 0x00000001L // VFF_DEBUG
               ? BuildDebug
               : BuildRelease;

    const int major = HIWORD( verInfo->dwFileVersionMS );
    const int minor = LOWORD( verInfo->dwFileVersionMS );
    const int patch = HIWORD( verInfo->dwFileVersionLS );
    const int build = LOWORD( verInfo->dwFileVersionLS );

    outVersion.set( major, minor, patch, build );

    return true;
}

#endif // Q_OS_WIN
