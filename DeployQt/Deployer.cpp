#include "Deployer.h"

#ifdef Q_OS_WIN

#include "Logger.h"
#include "ModuleInfo.h"

#include <limits>
#include <Windows.h>

#include <QCoreApplication>
#include <QDir>
#include <QFileInfo>
#include <QList>
#include <QPair>
#include <QProcess>
#include <QString>
#include <QTextStream>
#include <QUuid>

// The official Qt Windows deployment guidelines:
// http://qt-project.org/doc/qt-5.0/qtdoc/deployment-windows.html

using namespace WaterRippleSimulator::DeployQt;

const QString Deployer::QT_LIBRARY_REG_EXP_TEMPLATE = ".*[/\\\\]?Qt(%1d?[0-9]+|[0-9]+%1d?)\\.dll$";
const QString Deployer::LIBRARY_DEBUG_REG_EXP_TEMPLATE( "^%1d\\.dll$" );
const QString Deployer::LIBRARY_RELEASE_REG_EXP_TEMPLATE( "^%1\\.dll$" );
const QString Deployer::QT_CORE_LIBRARY_BASE_NAME( "Core" );
const QString Deployer::PLUGINS_DIR_NAME( "plugins" );

Deployer::Deployer()
{
}

void Deployer::deploy( const QString& targetFilePath, const QStringList& additionalLibraryIncludeDirPaths )
{
    const QFileInfo targetFileInfo( targetFilePath );
    if ( ! targetFileInfo.exists() )
    {
        return;
    }

    QStringList level1Dependencies = Deployer::filteredDependenciesFilePaths( targetFilePath, additionalLibraryIncludeDirPaths );
    if ( level1Dependencies.isEmpty() )
    {
        return;
    }

    QList< QPair< QString, QString > > level2Dependencies = Deployer::pluginsDelayLoadedDependenciesPaths( level1Dependencies, additionalLibraryIncludeDirPaths );
    for ( int i = 0; i < level2Dependencies.size(); ++ i )
    {
        const QPair< QString, QString >& additionalDependency = level2Dependencies[ i ];
        if ( level1Dependencies.contains( additionalDependency.first, Qt::CaseInsensitive ) )
        {
            level2Dependencies.removeAt( i );
            -- i;
            continue;
        }

        const QStringList level3Dependencies = Deployer::filteredDependenciesFilePaths( additionalDependency.first, additionalLibraryIncludeDirPaths );
        foreach ( const QString& level3Dependency, level3Dependencies )
        {
            if ( ! level1Dependencies.contains( level3Dependency, Qt::CaseInsensitive ) )
            {
                level1Dependencies.append( level3Dependency );
            }
        }
    }

    QList< QPair< QString, QString > > allDependencies;
    foreach ( const QString& level1Dependency, level1Dependencies )
    {
        allDependencies.append( QPair< QString, QString >( level1Dependency, QString() ) );
    }

    const int level2DependenciesCount = level2Dependencies.size();
    for ( int i = 0; i < level2DependenciesCount; ++ i )
    {
        const QPair< QString, QString >& level2Dependency = level2Dependencies[ i ];
        if ( ! level1Dependencies.contains( level2Dependency.first, Qt::CaseInsensitive ) )
        {
            allDependencies.append( level2Dependency );
        }
    }

    Logger::log( "Deploying dependencies for target module: " + targetFileInfo.canonicalFilePath() );

    const QDir targetFolder = targetFileInfo.absoluteDir();
    const int totalDependenciesCount = allDependencies.size();
    for ( int i = 0; i < totalDependenciesCount; ++ i )
    {
        const QPair< QString, QString >& dependency = allDependencies[ i ];
        QDir targetDependencyFolder = targetFolder;
        if ( ! dependency.second.isEmpty() )
        {
            if ( ! targetDependencyFolder.mkpath( dependency.second ) )
            {
                Logger::log( "failed to create relateive directory path: "
                             + dependency.second
                             + "\n in directory: "
                             + targetFolder.canonicalPath() );
                continue;
            }

            if ( ! targetDependencyFolder.cd( dependency.second ) )
            {
                Logger::log( "cannot access relateive directory path: "
                             + dependency.second
                             + "\n in directory: "
                             + targetFolder.canonicalPath() );
            }
        }

        const QString deployedDependencyFilePath = QDir::toNativeSeparators( targetDependencyFolder.filePath(
                                                                                 QFileInfo( dependency.first ).fileName() ) );
        const bool deployed = QFile::copy( dependency.first, deployedDependencyFilePath );
        QString deployResultMessage;
        QTextStream logStream( &deployResultMessage, QIODevice::WriteOnly );
        logStream << QString( "\tdeploying dependency %1/%2:" ).arg( i + 1 ).arg( totalDependenciesCount ) << endl
                  << "\tfrom:\t" << dependency.first << endl
                  << "\tto:\t" << deployedDependencyFilePath << endl
                  << "\tstatus:\t" << ( deployed ? "success" : "failure" ) << endl;
        Logger::log( deployResultMessage );
    }
}

bool Deployer::isDependencyWalkerResultOk( const int dependencyWalkerResult )
{
    return ! ( DependencyWalkerResults( dependencyWalkerResult )
               & ( DependencyWalkerResults( DependencyWalkerResultProcessingError1 )
                   | DependencyWalkerResultProcessingError2
                   | DependencyWalkerResultProcessingError3
                   | DependencyWalkerResultProcessingError4
                   | DependencyWalkerResultProcessingError5
                   | DependencyWalkerResultProcessingError6
                   | DependencyWalkerResultProcessingError7
                   | DependencyWalkerResultProcessingError8 ) );
}

QFileInfo Deployer::qtCoreFileInfo( const QStringList& inDependenciesFilePaths )
{
    static const QRegExp QT_CORE_LIBRARY_REG_EXP( QT_LIBRARY_REG_EXP_TEMPLATE.arg( QT_CORE_LIBRARY_BASE_NAME ), Qt::CaseInsensitive );
    const int index = inDependenciesFilePaths.indexOf( QT_CORE_LIBRARY_REG_EXP );
    if ( index == -1 )
    {
        Logger::log( "QtCore library not found as a dependency." );

        return QFileInfo();
    }

    const QFileInfo qtCoreFileInfo( inDependenciesFilePaths[ index ] );
    if ( ! qtCoreFileInfo.exists() )
    {
        Logger::log( "QtCore library file does not exist." );

        return QFileInfo();
    }

    return qtCoreFileInfo;
}

QDir Deployer::qtFrameworkDir( const QFileInfo& inQtCoreFileInfo )
{
    QDir frameworkDir = Deployer::qtLibDir( inQtCoreFileInfo );
    if ( ! frameworkDir.cdUp() )
    {
        return QDir();
    }

    return frameworkDir;
}

QDir Deployer::qtLibDir( const QFileInfo& inQtCoreFileInfo )
{
    return inQtCoreFileInfo.dir();
}

QDir Deployer::qtBinDir( const QFileInfo& inQtCoreFileInfo )
{
    QDir binDir = Deployer::qtFrameworkDir( inQtCoreFileInfo );

    if ( ! binDir.cd( "bin" ) )
    {
        return QDir();
    }

    return binDir;
}

QDir Deployer::qtPluginsDir( const QFileInfo& inQtCoreFileInfo )
{
    QDir pluginsDir = Deployer::qtFrameworkDir( inQtCoreFileInfo );

    if ( ! pluginsDir.cd( PLUGINS_DIR_NAME ) )
    {
        return QDir();
    }

    return pluginsDir;
}

QStringList Deployer::allDependenciesFilePaths( const QString& targetFilePath, const QStringList& additionalLibraryIncludeDirPaths )
{
    const QFileInfo targetFileInfo( targetFilePath );
    if ( ! targetFileInfo.exists() )
    {
        Logger::log( "target file \"" + targetFilePath + "\" not found." );

        return QStringList();
    }

    const QDir appDir = qApp->applicationDirPath();
    QString dependsOutputFileNameSuffix = QUuid::createUuid().toString();
    dependsOutputFileNameSuffix.chop( 1 );
    dependsOutputFileNameSuffix.remove( 0, 1 );
    const QString dependsOutputFilePath = appDir.filePath( QString( "dependencyTable_%1.csv" ).arg( dependsOutputFileNameSuffix ) );
    QFile dependsOutputFile( dependsOutputFilePath );
    if ( dependsOutputFile.exists() )
    {
        dependsOutputFile.remove();
    }

    static bool environmentUpdated = false;
    if ( ! environmentUpdated )
    {
#ifdef max
#undef max
#endif

        const int environmentVariableValueMaxByteCount = std::numeric_limits< qint16 >::max();
        QByteArray pathEnvironmentVariableValue( environmentVariableValueMaxByteCount, '\0' );
        const char PATH_ENVIRONMENT_VARIABLE_NAME[] = "PATH";
        const int charactersCount = ::GetEnvironmentVariableA( PATH_ENVIRONMENT_VARIABLE_NAME, pathEnvironmentVariableValue.data(), pathEnvironmentVariableValue.size() );
        if ( charactersCount >= 0 && ::GetLastError() != ERROR_ENVVAR_NOT_FOUND )
        {
            pathEnvironmentVariableValue.truncate( charactersCount );
            static const char ENV_VARIABLE_VALUES_SEPARATOR = ';';
            pathEnvironmentVariableValue.append( ENV_VARIABLE_VALUES_SEPARATOR );
            pathEnvironmentVariableValue += additionalLibraryIncludeDirPaths.join( QChar( QLatin1Char( ENV_VARIABLE_VALUES_SEPARATOR ) ) );
            if ( pathEnvironmentVariableValue.size() <= environmentVariableValueMaxByteCount )
            {
                if ( ::SetEnvironmentVariableA( PATH_ENVIRONMENT_VARIABLE_NAME, pathEnvironmentVariableValue.constData() ) == FALSE )
                {
                    Logger::log( QString( "Failed to set %1 environment variable\'s value." ).arg( PATH_ENVIRONMENT_VARIABLE_NAME ) );
                }
            }
            else
            {
                Logger::log( QString( "The updated %1 environment variable\'s value exceeds the maximum of %2 bytes." )
                             .arg( PATH_ENVIRONMENT_VARIABLE_NAME )
                             .arg( environmentVariableValueMaxByteCount ) );
            }
        }
        else
        {
            Logger::log( QString( "Cannot retrieve %1 environment variable\'s value." ).arg( PATH_ENVIRONMENT_VARIABLE_NAME ) );
        }

        environmentUpdated = true;
    }

    const int dependencyWalkerResult =
            QProcess::execute(
                QFileInfo( appDir, DEPENDS_FILE_NAME ).absoluteFilePath(),
                QStringList()
                << "/c"
                << "/f:1"
                << "/ph:1"
                << "/pl:1"
                << "/pg:1"
                << ( "/oc:" + dependsOutputFilePath )
                << targetFilePath );
    if ( ! isDependencyWalkerResultOk( dependencyWalkerResult ) )
    {
        Logger::log( "target file \"" + targetFilePath + "\" not found." );

        return QStringList();
    }

    if ( ! dependsOutputFile.exists() )
    {
        Logger::log( "the generated temporary file that contain the target's dependencies - \""
                     + dependsOutputFilePath
                     + "\", was not found." );

        return QStringList();
    }

    if ( ! dependsOutputFile.open( QIODevice::ReadOnly ) )
    {
        Logger::log( "the generated temporary file that contain the target's dependencies - \""
                     + dependsOutputFilePath +
                     "\", exists but cannot be opened for reading." );

        return QStringList();
    }

    QStringList filePaths;

    // column headers line
    dependsOutputFile.readLine();
    // target file line
    dependsOutputFile.readLine();
    while ( ! dependsOutputFile.atEnd() )
    {
        QString dependencyFilePath = QString::fromLatin1( dependsOutputFile.readLine() ).split( QLatin1Char( ',' ) ).at( 1 );
        if ( dependencyFilePath.isEmpty() )
        {
            continue;
        }

        dependencyFilePath.remove( QChar( QLatin1Char( '\"' ) ) );
        const QFileInfo dependencyFileInfo( dependencyFilePath );
        if ( dependencyFileInfo.exists() )
        {
            filePaths.append( QDir::toNativeSeparators( dependencyFileInfo.canonicalFilePath() ) );
        }
    }

    if ( filePaths.isEmpty() )
    {
        Logger::log( "not a single dependency was found." );
    }

    dependsOutputFile.close();
    dependsOutputFile.remove();

    return filePaths;
}

QStringList Deployer::filteredDependenciesFilePaths( const QStringList& inDependenciesFilePaths, const QStringList& additionalLibraryIncludeDirPaths )
{
    if ( inDependenciesFilePaths.isEmpty() )
    {
        return QStringList();
    }

    const QFileInfo coreFileInfo = Deployer::qtCoreFileInfo( inDependenciesFilePaths );
    if ( ! coreFileInfo.exists() )
    {
        return QStringList();
    }

    const QDir libDir = Deployer::qtLibDir( coreFileInfo );
    const QDir binDir = Deployer::qtBinDir( coreFileInfo );

    QList< QDir > dependencySearchDirs;
    if ( libDir.exists() )
    {
        dependencySearchDirs.append( libDir );
    }

    if ( libDir.exists() )
    {
        dependencySearchDirs.append( binDir );
    }

    foreach ( const QString& additionalLibraryIncludeDirPath, additionalLibraryIncludeDirPaths )
    {
        const QDir additionalLibraryIncludeDir( additionalLibraryIncludeDirPath );
        if ( additionalLibraryIncludeDir.exists() )
        {
            dependencySearchDirs.append( additionalLibraryIncludeDir );
        }
    }

    QStringList filteredDepsFilePaths;
    foreach ( const QString& dependencyFilePath, inDependenciesFilePaths )
    {
        const QFileInfo dependencyFileInfo( dependencyFilePath );
        if ( ! dependencyFileInfo.exists() )
        {
            Logger::log( "the dependency library \"" + dependencyFilePath + "\" does not exist." );

            continue;
        }

        static const QRegExp stdLibrariesRegExp( "^(msvc|mingwm|libgcc).*\\.dll$", Qt::CaseInsensitive );
        const QDir dependencyDir = dependencyFileInfo.dir();
        if ( dependencySearchDirs.contains( dependencyDir )
             || stdLibrariesRegExp.exactMatch( dependencyFileInfo.fileName() ) )
        {
            filteredDepsFilePaths.append( dependencyFilePath );
        }
    }

    if ( filteredDepsFilePaths.isEmpty() )
    {
        Logger::log( "not a single dependency was filtered out." );
    }

    return filteredDepsFilePaths;
}

QStringList Deployer::filteredDependenciesFilePaths( const QString& targetFilePath, const QStringList& additionalLibraryIncludeDirPaths )
{
    return Deployer::filteredDependenciesFilePaths( Deployer::allDependenciesFilePaths( targetFilePath, additionalLibraryIncludeDirPaths ), additionalLibraryIncludeDirPaths );
}

QList< QPair< QString, QString > > Deployer::pluginsDelayLoadedDependenciesPaths( const QStringList& inDependenciesFilePaths, const QStringList& additionalLibraryIncludeDirPaths )
{
    if ( inDependenciesFilePaths.isEmpty() )
    {
        return QList< QPair< QString, QString > >();
    }

    const QFileInfo qtCoreFi = Deployer::qtCoreFileInfo( inDependenciesFilePaths );
    if ( ! qtCoreFi.exists() )
    {
        return QList< QPair< QString, QString > >();
    }

    const ModuleInfo qtCoreModuleInfo( qtCoreFi.filePath() );
    if ( ! qtCoreModuleInfo.isValid() )
    {
        return QList< QPair< QString, QString > >();
    }

    QList< QPair< QString, QString > > outPaths;

    const bool isQt5AndAbove = qtCoreModuleInfo.version().major() >= 5;
    const bool isDebugBuild = qtCoreModuleInfo.build() == ModuleInfo::BuildDebug;
    if ( isQt5AndAbove )
    {
        // TODO: make the detection of the ANGLE library used
        // as a dependency by some Qt 5 library modules automatic
        // http://qt-project.org/forums/viewthread/21269/

        QString libEGLFileName = "libEGL";
        if ( isDebugBuild )
        {
            libEGLFileName += QChar( QLatin1Char( 'd' ) );
        }

        libEGLFileName += ".dll";

        const QFileInfo eglLibraryInfo( Deployer::qtLibDir( qtCoreFi ), libEGLFileName );
        if ( eglLibraryInfo.exists() )
        {
            static const QString DEPENDENCIES_DEPLOY_RELATIVE_DIR_PATH;
            outPaths.append( QPair< QString, QString >( QDir::toNativeSeparators( eglLibraryInfo.canonicalFilePath() ), DEPENDENCIES_DEPLOY_RELATIVE_DIR_PATH ) );
        }
        else
        {
            Logger::log( "the dependency library \"" + eglLibraryInfo.filePath() + "\" does not exist." );
        }
    }

    QStringList pluginTypeRelativeDirPaths;
    if ( inDependenciesFilePaths.indexOf( QRegExp( QT_LIBRARY_REG_EXP_TEMPLATE.arg( "Gui" ), Qt::CaseInsensitive ) ) >= 0 )
    {
        pluginTypeRelativeDirPaths.append( "imageformats" );
    }

    if ( inDependenciesFilePaths.indexOf( QRegExp( QT_LIBRARY_REG_EXP_TEMPLATE.arg( "Sql" ), Qt::CaseInsensitive ) ) >= 0 )
    {
        pluginTypeRelativeDirPaths.append( "sqldrivers" );
    }

    if ( inDependenciesFilePaths.indexOf( QRegExp( QT_LIBRARY_REG_EXP_TEMPLATE.arg( "Network" ), Qt::CaseInsensitive ) ) >= 0 )
    {
        pluginTypeRelativeDirPaths.append( "bearer" );
    }

    if ( inDependenciesFilePaths.indexOf( QRegExp( QT_LIBRARY_REG_EXP_TEMPLATE.arg( "Multimedia" ), Qt::CaseInsensitive ) ) >= 0 )
    {
        pluginTypeRelativeDirPaths.append( "mediaservice" );
        pluginTypeRelativeDirPaths.append( "playlistformats" );
    }

    if ( isQt5AndAbove )
    {
        pluginTypeRelativeDirPaths.append( "platforms" );
    }

    QDir pluginsDir;
    QStringList pluginDirPathCandidates;
    pluginDirPathCandidates.reserve( 1 + additionalLibraryIncludeDirPaths.size() );
    pluginDirPathCandidates << Deployer::qtPluginsDir( qtCoreFi ).canonicalPath()
                            << additionalLibraryIncludeDirPaths;
    QStringList pluginsRelativeDirPaths;
    pluginsRelativeDirPaths.reserve( 2 );
    pluginsRelativeDirPaths << "."
                            << ( QString( ".." ) + QDir::separator() + PLUGINS_DIR_NAME );
    foreach ( const QString& pluginDirPathCandidate, pluginDirPathCandidates )
    {
        QDir pluginDirCandidate( pluginDirPathCandidate );
        if ( ! pluginDirCandidate.exists() )
        {
            continue;
        }

        bool pluginDirFound = false;
        foreach ( const QString& pluginsRelativeDirPath, pluginsRelativeDirPaths )
        {
            if ( ! pluginDirCandidate.cd( pluginsRelativeDirPath ) )
            {
                continue;
            }

            bool containsAllNeededPluginFolders = true;
            foreach ( const QString& pluginTypeRelativeDirPath, pluginTypeRelativeDirPaths )
            {
                QDir pluginTypeDirCandidate = pluginDirCandidate;
                if ( ! pluginTypeDirCandidate.cd( pluginTypeRelativeDirPath ) )
                {
                    containsAllNeededPluginFolders = false;
                    break;
                }
            }

            if ( containsAllNeededPluginFolders )
            {
                pluginsDir = pluginDirCandidate;
                pluginDirFound = true;
                break;
            }
        }

        if ( pluginDirFound )
        {
            break;
        }
    }

    foreach ( const QString& pluginTypeRelativeDirPath, pluginTypeRelativeDirPaths )
    {
        QDir pluginTypeDir = pluginsDir;
        if ( ! pluginTypeDir.cd( pluginTypeRelativeDirPath ) )
        {
            Logger::log( QString( "no \"%1\" plugins subdirectory found" ).arg( pluginTypeRelativeDirPath ) );
            continue;
        }

        static const QStringList FILE_TYPES_FILTER = QStringList() << "*.dll";
        const QStringList pluginFileNames = pluginTypeDir.entryList( FILE_TYPES_FILTER );
        if ( pluginFileNames.isEmpty() )
        {
            Logger::log( QString( "no plugins found in subdirectory: \"%1\"" ).arg( pluginTypeRelativeDirPath ) );
            continue;
        }

        foreach ( const QString& pluginFileName, pluginFileNames )
        {
            const QString pluginFilePath = QDir::toNativeSeparators( pluginTypeDir.filePath( pluginFileName ) );
            const ModuleInfo pluginModuleInfo( pluginFilePath );

            bool deployPlugin = false;
            if ( pluginModuleInfo.isValid() )
            {
                deployPlugin = pluginModuleInfo.build() == qtCoreModuleInfo.build();
            }
            else
            {
                const QString pluginCompleteBaseName = QFileInfo( pluginFilePath ).completeBaseName();
                const int qtMajorVersion = qtCoreModuleInfo.version().major();
                bool isPluginDebugVersion = false;
                if ( qtMajorVersion >= 5 )
                {
                    isPluginDebugVersion = pluginCompleteBaseName.endsWith( QChar( QLatin1Char( 'd' ) ) );
                }
                else
                {
                    isPluginDebugVersion = pluginCompleteBaseName.endsWith( QString( "d%1" ).arg( qtMajorVersion ) );
                }

                const bool isQtDebugVersion = qtCoreModuleInfo.build() == ModuleInfo::BuildDebug;
                deployPlugin = isQtDebugVersion == isPluginDebugVersion;
            }

            if ( deployPlugin )
            {
                outPaths.append( QPair< QString, QString >( pluginFilePath, pluginTypeRelativeDirPath ) );
            }
        }
    }

    return outPaths;
}

bool Deployer::qtVersionBuild( const QString& qtCoreLibraryFileName, int& qtMajorVersion, bool& isDebug )
{
    static const QString QT_5_AND_ABOVE_LIBRARY_REG_EXP_TEMPLATE = QString( "^Qt[5-9][0-9]*%1d?\\.dll$" ).arg( QT_CORE_LIBRARY_BASE_NAME );
    static const QString QT_4_AND_BELOW_LIBRARY_REG_EXP_TEMPLATE = QString( "^Qt%1d?[0-4]\\.dll$" ).arg( QT_CORE_LIBRARY_BASE_NAME );

    static const QRegExp QT_5_AND_ABOVE_LIBRARY_REG_EXP( QT_5_AND_ABOVE_LIBRARY_REG_EXP_TEMPLATE, Qt::CaseInsensitive );
    static const QRegExp QT_4_AND_BELOW_LIBRARY_REG_EXP( QT_4_AND_BELOW_LIBRARY_REG_EXP_TEMPLATE, Qt::CaseInsensitive );
    if ( qtCoreLibraryFileName.contains( QT_5_AND_ABOVE_LIBRARY_REG_EXP ) )
    {
        const int startIndex = sizeof( "Qt" ) / sizeof( char );
        const int endIndex = qtCoreLibraryFileName.indexOf( QRegExp( "[a-zA-Z]" ), startIndex );
        const QString versionString = qtCoreLibraryFileName.mid( startIndex, endIndex - startIndex + 1 );
        bool converted = false;
        const int qtMajorVersionCandidate = versionString.toInt( & converted );
        if ( ! converted )
        {
            return false;
        }

        qtMajorVersion = qtMajorVersionCandidate;
    }
    else if ( qtCoreLibraryFileName.contains( QT_4_AND_BELOW_LIBRARY_REG_EXP ) )
    {
        const int startIndex = sizeof( "Qt" ) / sizeof( char );
        const int endIndex = qtCoreLibraryFileName.indexOf( QRegExp( "[a-zA-Z]" ), startIndex );
        const QString versionString = qtCoreLibraryFileName.mid( startIndex, endIndex - startIndex + 1 );
        bool converted = false;
        const int qtMajorVersionCandidate = versionString.toInt( & converted );
        if ( ! converted )
        {
            return false;
        }

        qtMajorVersion = qtMajorVersionCandidate;
    }

    isDebug = qtCoreLibraryFileName.endsWith( "d.dll", Qt::CaseInsensitive );

    return true;
}

#endif // Q_OS_WIN
