LOCALE_ISO_NAMES = it \
                   de \
                   el \
                   es \
                   fr \
                   nl \
                   pl \
                   pt \
                   ru \
                   tr

TRANSLATION_LOCALES = $$join( LOCALE_ISO_NAMES, : )
TRANSLATION_FILE_NAME_FORMAT = %1_%2.%3

DEFINES += TRANSLATION_LOCALES=\\\"$${TRANSLATION_LOCALES}\\\"

TRANSLATIONS = $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 0  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 1  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 2  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 3  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 4  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 5  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 6  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 7  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 8  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 9  ), ts )
