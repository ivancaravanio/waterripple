TEMPLATE = app
TARGET = macdeployqt

# DESTDIR = $$QT.designer.bins

SOURCES += main.cpp
CONFIG -= app_bundle

CONFIG += c++11

macx {
    QMAKE_CXXFLAGS += -std=c++11 -stdlib=libc++ -mmacosx-version-min=10.7
    LIBS += -stdlib=libc++ -mmacosx-version-min=10.7
}

include(../shared/shared.pri)

# target.path = $$[QT_INSTALL_BINS]
# INSTALLS += target

QMAKE_CLEAN  += $$TARGET
