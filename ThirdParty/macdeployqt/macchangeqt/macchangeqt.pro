TEMPLATE = app
TARGET = macchangeqt

SOURCES += main.cpp
CONFIG -= app_bundle

CONFIG += c++11

macx {
    QMAKE_CXXFLAGS += -std=c++11 -stdlib=libc++ -mmacosx-version-min=10.7
    LIBS += -stdlib=libc++ -mmacosx-version-min=10.7
}

include(../shared/shared.pri)

QMAKE_CLEAN += $$TARGET
