TEMPLATE = app
TARGET = tst_deployment_mac

greaterThan(QT_MAJOR_VERSION, 4): QT += testlib
else: CONFIG += qtestlib

SOURCES += tst_deployment_mac.cpp
HEADERS += tst_deployment_mac.h

include(../shared/shared.pri)
