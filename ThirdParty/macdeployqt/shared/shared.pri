INCLUDEPATH += $${PWD}

SOURCES += \
    $${PWD}/moduleinfo.cpp \
    $${PWD}/abstractloggerstream.cpp \
    $${PWD}/logger.cpp \
    $${PWD}/deployer.cpp

HEADERS += \
    $${PWD}/moduleinfo.h \
    $${PWD}/abstractloggerstream.h \
    $${PWD}/logger.h \
    $${PWD}/deployer.h
