/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#ifndef MACDEPLOYQT_SHARED_LOGGER_H
#define MACDEPLOYQT_SHARED_LOGGER_H

#include "abstractloggerstream.h"

#include <QFile>
#include <QString>
#include <QTextStream>
#include <QWeakPointer>
#include <QSharedPointer>

namespace MacDeployQt {
namespace Shared {

enum SeverityLevel
{
    SeverityLevelNone = 0x0,
    SeverityLevelInfo = 0x1 << 0,
    SeverityLevelWarning = 0x1 << 1,
    SeverityLevelError = 0x1 << 2
};

Q_DECLARE_FLAGS(SeverityLevels, SeverityLevel)

enum VerbosityLevel
{
    VerbosityLevelNone = 0,
    VerbosityLevelErrors = 1,
    VerbosityLevelErrorsAndWarnings = 2,
    VerbosityLevelInfo = 3
};

class Logger
{
private:
    Logger();
    Logger(const QString &filePath, const SeverityLevels &severityLevels);
    Logger(QIODevice *device, const SeverityLevels &severityLevels);
    Logger(FILE *fileHandle, const SeverityLevels &severityLevels);
    Logger(QString &string, const SeverityLevels &severityLevels);

public:
    ~Logger();

    static Logger& instance();

    void init(const QString &filePath, const SeverityLevels &severityLevels);
    void init(QIODevice *device, const SeverityLevels &severityLevels);
    void init(FILE *fileHandle, const SeverityLevels &severityLevels);
    void init(QString &string, const SeverityLevels &severityLevels);

    static SeverityLevels verbosityLevelToSeverityLevels(const VerbosityLevel verbosityLevel);

    void setSeverityLevels(const SeverityLevels &severityLevels);

    AbstractLoggerStream &logInfo();
    AbstractLoggerStream &logWarning();
    AbstractLoggerStream &logError();

private:
    QString commonPrefix() const;
    QString commonPrefix(const SeverityLevel severityLevel) const;
    static QString severityLevelString(const SeverityLevel severityLevel);

private:
    class BufferStream : public AbstractLoggerStream
    {
    public:
        BufferStream();
        explicit BufferStream(QIODevice *device);
        explicit BufferStream(FILE *fileHandle, QIODevice::OpenMode openMode = QIODevice::ReadWrite);
        explicit BufferStream(QString *string, QIODevice::OpenMode openMode = QIODevice::ReadWrite);
        explicit BufferStream(QByteArray *array, QIODevice::OpenMode openMode = QIODevice::ReadWrite);
        virtual ~BufferStream();

        virtual AbstractLoggerStream &operator<<(QChar ch);
        virtual AbstractLoggerStream &operator<<(char ch);
        virtual AbstractLoggerStream &operator<<(signed short i);
        virtual AbstractLoggerStream &operator<<(unsigned short i);
        virtual AbstractLoggerStream &operator<<(signed int i);
        virtual AbstractLoggerStream &operator<<(unsigned int i);
        virtual AbstractLoggerStream &operator<<(signed long i);
        virtual AbstractLoggerStream &operator<<(unsigned long i);
        virtual AbstractLoggerStream &operator<<(qlonglong i);
        virtual AbstractLoggerStream &operator<<(qulonglong i);
        virtual AbstractLoggerStream &operator<<(float f);
        virtual AbstractLoggerStream &operator<<(double f);
        virtual AbstractLoggerStream &operator<<(const QString &s);
        virtual AbstractLoggerStream &operator<<(QLatin1String s);
        virtual AbstractLoggerStream &operator<<(const QByteArray &array);
        virtual AbstractLoggerStream &operator<<(const char *c);
        virtual AbstractLoggerStream &operator<<(const void *ptr);
        virtual AbstractLoggerStream &operator<<(QTextStreamFunction f);
        virtual AbstractLoggerStream &operator<<(QTextStreamManipulator m);

        bool wasWrittenTo() const;

    private:
        Q_DISABLE_COPY(BufferStream)

    private:
        QTextStream m_textStream;
        bool m_wasWrittenTo;
    };

    class LineWriterStream : public AbstractLoggerStream
    {
    public:
        LineWriterStream();
        explicit LineWriterStream(const QWeakPointer< BufferStream > &bufferStream);
        virtual ~LineWriterStream();

        virtual AbstractLoggerStream &operator<<(QChar ch);
        virtual AbstractLoggerStream &operator<<(char ch);
        virtual AbstractLoggerStream &operator<<(signed short i);
        virtual AbstractLoggerStream &operator<<(unsigned short i);
        virtual AbstractLoggerStream &operator<<(signed int i);
        virtual AbstractLoggerStream &operator<<(unsigned int i);
        virtual AbstractLoggerStream &operator<<(signed long i);
        virtual AbstractLoggerStream &operator<<(unsigned long i);
        virtual AbstractLoggerStream &operator<<(qlonglong i);
        virtual AbstractLoggerStream &operator<<(qulonglong i);
        virtual AbstractLoggerStream &operator<<(float f);
        virtual AbstractLoggerStream &operator<<(double f);
        virtual AbstractLoggerStream &operator<<(const QString &s);
        virtual AbstractLoggerStream &operator<<(QLatin1String s);
        virtual AbstractLoggerStream &operator<<(const QByteArray &array);
        virtual AbstractLoggerStream &operator<<(const char *c);
        virtual AbstractLoggerStream &operator<<(const void *ptr);
        virtual AbstractLoggerStream &operator<<(QTextStreamFunction f);
        virtual AbstractLoggerStream &operator<<(QTextStreamManipulator m);

        void setStream(const QWeakPointer<BufferStream> &bufferStream);

    private:
        Q_DISABLE_COPY(LineWriterStream)

    private:
        QWeakPointer<BufferStream> m_bufferStream;
    };

private:
    Q_DISABLE_COPY(Logger)

private:
    LineWriterStream m_infoStream;
    LineWriterStream m_warningStream;
    LineWriterStream m_errorStream;

    QSharedPointer<BufferStream> m_stream;
    QFile m_file;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(MacDeployQt::Shared::SeverityLevels)

}
}

#endif // MACDEPLOYQT_SHARED_LOGGER_H
