/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#ifndef MACDEPLOYQT_SHARED_DEPLOYER_H
#define MACDEPLOYQT_SHARED_DEPLOYER_H

#include "moduleinfo.h"

#include <QList>
#include <QDebug>
#include <QString>
#include <QStringList>

class tst_deployment_mac;

namespace MacDeployQt {
namespace Shared {

struct FrameworkInfo
{
    bool operator==(const FrameworkInfo &info) const;
    bool operator!=(const FrameworkInfo &info) const;

    QString frameworkDirectory;
    QString frameworkName;
    QString frameworkPath;
    QString binaryDirectory;
    QString binaryName;
    QString binaryPath;
    QString version;
    QString installName;
    QString deployedInstallName;
    QString sourceFilePath;
    QString destinationDirectory;
};

class Deployer
{
    friend class tst_deployment_mac;

private:
    Deployer();

public:
    static void changeQtFrameworks(
            const ModuleInfo &mainModuleInfo,
            const QString &qtPath,
            const bool useDebugLibs,
            const QStringList &additionalLibraryPaths = QStringList());

    static void deployQtFrameworks(
            const ModuleInfo &mainModuleInfo,
            const QList<ModuleInfo> &additionalExecutables,
            const bool useDebugLibs,
            const bool deployPlugins,
            const bool createDmg,
            const bool runStrip,
            const QStringList &additionalLibraryPaths = QStringList());

private:
    struct DeploymentInfo
    {
        DeploymentInfo();

        QString qtPath;
        QString pluginPath;
        QStringList deployedFrameworks;
        bool useLoaderPath;
    };

    static void changeQtFrameworks(
            const QList<FrameworkInfo> &frameworks,
            const QList<ModuleInfo> &modules,
            const QString &qtPath);

    static bool copyFilePrintStatus(const QString &from, const QString &to);

    static FrameworkInfo parseOtoolLibraryLine(
            const ModuleInfo &mainModuleInfo,
            const QString &line,
            const bool useDebugLibs,
            const QStringList &additionalLibraryPaths = QStringList());
    static QList<FrameworkInfo> getQtFrameworks(
            const ModuleInfo &mainModuleInfo,
            const ModuleInfo &moduleInfo,
            const bool useDebugLibs,
            const QStringList &additionalLibraryPaths = QStringList());
    static QList<FrameworkInfo> getQtFrameworks(
            const ModuleInfo &mainModuleInfo,
            const QStringList &otoolLines,
            const bool useDebugLibs,
            const QStringList &additionalLibraryPaths = QStringList());
    static QList<FrameworkInfo> getQtFrameworksForPaths(
            const ModuleInfo &mainModuleInfo,
            const QList<ModuleInfo> &modules,
            const bool useDebugLibs,
            const QStringList &additionalLibraryPaths);
    static void recursiveCopy(const QString &sourcePath, const QString &destinationPath);
    static QString copyFramework(const FrameworkInfo &framework, const ModuleInfo &mainModuleInfo);
    static DeploymentInfo deployQtFrameworks(
            const ModuleInfo &mainModuleInfo,
            const QList<ModuleInfo> &additionalExecutables,
            const bool useDebugLibs,
            const bool runStrip,
            const QStringList &additionalLibraryPaths = QStringList());
    static DeploymentInfo deployQtFrameworks(
            const QList<FrameworkInfo> &frameworks,
            const ModuleInfo &mainModuleInfo,
            const QList<ModuleInfo> &modules,
            const bool useDebugLibs,
            const bool useLoaderPath,
            const bool runStrip,
            const QStringList &additionalLibraryPaths);
    static void createQtConf(const ModuleInfo &moduleInfo);
    static void deployPlugins(
            const ModuleInfo &mainModuleInfo,
            const QString &pluginSourcePath,
            const QString &pluginDestinationPath,
            const DeploymentInfo &deploymentInfo,
            const bool useDebugLibs,
            const bool runStrip,
            const QStringList &additionalLibraryPaths);
    static void deployPlugins(
            const ModuleInfo &mainModuleInfo,
            const DeploymentInfo &deploymentInfo,
            const bool useDebugLibs,
            const bool runStrip,
            const QStringList &additionalLibraryPaths = QStringList());
    static void runInstallNameTool(const QStringList &options);
    static void changeIdentification(const QString &id, const QString &binaryPath);
    static void changeInstallName(
            const ModuleInfo &moduleInfo,
            const FrameworkInfo &framework,
            const QList<ModuleInfo> &modules,
            const bool useLoaderPath);
    static void changeInstallName(
            const QString &oldName,
            const QString &newName,
            const ModuleInfo &moduleInfo);
    static void runStrip(const QString &binaryPath);
    static void createDiskImage(const ModuleInfo &moduleInfo);
};

}
}

MacDeployQt::Shared::AbstractLoggerStream &operator<<(MacDeployQt::Shared::AbstractLoggerStream &stream, const MacDeployQt::Shared::FrameworkInfo &info);

#endif // MACDEPLOYQT_SHARED_DEPLOYER_H
