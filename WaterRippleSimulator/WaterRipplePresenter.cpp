#include "WaterRipplePresenter.h"

#include "Helpers/CoordinateSystemIndicator.h"
#include "Helpers/OpenGlErrorLogger.h"
#include "Helpers/OpenGlTimeCounter.h"
#include "Helpers/WaterPool.h"
#include "Helpers/WaterSurfaceCpu.h"
#include "Helpers/WaterSurfaceGpu.h"
#include "Helpers/WaterSurfaceGpuPrecomputed.h"
#include "glext.h"
#include "Utilities/MathUtilities.h"

#include <QDebug>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QOpenGLFunctions_3_3_Core>
#include <QString>
#include <QTimer>
#include <QVector3D>

#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QStandardPaths>
#include <QStringBuilder>
#include <QDir>

// #define USE_PERSPECTIVE_PROJECTION

using namespace WaterRippleSimulator;

using namespace WaterRippleSimulator::Utilities;

const QColor WaterRipplePresenter::s_defaultClearColor{ 255, 255, 255, 255 };
const int WaterRipplePresenter::s_randomRipplePeriodStep = 200;

WaterRipplePresenter::WaterRipplePresenter( QWidget* parent, Qt::WindowFlags f )
    : QOpenGLWidget( parent, f )
    , AbstractOpenGlClient{ nullptr }
    , m_alteredMatrixType{ CommonEntities::MatrixType::Invalid }
    , m_hasMouseDragStarted{ false }

    , m_coordinateSystemIndicator{ nullptr }
    , m_activeWaterSurface{ nullptr }
    , m_waterPool{ nullptr }

    , m_randomRippleTimer{ nullptr }
    , m_waterPropagationTimer{ nullptr }
    , m_randomWaterRippleScheduled{ false }
    , m_sceneBackgroundColor{ s_defaultClearColor }
    , m_framesCount{ 0 }
    , m_fpsTimer{ nullptr }
    , m_fps{ 0.0 }
{
    QSurfaceFormat surfaceFormat;

    this->setFocusPolicy( Qt::WheelFocus );

    // https://www.opengl.org/wiki/Swap_Interval
    // http://stackoverflow.com/questions/11312318/my-limited-fps-60
    surfaceFormat.setSwapInterval( 0 ); // turn vsync off
    surfaceFormat.setRenderableType( QSurfaceFormat::OpenGL );
    surfaceFormat.setVersion( 3, 3 );
    surfaceFormat.setProfile( QSurfaceFormat::CoreProfile );
    this->setFormat( surfaceFormat );

    m_fpsTimer = new QTimer( this );
    m_fpsTimer->setInterval( 1000 );
    connect( m_fpsTimer, SIGNAL(timeout()), this, SLOT(calcFps()) );

    m_waterPropagationTimer = new QTimer( this );

    const int desiredFps = 0;
    const int fps = qMax( 0, desiredFps );
    m_waterPropagationTimer->setInterval( fps == 0
                                          ? 0
                                          : qRound( 1000.0 / fps ) );
    connect( m_waterPropagationTimer,
             SIGNAL(timeout()),
             this,
             SLOT(update()) );
}

WaterRipplePresenter::~WaterRipplePresenter()
{
    this->serializeFpsData();
}

void WaterRipplePresenter::init()
{
    if ( m_openGlFunctions != nullptr )
    {
        return;
    }

    m_openGlFunctions = this->context()->versionFunctions< QOpenGLFunctions_3_3_Core >();
    if ( m_openGlFunctions == nullptr )
    {
        const QSurfaceFormat surfaceFormat = this->format();
        qDebug() << QString( "OpenGL version %1.%2 (%3 profile) is not supported." )
                    .arg( surfaceFormat.majorVersion() )
                    .arg( surfaceFormat.minorVersion() )
                    .arg( surfaceFormat.profile() == QSurfaceFormat::CoreProfile
                          ? "Core"
                          : "Compatibility" );
        return;
    }

    if ( ! m_openGlFunctions->initializeOpenGLFunctions() )
    {
        return;
    }

    OpenGlErrorLogger::instance().setOpenGlFunctions( m_openGlFunctions );

    m_openGlFunctions->glEnable( GL_LINE_SMOOTH );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glEnable( GL_POLYGON_SMOOTH );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glEnable( GL_DEPTH_TEST );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glClearColor( m_sceneBackgroundColor.redF(),
                                     m_sceneBackgroundColor.greenF(),
                                     m_sceneBackgroundColor.blueF(),
                                     m_sceneBackgroundColor.alphaF() );
    LOG_OPENGL_ERROR();

    // m_openGlFunctions->glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
    // LOG_OPENGL_ERROR();

    const QSurfaceFormat surfaceFormat = this->format();

    m_coordinateSystemIndicator = new CoordinateSystemIndicator( m_openGlFunctions, surfaceFormat, this );
    m_coordinateSystemIndicator->init();

    WaterSurfaceCpu* const            waterSurfaceCpu            = new WaterSurfaceCpu( m_openGlFunctions, surfaceFormat, this );
    WaterSurfaceGpu* const            waterSurfaceGpu            = new WaterSurfaceGpu( m_openGlFunctions, surfaceFormat, this );
    WaterSurfaceGpuPrecomputed* const waterSurfaceGpuPrecomputed = new WaterSurfaceGpuPrecomputed( m_openGlFunctions, surfaceFormat, this );
    m_waterSurfaces.insert( WaterSettings::WavePropagationMethodCpu,                           waterSurfaceCpu            );
    m_waterSurfaces.insert( WaterSettings::WavePropagationMethodGpu,                           waterSurfaceGpu            );
    m_waterSurfaces.insert( WaterSettings::WavePropagationMethodGpuPrecomputedNormalsVertices, waterSurfaceGpuPrecomputed );
    this->resetActiveWaterSurface();

    for ( AbstractWaterSurface* const waterSurface : m_waterSurfaces )
    {
        waterSurface->init();
    }

    m_waterPool = new WaterPool( m_openGlFunctions, surfaceFormat, this->meshRect(), this );
    m_waterPool->init();

    Light light;
    light.pos = QVector3D( -1.0f, 0.5f, 0.5f );
    light.color.setRgb( 255, 255, 255 );
    light.attenuation.constant  = 0.8f;
    light.attenuation.linear    = 0.1f;
    light.attenuation.quadratic = 0.001f;
    // light.attenuation.constant  = 0.0f;
    // light.attenuation.linear    = 0.0f;
    // light.attenuation.quadratic = 0.0f;
    light.intensity             = 3.5f;

    this->setLight( light );

    Material material;
    material.color.setRgb( 0x1C, 0x6B, 0xA0 );
    material.reflectance.emissive = QVector3D( 0.0f, 0.0f, 0.0f );
    material.reflectance.ambient = QVector3D( 0.9f, 0.9f, 0.9f );
    material.reflectance.diffuse = QVector3D( 0.5f, 0.5f, 0.5f );
    material.reflectance.specular = QVector3D( 0.1f, 0.1f, 0.1f );
    // material.reflectance.ambient = QVector3D( 1.0f, 1.0f, 1.0f );
    // material.reflectance.diffuse = QVector3D( 1.0f, 1.0f, 1.0f );
    // material.reflectance.specular = QVector3D( 1.0f, 1.0f, 1.0f );
    material.shininess = 100.0f;

    this->setMaterial( material );

    // connect( waterSurfaceCpu,             SIGNAL(updateRequested()), this, SLOT(update()) );
    // connect( waterSurfaceGpu,             SIGNAL(updateRequested()), this, SLOT(update()) );
    // connect( waterSurfaceGpuPrecomputed,  SIGNAL(updateRequested()), this, SLOT(update()) );
    // connect( m_waterPool,                 SIGNAL(updateRequested()), this, SLOT(update()) );
    // connect( m_coordinateSystemIndicator, SIGNAL(updateRequested()), this, SLOT(update()) );

    m_randomRippleTimer = new QTimer( this );
    m_randomRippleTimer->setInterval( WaterSettings::s_rainDropsFallTimeIntervalDefault );
    connect( m_randomRippleTimer, SIGNAL(timeout()), this, SLOT(scheduleWaterRippleAtRandomPoint()) );

    this->applyWaterSettings( WaterSettings::WaterPropertiesAll );
}

void WaterRipplePresenter::setSceneBackgroundColor( const QColor& aSceneBackgroundColor )
{
    if ( ! aSceneBackgroundColor.isValid()
         || aSceneBackgroundColor == m_sceneBackgroundColor )
    {
        return;
    }

    m_sceneBackgroundColor = aSceneBackgroundColor;
    this->update();
}

QColor WaterRipplePresenter::sceneBackgroundColor() const
{
    return m_sceneBackgroundColor;
}

void WaterRipplePresenter::setMaterial( const Material& aMaterial )
{
    m_material = aMaterial;
    for ( AbstractWaterSurface* const waterSurface : m_waterSurfaces )
    {
        waterSurface->setMaterial( m_material );
    }

    m_waterPool->setMaterial( aMaterial );
}

Material WaterRipplePresenter::material() const
{
    return m_material;
}

void WaterRipplePresenter::setLight( const Light& aLight )
{
    m_light = aLight;
    for ( AbstractWaterSurface* const waterSurface : m_waterSurfaces )
    {
        waterSurface->setLight( m_light );
    }

    m_waterPool->setLight( aLight );
}

Light WaterRipplePresenter::light() const
{
    return m_light;
}

WaterSettings WaterRipplePresenter::waterSettings() const
{
    return m_waterSettings;
}

qreal WaterRipplePresenter::fps() const
{
    return m_fps;
}

void WaterRipplePresenter::displayVideoFrame( const QVideoFrame& videoFrame )
{
    m_activeWaterSurface->obtainColorFromTexture( true );
    m_activeWaterSurface->displayVideoFrame( videoFrame );
    this->update();
}

void WaterRipplePresenter::clearVideoDisplay()
{
    m_activeWaterSurface->obtainColorFromTexture( false );
    this->update();
}

void WaterRipplePresenter::resetOrientation()
{
    m_modelMatrix.setToIdentity();
    m_viewMatrix.setToIdentity();

    this->applyMatrix( CommonEntities::MatrixType::Model, m_modelMatrix, false );
    this->applyMatrix( CommonEntities::MatrixType::View, m_viewMatrix, true );
}

void WaterRipplePresenter::calmWater()
{
    for ( AbstractWaterSurface* const waterSurface : m_waterSurfaces )
    {
        waterSurface->calmWater();
    }
}

void WaterRipplePresenter::setWaterSettings( const WaterSettings& aWaterSettings )
{
    const WaterSettings::WaterProperties changedProperties = aWaterSettings.diff( m_waterSettings );
    m_waterSettings = aWaterSettings;
    this->applyWaterSettings( changedProperties );
}

void WaterRipplePresenter::initializeGL()
{
    this->init();
}

bool WaterRipplePresenter::isGLInitialized() const
{
    return m_openGlFunctions != nullptr;
}

void WaterRipplePresenter::paintGL()
{
    if ( m_openGlFunctions == nullptr )
    {
        return;
    }

    // needed since prior to calling paintGL, QOpenGL(Widget/Window) call glViewport
    ViewportAlternator vpAlternator = this->viewportAlternator();
    vpAlternator.change();

    m_openGlFunctions->glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    LOG_OPENGL_ERROR();

    if ( m_randomWaterRippleScheduled )
    {
        m_activeWaterSurface->rippleWaterAtRandomPoint();
        m_randomWaterRippleScheduled = false;
    }

    m_activeWaterSurface->draw( m_waterSettings.shouldPropagateWaves() );
    m_waterPool->draw();

    if ( m_waterSettings.shouldShowCoordinateSystemIndicator() )
    {
        m_coordinateSystemIndicator->draw();
    }

    if ( ! m_fpsTimer->isActive() )
    {
        m_fpsTimer->start();
    }

    ++ m_framesCount;
}

void WaterRipplePresenter::resizeGL( int w, int h )
{
    // QOpenGL(Widget|Window) renders in a framebuffer object instead of directly to the windowing system's one
    // and moreover changes the viewport by itself prior to each rendering call
    // setup only perspective here if needed

    m_projectionMatrix.setToIdentity();

#ifdef USE_PERSPECTIVE_PROJECTION
    // TODO: fix

    // Calculate aspect ratio
    const qreal aspect = qreal( w ) / qreal( h == 0 ? 1 : h );

    // Set near plane to 3.0, far plane to 7.0, field of view 45 degrees
    const qreal zNear = 3.0;
    const qreal zFar = 7.0;
    const qreal fov = 45.0;

    QMatrix4x4 projectionFix;
    projectionFix.translate( 0.0f, 0.0f, - ( zFar - zNear ) / 2.0f );
    this->applyMatrix( CommonEntities::MatrixType::Model, m_modelMatrix * projectionFix, true );

    // Set perspective projection
    m_projectionMatrix.perspective( fov, aspect, zNear, zFar );
#else
    Q_UNUSED( w )
    Q_UNUSED( h )

    const float volumeHalfExtent = 1.15f;

    // Set perspective projection
    m_projectionMatrix.ortho( -volumeHalfExtent, // x-left
                               volumeHalfExtent, // x-right
                              -volumeHalfExtent, // y-bottom
                               volumeHalfExtent, // y-top
                              -volumeHalfExtent, // z-near
                               volumeHalfExtent  // z-far
                              );
#endif

    this->applyMatrix( CommonEntities::MatrixType::Projection, m_projectionMatrix, true );
}

void WaterRipplePresenter::keyPressEvent( QKeyEvent* event )
{
    QOpenGLWidget::keyPressEvent( event );

    const int key = event->key();
    if ( key == Qt::Key_Left
         || key == Qt::Key_Right
         || key == Qt::Key_Up
         || key == Qt::Key_Down )
    {
        static const float angleStep = 5.0f;
        float yRot = 0.0f;
        float xRot = 0.0f;
        switch( key )
        {
            case Qt::Key_Left:
                yRot = - angleStep;
                break;
            case Qt::Key_Right:
                yRot = angleStep;
                break;
            case Qt::Key_Up:
                xRot = - angleStep;
                break;
            case Qt::Key_Down:
                xRot = angleStep;
                break;
        }

        QMatrix4x4 keyMatrix;
        if ( ! qFuzzyIsNull( xRot ) )
        {
            keyMatrix.rotate( xRot, 1.0f, 0.0f );
        }

        if ( ! qFuzzyIsNull( yRot ) )
        {
            keyMatrix.rotate( yRot, 0.0f, 1.0f );
        }

        m_modelMatrix = keyMatrix * m_modelMatrix;

        this->applyMatrix( CommonEntities::MatrixType::Model, m_mouseCacheMatrix * m_modelMatrix, true );
    }
    else if ( key == Qt::Key_Space )
    {
        const bool shouldPropagateWater = ! m_waterSettings.shouldPropagateWaves();
        m_waterSettings.setShouldPropagateWaves( shouldPropagateWater );

        emit shouldPropagateWaterChanged( shouldPropagateWater );

        this->update();
    }
    else if ( key == Qt::Key_C )
    {
        this->calmWater();
    }
    else if ( key == Qt::Key_T )
    {
        this->resetOrientation();
    }
    else if ( key == Qt::Key_R )
    {
        if ( m_randomRippleTimer->isActive() )
        {
            m_randomRippleTimer->stop();
        }
        else
        {
            m_randomRippleTimer->start();
        }
    }
    else if ( key == Qt::Key_Plus
              || key == Qt::Key_Minus )
    {
        m_randomRippleTimer->setInterval( qMax( m_randomRippleTimer->interval()
                                                + ( key == Qt::Key_Minus ? 1 : -1 )
                                                * s_randomRipplePeriodStep,
                                                s_randomRipplePeriodStep ) );
    }
}

void WaterRipplePresenter::mousePressEvent( QMouseEvent* event )
{
    QOpenGLWidget::mousePressEvent( event );

    if ( ! this->shouldHandleMouseEvent( event ) )
    {
        return;
    }

    const QPoint mousePos = event->pos();
    if ( event->buttons().testFlag( Qt::RightButton ) )
    {
        this->scheduleWaterRippleAtViewportPt( mousePos );
    }
    else
    {
        m_hasMouseDragStarted = true;
        m_mousePressInitialPos = mousePos;
        m_alteredMatrixType = CommonEntities::MatrixType::Model;
    }
}

void WaterRipplePresenter::mouseMoveEvent( QMouseEvent* event )
{
    QOpenGLWidget::mouseMoveEvent( event );

    if ( ! this->shouldHandleMouseEvent( event ) )
    {
        return;
    }

    if ( event->buttons().testFlag( Qt::RightButton ) )
    {
        this->scheduleWaterRippleAtViewportPt( event->pos() );
    }
    else
    {
        this->updateMouseMatrix( event );
    }
}

void WaterRipplePresenter::mouseReleaseEvent( QMouseEvent* event )
{
    QOpenGLWidget::mouseReleaseEvent( event );

    this->updateMouseMatrix( event );
    m_modelMatrix = m_mouseCacheMatrix * m_modelMatrix;
    m_mouseCacheMatrix.setToIdentity();

    m_hasMouseDragStarted = false;
    m_alteredMatrixType = CommonEntities::MatrixType::Invalid;
    m_mousePressInitialPos = QPoint();
}

#ifndef QT_NO_WHEELEVENT
void WaterRipplePresenter::wheelEvent( QWheelEvent* event )
{
    QOpenGLWidget::wheelEvent( event );
}
#endif

void WaterRipplePresenter::applyMatrix(
        const CommonEntities::MatrixType matrixType,
        const QMatrix4x4& matrix,
        const bool updateUi )
{
    if ( m_openGlFunctions == nullptr )
    {
        return;
    }

    for ( AbstractWaterSurface* const waterSurface : m_waterSurfaces )
    {
        waterSurface->setMatrix( matrixType, matrix );
    }

    m_waterPool->setMatrix( matrixType, matrix );

    // TODO: later on, change to view matrix, when view matrix handling is implemented
    m_coordinateSystemIndicator->setMatrix( this->mvpMatrix( matrixType, matrix ) );

    if ( updateUi )
    {
        this->update();
    }
}

QRect WaterRipplePresenter::viewport( const int w, const int h ) const
{
    const int sideSize = qMin( w, h );
    QRect targetRect( QPoint{ 0, 0 }, QSize{ sideSize, sideSize } );
    targetRect.moveCenter( this->rect().center() );

    return targetRect;
}

QRect WaterRipplePresenter::viewport() const
{
    return this->viewport( this->width(), this->height() );
}

ViewportAlternator WaterRipplePresenter::viewportAlternator( const bool autoRollback ) const
{
    return AbstractOpenGlClient::viewportAlternator(
                this->viewport(),
                autoRollback );
}

QVector3D WaterRipplePresenter::viewportToWaterMeshPt( const QPoint& viewportPt ) const
{
    const QRect vp = this->viewport();
    QPointF clickPos2 = viewportPt - vp.center();
    clickPos2.rx() /= vp.width() / 2.0;
    clickPos2.ry() /= - vp.height() / 2.0;

    QVector3D clickPos3{ clickPos2 };
    clickPos3.setZ( CommonEntities::s_drawingVolumeBoundsYMax * 2.0f );

    const QVector3D dir{ 0.0f, 0.0f, -1.0f };

    const QRectF waterRect = this->meshRect();
    const QMatrix4x4 mvpMatrix = this->mvpMatrix();

    QMatrix4x4 waterRectMvpMatrix;
    waterRectMvpMatrix.rotate( 180.0f, 1.0, 0.0 ); // OpenGL's Y direction is upwards, while Qt's one is downwards
    waterRectMvpMatrix = mvpMatrix * waterRectMvpMatrix;

    const QVector3D waterRectBottomLeft = waterRectMvpMatrix.map( QVector3D{ waterRect.bottomLeft() } );
    const QVector3D waterRectBottomRight = waterRectMvpMatrix.map( QVector3D{ waterRect.bottomRight() } );
    const QVector3D waterRectTopRight = waterRectMvpMatrix.map( QVector3D{ waterRect.topRight() } );
    const QVector3D waterRectTopLeft = waterRectMvpMatrix.map( QVector3D{ waterRect.topLeft() } );

    const Triangle3 t1{ waterRectBottomLeft,
                        waterRectBottomRight,
                        waterRectTopRight };
    const Triangle3 t2{ waterRectTopRight,
                        waterRectTopLeft,
                        waterRectBottomLeft };

    for ( const Triangle3& t : { t1, t2 } )
    {
        QVector3D intersectionPtBarycentric;
        if ( ! t.intersectsWithLine( clickPos3, dir, & intersectionPtBarycentric ) )
        {
            continue;
        }

        const QVector3D intersectionPt = t.fromBarycentricPt( intersectionPtBarycentric.y(), intersectionPtBarycentric.z() );
        const QVector3D intersectionPtFinal = mvpMatrix.inverted().map( intersectionPt );

        return intersectionPtFinal;
    }

    return QVector3D();
}

void WaterRipplePresenter::scheduleWaterRippleAtViewportPt( const QPoint& viewportPt )
{
    m_activeWaterSurface->rippleWaterAt( this->viewportToWaterMeshPt( viewportPt ) );
}

bool WaterRipplePresenter::shouldHandleMouseEvent( QMouseEvent* const mouseEvent ) const
{
    const Qt::MouseButtons pressedMouseButtons = mouseEvent->buttons();
    const bool isRippleMode = pressedMouseButtons.testFlag( Qt::RightButton );
    if ( ! pressedMouseButtons.testFlag( Qt::LeftButton )
         && ! isRippleMode )
    {
        return false;
    }

    const QPoint mousePos = mouseEvent->pos();
    const QRect vp = this->viewport();
    if ( isRippleMode && ! vp.contains( mousePos ) )
    {
        return false;
    }

    return true;
}

QRectF WaterRipplePresenter::meshRect() const
{
    return m_activeWaterSurface->meshRect();
}

void WaterRipplePresenter::applyMeshSize()
{
    const QSize meshSize = m_waterSettings.meshSize();
    for ( AbstractWaterSurface* const waterSurface : m_waterSurfaces )
    {
        waterSurface->setMeshSize( meshSize );
    }

    m_waterPool->setWaterRect( this->meshRect() );
}

void WaterRipplePresenter::applyShouldDrawAsWireframe()
{
    const bool shouldDrawAsWireframe = m_waterSettings.shouldDrawAsWireframe();
    for ( AbstractWaterSurface* const waterSurface : m_waterSurfaces )
    {
        waterSurface->setShouldDrawAsWireframe( shouldDrawAsWireframe );
    }
}

void WaterRipplePresenter::applyWavePropagationMethod()
{
    this->resetActiveWaterSurface();
}

void WaterRipplePresenter::applyWaterRippleMagnitude()
{
    const float waterRippleMagnitude = m_waterSettings.waterRippleMagnitude();
    for ( AbstractWaterSurface* const waterSurface : m_waterSurfaces )
    {
        waterSurface->setWaterRippleMagnitude( waterRippleMagnitude );
    }
}

void WaterRipplePresenter::applyWaterRippleRadiusFactor()
{
    const QVector2D& r = m_waterSettings.waterRippleRadiusFactor();
    for ( AbstractWaterSurface* const waterSurface : m_waterSurfaces )
    {
        waterSurface->setWaterRippleRadiusFactor( r );
    }
}

void WaterRipplePresenter::applyWaterRippleRotationAngle()
{
    const float angle = m_waterSettings.waterRippleRotationAngle();
    for ( AbstractWaterSurface* const waterSurface : m_waterSurfaces )
    {
        waterSurface->setWaterRippleRotationAngle( angle );
    }
}

void WaterRipplePresenter::applyWavePropagationSpeed()
{
    const float speed = m_waterSettings.wavePropagationSpeed();
    for ( AbstractWaterSurface* const waterSurface : m_waterSurfaces )
    {
        waterSurface->setWavePropagationSpeed( speed );
    }
}

void WaterRipplePresenter::applyWaveHeightDampingOverTimeFactor()
{
    const float factor = m_waterSettings.waveHeightDampingOverTimeFactor();
    for ( AbstractWaterSurface* const waterSurface : m_waterSurfaces )
    {
        waterSurface->setWaveHeightDampingOverTimeFactor( factor );
    }
}

void WaterRipplePresenter::applyWaveHeightClampingFactor()
{
    const float factor = m_waterSettings.waveHeightClampingFactor();
    for ( AbstractWaterSurface* const waterSurface : m_waterSurfaces )
    {
        waterSurface->setWaveHeightClampingFactor( factor );
    }
}

void WaterRipplePresenter::applyWaterRippleMethod()
{
    const WaterSettings::WaterRippleMethod method = m_waterSettings.waterRippleMethod();
    for ( AbstractWaterSurface* const waterSurface : m_waterSurfaces )
    {
        waterSurface->setWaterRippleMethod( method );
    }
}

void WaterRipplePresenter::applyShouldPropagateWaves()
{
    if ( m_waterSettings.shouldPropagateWaves() )
    {
        m_waterPropagationTimer->start();
    }
    else
    {
        m_waterPropagationTimer->stop();
    }
}

void WaterRipplePresenter::applyShouldSimulateRain()
{
    if ( m_waterSettings.shouldSimulateRain() )
    {
        m_randomRippleTimer->start();
    }
    else
    {
        m_randomRippleTimer->stop();
    }
}

void WaterRipplePresenter::applyRainDropsFallTimeInterval()
{
    m_randomRippleTimer->setInterval( m_waterSettings.rainDropsFallTimeInterval() );
}

void WaterRipplePresenter::applyRainDropsCountPerFall()
{

}

void WaterRipplePresenter::applyWaterSettings( const WaterSettings::WaterProperties changedProperties )
{
    if ( changedProperties.testFlag( WaterSettings::WaterPropertyMeshSize ) )
    {
        this->applyMeshSize();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyShouldDrawAsWireframe ) )
    {
        this->applyShouldDrawAsWireframe();
    }

    // wave propagation
    if ( changedProperties.testFlag( WaterSettings::WaterPropertyShouldPropagateWaves ) )
    {
        this->applyShouldPropagateWaves();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyWavePropagationMethod ) )
    {
        this->applyWavePropagationMethod();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyWavePropagationSpeed ) )
    {
        this->applyWavePropagationSpeed();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyWaveHeightDampingOverTimeFactor ) )
    {
        this->applyWaveHeightDampingOverTimeFactor();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyWaveHeightClampingFactor ) )
    {
        this->applyWaveHeightClampingFactor();
    }

    // wave propagation
    if ( changedProperties.testFlag( WaterSettings::WaterPropertyWaterRippleMethod ) )
    {
        this->applyWaterRippleMethod();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyWaterRippleMagnitude ) )
    {
        this->applyWaterRippleMagnitude();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyWaterRippleRadiusFactor ) )
    {
        this->applyWaterRippleRadiusFactor();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyWaterRippleRotationAngle ) )
    {
        this->applyWaterRippleRotationAngle();
    }

    // rain
    if ( changedProperties.testFlag( WaterSettings::WaterPropertyShouldSimulateRain ) )
    {
        this->applyShouldSimulateRain();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyRainDropsFallTimeInterval ) )
    {
        this->applyRainDropsFallTimeInterval();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyRainDropsCountPerFall ) )
    {
        this->applyRainDropsCountPerFall();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyShouldShowCoordinateSystemIndicator ) )
    {
        this->update();
    }
}

void WaterRipplePresenter::setFps( const qreal aFps )
{
    if ( ( aFps < 0.0 && ! qFuzzyIsNull( aFps ) )
         || qFuzzyCompare( aFps, m_fps ) )
    {
        return;
    }

    m_fps = aFps;
    m_fpsData.append( m_fps );

    emit fpsChanged( aFps );
}

AbstractWaterSurface* WaterRipplePresenter::activeWaterSurface() const
{
    return m_waterSurfaces.value( m_waterSettings.wavePropagationMethod(), nullptr );
}

void WaterRipplePresenter::resetActiveWaterSurface()
{
    AbstractWaterSurface* const previousActiveWaterSurface = m_activeWaterSurface;
    m_activeWaterSurface = this->activeWaterSurface();

    if ( previousActiveWaterSurface != nullptr )
    {
        m_activeWaterSurface->obtainColorFromTexture( previousActiveWaterSurface->obtainsColorFromTexture() );
    }
}

void WaterRipplePresenter::serializeFpsData() const
{
    const QString desktopDirPath = QStandardPaths::writableLocation( QStandardPaths::DesktopLocation );

    QString wavePropagationMethodDescription;
    switch ( m_waterSettings.wavePropagationMethod() )
    {
        case WaterSettings::WavePropagationMethodCpu:
            wavePropagationMethodDescription = "cpu";
            break;
        case WaterSettings::WavePropagationMethodGpu:
            wavePropagationMethodDescription = "gpu1";
            break;
        case WaterSettings::WavePropagationMethodGpuPrecomputedNormalsVertices:
            wavePropagationMethodDescription = "gpu2";
            break;
    }

    const QChar dirSep = QDir::separator();
    const QString simPerformanceDataDirPath = desktopDirPath
                                              % dirSep % "waterRippleSimulationPerfData"
                                              % dirSep % wavePropagationMethodDescription;
    if ( ! QDir( simPerformanceDataDirPath ).exists() )
    {
        if ( ! QDir().mkpath( simPerformanceDataDirPath ) )
        {
            return;
        }
    }

    const QString perfReportFileName = simPerformanceDataDirPath
                                       % dirSep % QString( "%1x%2_" )
                                                  .arg( m_waterSettings.meshSize().width() )
                                                  .arg( m_waterSettings.meshSize().height() )
                                       % QDateTime::currentDateTime().toString( "yyyy-MM-dd_hh-mm-ss" )
                                       % ".txt";
    QFile f( perfReportFileName );
    if ( ! f.open( QIODevice::WriteOnly ) )
    {
        return;
    }

    QTextStream ts( & f );
    const int measurementsCounts = m_fpsData.size();
    for ( int i = 0; i < measurementsCounts; ++ i )
    {
        ts << i << ": " << QString::number( m_fpsData[ i ], 'f', 3 ) << endl;
    }
}

void WaterRipplePresenter::updateMouseMatrix( QMouseEvent* const mouseEvent )
{
    if ( mouseEvent == nullptr
         || ! m_hasMouseDragStarted )
    {
        return;
    }

    const Qt::MouseButtons pressedMouseButtons = mouseEvent->buttons();
    if ( ! pressedMouseButtons.testFlag( Qt::LeftButton )
         && ! pressedMouseButtons.testFlag( Qt::RightButton ) )
    {
        return;
    }

    const QPoint mouseMoveVector = mouseEvent->pos() - m_mousePressInitialPos;

    const float fullRotationAngle = 360.0f;
    const float yAngle = MathUtilities::to360DegreesAngle( ( float( mouseMoveVector.x() ) / this->width() ) * fullRotationAngle );
    const float xAngle = MathUtilities::to360DegreesAngle( ( float( mouseMoveVector.y() ) / this->height() ) * fullRotationAngle );

    m_mouseCacheMatrix.setToIdentity();
    m_mouseCacheMatrix.rotate( xAngle, 1, 0 );
    m_mouseCacheMatrix.rotate( yAngle, 0, 1 );

    this->applyAlteredMatrix();
}

void WaterRipplePresenter::applyAlteredMatrix()
{
    this->applyMatrix( m_alteredMatrixType, this->completeAlteredMatrix(), true );
}

QMatrix4x4 WaterRipplePresenter::completeMatrix( const CommonEntities::MatrixType matrixType ) const
{
    return m_mouseCacheMatrix * this->matrix( matrixType );
}

QMatrix4x4 WaterRipplePresenter::completeAlteredMatrix() const
{
    return this->isAlteringAnyMatrix()
           ? this->completeMatrix( m_alteredMatrixType )
           : QMatrix4x4();
}

bool WaterRipplePresenter::isAlteringAnyMatrix() const
{
    return m_alteredMatrixType != CommonEntities::MatrixType::Invalid;
}

QMatrix4x4 WaterRipplePresenter::matrix( const CommonEntities::MatrixType matrixType ) const
{
    switch ( matrixType )
    {
        case CommonEntities::MatrixType::Model:
            return m_modelMatrix;
        case CommonEntities::MatrixType::View:
            return m_viewMatrix;
        case CommonEntities::MatrixType::Projection:
            return m_projectionMatrix;
        default:
            break;
    }

    return QMatrix4x4();
}

QMatrix4x4 WaterRipplePresenter::mvpMatrix(
        const CommonEntities::MatrixType replaceMatrixType,
        const QMatrix4x4& replaceMatrix ) const
{
    switch ( replaceMatrixType )
    {
        case CommonEntities::MatrixType::Model:
            return m_projectionMatrix * m_viewMatrix * replaceMatrix;
        case CommonEntities::MatrixType::View:
            return m_projectionMatrix * replaceMatrix * m_modelMatrix;
        case CommonEntities::MatrixType::Projection:
            return replaceMatrix * m_viewMatrix * m_modelMatrix;
        default:
            break;
    }

    return m_projectionMatrix * m_viewMatrix * m_modelMatrix;
}

void WaterRipplePresenter::calcFps()
{
    this->setFps( m_framesCount * qreal( 1000 ) / m_fpsTimer->interval() );
    m_framesCount = 0;
}

void WaterRipplePresenter::scheduleWaterRippleAtRandomPoint()
{
    m_randomWaterRippleScheduled = true;
    this->update();
}
