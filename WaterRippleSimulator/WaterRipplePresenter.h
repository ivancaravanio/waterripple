#ifndef WATERRIPPLESIMULATOR_WATERRIPPLEPRESENTER_H
#define WATERRIPPLESIMULATOR_WATERRIPPLEPRESENTER_H

#include "Data/Light.h"
#include "Data/Material.h"
#include "Data/WaterSettings.h"
#include "Helpers/AbstractOpenGlClient.h"
#include "Helpers/ViewportAlternator.h"
#include "WaterRippleSimulator_namespace.h"

#include <QColor>
#include <QHash>
#include <QMatrix4x4>
#include <QOpenGLWidget>
#include <QVector3D>
#include <QVideoFrame>

QT_FORWARD_DECLARE_CLASS( QMouseEvent )
QT_FORWARD_DECLARE_CLASS( QTimer )

namespace WaterRippleSimulator {
namespace Helpers {

class AbstractWaterSurface;
class CoordinateSystemIndicator;
class WaterPool;

}

using namespace WaterRippleSimulator::Data;
using namespace WaterRippleSimulator::Helpers;

class WaterRipplePresenter : public QOpenGLWidget, public AbstractOpenGlClient
{
    Q_OBJECT

public:
    explicit WaterRipplePresenter( QWidget* parent = nullptr, Qt::WindowFlags f = 0 );
    ~WaterRipplePresenter() Q_DECL_OVERRIDE;

    void init() Q_DECL_OVERRIDE;

    void setSceneBackgroundColor( const QColor& aSceneBackgroundColor );
    QColor sceneBackgroundColor() const;

    void setMaterial( const Material& aMaterial );
    Material material() const;

    void setLight( const Light& aLight );
    Light light() const;

    void setWaterSettings( const WaterSettings& aWaterSettings );
    WaterSettings waterSettings() const;

    qreal fps() const;

signals:
    void fpsChanged( const qreal fps );
    void shouldPropagateWaterChanged( const bool shouldPropagateWater );

public slots:
    void displayVideoFrame( const QVideoFrame& videoFrame );
    void clearVideoDisplay();

    void resetOrientation();
    void calmWater();

protected:
    void initializeGL() Q_DECL_OVERRIDE;
    void paintGL() Q_DECL_OVERRIDE;
    void resizeGL( int w, int h ) Q_DECL_OVERRIDE;

    bool isGLInitialized() const;

    void keyPressEvent( QKeyEvent* event ) Q_DECL_OVERRIDE;

    void mousePressEvent( QMouseEvent* event ) Q_DECL_OVERRIDE;
    void mouseMoveEvent( QMouseEvent* event ) Q_DECL_OVERRIDE;
    void mouseReleaseEvent( QMouseEvent* event ) Q_DECL_OVERRIDE;
#ifndef QT_NO_WHEELEVENT
    void wheelEvent( QWheelEvent* event ) Q_DECL_OVERRIDE;
#endif

private:
    static const QColor s_defaultClearColor;
    static const int s_randomRipplePeriodStep;

private:
    void applyMatrix(
            const CommonEntities::MatrixType matrixType,
            const QMatrix4x4& matrix,
            const bool updateUi );

    void updateMouseMatrix( QMouseEvent* const mouseEvent );
    void applyAlteredMatrix();
    QMatrix4x4 completeMatrix( const CommonEntities::MatrixType matrixType ) const;
    QMatrix4x4 completeAlteredMatrix() const;
    bool isAlteringAnyMatrix() const;

    QMatrix4x4 matrix( const CommonEntities::MatrixType matrixType ) const;
    QMatrix4x4 mvpMatrix(
            const CommonEntities::MatrixType replaceMatrixType = CommonEntities::MatrixType::Invalid,
            const QMatrix4x4& replaceMatrix = QMatrix4x4() ) const;
    QRect viewport( const int w, const int h ) const;
    QRect viewport() const;
    ViewportAlternator viewportAlternator( const bool autoRollback = true ) const;
    QVector3D viewportToWaterMeshPt( const QPoint& viewportPt ) const;
    void scheduleWaterRippleAtViewportPt( const QPoint& viewportPt );
    bool shouldHandleMouseEvent( QMouseEvent* const mouseEvent ) const;

    QRectF meshRect() const;

    void applyMeshSize();
    void applyShouldDrawAsWireframe();

    void applyShouldPropagateWaves();
    void applyWavePropagationMethod();
    void applyWavePropagationSpeed();
    void applyWaveHeightDampingOverTimeFactor();
    void applyWaveHeightClampingFactor();

    void applyWaterRippleMethod();
    void applyWaterRippleMagnitude();
    void applyWaterRippleRadiusFactor();
    void applyWaterRippleRotationAngle();

    void applyShouldSimulateRain();
    void applyRainDropsFallTimeInterval();
    void applyRainDropsCountPerFall();

    void applyWaterSettings( const WaterSettings::WaterProperties changedProperties );

    void setFps( const qreal aFps );

    AbstractWaterSurface* activeWaterSurface() const;
    void resetActiveWaterSurface();
    void serializeFpsData() const;

private slots:
    void calcFps();
    void scheduleWaterRippleAtRandomPoint();

private:
    QMatrix4x4                 m_modelMatrix;
    QMatrix4x4                 m_viewMatrix;
    QMatrix4x4                 m_projectionMatrix;
    CommonEntities::MatrixType m_alteredMatrixType;
    QMatrix4x4                 m_mouseCacheMatrix;
    bool                       m_hasMouseDragStarted;
    QPoint                     m_mousePressInitialPos;

    CoordinateSystemIndicator* m_coordinateSystemIndicator;
    QHash< WaterSettings::WavePropagationMethod, AbstractWaterSurface* > m_waterSurfaces;
    AbstractWaterSurface* m_activeWaterSurface;
    WaterPool* m_waterPool;

    QTimer*       m_randomRippleTimer;
    QTimer*       m_waterPropagationTimer;
    bool          m_randomWaterRippleScheduled;
    QColor        m_sceneBackgroundColor;
    WaterSettings m_waterSettings;

    int           m_framesCount;
    QTimer*       m_fpsTimer;
    qreal         m_fps;

    Material      m_material;
    Light         m_light;
    QList< qreal > m_fpsData;
};

}

#endif // WATERRIPPLESIMULATOR_WATERRIPPLEPRESENTER_H
