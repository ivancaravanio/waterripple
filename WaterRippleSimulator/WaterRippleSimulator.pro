QT *= core \
      gui \
      multimedia \
      opengl

greaterThan( QT_MAJOR_VERSION, 4 ) {
    QT *= widgets \
          multimediawidgets
}

include( ../Build/Global.pri )

DEFINES *= APPLICATION_NAME=\\\"$${WATER_RIPPLE_SIMULATOR_APP_NAME}\\\"

TARGET = $$WATER_RIPPLE_SIMULATOR_APP_NAME
TEMPLATE = app

SOURCES += \
    Data/Light.cpp \
    Data/Material.cpp \
    Data/Mesh3.cpp \
    Data/ShaderInfo.cpp \
    Data/Triangle2.cpp \
    Data/Triangle3.cpp \
    Data/VaoData.cpp \
    Data/VertexData.cpp \
    Data/VertexDataGpu.cpp \
    Data/WaterSettings.cpp \
    Helpers/AbstractOpenGlClient.cpp \
    Helpers/AbstractWaterSurface.cpp \
    Helpers/ClearColorAlternator.cpp \
    Helpers/CoordinateSystemIndicator.cpp \
    Helpers/FramebufferBinder.cpp \
    Helpers/OpenGlCapabilityEnabler.cpp \
    Helpers/OpenGlErrorLogger.cpp \
    Helpers/OpenGlObjectBinder.cpp \
    Helpers/OpenGlTimeCounter.cpp \
    Helpers/ShaderProgram.cpp \
    Helpers/ShaderProgramActivator.cpp \
    Helpers/ViewportAlternator.cpp \
    Helpers/WaterDataCalculator.cpp \
    Helpers/WaterPerVertexDataCalculator.cpp \
    Helpers/WaterPool.cpp \
    Helpers/WaterRippler.cpp \
    Helpers/WaterRipplerCpu.cpp \
    Helpers/WaterSurfaceCpu.cpp \
    Helpers/WaterSurfaceGpu.cpp \
    Helpers/WaterSurfaceGpuPrecomputed.cpp \
    main.cpp \
    MainController.cpp \
    MainWindow.cpp \
    MediaPlayerControl.cpp \
    MediaPlayerManager.cpp \
    Utilities/GeneralUtilities.cpp \
    Utilities/GlUtilities.cpp \
    Utilities/ImageUtilities.cpp \
    Utilities/MathUtilities.cpp \
    # VideoDisplay.cpp \
    VideoSurface.cpp \
    WaterRipplePresenter.cpp \
    WaterSettingsEditor.cpp \
    WaterRippleSimulator_namespace.cpp

HEADERS += \
    Data/Light.h \
    Data/Material.h \
    Data/Mesh3.h \
    Data/ShaderInfo.h \
    Data/Triangle2.h \
    Data/Triangle3.h \
    Data/VaoData.h \
    Data/VertexData.h \
    Data/VertexDataGpu.h \
    Data/WaterSettings.h \
    glext.h \
    Helpers/AbstractOpenGlClient.h \
    Helpers/AbstractWaterSurface.h \
    Helpers/ClearColorAlternator.h \
    Helpers/CoordinateSystemIndicator.h \
    Helpers/FramebufferBinder.h \
    Helpers/OpenGlCapabilityEnabler.h \
    Helpers/OpenGlErrorLogger.h \
    Helpers/OpenGlObjectBinder.h \
    Helpers/OpenGlTimeCounter.h \
    Helpers/ShaderProgram.h \
    Helpers/ShaderProgramActivator.h \
    Helpers/ViewportAlternator.h \
    Helpers/WaterDataCalculator.h \
    Helpers/WaterPerVertexDataCalculator.h \
    Helpers/WaterPool.h \
    Helpers/WaterRippler.h \
    Helpers/WaterRipplerCpu.h \
    Helpers/WaterSurfaceCpu.h \
    Helpers/WaterSurfaceGpu.h \
    Helpers/WaterSurfaceGpuPrecomputed.h \
    MainController.h \
    MainWindow.h \
    MediaPlayerControl.h \
    MediaPlayerManager.h \
    Utilities/GeneralUtilities.h \
    Utilities/GlUtilities.h \
    Utilities/ImageUtilities.h \
    Utilities/MathUtilities.h \
    # VideoDisplay.h \
    VideoSurface.h \
    WaterRipplePresenter.h \
    WaterSettingsEditor.h \
    WaterRippleSimulator_namespace.h \
    WaterRippleSimulator_precompiled.h

RESOURCES += \
    Resources.qrc

OTHER_FILES += \
    CoordinateSystemIndicator.frag \
    CoordinateSystemIndicator.vert \
    VideoDisplay.frag \
    VideoDisplay.vert \
    WaterCalmer.frag \
    WaterDataCalculator.vert \
    WaterPerVertexDataCalculator.frag \
    WaterPool.frag \
    WaterPool.vert \
    WaterRippler.frag \
    WaterSurface.frag \
    WaterSurface.vert \
    WaterSurfaceGpu.vert \
    WaterSurfaceGpuPrecomputed.vert \
    WaterVertexHeightCalculator.frag

# precompiled header
CONFIG *= precompile_header
PRECOMPILED_HEADER = WaterRippleSimulator_precompiled.h

# C++11
CONFIG *= c++11
macx {
    QMAKE_CXXFLAGS += -std=c++11 -stdlib=libc++ -mmacosx-version-min=10.7
    LIBS += -stdlib=libc++ -mmacosx-version-min=10.7
}

include( ../Build/Versions.pri )

VERSION_MAJ = $$WATER_RIPPLE_SIMULATOR_APPLICATION_VER_MAJ
VERSION_MIN = $$WATER_RIPPLE_SIMULATOR_APPLICATION_VER_MIN
VERSION_PAT = $$WATER_RIPPLE_SIMULATOR_APPLICATION_VER_PAT
ICON_COMPLETE_BASE_FILE_PATH = $${PWD}/Resources/icons/icon

include( ../Build/Build.pri )

INCLUDEPATH += $${PWD}/../ThirdParty/glm
