#include "MediaPlayerControl.h"

#include "Utilities/MathUtilities.h"

#include <QApplication>
#include <QCheckBox>
#include <QDir>
#include <QDoubleSpinBox>
#include <QEvent>
#include <QFileDialog>
#include <QFileInfo>
#include <QFontMetrics>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#ifdef USE_PREVIEW_PROGRESS_OPACITY_ANIMATION
    #include <QPropertyAnimation>
#endif
#include <QPushButton>
#include <QSlider>
#include <QSplitter>
#include <QStringBuilder>
#include <QStyleOptionSlider>
#include <QTime>
#include <QTimer>
#include <QtMath>

#if QT_VERSION >= QT_VERSION_CHECK( 5, 0, 0 )
    #include <QStandardPaths>
#else
    #include <QDesktopServices>
#endif

#ifdef PERFORM_TESTS
    #include <QFontDatabase>
#endif

#ifndef QT_NO_DRAGANDDROP
    #include <QDragEnterEvent>
    #include <QDropEvent>
    #include <QMimeData>
#endif

#include <limits>

using namespace WaterRippleSimulator::Utilities;
using namespace WaterRippleSimulator;

const int MediaPlayerControl::s_volumeLevelMin     = 0;
const int MediaPlayerControl::s_volumeLevelInitial = 20;
const int MediaPlayerControl::s_volumeLevelMax     = 100;

const qreal MediaPlayerControl::s_playbackRateStep = 0.05;
const qreal MediaPlayerControl::s_playbackRateMin  = MediaPlayerControl::s_playbackRateStep;
const qreal MediaPlayerControl::s_playbackRateMax  = 10.0;

const QList< QMediaPlayer::State > MediaPlayerControl::s_playerStates =
        QList< QMediaPlayer::State >()
        << QMediaPlayer::PlayingState
        << QMediaPlayer::PausedState
        << QMediaPlayer::StoppedState;

const QStringList MediaPlayerControl::s_allowedFileExtensions =
        QStringList()
        << "avi" << "mov"  << "wmv"
        << "mp4" << "mkv"  << "3gp"
        << "mpg" << "mpeg" << "flv";

QT_BEGIN_NAMESPACE
    Q_WIDGETS_EXPORT QStyleOptionSlider qt_qsliderStyleOption(QSlider *slider);
QT_END_NAMESPACE

QT_USE_NAMESPACE

MediaPlayerControl::MediaPlayerControl( QWidget* parent )
    : QWidget( parent )
    , m_mediaUrlDisplay( nullptr )
    , m_openMediaFileButton( nullptr )
    , m_closeMediaFileButton( nullptr )
    , m_playButton( nullptr )
    , m_pauseButton( nullptr )
    , m_stopButton( nullptr )
    , m_progressEditor( nullptr )
    , m_progressMeasurementMode( ProgressMeasurementModeElapsed )
    , m_progressDisplay( nullptr )
    , m_durationDisplay( nullptr )
    , m_stepBackwardButton( nullptr )
    , m_jumpIntervalEditor( nullptr )
    , m_stepForwardButton( nullptr )
    , m_stepControlsContainer( nullptr )
    , m_playbackRateEditor( nullptr )
    , m_muteEditor( nullptr )
    , m_volumeLevelEditor( nullptr )
    , m_volumeLevelDisplay( nullptr )
    , m_volumeControlsContainer( nullptr )
    , m_previewProgressDisplay( nullptr )
    , m_partialPlaybackControlsSplitter( nullptr )
#ifdef USE_PREVIEW_PROGRESS_OPACITY_ANIMATION
    , m_previewProgressDisplayOpacityAnimation( nullptr )
#endif
    , m_mediaPlayer( nullptr )
    , m_mediaStatusUpdateTimer( nullptr )
    , m_progressDataUpdateTimer( nullptr )
    , m_progressEditorStepsCountResetTimer( nullptr )
    , m_previewProgressUpdateTimer( nullptr )
    , m_isProgressEditorPressed( false )
    , m_isProgressEditorHovered( false )
#ifdef PERFORM_TESTS
    , m_pseudoUpdateTimer( 0 )
#endif
{
#ifndef QT_NO_DRAGANDDROP
    this->setAcceptDrops( true );
#endif

    m_mediaUrlDisplay = new QLineEdit;
    m_mediaUrlDisplay->setReadOnly( true );

    QStyle* const style = this->style();

    m_openMediaFileButton = new QPushButton( style->standardIcon( QStyle::SP_DialogOpenButton ), QString() );
    connect( m_openMediaFileButton, SIGNAL(clicked()), this, SLOT(openMediaFileInteractively()) );

    m_closeMediaFileButton = new QPushButton( style->standardIcon( QStyle::SP_DialogCloseButton ), QString() );
    connect( m_closeMediaFileButton, SIGNAL(clicked()), this, SLOT(clearMediaFilePath()) );

    m_playButton = new QPushButton( style->standardIcon( QStyle::SP_MediaPlay ), QString() );
    connect( m_playButton, SIGNAL(clicked()), SLOT(updatePlayerStateData()) );

    m_pauseButton = new QPushButton( style->standardIcon( QStyle::SP_MediaPause ), QString() );
    connect( m_pauseButton, SIGNAL(clicked()), SLOT(updatePlayerStateData()) );

    m_stopButton = new QPushButton( style->standardIcon( QStyle::SP_MediaStop ), QString() );
    connect( m_stopButton, SIGNAL(clicked()), SLOT(updatePlayerStateData()) );

    QWidget* const playerStateChangeControlsContainer = new QWidget;
    QHBoxLayout* const playerStateChangeControlsContainerLayout = new QHBoxLayout( playerStateChangeControlsContainer );
    playerStateChangeControlsContainerLayout->setSpacing( 0 );
    playerStateChangeControlsContainerLayout->setContentsMargins( QMargins() );
    playerStateChangeControlsContainerLayout->addWidget( m_playButton );
    playerStateChangeControlsContainerLayout->addWidget( m_pauseButton );
    playerStateChangeControlsContainerLayout->addWidget( m_stopButton );

    m_progressEditor = new QSlider( Qt::Horizontal );
    m_progressEditor->setTracking( true );
    m_progressEditor->installEventFilter( this );
    connect( m_progressEditor, SIGNAL(sliderMoved(int)), this, SLOT(updatePreviewProgressDisplay()) );
    connect( m_progressEditor, SIGNAL(sliderReleased()), this, SLOT(updateProgressData()) );
    connect( m_progressEditor, SIGNAL(actionTriggered(int)), this, SLOT(onActionTriggered()) );
    connect( m_progressEditor, SIGNAL(rangeChanged(int,int)), this, SLOT(updateProgressDisplay()) );

    m_progressDisplay = new QPushButton;
    m_progressDisplay->setObjectName( "progressDisplay" );
    m_progressDisplay->setContentsMargins( QMargins() );
    connect( m_progressDisplay, SIGNAL(clicked()), this, SLOT(switchProgressMeasurementMode()) );

    m_progressDurationDelimiter = new QLabel;
    m_progressDurationDelimiter->setText( QString( QChar( QLatin1Char( '/' ) ) ) );
    m_progressDurationDelimiter->setAlignment( Qt::AlignCenter );

    m_durationDisplay = new QLabel;
    m_durationDisplay->setObjectName( "durationDisplay" );

    static const int stepRepeatInitialDelay = 200;
    static const int stepRepeatInterval     = 150;

    m_stepBackwardButton = new QPushButton( style->standardIcon( QStyle::SP_MediaSkipBackward ), QString() );
    m_stepBackwardButton->setAutoRepeat( true );
    m_stepBackwardButton->setAutoRepeatDelay( stepRepeatInitialDelay );
    m_stepBackwardButton->setAutoRepeatInterval( stepRepeatInterval );
    connect( m_stepBackwardButton, SIGNAL(clicked()), this, SLOT(stepBackward()) );

    m_jumpIntervalEditor = new QSpinBox;
    m_jumpIntervalEditor->setAlignment( Qt::AlignRight );
    m_jumpIntervalEditor->setMinimum( 0 );
    m_jumpIntervalEditor->setMaximum( 60 * 60 * 1000 );

    m_stepForwardButton = new QPushButton( style->standardIcon( QStyle::SP_MediaSkipForward ), QString() );
    m_stepForwardButton->setAutoRepeat( true );
    m_stepForwardButton->setAutoRepeatDelay( stepRepeatInitialDelay );
    m_stepForwardButton->setAutoRepeatInterval( stepRepeatInterval );
    connect( m_stepForwardButton, SIGNAL(clicked()), this, SLOT(stepForward()) );

    m_stepControlsContainer = new QWidget;
    QHBoxLayout* const stepControlsContainerLayout = new QHBoxLayout( m_stepControlsContainer );
    stepControlsContainerLayout->setContentsMargins( QMargins() );
    stepControlsContainerLayout->addWidget( m_stepBackwardButton, 0 );
    stepControlsContainerLayout->addWidget( m_jumpIntervalEditor, 1 );
    stepControlsContainerLayout->addWidget( m_stepForwardButton, 0 );

    m_playbackRateEditor = new QDoubleSpinBox;
    m_playbackRateEditor->setRange( s_playbackRateMin, s_playbackRateMax );
    m_playbackRateEditor->setSingleStep( s_playbackRateStep );
    m_playbackRateEditor->setAlignment( Qt::AlignRight );
    connect( m_playbackRateEditor, SIGNAL(valueChanged(double)), this, SLOT(updatePlaybackRateData()) );

    m_muteEditor = new QPushButton;
    m_muteEditor->setCheckable( true );
    connect( m_muteEditor, SIGNAL(clicked()), this, SLOT(updateIsMutedData()) );

    m_volumeLevelEditor = new QSlider( Qt::Horizontal );
    m_volumeLevelEditor->setRange( s_volumeLevelMin, s_volumeLevelMax );
    connect( m_volumeLevelEditor, SIGNAL(valueChanged(int)), this, SLOT(updateVolumeLevelData()) );

    m_volumeLevelDisplay = new QLabel;

    m_volumeControlsContainer = new QWidget;
    QHBoxLayout* const volumeControlsContainerLayout = new QHBoxLayout( m_volumeControlsContainer );
    volumeControlsContainerLayout->setContentsMargins( QMargins() );
    volumeControlsContainerLayout->addWidget( m_muteEditor, 0 );
    volumeControlsContainerLayout->addWidget( m_volumeLevelEditor, 1 );
    volumeControlsContainerLayout->addWidget( m_volumeLevelDisplay, 0 );

    m_partialPlaybackControlsSplitter = new QSplitter;
    m_partialPlaybackControlsSplitter->addWidget( m_stepControlsContainer );
    m_partialPlaybackControlsSplitter->addWidget( m_playbackRateEditor );
    m_partialPlaybackControlsSplitter->addWidget( m_volumeControlsContainer );

    QHBoxLayout* const mediaFilePathControlsLayout = new QHBoxLayout;
    mediaFilePathControlsLayout->addWidget( m_mediaUrlDisplay, 1 );
    mediaFilePathControlsLayout->addWidget( m_openMediaFileButton, 0 );
    mediaFilePathControlsLayout->addWidget( m_closeMediaFileButton, 0 );

    QHBoxLayout* const progressControlsLayout = new QHBoxLayout;
    progressControlsLayout->addWidget( m_progressEditor, 1 );
    progressControlsLayout->addWidget( m_progressDisplay, 0 );
    progressControlsLayout->addWidget( m_progressDurationDelimiter, 0 );
    progressControlsLayout->addWidget( m_durationDisplay, 0 );

    QHBoxLayout* const playbackControlsLayout = new QHBoxLayout;
    playbackControlsLayout->addWidget( playerStateChangeControlsContainer, 0 );
    playbackControlsLayout->addWidget( m_partialPlaybackControlsSplitter, 1 );

    QVBoxLayout* const playerControlsLayout = new QVBoxLayout( this );
    playerControlsLayout->setContentsMargins( QMargins() );
    playerControlsLayout->addLayout( mediaFilePathControlsLayout );
    playerControlsLayout->addLayout( progressControlsLayout );
    playerControlsLayout->addLayout( playbackControlsLayout );

    this->resetMediaPlayer();
    this->updateVolumeLevelDisplaySize();

    this->retranslate();
}

MediaPlayerControl::~MediaPlayerControl()
{
}

void MediaPlayerControl::setMediaUrl( const QUrl& aMediaUrl )
{
    if ( aMediaUrl == m_mediaPlayer->media().canonicalUrl() )
    {
        return;
    }

    m_mediaPlayer->setMedia( aMediaUrl.isValid()
                             ? QMediaContent( aMediaUrl )
                             : QMediaContent() );
    this->updateMediaDisplay();
}

void MediaPlayerControl::setMediaFilePath( const QString& aMediaFilePath )
{
    this->setMediaUrl( QUrl::fromLocalFile( aMediaFilePath ) );
}

void MediaPlayerControl::clearMediaFilePath()
{
    this->setMediaUrl( QUrl() );
}

void MediaPlayerControl::setPlayerState( const QMediaPlayer::State aPlayerState )
{
    switch ( aPlayerState )
    {
        case QMediaPlayer::PlayingState:
            m_mediaPlayer->play();
            break;
        case QMediaPlayer::PausedState:
            m_mediaPlayer->pause();
            break;
        case QMediaPlayer::StoppedState:
            m_mediaPlayer->stop();
            break;
        default:
            break;
    }
}

void MediaPlayerControl::play()
{
    this->setPlayerState( QMediaPlayer::PlayingState );
}

void MediaPlayerControl::pause()
{
    this->setPlayerState( QMediaPlayer::PausedState );
}

void MediaPlayerControl::stop()
{
    this->setPlayerState( QMediaPlayer::StoppedState );
}

void MediaPlayerControl::setIsMuted( const bool aIsMuted )
{
    m_mediaPlayer->setMuted( aIsMuted );
}

void MediaPlayerControl::setVolumeLevel( const int aVolumeLevel )
{
    m_mediaPlayer->setVolume( aVolumeLevel );
}

void MediaPlayerControl::setPlaybackRate( const qreal& aPlaybackRate )
{
    m_mediaPlayer->setPlaybackRate( aPlaybackRate );
}

void MediaPlayerControl::setProgressMeasurementMode( const MediaPlayerControl::ProgressMeasurementMode aProgressMeasurementMode )
{
    if ( ! MediaPlayerControl::isProgressMeasurementModeValid( aProgressMeasurementMode )
         || aProgressMeasurementMode == m_progressMeasurementMode )
    {
        return;
    }

    m_progressMeasurementMode = aProgressMeasurementMode;
    this->updateProgressDisplay();
}

void MediaPlayerControl::switchProgressMeasurementMode()
{
    ProgressMeasurementMode newProgressMeasurementMode = m_progressMeasurementMode;
    switch ( m_progressMeasurementMode )
    {
        case ProgressMeasurementModeElapsed:
            newProgressMeasurementMode = ProgressMeasurementModeRemaining;
            break;
        case ProgressMeasurementModeRemaining:
            newProgressMeasurementMode = ProgressMeasurementModeElapsed;
            break;
    }

    this->setProgressMeasurementMode( newProgressMeasurementMode );
}

void MediaPlayerControl::showEvent( QShowEvent* event )
{
    QWidget::showEvent( event );

    QMetaObject::invokeMethod(
        this,
        QT_STRINGIFY2( resizePartialPlaybackControlsOptimally ),
        Qt::QueuedConnection );

#ifdef PERFORM_TESTS
    if ( m_pseudoUpdateTimer == nullptr )
    {
        m_pseudoUpdateTimer = new QTimer( this );
        connect( m_pseudoUpdateTimer, SIGNAL(timeout()), this, SLOT(setRandomAppFont()) );
    }

    m_pseudoUpdateTimer->start( 2000 );
#endif
}

void MediaPlayerControl::changeEvent( QEvent* event )
{
    QWidget::changeEvent( event );

    if ( event->type() == QEvent::ApplicationFontChange )
    {
        this->updateVolumeLevelDisplaySize();
    }
}

#ifndef QT_NO_DRAGANDDROP
void MediaPlayerControl::dragEnterEvent( QDragEnterEvent* event )
{
    QWidget::dragEnterEvent( event );

    const QUrl url = MediaPlayerControl::urlFromMimeData( event->mimeData() );
    if ( url.isValid() )
    {
        event->acceptProposedAction();
    }
}

void MediaPlayerControl::dropEvent( QDropEvent* event )
{
    QWidget::dropEvent( event );

    const QUrl url = MediaPlayerControl::urlFromMimeData( event->mimeData() );
    if ( url.isValid() )
    {
        this->setMediaUrl( url );
    }
}
#endif

void MediaPlayerControl::openMediaFileInteractively()
{
    const QString initialDirPath =
        #if QT_VERSION >= QT_VERSION_CHECK( 5, 0, 0 )
            QStandardPaths::writableLocation( QStandardPaths::DesktopLocation );
        #else
            QDesktopServices::storageLocation( QDesktopServices::DesktopLocation );
        #endif

    static QString gluedFileExtensions;
    if ( gluedFileExtensions.isEmpty() )
    {
        foreach ( const QString& allowedFileExtension, s_allowedFileExtensions )
        {
            gluedFileExtensions += "*." % allowedFileExtension % QChar( QLatin1Char( ' ' ) );
        }

        gluedFileExtensions = gluedFileExtensions.trimmed();
    }

    QString mediaFilePath = QFileDialog::getOpenFileName(
                                QApplication::activeWindow(),
                                tr( "Choose media file" ),
                                initialDirPath,
                                tr( "Video files (%1)" ).arg( gluedFileExtensions ) );
    if ( mediaFilePath.isEmpty() )
    {
        return;
    }

    {
        const QFileInfo mediaFileInfo( mediaFilePath );
        if ( mediaFileInfo.isSymLink() )
        {
            mediaFilePath = mediaFileInfo.symLinkTarget();
        }
    }

    const QFileInfo actualMediaFileInfo( mediaFilePath );
    if ( ! s_allowedFileExtensions.contains( actualMediaFileInfo.suffix(), Qt::CaseInsensitive ) )
    {
        return;
    }

    this->setMediaFilePath( mediaFilePath );
}

void MediaPlayerControl::onMediaStatusChanged()
{
    if ( m_mediaStatusUpdateTimer == nullptr )
    {
        m_mediaStatusUpdateTimer = new QTimer( this );
        connect( m_mediaStatusUpdateTimer, SIGNAL(timeout()), this, SLOT(onMediaStatusChanged()) );
        m_mediaStatusUpdateTimer->setInterval( 200 );
    }

    if ( m_mediaStatusUpdateTimer != nullptr
         && this->sender() == m_mediaStatusUpdateTimer )
    {
        if ( ! this->isMediaValid()
             && ! m_mediaPlayer->media().isNull() )
        {
            // if QMediaPlayer doesn't recognize the input media file or it is corrupted,
            // clearing the content of the media player object by setting it an empty one,
            // doesn't reset its state to a neutral one
            // it is even impossible to subsequently load a valid content
            // force clear the state
            this->resetMediaPlayer();
            return;
        }
    }
    else
    {
        m_mediaStatusUpdateTimer->start();
        return;
    }

    this->updateMediaDisplay();
}

void MediaPlayerControl::updateMediaUrlDisplay()
{
    m_mediaUrlDisplay->setText( this->uiFriendlyMediaFilePath() );
}

void MediaPlayerControl::updateMediaDisplay()
{
    this->updateMediaUrlDisplay();
    this->updateControlsEnablence();
    this->updateProgressAndDurationDisplay();
}

void MediaPlayerControl::onPlayerStateDataChanged( const QMediaPlayer::State aPlayerState )
{
    this->updatePlayerStateDisplay();

    emit playerStateChanged( aPlayerState );

    switch ( aPlayerState )
    {
        case QMediaPlayer::StoppedState:
            emit stopped();
            break;
        case QMediaPlayer::PlayingState:
            emit startedPlaying();
            break;
        case QMediaPlayer::PausedState:
            emit paused();
            break;
    }
}

void MediaPlayerControl::updatePlayerStateData()
{
    QObject* const senderObj = this->sender();
    if ( senderObj == nullptr )
    {
        return;
    }

    QPushButton* const playerStateChangeRequestSender = qobject_cast< QPushButton* >( senderObj );
    if ( playerStateChangeRequestSender != m_playButton
         && playerStateChangeRequestSender != m_pauseButton
         && playerStateChangeRequestSender != m_stopButton )
    {
        return;
    }

    const QMediaPlayer::State requestedState = this->playerStateFromPlayerButton( playerStateChangeRequestSender );
    if ( requestedState == m_mediaPlayer->state() )
    {
        return;
    }

    this->setPlayerState( requestedState );
}

void MediaPlayerControl::updatePlayerStateDisplay()
{
    const QMediaPlayer::State mediaPlayerState = this->playerState();
    const bool isMediaValid = this->isMediaValid();
    m_playButton->setEnabled( isMediaValid && mediaPlayerState != QMediaPlayer::PlayingState );
    m_pauseButton->setEnabled( isMediaValid && mediaPlayerState == QMediaPlayer::PlayingState );
    m_stopButton->setEnabled( isMediaValid && mediaPlayerState != QMediaPlayer::StoppedState );
}

void MediaPlayerControl::updateProgressData()
{
    if ( this->sender() != m_progressDataUpdateTimer )
    {
        if ( m_progressDataUpdateTimer == nullptr )
        {
            m_progressDataUpdateTimer = new QTimer( this );
            m_progressDataUpdateTimer->setSingleShot( true );

            // void QAbstractSlider::setRepeatAction(SliderAction action, int thresholdTime = 500, int repeatTime = 50);
            //
            // the timer interval should be larger than the "repeatTime"
            m_progressDataUpdateTimer->setInterval( 550 );
            connect( m_progressDataUpdateTimer, SIGNAL(timeout()), this, SLOT(updateProgressData()) );
        }

        m_progressDataUpdateTimer->start();
        return;
    }

    m_mediaPlayer->setPosition( this->mediaPlayerPositionFromProgress( m_progressEditor->value() ) );
}

void MediaPlayerControl::updateProgressDisplay()
{
    if ( m_progressDataUpdateTimer != nullptr )
    {
        if ( m_progressDataUpdateTimer->isActive() )
        {
            return;
        }
    }

    const qint64 duration = m_mediaPlayer->duration();
    const bool isDurationValid = duration > Q_INT64_C( 0 );
    const qint64 elapsedTime = m_mediaPlayer->position();
    const QString elapsedTimeString = MediaPlayerControl::msecsToString(
                                          isDurationValid
                                          ? ( m_progressMeasurementMode == ProgressMeasurementModeElapsed
                                              ? elapsedTime
                                              : duration - elapsedTime )
                                          : Q_INT64_C( -1 ) );
    m_progressDisplay->setText( elapsedTimeString );

    const int progressEditorMinValue = m_progressEditor->minimum();
    const int progressEditorMaxValue = m_progressEditor->maximum();
    const int progressEditorNewValue = isDurationValid
                                       ? qBound( progressEditorMinValue,
                                                 qRound( ( qreal( elapsedTime ) / duration )
                                                         * ( progressEditorMaxValue - progressEditorMinValue ) )
                                                 + progressEditorMinValue,
                                                 progressEditorMaxValue )
                                       : progressEditorMinValue;

    // qDebug() << "updateProgressDisplay: progressEditorValue: old(" << m_progressEditor->value() << "), new(" << progressEditorNewValue << ")";
    m_progressEditor->setEnabled( isDurationValid );
    m_progressEditor->setValue( progressEditorNewValue );

    m_stepBackwardButton->setEnabled( isDurationValid
                                      ? elapsedTime > 0
                                      : false );
    m_stepForwardButton->setEnabled( isDurationValid
                                     ? elapsedTime < duration
                                     : false );
}

void MediaPlayerControl::stepBackward()
{
    const qint64 minPosition = Q_INT64_C( 0 );
    const qint64 currentPosition = m_mediaPlayer->position();
    if ( currentPosition > minPosition )
    {
        m_mediaPlayer->setPosition( qMax( currentPosition - static_cast< qint64 >( m_jumpIntervalEditor->value() ), minPosition ) );
    }
}

void MediaPlayerControl::stepForward()
{
    const qint64 maxPosition = m_mediaPlayer->duration();
    const qint64 currentPosition = m_mediaPlayer->position();
    if ( currentPosition < maxPosition )
    {
        m_mediaPlayer->setPosition( qMin( currentPosition + static_cast< qint64 >( m_jumpIntervalEditor->value() ), maxPosition ) );
    }
}

void MediaPlayerControl::updateIsMutedData()
{
    m_mediaPlayer->setMuted( m_muteEditor->isChecked() );
}

void MediaPlayerControl::updateIsMutedDisplay()
{
    const bool isMuted = m_mediaPlayer->isMuted();
    m_muteEditor->setIcon(
                this->style()->standardIcon(
                    isMuted
                    ? QStyle::SP_MediaVolumeMuted
                    : QStyle::SP_MediaVolume ) );
    m_muteEditor->setChecked( isMuted );
}

void MediaPlayerControl::updateVolumeLevelData()
{
    m_mediaPlayer->setVolume( m_volumeLevelEditor->value() );
}

void MediaPlayerControl::updateVolumeLevelDisplay()
{
    const int volumeLevel = m_mediaPlayer->volume();
    m_volumeLevelEditor->setValue( volumeLevel );
    m_volumeLevelDisplay->setText(
                MediaPlayerControl::volumeLevelDisplayFormat().arg(
                    volumeLevel,
                    MediaPlayerControl::volumeLevelMaxDigitsCount(),
                    10,
                    QLatin1Char( ' ' ) ) );
}

void MediaPlayerControl::updatePlaybackRateData()
{
    const qreal newPlaybackRate = static_cast< qreal >( m_playbackRateEditor->value() );
    if ( qFuzzyCompare( newPlaybackRate, m_mediaPlayer->playbackRate() ) )
    {
        return;
    }

    m_mediaPlayer->setPlaybackRate( newPlaybackRate );
}

void MediaPlayerControl::updatePlaybackRateDisplay()
{
    if ( m_mediaPlayer != nullptr && this->sender() == m_mediaPlayer )
    {
        qDebug() << "playbackRateChanged:" << this->playbackRate();
    }

    const double playbackRateEditorNewValue = static_cast< double >( m_mediaPlayer->playbackRate() );
    if ( qFuzzyCompare( playbackRateEditorNewValue, m_playbackRateEditor->value() ) )
    {
        return;
    }

    m_playbackRateEditor->setValue( playbackRateEditorNewValue );
}

#ifdef PERFORM_TESTS
void MediaPlayerControl::setRandomAppFont()
{
    QFontDatabase fdb;
    const QStringList fontFamiliesNames = fdb.families();
    const int fontFamiliesNamesCount = fontFamiliesNames.size();
    if ( fontFamiliesNamesCount == 0 )
    {
        return;
    }

    QFont appFont = QApplication::font();
    const QString currentFontFamily = appFont.family();
    forever
    {
        const QString fontFamilyName = fontFamiliesNames[ qrand() % fontFamiliesNamesCount ];
        if ( fontFamilyName != currentFontFamily )
        {
            appFont.setFamily( fontFamilyName );
            break;
        }
    }

    static const int fontPointSizeMin = 8;
    static const int fontPointSizeMax = 36;
    const int currentFontPointSize = appFont.pointSize();
    forever
    {
        const int fontPointSize = MathUtilities::rangeRand( fontPointSizeMin, fontPointSizeMax );
        if ( currentFontPointSize != fontPointSize )
        {
            appFont.setPointSize( fontPointSize );
            break;
        }
    }

    qDebug() << "font: family(" << appFont.family() << "), pointSize(" << appFont.pointSize() << ")";

    QApplication::setFont( appFont );
}
#endif

QMediaPlayer::State MediaPlayerControl::playerState() const
{
    return m_mediaPlayer->state();
}

bool MediaPlayerControl::isPlaying() const
{
    return this->playerState() == QMediaPlayer::PlayingState;
}

bool MediaPlayerControl::isPaused() const
{
    return this->playerState() == QMediaPlayer::PausedState;
}

bool MediaPlayerControl::isStopped() const
{
    return this->playerState() == QMediaPlayer::StoppedState;
}

int MediaPlayerControl::isMuted() const
{
    return m_mediaPlayer->isMuted();
}

int MediaPlayerControl::volumeLevel() const
{
    return m_mediaPlayer->volume();
}

qreal MediaPlayerControl::playbackRate() const
{
    return m_mediaPlayer->playbackRate();
}

MediaPlayerControl::ProgressMeasurementMode MediaPlayerControl::progressMeasurementMode() const
{
    return m_progressMeasurementMode;
}

bool MediaPlayerControl::isProgressMeasurementModeValid(
    const MediaPlayerControl::ProgressMeasurementMode progressMeasurementMode )
{
    return progressMeasurementMode == MediaPlayerControl::ProgressMeasurementModeElapsed
           || progressMeasurementMode == MediaPlayerControl::ProgressMeasurementModeRemaining;
}

void MediaPlayerControl::setVideoOutput( QVideoWidget* const aVideoOutput )
{
    m_mediaPlayer->setVideoOutput( aVideoOutput );

    m_videoOutputVideoWidget = aVideoOutput;
    m_videoOutputGraphicsVideoItem.clear();
    m_videoOutputAbstractVideoSurface.clear();
}

void MediaPlayerControl::setVideoOutput( QGraphicsVideoItem* const aVideoOutput )
{
    m_mediaPlayer->setVideoOutput( aVideoOutput );

    m_videoOutputVideoWidget.clear();
    m_videoOutputGraphicsVideoItem = aVideoOutput;
    m_videoOutputAbstractVideoSurface.clear();
}

void MediaPlayerControl::setVideoOutput( QAbstractVideoSurface* const aVideoOutput )
{
    m_mediaPlayer->setVideoOutput( aVideoOutput );

    m_videoOutputVideoWidget.clear();
    m_videoOutputGraphicsVideoItem.clear();
    m_videoOutputAbstractVideoSurface = aVideoOutput;
}

bool MediaPlayerControl::eventFilter( QObject* obj, QEvent* event )
{
    if ( obj == m_progressEditor )
    {
        switch ( event->type() )
        {
            case QEvent::MouseButtonPress:
                m_isProgressEditorPressed = true;
                this->updatePreviewProgressDisplay();
                break;
            case QEvent::MouseButtonRelease:
                m_isProgressEditorPressed = false;
                this->updatePreviewProgressDisplay();
                break;
            case QEvent::Resize:
                if ( m_progressEditorStepsCountResetTimer == nullptr )
                {
                    m_progressEditorStepsCountResetTimer = new QTimer( this );
                    m_progressEditorStepsCountResetTimer->setSingleShot( true );
                    m_progressEditorStepsCountResetTimer->setInterval( 200 );
                    connect( m_progressEditorStepsCountResetTimer, SIGNAL(timeout()), this, SLOT(resetProgressEditorStepsCount()) );
                }

                m_progressEditorStepsCountResetTimer->start();
                break;
            case QEvent::HoverEnter:
                m_isProgressEditorHovered = true;
                // don't break
            case QEvent::HoverMove: // don't break
                this->updatePreviewProgressDisplay();
                break;
            case QEvent::HoverLeave:
                m_isProgressEditorHovered = false;
                this->tryHidePreviewProgressDisplay();
                break;
            default:
                break;
        }
    }

    return QWidget::eventFilter( obj, event );
}

void MediaPlayerControl::retranslate()
{
    m_jumpIntervalEditor->setSuffix( tr( "ms" ) );
    m_playbackRateEditor->setSuffix( tr( "x" ) );
}

QPushButton* MediaPlayerControl::playerButtonFromPlayerState( const QMediaPlayer::State playerState ) const
{
    switch ( playerState )
    {
        case QMediaPlayer::PlayingState:
            return m_playButton;
        case QMediaPlayer::PausedState:
            return m_pauseButton;
        case QMediaPlayer::StoppedState:
            return m_stopButton;
        default:
            break;
    }

    return 0;
}

QMediaPlayer::State MediaPlayerControl::playerStateFromPlayerButton( QPushButton* const playerStateButton ) const
{
    if ( playerStateButton == m_playButton )
    {
        return QMediaPlayer::PlayingState;
    }

    if ( playerStateButton == m_pauseButton )
    {
        return QMediaPlayer::PausedState;
    }

    if ( playerStateButton == m_stopButton )
    {
        return QMediaPlayer::StoppedState;
    }

    return QMediaPlayer::StoppedState;
}

QString MediaPlayerControl::msecsToString( const qint64& msecs )
{
    if ( msecs < 0 )
    {
        static const QString invalidTimeString( "--:--:--" );
        return invalidTimeString;
    }

    static const qint64 msecsPerSecond   = 1000;
    static const qint64 secondsPerMinute = 60;
    static const qint64 minutesPerHour   = 60;
    static const qint64 hoursPerDay      = 24;

    static const qint64 dateTimeMultipliers[] =
    {
        hoursPerDay,
        minutesPerHour,
        secondsPerMinute,
        msecsPerSecond
    };

    static const qint64 dateTimeMultipliersCount = sizeof( dateTimeMultipliers ) / sizeof( qint64 );

    enum DateTimeUnitIndex
    {
        DateTimeUnitIndexDay    = 0,
        DateTimeUnitIndexHour   = 1,
        DateTimeUnitIndexMinute = 2,
        DateTimeUnitIndexSecond = 3
    };

    QString dateTimeString;
    qint64 msecsLeft = msecs;
    for ( int i = DateTimeUnitIndexDay; i < dateTimeMultipliersCount; ++ i )
    {
        qint64 multiplier = Q_INT64_C( 1 );
        for ( int j = i; j < dateTimeMultipliersCount; ++ j )
        {
            multiplier *= dateTimeMultipliers[ j ];
        }

        const qint64 dateTimePart = msecsLeft / multiplier;
        msecsLeft -= dateTimePart * multiplier;

        if ( i == DateTimeUnitIndexDay )
        {
            if ( dateTimePart > 0 )
            {
                dateTimeString += QString::number( dateTimePart ) % "d ";
            }
        }
        else
        {
            const int digitsCount = qFloor(
                                        log10( i > 0
                                               ? dateTimeMultipliers[ i - 1 ] - 1
                                               : 1 ) ) + 1;
            dateTimeString += QString( "%1" ).arg( dateTimePart,
                                                   digitsCount,
                                                   10,
                                                   QLatin1Char( '0' ) );
            if ( i != DateTimeUnitIndexSecond )
            {
                dateTimeString += ':';
            }
        }
    }

    return dateTimeString;
}

void MediaPlayerControl::updateVolumeLevelDisplaySize()
{
    if ( m_volumeLevelDisplay == nullptr )
    {
        return;
    }

    const QFontMetrics volumeLevelDisplayFontMetrics = m_volumeLevelDisplay->fontMetrics();
    int maxCharWidth = -1;
    const QString volumeLevelDisplayFormat = MediaPlayerControl::volumeLevelDisplayFormat();
    static const QString chars = volumeLevelDisplayFormat.arg( "0123456789" );
    foreach ( const QChar& c, chars )
    {
        const int charWidth = volumeLevelDisplayFontMetrics.width( c );
        if ( charWidth > maxCharWidth )
        {
            maxCharWidth = charWidth;
        }
    }

    static const QChar contentReplacementSymbol( QLatin1Char( '_' ) );
    const int charactersCount = MediaPlayerControl::volumeLevelMaxDigitsCount()
                                + volumeLevelDisplayFormat
                                  .arg( contentReplacementSymbol )
                                  .remove( contentReplacementSymbol ).size();
    m_volumeLevelDisplay->setMinimumWidth( charactersCount * maxCharWidth );
    m_volumeLevelDisplay->setAlignment( Qt::AlignCenter );
}

int MediaPlayerControl::volumeLevelMaxDigitsCount()
{
    return MathUtilities::digitsCount( s_volumeLevelMax );
}

QString MediaPlayerControl::volumeLevelDisplayFormat()
{
    return tr( "%1%" );
}

void MediaPlayerControl::updateControlsEnablence()
{
    const bool hasValidMedia = this->isMediaValid();

    QWidget* const enablenceAlteredControls[] =
    {
        m_progressDisplay,
        m_progressEditor,
        m_stepBackwardButton,
        m_stepForwardButton,
        m_closeMediaFileButton
    };

    const int enablenceAlteredControlsCount = sizeof( enablenceAlteredControls ) / sizeof( QWidget* );
    for ( int i = 0; i < enablenceAlteredControlsCount; ++ i )
    {
        enablenceAlteredControls[ i ]->setEnabled( hasValidMedia );
    }

    this->updatePlayerStateDisplay();
}

QString MediaPlayerControl::uiFriendlyMediaFilePath() const
{
    if ( ! this->isMediaValid() )
    {
        return QString();
    }

    const QUrl mediaUrl = this->mediaUrl();
    return mediaUrl.isLocalFile()
           ? QDir::toNativeSeparators( mediaUrl.toLocalFile() )
           : mediaUrl.toString();
}

QRect MediaPlayerControl::progressEditorSubControlRect( const QStyle::SubControl sliderSubControl ) const
{
    if (    sliderSubControl != QStyle::SC_SliderGroove
         && sliderSubControl != QStyle::SC_SliderHandle
         && sliderSubControl != QStyle::SC_SliderTickmarks )
    {
        return QRect();
    }

    // NOTE: if the handle is outside the groove:
    // QSlider::groove:horizontal {
    //     border: 1px solid #999999;
    //     height: 8px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */
    //     background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);
    //     margin: 2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */
    // }

    // QSlider::handle:horizontal {
    //     background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);
    //     border: 1px solid #5c5c5c;
    //     width: 18px;
    //     margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */
    //     border-radius: 3px;
    // }
    //
    // ... the size of the groove is reported along with the margins which amount cannot be deduced

    const QStyleOptionSlider sliderStyleOption = qt_qsliderStyleOption( m_progressEditor );

    return m_progressEditor->style()->subControlRect(
                QStyle::CC_Slider,
                & sliderStyleOption,
                sliderSubControl,
                m_progressEditor );
}

int MediaPlayerControl::progressEditorStepsCountHint() const
{
    const QRect progressEditorGrooveRect = this->progressEditorSubControlRect( QStyle::SC_SliderGroove );

    return qMin( ( m_progressEditor->orientation() == Qt::Horizontal
                   ? progressEditorGrooveRect.width()
                   : progressEditorGrooveRect.height() ),
                 static_cast< int >(
                     qBound( Q_INT64_C( 0 ),
                             this->isMediaValid()
                             ? m_mediaPlayer->duration()
                             : Q_INT64_C( -1 ),
                             static_cast< qint64 >( std::numeric_limits< int >::max() ) ) + 1 ) );
}

qint64 MediaPlayerControl::mediaPlayerPositionFromProgress( const int progressEditorValue ) const
{
    const int progressEditorMinValue = m_progressEditor->minimum();
    const int progressEditorMaxValue = m_progressEditor->maximum();
    if ( progressEditorValue < progressEditorMinValue
         || progressEditorValue > progressEditorMaxValue )
    {
        return Q_INT64_C( -1 );
    }

    const qint64 duration = m_mediaPlayer->duration();
    const int progressEditorRangeDiff = progressEditorMaxValue - progressEditorMinValue;

    return progressEditorRangeDiff > 0
           ? qBound( Q_INT64_C( 0 ),
                     qRound64( ( qreal( progressEditorValue - progressEditorMinValue ) / progressEditorRangeDiff )
                               * duration ),
                     duration )
           : Q_INT64_C( 0 );
}

void MediaPlayerControl::setPreviewProgressDisplayVisibility( const bool shouldBeVisible )
{
    if ( m_previewProgressDisplay == nullptr )
    {
        return;
    }

#ifdef USE_PREVIEW_PROGRESS_OPACITY_ANIMATION
    if ( m_previewProgressDisplayOpacityAnimation == nullptr )
    {
        m_previewProgressDisplayOpacityAnimation = new QPropertyAnimation( m_previewProgressDisplay, "windowOpacity", this );
        m_previewProgressDisplayOpacityAnimation->setDuration( 400 );
        m_previewProgressDisplayOpacityAnimation->setEasingCurve( QEasingCurve::OutCubic );
    }

    const qreal previewProgressOpacity = m_previewProgressDisplay->windowOpacity();
    const bool isAnimationRunning = m_previewProgressDisplayOpacityAnimation->state() == QAbstractAnimation::Running;
    const qreal desiredOpacity = shouldBeVisible ? 1.0 : 0.0;
    if ( qFuzzyCompare( isAnimationRunning
                        ? m_previewProgressDisplayOpacityAnimation->endValue().toReal()
                        : previewProgressOpacity, desiredOpacity ) )
    {
        return;
    }

    m_previewProgressDisplayOpacityAnimation->setEndValue( desiredOpacity );
    if ( ! isAnimationRunning )
    {
        m_previewProgressDisplayOpacityAnimation->setStartValue( shouldBeVisible ? 0.0 : 1.0 );
        m_previewProgressDisplayOpacityAnimation->start();
    }
#else
    m_previewProgressDisplay->setVisible( shouldBeVisible );
#endif
}

void MediaPlayerControl::updateDurationDisplay()
{
    m_durationDisplay->setText( MediaPlayerControl::msecsToString( m_mediaPlayer->duration() ) );
}

void MediaPlayerControl::updateProgressAndDurationDisplay()
{
    this->updateProgressDisplay();
    this->updateDurationDisplay();
}

void MediaPlayerControl::resetProgressEditorStepsCount()
{
    const int maxStepHint = this->progressEditorStepsCountHint();
    m_progressEditor->setMaximum( maxStepHint + m_progressEditor->minimum() - 1 );
}

void MediaPlayerControl::tryHidePreviewProgressDisplay()
{
    if ( m_isProgressEditorHovered || m_isProgressEditorPressed )
    {
        return;
    }

    if ( m_previewProgressUpdateTimer != nullptr )
    {
        if ( m_previewProgressUpdateTimer->isActive() )
        {
            return;
        }
    }

    if ( m_previewProgressDisplay != nullptr )
    {
        this->setPreviewProgressDisplayVisibility( false );
    }
}

void MediaPlayerControl::onActionTriggered()
{
    this->updatePreviewProgressDisplay();
    this->updateProgressData();
}

void MediaPlayerControl::resizePartialPlaybackControlsOptimally()
{
    const int stepControlsOptimalWidth            = m_stepControlsContainer->sizeHint().width();
    const int playbackRateEditorOptimalWidth      = m_playbackRateEditor->sizeHint().width();
    const int splitterWidth                       = m_partialPlaybackControlsSplitter->width();
    const int volumeControlsContainerDesiredWidth =
            splitterWidth
            - stepControlsOptimalWidth
            - playbackRateEditorOptimalWidth
            - ( splitterWidth
                - m_stepControlsContainer->width()
                - m_playbackRateEditor->width()
                - m_volumeControlsContainer->width() );

    QList< int > splitterControlsWidths;
    splitterControlsWidths.reserve( 3 );
    splitterControlsWidths << stepControlsOptimalWidth
                           << playbackRateEditorOptimalWidth
                           << volumeControlsContainerDesiredWidth;

    m_partialPlaybackControlsSplitter->setSizes( splitterControlsWidths );
}

void MediaPlayerControl::resetMediaPlayer()
{
    bool isMuted = false;
    int volumeLevel = s_volumeLevelInitial;
    qreal playbackRate = 1.0;

    if ( m_mediaPlayer != nullptr )
    {
        // some parameters are loaded media-dependent
        // use the UI configured ones to initialize the media player object
        isMuted = m_muteEditor->isChecked();
        volumeLevel = m_volumeLevelEditor->value();
        playbackRate = m_playbackRateEditor->value();

        // delete the media player after getting the aforementioned properties
        disconnect( m_mediaPlayer, SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(onPlayerStateDataChanged(QMediaPlayer::State)) );
        disconnect( m_mediaPlayer, SIGNAL(durationChanged(qint64)), this, SLOT(updateDurationDisplay()) );
        disconnect( m_mediaPlayer, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)), this, SLOT(onMediaStatusChanged()) );
        disconnect( m_mediaPlayer, SIGNAL(positionChanged(qint64)), this, SLOT(updateProgressDisplay()) );
        disconnect( m_mediaPlayer, SIGNAL(durationChanged(qint64)), this, SLOT(updateProgressAndDurationDisplay()) );
        disconnect( m_mediaPlayer, SIGNAL(playbackRateChanged(qreal)), this, SLOT(updatePlaybackRateDisplay()) );
        disconnect( m_mediaPlayer, SIGNAL(mutedChanged(bool)), this, SLOT(updateIsMutedDisplay()) );
        disconnect( m_mediaPlayer, SIGNAL(volumeChanged(int)), this, SLOT(updateVolumeLevelDisplay()) );
        m_mediaPlayer->deleteLater();
    }

    m_mediaPlayer = new QMediaPlayer( this, QMediaPlayer::VideoSurface );
    connect( m_mediaPlayer, SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(onPlayerStateDataChanged(QMediaPlayer::State)) );
    connect( m_mediaPlayer, SIGNAL(durationChanged(qint64)), this, SLOT(updateDurationDisplay()) );
    connect( m_mediaPlayer, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)), this, SLOT(onMediaStatusChanged()) );
    connect( m_mediaPlayer, SIGNAL(positionChanged(qint64)), this, SLOT(updateProgressDisplay()) );
    connect( m_mediaPlayer, SIGNAL(durationChanged(qint64)), this, SLOT(updateProgressAndDurationDisplay()) );
    connect( m_mediaPlayer, SIGNAL(playbackRateChanged(qreal)), this, SLOT(updatePlaybackRateDisplay()) );
    connect( m_mediaPlayer, SIGNAL(mutedChanged(bool)), this, SLOT(updateIsMutedDisplay()) );
    connect( m_mediaPlayer, SIGNAL(volumeChanged(int)), this, SLOT(updateVolumeLevelDisplay()) );

    if ( ! m_videoOutputVideoWidget.isNull() )
    {
        m_mediaPlayer->setVideoOutput( m_videoOutputVideoWidget.data() );
    }

    if ( ! m_videoOutputGraphicsVideoItem.isNull() )
    {
        m_mediaPlayer->setVideoOutput( m_videoOutputGraphicsVideoItem.data() );
    }

    if ( ! m_videoOutputAbstractVideoSurface.isNull() )
    {
        m_mediaPlayer->setVideoOutput( m_videoOutputAbstractVideoSurface.data() );
    }

    this->resetProgressEditorStepsCount();
    this->updatePlayerStateDisplay();
    this->updateProgressAndDurationDisplay();

    this->setIsMuted( isMuted );
    this->updateIsMutedDisplay();

    this->setVolumeLevel( volumeLevel );
    this->updateVolumeLevelDisplay();

    this->setPlaybackRate( playbackRate );
    this->updatePlaybackRateDisplay();

    this->updateControlsEnablence();
}

void MediaPlayerControl::updatePreviewProgressDisplay()
{
    int progressEditorLocalX = 0;
    int mediaPlayerControlLocalX = 0;

    const QRect grooveRect = this->progressEditorSubControlRect( QStyle::SC_SliderGroove );
    const int grooveLeft = grooveRect.left();
    const int progressEditorValueMin = m_progressEditor->minimum();
    const int progressEditorRange = m_progressEditor->maximum() - progressEditorValueMin;
    if ( m_isProgressEditorHovered || m_isProgressEditorPressed )
    {
        const QPoint cursorPosGlobal = QCursor::pos();
        progressEditorLocalX = m_progressEditor->mapFromGlobal( cursorPosGlobal ).x();
        mediaPlayerControlLocalX = this->mapFromGlobal( cursorPosGlobal ).x();
    }
    else
    {
        // slider handle's center x does not correspond to the slider value's x:
        // if the slider handle is in the beginning/end of the slider groove
        // then the value's x is in the beginning/end of the slider's handle
        progressEditorLocalX = qRound( ( qreal( m_progressEditor->value() - progressEditorValueMin )
                                         / qMax( progressEditorRange, 1 ) ) * grooveRect.width() )
                               + grooveLeft;
        mediaPlayerControlLocalX = m_progressEditor->mapTo( this, QPoint( 0, 0 ) ).x()
                                   + this->progressEditorSubControlRect( QStyle::SC_SliderHandle ).center().x();
    }

    const int grooveRight = grooveRect.right();
    if ( progressEditorLocalX < grooveLeft
         || progressEditorLocalX > grooveRight )
    {
        return;
    }

    const int grooveHDiff = grooveRight - grooveLeft;
    const qreal previewProgressRatio = grooveHDiff > 0
                                       ? qreal( progressEditorLocalX - grooveLeft ) / grooveHDiff
                                       : 0.0;

    const int progressEditorNewValue = qRound( progressEditorRange * previewProgressRatio ) + progressEditorValueMin;
    const qint64 mediaPlayerPosition = this->mediaPlayerPositionFromProgress( progressEditorNewValue );
    const qint64 displayPosition = m_progressMeasurementMode == ProgressMeasurementModeElapsed
                                   ? mediaPlayerPosition
                                   : m_mediaPlayer->duration() - mediaPlayerPosition;

    if ( m_previewProgressDisplay == nullptr )
    {
        m_previewProgressDisplay = new QLabel( this );
        m_previewProgressDisplay->setAlignment( Qt::AlignCenter );
        m_previewProgressDisplay->setObjectName( "previewProgressDisplay" );
        m_previewProgressDisplay->setVisible( true );
        m_previewProgressDisplay->setWindowOpacity( 0.0 );
    }

    m_previewProgressDisplay->setText( MediaPlayerControl::msecsToString( displayPosition ) );
    m_previewProgressDisplay->resize( m_previewProgressDisplay->minimumSizeHint() );
    m_previewProgressDisplay->move( QPoint( mediaPlayerControlLocalX,
                                            m_progressEditor->geometry().top() )
                                    - QPoint( m_previewProgressDisplay->width(),
                                              m_previewProgressDisplay->height() ) / 2 );

    this->setPreviewProgressDisplayVisibility( true );

    if ( m_previewProgressUpdateTimer == nullptr )
    {
        m_previewProgressUpdateTimer = new QTimer( this );
        m_previewProgressUpdateTimer->setSingleShot( true );
        m_previewProgressUpdateTimer->setInterval( 200 );
        connect( m_previewProgressUpdateTimer, SIGNAL(timeout()), this, SLOT(tryHidePreviewProgressDisplay()) );
    }

    m_previewProgressUpdateTimer->start();
}

QUrl MediaPlayerControl::mediaUrl() const
{
    return m_mediaPlayer->media().canonicalUrl();
}

bool MediaPlayerControl::isMediaValid() const
{
    const QMediaPlayer::MediaStatus mediaStatus = m_mediaPlayer->mediaStatus();
    return mediaStatus != QMediaPlayer::UnknownMediaStatus
           && mediaStatus != QMediaPlayer::NoMedia
           && mediaStatus != QMediaPlayer::InvalidMedia
           && m_mediaPlayer->error() == QMediaPlayer::NoError
           && m_mediaPlayer->duration() > Q_INT64_C( 0 )
           && ! m_mediaPlayer->media().isNull()
           && ! m_mediaPlayer->currentMedia().isNull();
}

QUrl MediaPlayerControl::urlFromMimeData( const QMimeData* const mimeData )
{
    if ( mimeData == nullptr )
    {
        return QUrl();
    }

    QUrl url;
    if ( mimeData->hasUrls() )
    {
        const QList< QUrl > urls = mimeData->urls();
        if ( ! urls.isEmpty() )
        {
            url = urls.first();
        }
    }

    if ( url.isEmpty() && mimeData->hasText() )
    {
        const QUrl urlCandidate( mimeData->text() );
        if ( urlCandidate.isValid() )
        {
            url = urlCandidate;
        }
    }

    if ( url.isEmpty() )
    {
        return url;
    }

    if ( url.isLocalFile() )
    {
        const QString localFile = url.toLocalFile();
        const QFileInfo localFileInfo( localFile );
        if ( ! s_allowedFileExtensions.contains( localFileInfo.suffix(), Qt::CaseInsensitive ) )
        {
            return QUrl();
        }
    }

    return url;
}
