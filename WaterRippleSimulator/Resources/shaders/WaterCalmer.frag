#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 */

// 0: in
in vec2 vfTexCoords;

// 1: out
layout (location = 0) out float height;
layout (location = 1) out float velocity;

void main()
{
    height = 0.0f;
    velocity = 0.0f;
}
