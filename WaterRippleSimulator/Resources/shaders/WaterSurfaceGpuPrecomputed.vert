#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 * 3: const
 */

// 0: in
layout (location = 0) in ivec2 vVertexTexCoord;

// 1: out
out vec3 vfVertexPos;
out vec2 vfVertexTexCoord;
out vec3 vfVertexNormal;
out vec3 vfEyePos;

// 2: uniform
uniform mat4 modelMatrix = mat4( 1.0f );
uniform mat3 normalMatrix = mat3( 1.0f );
uniform mat4 viewMatrix = mat4( 1.0f );
uniform mat4 projectionMatrix = mat4( 1.0f );

uniform sampler2D vertexPosSampler;
uniform sampler2D vertexNormalSampler;
uniform ivec2 meshVertexMaxIndices = ivec2( -1, -1 );

// 4: locals
uniform vec3 eyePos = vec3( 0.0, 0.0, 1.0 );

void main()
{
    vec3 vertexPos = texelFetch( vertexPosSampler, vVertexTexCoord, 0 ).rgb;
    vec3 vertexNormal = texelFetch( vertexNormalSampler, vVertexTexCoord, 0 ).rgb;

    vec4 vertexPos4 = modelMatrix * vec4( vertexPos, 1.0f );
    vfVertexPos = vec3( vertexPos4 );
    vfVertexTexCoord = vec2( vVertexTexCoord ) / vec2( meshVertexMaxIndices );
    vfVertexTexCoord.y = 1.0f - vfVertexTexCoord.y;
    vfVertexNormal = normalMatrix * vertexNormal;
    vfEyePos = vec3( viewMatrix * vec4( eyePos, 1.0f ) );

    gl_Position = projectionMatrix * viewMatrix * vertexPos4;
}
