#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 */

// 0: in
layout (location = 0) in vec3 vVertexPos;
layout (location = 1) in vec2 vVertexTexCoord;

// 1: out
out vec2 vfVertexTexCoord;

// 2: uniform
uniform mat4 modelMatrix = mat4( 1.0f );
uniform mat3 normalMatrix = mat3( 1.0f );
uniform mat4 viewMatrix = mat4( 1.0f );
uniform mat4 projectionMatrix = mat4( 1.0f );

void main()
{
    vec3 vertexPos = vec3( modelMatrix * vec4( vVertexPos, 1.0f ) );
    vfVertexTexCoord = vVertexTexCoord;
    gl_Position = projectionMatrix * viewMatrix * vec4( vertexPos, 1.0f );
}
