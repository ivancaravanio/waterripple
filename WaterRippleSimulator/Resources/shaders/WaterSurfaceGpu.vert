#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 * 3: const
 */

// 0: in
layout (location = 0) in ivec2 vVertexTexCoord;

// 1: out
out vec3 vfVertexPos;
out vec2 vfVertexTexCoord;
out vec3 vfVertexNormal;
out vec3 vfEyePos;

// 2: uniform
uniform mat4 modelMatrix = mat4( 1.0f );
uniform mat3 normalMatrix = mat3( 1.0f );
uniform mat4 viewMatrix = mat4( 1.0f );
uniform mat4 projectionMatrix = mat4( 1.0f );

uniform sampler2D heightSampler;
uniform ivec2 meshVertexMaxIndices = ivec2( -1, -1 );
uniform vec2 meshBottomLeft = vec2( -1.0, -1.0 );
uniform vec2 meshTopRight = vec2( 1.0, 1.0 );

// 4: locals
uniform vec3 eyePos = vec3( 0.0, 0.0, 1.0 );

void main()
{
    vfVertexTexCoord = vec2( vVertexTexCoord ) / vec2( meshVertexMaxIndices );

    float centerHeight = texelFetch( heightSampler, vVertexTexCoord, 0 ).r;
    vec3 vertexPos = vec3( vfVertexTexCoord * ( meshTopRight - meshBottomLeft ) + meshBottomLeft,
                           centerHeight );

    // float leftHeight   = texelFetch( heightSampler, ivec2( max( vVertexTexCoord.s - 1, 0 ),                         vVertexTexCoord.t + 0 ),                             0 ).r;
    // float rightHeight  = texelFetch( heightSampler, ivec2( min( vVertexTexCoord.s + 1, meshVertexMaxIndices.x ),    vVertexTexCoord.t + 0 ),                             0 ).r;
    // float topHeight    = texelFetch( heightSampler, ivec2(      vVertexTexCoord.s + 0,                           min( vVertexTexCoord.t + 1, meshVertexMaxIndices.y ) ), 0 ).r;
    // float bottomHeight = texelFetch( heightSampler, ivec2(      vVertexTexCoord.s + 0,                           max( vVertexTexCoord.t - 1, 0 ) ),                      0 ).r;
    float leftHeight   = vVertexTexCoord.s > 0
                         ? texelFetchOffset( heightSampler, vVertexTexCoord, 0, ivec2( -1,  0 ) ).r
                         : centerHeight;
    float rightHeight  = vVertexTexCoord.s < meshVertexMaxIndices.x
                         ? texelFetchOffset( heightSampler, vVertexTexCoord, 0, ivec2(  1,  0 ) ).r
                         : centerHeight;
    float topHeight    = vVertexTexCoord.t < meshVertexMaxIndices.y
                         ? texelFetchOffset( heightSampler, vVertexTexCoord, 0, ivec2(  0,  1 ) ).r
                         : centerHeight;
    float bottomHeight = vVertexTexCoord.t > 0
                         ? texelFetchOffset( heightSampler, vVertexTexCoord, 0, ivec2(  0, -1 ) ).r
                         : centerHeight;

    vec3 normalXDir = vec3( 1.0, 0.0, rightHeight - leftHeight );
    vec3 normalYDir = vec3( 0.0, 1.0, topHeight - bottomHeight );
    vec3 vertexNormal = normalize( cross( normalXDir, normalYDir ) );

    vec4 vertexPos4 = modelMatrix * vec4( vertexPos, 1.0f );
    vfVertexPos = vec3( vertexPos4 );
    vfVertexNormal = normalMatrix * vertexNormal;
    vfEyePos = vec3( viewMatrix * vec4( eyePos, 1.0f ) );
    vfVertexTexCoord.y = 1.0f - vfVertexTexCoord.y;

    gl_Position = projectionMatrix * viewMatrix * vertexPos4;
}
