#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 */

// 0: in
in vec2 vfTexCoords;

// 1: out
layout (location = 0) out float rippledHeight;

// 2: uniform
uniform sampler2D orgHeightTexSampler;
uniform ivec2 orgHeightTexMaxIndices = ivec2( 0, 0 );

const int circularTrigMethod = 0;
const int ellipticTrigMethod = 1;
const int ellipticGaussMethod = 2;

uniform int method = circularTrigMethod;
uniform float magnitude = 0.0f;
uniform vec2 radius = vec2( 0.0f );
uniform mat4 rotationMatrix = mat4( 1.0 );

uniform ivec2 epicenter = ivec2( 0, 0 );

const float eps = 1E-6f;
const vec2 xAxis = vec2( 1.0f, 0.0 );

float trigCirleRipple( in ivec2 vertexIndex )
{
    float distance = length( vec2( vertexIndex - epicenter ) );
    if ( distance > radius.x )
    {
        discard;
    }

    float distanceFactor = distance / radius;
    return magnitude * ( cos( distanceFactor * radians( 180.0 ) ) + 1.0f );
}

float trigEllipticRipple( in ivec2 vertexIndex )
{
    float xxRadius = radius.x * radius.x;
    float yyRadius = radius.y * radius.y;
    float xyRadius = radius.x * radius.y;

    vec2 centerDistanceVec = vec2( rotationMatrix * vec4( vertexIndex - epicenter, 0.0, 1.0 ) );
    // vec2 centerDistanceVec = vec2( vertexIndex - epicenter );
    float distance = length( centerDistanceVec );
    vec2 centerDistanceVecNorm = distance > 1.0f
                                 ? normalize( centerDistanceVec )
                                 : centerDistanceVec;

    float xRadiusFactor = abs( dot( centerDistanceVecNorm, xAxis ) );
    float xxRadiusFactor = xRadiusFactor * xRadiusFactor;

    float tanSquaredRadiusFactor = ( 1.0f - xxRadiusFactor )
                                   / ( xxRadiusFactor );
    float sgn = ( xRadiusFactor > 0.0f
                  || abs( xRadiusFactor ) < eps
                  ? 1.0f
                  : -1.0f );
    float ellipsePtX = sgn
                       * xyRadius
                       / sqrt( yyRadius + xxRadius * tanSquaredRadiusFactor );
    float ellipsePtY = sgn
                       * xyRadius
                       / sqrt( xxRadius + yyRadius / tanSquaredRadiusFactor );
    vec2 ellipsePt = vec2( ellipsePtX, ellipsePtY );

    float distanceRatio = distance /length( ellipsePt );
    if ( distanceRatio > 1.0f )
    {
        return 0.0;
    }

    return clamp( magnitude * ( cos( distanceRatio * radians( 180.0 ) ) + 1.0f ), min( 0.0f, magnitude ), max( 0.0, magnitude ) );
}

float gaussEllipticRipple( in ivec2 vertexIndex )
{
    return 0.0f;
}

void main()
{
    // don't validate ripple point's position
    // ripple's epicenter is allowed to be outside texture bounds

    if ( radius.x < eps
         || radius.y < eps
         || ( method != circularTrigMethod
              && method != ellipticTrigMethod
              && method != ellipticGaussMethod )
         || abs( magnitude ) < eps
         || orgHeightTexMaxIndices.x < 0
         || orgHeightTexMaxIndices.y < 0 )
    {
        discard;
    }

    ivec2 vertexIndex = ivec2( floor( gl_FragCoord.xy ) );
    if ( vertexIndex.x <= 0
         || vertexIndex.x >= orgHeightTexMaxIndices.x
         || vertexIndex.y <= 0
         || vertexIndex.y >= orgHeightTexMaxIndices.y )
    {
        discard;
    }

    float heightOffset = 0.0f;
    if ( method == circularTrigMethod )
    {
        heightOffset = trigCirleRipple( vertexIndex );
    }
    else if ( method == ellipticTrigMethod )
    {
        heightOffset = trigEllipticRipple( vertexIndex );
    }
    else if ( method == ellipticGaussMethod )
    {
        heightOffset = gaussEllipticRipple( vertexIndex );
    }

    rippledHeight = texture2D( orgHeightTexSampler, vfTexCoords ).r + heightOffset;
}
