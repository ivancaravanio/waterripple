#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 */

// 1: out
out vec2 vfTexCoords;

// 2: uniform

// 4: locals
// (x,y) only
const vec2[] pos = vec2[ 4 ]
(
    vec2( -1.0f, -1.0f ),
    vec2(  1.0f, -1.0f ),
    vec2(  1.0f,  1.0f ),
    vec2( -1.0f,  1.0f )
);

const vec2[] tex = vec2[ 4 ]
(
    vec2( 0.0f, 0.0f ),
    vec2( 1.0f, 0.0f ),
    vec2( 1.0f, 1.0f ),
    vec2( 0.0f, 1.0f )
);

void main()
{
    vfTexCoords = tex[ gl_VertexID ];
    gl_Position = vec4( pos[ gl_VertexID ], 0.0f, 1.0f );
}
