#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 */

// 0: in
in vec2 vfTexCoords;

// 1: out
layout (location = 0) out float hNew;
layout (location = 1) out float vNew;

// 2: uniform
uniform sampler2D oldHeightTexSampler;
uniform sampler2D oldVelocityTexSampler;
uniform float dt = 0.0f;
uniform float speed = 5.0f;
uniform float colWidth = 1.0f;
uniform float velocityScale = 0.995f; // velocityScale < 1, smaller time step -> stronger effect
uniform float colHeightMaxSlope = 0.9f;

// provide the mesh size through a uniform instead of calling textureSize
uniform ivec2 meshVertexMaxIndices = ivec2( -1, -1 );

// 3: locals
const float eps = 1E-7f;

void main()
{
    if ( dt < eps
         || speed < 0.0f
         || colWidth < 0.0f
         || velocityScale < 0.0f
         || colHeightMaxSlope < 0.0f
         || meshVertexMaxIndices.x < 0
         || meshVertexMaxIndices.y < 0 )
    {
        discard;
    }

    // should we calculate the new heights of the boundary vertices
    // http://www.roxlu.com/downloads/scholar/001.fluid.height_field_simulation.pdf:
    // Clamp at boundary e.g. def. u[-1,j] = u[0,j]:
    // boundary heights should not be calculated
    // they should only be taken into consideration when calculating the inner ones
    ivec2 vertexIndex = ivec2( floor( gl_FragCoord.xy ) );
    if ( vertexIndex.x <= 0
         || vertexIndex.x >= meshVertexMaxIndices.x
         || vertexIndex.y <= 0
         || vertexIndex.y >= meshVertexMaxIndices.y )
    {
        discard;
    }

    float maxSpeed = colWidth / dt;
    if ( speed > maxSpeed )
    {
        discard;
    }

    float hCenter = texture2D( oldHeightTexSampler, vfTexCoords ).r;
    float hTop    = vertexIndex.y < meshVertexMaxIndices.y
                    ? textureOffset( oldHeightTexSampler, vfTexCoords, ivec2( 0, 1 ) ).r
                    : hCenter;
    float hBottom = vertexIndex.y > 0
                    ? textureOffset( oldHeightTexSampler, vfTexCoords, ivec2( 0, -1 ) ).r
                    : hCenter;
    float hRight  = vertexIndex.x < meshVertexMaxIndices.x
                    ? textureOffset( oldHeightTexSampler, vfTexCoords, ivec2( 1, 0 ) ).r
                    : hCenter;
    float hLeft   = vertexIndex.x > 0
                    ? textureOffset( oldHeightTexSampler, vfTexCoords, ivec2( -1, 0 ) ).r
                    : hCenter;

    float maxOffset = colHeightMaxSlope * colWidth;
    float offset = hTop + hBottom + hRight + hLeft - 4.0f * hCenter;

    // if ( offset > maxOffset )
    // {
    //     offset -= maxOffset;
    // }
    // else if ( offset < -maxOffset )
    // {
    //     offset += maxOffset;
    // }

    // or
    offset = clamp( offset, - maxOffset, maxOffset );

    float force = speed * speed * offset / colWidth / colWidth;
    vNew = texture2D( oldVelocityTexSampler, vfTexCoords ).r + force * dt;
    hNew = hCenter + vNew * dt;
    vNew *= velocityScale;
}
