#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 */

// 0: in
in vec2 vfTexCoords;

// 1: out
layout (location = 0) out vec3 pos;
layout (location = 1) out vec3 normal;

// 2: uniform
uniform sampler2D heightTexSampler;
uniform ivec2     heightTexMaxIndices = ivec2( -1, -1 );
uniform vec2      meshRectBottomLeft = vec2( -1.0, -1.0 );
uniform vec2      meshRectTopRight = vec2( 1.0, 1.0 );

void main()
{
    if ( heightTexMaxIndices.x < 0
         || heightTexMaxIndices.y < 0 )
    {
        discard;
    }

    float hCenter = texture2D( heightTexSampler, vfTexCoords ).r;
    vec2 vertexIndexF = floor( gl_FragCoord.xy );
    ivec2 vertexIndex = ivec2( vertexIndexF );
    vec2 relativeCoords = vertexIndexF / vec2( heightTexMaxIndices );
    pos = vec3( meshRectBottomLeft + relativeCoords * ( meshRectTopRight - meshRectBottomLeft ), hCenter );
    float hTop    = vertexIndex.y < heightTexMaxIndices.y
                    ? textureOffset( heightTexSampler, vfTexCoords, ivec2( 0, 1 ) ).r
                    : hCenter;
    float hBottom = vertexIndex.y > 0
                    ? textureOffset( heightTexSampler, vfTexCoords, ivec2( 0, -1 ) ).r
                    : hCenter;
    float hRight  = vertexIndex.x < heightTexMaxIndices.x
                    ? textureOffset( heightTexSampler, vfTexCoords, ivec2( 1, 0 ) ).r
                    : hCenter;
    float hLeft   = vertexIndex.x > 0
                    ? textureOffset( heightTexSampler, vfTexCoords, ivec2( -1, 0 ) ).r
                    : hCenter;

    vec3 normalXDir = vec3( 1.0f, 0.0f, hRight - hLeft );
    vec3 normalYDir = vec3( 0.0f, 1.0f, hTop - hBottom );
    normal = normalize( cross( normalXDir, normalYDir ) );
}
