#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 */

// 0: in

// 1: out
layout (location = 0) out vec4 fColor;

// 2: uniform
uniform vec4 color;

void main()
{
    fColor = color;
}

