#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 */

// 0: in
layout (location = 0) in vec3 vVertexPos;

// 1: out

// 2: uniform
uniform mat4 matrix = mat4( 1.0f );

void main()
{
    gl_Position = matrix * vec4( vVertexPos, 1.0f );
}
