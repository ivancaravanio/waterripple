#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 */

// 0: in
in vec2 vfVertexTexCoord;

// 1: out
layout (location = 0) out vec4 fColor;

// 2: uniform
uniform sampler2D colorTextureSampler;

void main()
{
    fColor = texture2D( colorTextureSampler, vfVertexTexCoord );
}
