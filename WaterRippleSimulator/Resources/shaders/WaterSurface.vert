#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 */

// 0: in
layout (location = 0) in vec2  vVertex2DPos;
layout (location = 1) in vec2  vVertexTexCoord;
layout (location = 2) in float vVertexHeight;
layout (location = 3) in vec3  vVertexNormal;

// 1: out
out vec3 vfVertexPos;
out vec2 vfVertexTexCoord;
out vec3 vfVertexNormal;
out vec3 vfEyePos;

// 2: uniform
uniform mat4 modelMatrix = mat4( 1.0f );
uniform mat3 normalMatrix = mat3( 1.0f );
uniform mat4 viewMatrix = mat4( 1.0f );
uniform mat4 projectionMatrix = mat4( 1.0f );
uniform vec3 eyePos = vec3( 0.0, 0.0, 1.0 );

void main()
{
    vec4 vertexPos4 = modelMatrix * vec4( vVertex2DPos, vVertexHeight, 1.0f );
    vfVertexPos = vec3( vertexPos4 );
    vfVertexTexCoord = vVertexTexCoord;
    vfVertexNormal = normalMatrix * vVertexNormal;
    vfEyePos = vec3( viewMatrix * vec4( eyePos, 1.0f ) );

    gl_Position = projectionMatrix * viewMatrix * vertexPos4;
}
