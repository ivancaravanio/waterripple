#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 */

struct PointLight
{
    vec3 pos;
    vec3 color;
    float kc;
    float kl;
    float kq;
    float intensity;
};

struct Material
{
    vec4 color;
    float shininess;
    vec3 re;
    vec3 ra;
    vec3 rd;
    vec3 rs;
};

// 0: in
in vec3 vfVertexPos;
in vec2 vfVertexTexCoord;
in vec3 vfVertexNormal;
in vec3 vfEyePos;

// 1: out
layout (location = 0) out vec4 fColor;

// 2: uniform
uniform Material material;
uniform PointLight light;
uniform vec3 ambientColor;
uniform sampler2D colorTexSampler;
uniform bool obtainsColorFromTexture;

//3: locals
const float eps = 1E-6f;

void main()
{
    vec4 materialColor = obtainsColorFromTexture
                         ? texture2D( colorTexSampler, vfVertexTexCoord )
                         : material.color;
    vec4 emissiveColor = materialColor * vec4( material.re, 1.0f );
    vec3 ambientColor = material.ra * ambientColor; // depends on position: light(0), eye(0)

    vec3 lightVector = light.pos - vfVertexPos;
    float lightSurfaceDistance = length( lightVector );
    float attenuation = 1.0f / ( light.kc
                                 + light.kl * lightSurfaceDistance
                                 + light.kq * lightSurfaceDistance * lightSurfaceDistance );
    vec3 finalLightColor = light.intensity * attenuation * light.color;

    vec3 lightUnitVector = normalize( lightVector );
    vec3 normal = normalize( vfVertexNormal );
    float diffuseColorIntensityLevel = max( 0.0f, dot( lightUnitVector, normal ) );

    // Lambertian term:
    // http://en.wikipedia.org/wiki/Lambertian_reflectance
    vec3 diffuseColor = material.rd * diffuseColorIntensityLevel * finalLightColor; // depends on position: light(1), eye(0)

    // Phong term:
    vec3 specularColor = vec3( 0.0 ); // depends on position: light(1), eye(1)
    if ( abs( diffuseColorIntensityLevel ) >= eps )
    {
        vec3 eyeUnitVector                = normalize( vfEyePos - vfVertexPos );
        vec3 halfUnitVector               = normalize( lightUnitVector + eyeUnitVector );
        float specularColorIntensityLevel = max( 0.0f, dot( normal, halfUnitVector ) );
        float specularColorIntensityPower = pow( specularColorIntensityLevel, material.shininess );
        specularColor                     = material.rs * specularColorIntensityPower * finalLightColor;
    }

    fColor = vec4( clamp( emissiveColor.rgb
                          + ( ambientColor + diffuseColor ) * materialColor.rgb
                          + specularColor,
                          vec3( 0.0f ),
                          vec3( 1.0f ) ),
                   materialColor.a );
}
