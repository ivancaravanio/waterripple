#ifndef WATERRIPPLESIMULATOR_MAINWINDOW_H
#define WATERRIPPLESIMULATOR_MAINWINDOW_H

#include <QString>
#include <QUrl>
#include <QWidget>

namespace WaterRippleSimulator {

class MediaPlayerControl;
class VideoSurface;
class WaterRipplePresenter;
class WaterSettingsEditor;

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow( QWidget* parent = nullptr );
    ~MainWindow() Q_DECL_OVERRIDE;

    void setMediaUrl( const QUrl& aMediaUrl );
    void setMediaFilePath( const QString& aMediaFilePath );

    void retranslate();

private slots:
    void resetSceneBackgroundColor();
    void applyWaterSettings();

private:
    VideoSurface*         m_videoSurface;
    WaterRipplePresenter* m_waterDisplay;
    WaterSettingsEditor*  m_waterSettingsEditor;
    MediaPlayerControl*   m_mediaPlayerControl;
};

}

#endif // WATERRIPPLESIMULATOR_MAINWINDOW_H
