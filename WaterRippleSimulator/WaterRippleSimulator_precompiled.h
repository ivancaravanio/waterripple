#ifndef WATERRIPPLESIMULATOR_WATERRIPPLESIMULATOR_PRECOMPILED_H
#define WATERRIPPLESIMULATOR_WATERRIPPLESIMULATOR_PRECOMPILED_H

#include <qmath.h>

#ifdef __cplusplus
#include <glm/mat2x2.hpp>
#include <glm/mat2x3.hpp>
#include <glm/mat3x2.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/matrix.hpp>
#include <glm/vec3.hpp>

#include <QAbstractVideoSurface>
#include <QApplication>
#include <QButtonGroup>
#include <QByteArray>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDebug>
#include <QDesktopWidget>
#include <QDir>
#include <QDoubleSpinBox>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QFormLayout>
#include <QFrame>
#include <QGridLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QImage>
#include <QLabel>
#include <QLineEdit>
#include <QList>
#include <QMap>
#include <QMatrix3x3>
#include <QMatrix4x4>
#include <QOpenGLContext>
#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLFunctions>
#include <QOpenGLPaintDevice>
#include <QOpenGLWidget>
#include <QPainter>
#include <QPoint>
#include <QPushButton>
#include <QRect>
#include <QScopedPointer>
#include <QScreen>
#include <QSlider>
#include <QSpinBox>
#include <QSplitter>
#include <QString>
#include <QStringBuilder>
#include <QStringList>
#include <QStyleOptionSlider>
#include <QtGlobal>
#include <QTimer>
#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#include <QWindow>

#if QT_VERSION >= QT_VERSION_CHECK( 5, 0, 0 )
    #include <QStandardPaths>
#else
    #include <QDesktopServices>
#endif

#ifndef QT_NO_DRAGANDDROP
    #include <QDragEnterEvent>
    #include <QDropEvent>
    #include <QMimeData>
#endif

#include <algorithm>
#include <cmath>
#include <limits>
#endif

#endif // WATERRIPPLESIMULATOR_WATERRIPPLESIMULATOR_PRECOMPILED_H
