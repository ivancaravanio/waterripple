#include "ShaderProgramActivator.h"

#include "../Utilities/GlUtilities.h"
#include "../WaterRippleSimulator_namespace.h"
#include "OpenGlErrorLogger.h"

#include <qopenglext.h>
#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Helpers;

using namespace WaterRippleSimulator::Utilities;

ShaderProgramActivator::ShaderProgramActivator()
    : m_openGlFunctions{ nullptr }
    , m_shaderProgramId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_previousShaderProgramId{ CommonEntities::s_invalidUnsignedGlValue }
{
}

ShaderProgramActivator::ShaderProgramActivator(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        const QSurfaceFormat& aOpenGlFormat,
        const GLuint aShaderProgramId )
    : m_openGlFunctions( aOpenGlFunctions )
    , m_openGlFormat( aOpenGlFormat )
    , m_shaderProgramId( aShaderProgramId )
    , m_previousShaderProgramId( CommonEntities::s_invalidUnsignedGlValue )
{
}

ShaderProgramActivator::~ShaderProgramActivator()
{
}

bool ShaderProgramActivator::isValid() const
{
    return m_openGlFunctions != nullptr
           && CommonEntities::isUnsignedGlValueValid( m_shaderProgramId );
}

GLuint ShaderProgramActivator::activeShaderProgramId() const
{
    return GlUtilities::activeShaderProgramId( m_openGlFunctions, m_openGlFormat );
}

void ShaderProgramActivator::activate()
{
    if ( ! this->isValid() )
    {
        return;
    }

    const GLuint activeShaderProgramId = this->activeShaderProgramId();
    if ( activeShaderProgramId != m_shaderProgramId )
    {
        m_previousShaderProgramId = activeShaderProgramId;
        m_openGlFunctions->glUseProgram( m_shaderProgramId );
        LOG_OPENGL_ERROR();
    }
}

void ShaderProgramActivator::deactivate()
{
    if ( ! CommonEntities::isUnsignedGlValueValid( m_previousShaderProgramId ) )
    {
        return;
    }

    m_openGlFunctions->glUseProgram( m_previousShaderProgramId );
    LOG_OPENGL_ERROR();

    m_previousShaderProgramId = CommonEntities::s_invalidUnsignedGlValue;
}
