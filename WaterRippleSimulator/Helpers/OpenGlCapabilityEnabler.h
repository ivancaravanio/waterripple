#ifndef WATERRIPPLESIMULATOR_HELPERS_OPENGLCAPABILITYENABLER_H
#define WATERRIPPLESIMULATOR_HELPERS_OPENGLCAPABILITYENABLER_H

#include <qopengl.h>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

class OpenGlCapabilityEnabler
{
public:
    OpenGlCapabilityEnabler();
    OpenGlCapabilityEnabler(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
            const GLenum aCapability,
            const bool aShouldEnable,
            const bool aAutoRollback = true );
    ~OpenGlCapabilityEnabler();

    bool isValid() const;

    GLenum capability() const;
    bool shouldEnable() const;
    bool isEnabled() const;

    void change();
    void rollback();

private:
    void change( const bool shouldEnable );

private:
    QOpenGLFunctions_3_3_Core* m_openGlFunctions;
    GLenum m_capability;
    bool   m_shouldEnable;
    bool   m_wasChanged;
    bool   m_autoRollback;
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_OPENGLCAPABILITYENABLER_H
