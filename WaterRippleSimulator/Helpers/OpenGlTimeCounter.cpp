#include "OpenGlTimeCounter.h"

#include "../WaterRippleSimulator_namespace.h"
#include "OpenGlErrorLogger.h"

#include <QDebug>
#include <QOpenGLFunctions_3_3_Core>
#include <qopenglext.h>
#include <QString>

using namespace WaterRippleSimulator::Helpers;

OpenGlTimeCounter::OpenGlTimeCounter(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions )
    : m_openGlFunctions( aOpenGlFunctions )
    , m_timeNanosecs( Q_UINT64_C( 0 ) )
    , m_isRunning( false )
{
    // do not activate + deactivate shader program,
    // since GPU calls time consumption is measured during the time it is active because of other calls
}

OpenGlTimeCounter::~OpenGlTimeCounter()
{
    if ( m_openGlFunctions == nullptr )
    {
        return;
    }

    const int timerQueriesCount = m_timerQueries.size();
    if ( timerQueriesCount > 0 )
    {
        m_openGlFunctions->glDeleteQueries( timerQueriesCount, m_timerQueries.constData() );
        m_timerQueries.clear();
    }
}

void OpenGlTimeCounter::start()
{
    if ( m_openGlFunctions == nullptr )
    {
        return;
    }

    if ( m_timerQueries.isEmpty() )
    {
        m_timerQueries.fill( CommonEntities::s_invalidUnsignedGlValue, TimerQueriesCount );
        m_openGlFunctions->glGenQueries( m_timerQueries.size(), m_timerQueries.data() );
        LOG_OPENGL_ERROR();
    }

    m_isRunning = true;
    m_timeNanosecs = Q_UINT64_C( 0 );

    const GLint startTimerQuery = m_timerQueries[ TimerQueryIndexStart ];
    m_openGlFunctions->glQueryCounter( startTimerQuery, GL_TIMESTAMP );
    LOG_OPENGL_ERROR();
}

void OpenGlTimeCounter::stop()
{
    if ( ! m_isRunning
         || m_openGlFunctions == nullptr )
    {
        return;
    }

    const GLint endTimerQuery = m_timerQueries[ TimerQueryIndexEnd ];
    m_openGlFunctions->glQueryCounter( endTimerQuery, GL_TIMESTAMP );
    LOG_OPENGL_ERROR();

    GLint isEndTimeAvailable = GL_FALSE;
    do {
        m_openGlFunctions->glGetQueryObjectiv( endTimerQuery, GL_QUERY_RESULT_AVAILABLE, &isEndTimeAvailable );
        LOG_OPENGL_ERROR();
    } while ( GL_FALSE == isEndTimeAvailable );

    const GLint startTimerQuery = m_timerQueries[ TimerQueryIndexStart ];

    GLuint64 startTime = Q_UINT64_C( 0 );
    m_openGlFunctions->glGetQueryObjectui64v( startTimerQuery, GL_QUERY_RESULT, &startTime );
    LOG_OPENGL_ERROR();

    GLuint64 endTime = Q_UINT64_C( 0 );
    m_openGlFunctions->glGetQueryObjectui64v( endTimerQuery, GL_QUERY_RESULT, &endTime );
    LOG_OPENGL_ERROR();

    m_isRunning = false;
    m_timeNanosecs = endTime - startTime;
}

bool OpenGlTimeCounter::isRunning() const
{
    return m_isRunning;
}

GLuint64 OpenGlTimeCounter::timeNanosecs() const
{
    return m_timeNanosecs;
}

qreal OpenGlTimeCounter::timeMillisecs() const
{
    return qreal( m_timeNanosecs ) / 1E6;
}

void OpenGlTimeCounter::dumpTime() const
{
    qDebug() << QString( "time (ms): %1" ).arg( this->timeMillisecs(), 0, 'f', 2 );
}
