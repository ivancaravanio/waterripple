#ifndef WATERRIPPLESIMULATOR_HELPERS_ABSTRACTWATERSURFACE_H
#define WATERRIPPLESIMULATOR_HELPERS_ABSTRACTWATERSURFACE_H

#include "../Data/Light.h"
#include "../Data/Material.h"
#include "../Data/Triangle3.h"
#include "../Data/WaterSettings.h"
#include "../WaterRippleSimulator_namespace.h"
#include "AbstractOpenGlClient.h"
#include "OpenGlObjectBinder.h"

#include <QHash>
#include <QList>
#include <QMatrix3x3>
#include <QMatrix4x4>
#include <QObject>
#include <QPair>
#include <QRectF>
#include <QSize>
#include <QTime>
#include <QtGlobal>
#include <QVector>
#include <QVector3D>
#include <QVideoFrame>

#include <gl/GL.h>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

using namespace WaterRippleSimulator::Data;

class AbstractWaterSurface : public QObject, public AbstractOpenGlClient
{
    Q_OBJECT

public:
    explicit AbstractWaterSurface(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
            QObject* const parent = nullptr );
    ~AbstractWaterSurface() Q_DECL_OVERRIDE;

    virtual void setMeshSize( const QSize& aMeshSize );
    const QSize& meshSize() const;
    QSize verticesCount() const;

    void setShouldDrawAsWireframe( const bool aShouldDrawAsWireframe );
    bool shouldDrawAsWireframe() const;

    virtual void init() Q_DECL_OVERRIDE;
    virtual void draw( const bool shouldPropagateWater ) = 0;

    void setMatrix( const CommonEntities::MatrixType matrixType, const QMatrix4x4& matrix );
    QMatrix4x4 matrix( const CommonEntities::MatrixType matrixType ) const;
    QMatrix3x3 normalMatrix() const;

    void setLight( const Light& aLight );
    const Light& light() const;

    void setMaterial( const Material& aMaterial );
    const Material& material() const;

    void obtainColorFromTexture( const bool aObtainsColorFromTexture );
    bool obtainsColorFromTexture() const;

    static QList< Triangle3 > triangulatedRect( const QRectF& rect );

    QRectF drawableAreaRect() const;
    QList< Triangle3 > drawableAreaRectTriangulated() const;

    QRectF meshRect() const;
    QList< Triangle3 > meshRectTriangulated() const;

    // wave propagation
    virtual void setWavePropagationSpeed( const float aWavePropagationSpeed ) = 0;
    virtual float wavePropagationSpeed() const = 0;

    virtual void setWaveHeightDampingOverTimeFactor( const float aWaveHeightDampingOverTimeFactor ) = 0;
    virtual float waveHeightDampingOverTimeFactor() const = 0;

    virtual void setWaveHeightClampingFactor( const float aWaveHeightClampingFactor ) = 0;
    virtual float waveHeightClampingFactor() const = 0;

    // water ripple
    virtual void setWaterRippleMethod( const WaterSettings::WaterRippleMethod aWaterRippleMethod ) = 0;
    virtual WaterSettings::WaterRippleMethod waterRippleMethod() const = 0;

    virtual void setWaterRippleMagnitude( const float aWaterRippleMagnitude ) = 0;
    virtual float waterRippleMagnitude() const = 0;

    virtual void setWaterRippleRadiusFactor( const QVector2D& aWaterRippleRadiusFactor ) = 0;
    virtual QVector2D waterRippleRadiusFactor() const = 0;
    QVector2D waterRippleRadius() const;

    virtual void setWaterRippleRotationAngle( const float aWaterRippleRotationAngle ) = 0;
    virtual float waterRippleRotationAngle() const = 0;

signals:
    void updateRequested();

public slots:
    void rippleWaterAtRandomPoint();
    void rippleWaterAt( const QVector3D& pt );
    virtual void rippleWaterAt( const int x, const int y ) = 0;
    virtual void propagateWater() = 0;
    void displayVideoFrame( const QVideoFrame& videoFrame );
    virtual void calmWater() = 0;

protected:
    virtual QList< GLuint > allGenericVertexAttributeIndices() const;
    void enableGenericVertexAttributeArrays( const bool enable );

    virtual void applyMeshSize() = 0;
    virtual void applyShouldDrawAsWireframe();
    virtual void applyMatrix( const CommonEntities::MatrixType matrixType ) = 0;
    virtual void applyLight() = 0;
    virtual void applyMaterial() = 0;
    virtual void applyObtainsColorFromTexture() = 0;

    OpenGlObjectBinder videoFrameTextureBinder( const bool autoUnbind = true ) const;

private:
    enum MatrixIndex
    {
        MatrixIndexInvalid    = -1,
        MatrixIndexModel      = 0,
        // normal matrix intentionally excluded
        MatrixIndexView       = 1,
        MatrixIndexProjection = 2,
        MatricesCount         = 3
    };

private:
    bool setMeshSizeInternal( const QSize& aMeshSize );
    bool setShouldDrawAsWireframeInternal( const bool aShouldDrawAsWireframe );
    bool setMatrixInternal(
            const CommonEntities::MatrixType matrixType,
            const QMatrix4x4& matrix );
    bool setLightInternal( const Light& aLight );
    bool setMaterialInternal( const Material& aMaterial );
    bool obtainColorFromTextureInternal( const bool aObtainsColorFromTexture );

    static int matrixIndexFromType( const CommonEntities::MatrixType matrixType );
    static int isMatrixIndexValid( const int matrixType );

private:
    QSize m_meshSize;
    bool m_shouldDrawAsWireframe;
    QVector< QMatrix4x4 > m_matrices;
    Light m_light;
    Material m_material;
    bool m_obtainsColorFromTexture;
    QSize m_videoFrameSize;
    GLuint m_videoFrameTextureId;
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_ABSTRACTWATERSURFACE_H
