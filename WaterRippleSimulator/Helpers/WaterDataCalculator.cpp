#include "WaterDataCalculator.h"

#include "../Data/ShaderInfo.h"
#include "../Data/WaterSettings.h"
#include "../Utilities/GeneralUtilities.h"
#include "../Utilities/GlUtilities.h"
#include "../Utilities/ImageUtilities.h"
#include "../Utilities/MathUtilities.h"
#include "../WaterRippleSimulator_namespace.h"
#include "OpenGlCapabilityEnabler.h"
#include "OpenGlErrorLogger.h"

#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Helpers;

using namespace WaterRippleSimulator::Utilities;
using namespace WaterRippleSimulator;

WaterDataCalculator::WaterDataCalculator(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        const QSurfaceFormat& aOpenGlFormat,
        const QSize& aMeshSize,
        QObject* const parent )
    : QObject{ parent }
    , AbstractOpenGlClient{ aOpenGlFunctions }
    , m_shaderProgram{ aOpenGlFunctions, aOpenGlFormat }
    , m_waterCalmerProgram{ aOpenGlFunctions, aOpenGlFormat }
    , m_textures( TexturesCount, CommonEntities::s_invalidUnsignedGlValue )
    , m_fboId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_vaoId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_useH1V1AsDrawBuffers{ true }
    , m_isPropagatingWater{ false }
    , m_isWaterCalmScheduled{ false }
    , m_wavePropagationSpeed{ WaterSettings::s_wavePropagationSpeedDefault }
    , m_waveHeightDampingOverTimeFactor{ WaterSettings::s_waveHeightClampingFactorDefault }
    , m_waveHeightClampingFactor{ WaterSettings::s_waveHeightClampingFactorDefault }
    , m_waterRippler{
          aOpenGlFunctions,
          aOpenGlFormat,
          WaterSettings::s_waterRippleMethodDefault,
          WaterSettings::s_waterRippleMagnitudeDefault,
          WaterSettings::s_waterRipple2DRadiusFactorDefault,
          WaterSettings::s_waterRippleRotationAngleDefault }
{
    this->setMeshSizeInternal( aMeshSize, false );
    this->updateOpenGlData();
}

WaterDataCalculator::~WaterDataCalculator()
{
    this->disposeOpenGlResources();
}

void WaterDataCalculator::setMeshSize( const QSize& aMeshSize )
{
    if ( this->setMeshSizeInternal( aMeshSize, true ) )
    {
        emit updateRequested();
    }
}

QSize WaterDataCalculator::meshSize() const
{
    return m_meshSize;
}

QSize WaterDataCalculator::verticesCount() const
{
    return MathUtilities::squaresToVerticesCount( m_meshSize );
}

void WaterDataCalculator::setWaterRippleMagnitude( const float aWaterRippleMagnitude )
{
    m_waterRipplerCpu.setMagnitude( aWaterRippleMagnitude );
    m_waterRippler.setMagnitude( aWaterRippleMagnitude );
}

float WaterDataCalculator::waterRippleMagnitude() const
{
    return m_waterRippler.magnitude();
}

void WaterDataCalculator::setWaterRippleRotationAngle( const float aWaterRippleRotationAngle )
{
    m_waterRipplerCpu.setRotationAngle( aWaterRippleRotationAngle );
    m_waterRippler.setRotationAngle( aWaterRippleRotationAngle );
}

float WaterDataCalculator::waterRippleRotationAngle() const
{
    return m_waterRippler.rotationAngle();
}

void WaterDataCalculator::setWaterRippleRadiusFactor( const QVector2D& aWaterRippleRadiusFactor )
{
    m_waterRipplerCpu.setRadiusFactor( aWaterRippleRadiusFactor );
    m_waterRippler.setRadiusFactor( aWaterRippleRadiusFactor );
}

QVector2D WaterDataCalculator::waterRippleRadiusFactor() const
{
    return m_waterRippler.radiusFactor();
}

void WaterDataCalculator::setWavePropagationSpeed( const float aWavePropagationSpeed )
{
    if ( ! WaterSettings::isWavePropagationSpeedValid( aWavePropagationSpeed )
         || qFuzzyCompare( aWavePropagationSpeed, m_wavePropagationSpeed ) )
    {
        return;
    }

    m_wavePropagationSpeed = aWavePropagationSpeed;
    this->applyWavePropagationSpeed();
}

float WaterDataCalculator::wavePropagationSpeed() const
{
    return m_wavePropagationSpeed;
}

void WaterDataCalculator::setWaveHeightDampingOverTimeFactor( const float aWaveHeightDampingOverTimeFactor )
{
    if ( ! WaterSettings::isWaveHeightDampingOverTimeFactorValid( aWaveHeightDampingOverTimeFactor )
         || qFuzzyCompare( aWaveHeightDampingOverTimeFactor, m_waveHeightDampingOverTimeFactor ) )
    {
        return;
    }

    m_waveHeightDampingOverTimeFactor = aWaveHeightDampingOverTimeFactor;
    this->applyWaveHeightDampingOverTimeFactor();
}

float WaterDataCalculator::waveHeightDampingOverTimeFactor() const
{
    return m_waveHeightDampingOverTimeFactor;
}

void WaterDataCalculator::setWaveHeightClampingFactor( const float aWaveHeightClampingFactor )
{
    if ( ! WaterSettings::isWaveHeightClampingFactorValid( aWaveHeightClampingFactor )
         || qFuzzyCompare( aWaveHeightClampingFactor, m_waveHeightClampingFactor ) )
    {
        return;
    }

    m_waveHeightClampingFactor = aWaveHeightClampingFactor;
    this->applyWaveHeightClampingFactor();
}

float WaterDataCalculator::waveHeightClampingFactor() const
{
    return m_waveHeightClampingFactor;
}

void WaterDataCalculator::setWaterRippleMethod( const WaterSettings::WaterRippleMethod aWaterRippleMethod )
{
    m_waterRipplerCpu.setMethod( aWaterRippleMethod );
    m_waterRippler.setMethod( aWaterRippleMethod );
}

WaterSettings::WaterRippleMethod WaterDataCalculator::waterRippleMethod() const
{
    return m_waterRippler.method();
}

bool WaterDataCalculator::setMeshSizeInternal( const QSize& aMeshSize, const bool shouldUpdateOpenGlData )
{
    if ( aMeshSize.isEmpty()
         || aMeshSize == m_meshSize )
    {
        return false;
    }

    m_meshSize = aMeshSize;
    m_waterRipplerCpu.setMeshSize( aMeshSize );
    if ( shouldUpdateOpenGlData )
    {
        this->updateMeshOpenGlData();
    }

    return true;
}

void WaterDataCalculator::updateMeshOpenGlData()
{
    if ( m_openGlFunctions == nullptr
         || m_meshSize.isEmpty()
         || ! m_shaderProgram.isValid() )
    {
        return;
    }

    const QSize verticesCount = this->verticesCount();
    const int xVerticesCount = verticesCount.width();
    const int yVerticesCount = verticesCount.height();
    const QVector< GLfloat > textureData( xVerticesCount * yVerticesCount, 0.0f );
    for ( int i = 0; i < TexturesCount; ++ i )
    {
        const GLuint textureId = m_textures[ i ];
        if ( ! CommonEntities::isUnsignedGlValueValid( textureId ) )
        {
            continue;
        }

        OpenGlObjectBinder textureBinder = this->textureBinder( textureId );
        textureBinder.bind();

        GlUtilities::setTexture( m_openGlFunctions,
                                 textureData.constData(),
                                 xVerticesCount,
                                 yVerticesCount,
                                 GL_R32F,
                                 GL_RED,
                                 GL_FLOAT,
                                 GlUtilities::TextureFilter::Nearest,
                                 GlUtilities::TextureFilter::Nearest,
                                 GlUtilities::TextureWrapMode::ClampToEdge,
                                 GlUtilities::TextureWrapMode::ClampToEdge,
                                 false );
    }

    m_shaderProgram.setUniformVariable2i(
                this->uniformVariableCachedLocation( UniformVariableIndexMeshVertexMaxIndices ),
                m_meshSize.width(),
                m_meshSize.height() );
}

void WaterDataCalculator::disposeOpenGlResources()
{
    if ( CommonEntities::isUnsignedGlValueValid( m_fboId ) )
    {
        m_openGlFunctions->glDeleteFramebuffers( 1, & m_fboId );
        LOG_OPENGL_ERROR();

        m_fboId = CommonEntities::s_invalidUnsignedGlValue;
    }

    QVector< GLuint > texturesToRelease;
    for ( int i = 0; i < TexturesCount; ++i )
    {
        GLuint& texture = m_textures[ i ];
        if ( CommonEntities::isUnsignedGlValueValid( texture ) )
        {
            texturesToRelease.append( texture );
            texture = CommonEntities::s_invalidUnsignedGlValue;
        }
    }

    const int texturesToReleaseCount = texturesToRelease.size();
    if ( texturesToReleaseCount > 0 )
    {
        m_openGlFunctions->glDeleteTextures( texturesToReleaseCount, texturesToRelease.constData() );
        LOG_OPENGL_ERROR();
    }
}

bool WaterDataCalculator::isWaterRippleScheduled() const
{
    return ! m_scheduledWaterRipplePoints.isEmpty();
}

GLuint WaterDataCalculator::oldHeightTextureId() const
{
    return m_textures[ m_useH1V1AsDrawBuffers
                       ? TextureIndexHeight0
                       : TextureIndexHeight1 ];
}

GLuint WaterDataCalculator::newHeightTextureId() const
{
    return m_textures[ m_useH1V1AsDrawBuffers
                       ? TextureIndexHeight1
                       : TextureIndexHeight0 ];
}

GLuint WaterDataCalculator::oldVelocityTextureId() const
{
    return m_textures[ m_useH1V1AsDrawBuffers
                       ? TextureIndexVelocity0
                       : TextureIndexVelocity1 ];
}

GLuint WaterDataCalculator::newVelocityTextureId() const
{
    return m_textures[ m_useH1V1AsDrawBuffers
                       ? TextureIndexVelocity1
                       : TextureIndexVelocity0 ];
}

FramebufferBinder WaterDataCalculator::fboBinder( const bool autoUnbind ) const
{
    return AbstractOpenGlClient::fboBinder(
                m_fboId,
                FramebufferBinder::BindingTarget::Both,
                autoUnbind );
}

ViewportAlternator WaterDataCalculator::viewportAlternator( const bool autoRollback ) const
{
    return AbstractOpenGlClient::viewportAlternator(
                QRect{ QPoint{ 0, 0 }, this->verticesCount() },
                autoRollback );
}

OpenGlObjectBinder WaterDataCalculator::vaoBinder( const bool autoUnbind ) const
{
    return AbstractOpenGlClient::vaoBinder( m_vaoId, autoUnbind );
}

OpenGlObjectBinder WaterDataCalculator::textureUnitBinder(
        const GLenum textureUnit,
        const bool autoUnbind ) const
{
    return AbstractOpenGlClient::textureUnitBinder( textureUnit, autoUnbind );
}

OpenGlObjectBinder WaterDataCalculator::textureBinder(
        const GLuint textureId,
        const bool autoUnbind ) const
{
    return AbstractOpenGlClient::textureBinder( textureId, autoUnbind );
}

void WaterDataCalculator::applyWavePropagationSpeed()
{
    m_shaderProgram.setUniformVariable1f(
                this->uniformVariableCachedLocation( UniformVariableIndexSpeed ),
                m_wavePropagationSpeed );
}

void WaterDataCalculator::applyWaveHeightDampingOverTimeFactor()
{
    m_shaderProgram.setUniformVariable1f(
                this->uniformVariableCachedLocation( UniformVariableIndexVelocityScale ),
                m_waveHeightDampingOverTimeFactor );
}

void WaterDataCalculator::applyWaveHeightClampingFactor()
{
    m_shaderProgram.setUniformVariable1f(
                this->uniformVariableCachedLocation( UniformVariableIndexColumnHeightMaxSlope ),
                m_waveHeightClampingFactor );
}

void WaterDataCalculator::updateOpenGlData()
{
    this->updateMeshOpenGlData();
    this->applyWavePropagationSpeed();
    this->applyWaveHeightDampingOverTimeFactor();
    this->applyWaveHeightClampingFactor();
}

void WaterDataCalculator::init()
{
    if ( ! m_uniformVariableCachedLocations.isEmpty() )
    {
        return;
    }

    m_waterRippler.init();

    m_openGlFunctions->glGenTextures( m_textures.size(), m_textures.data() );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glGenVertexArrays( 1, & m_vaoId );
    LOG_OPENGL_ERROR();

    bool isBuildOk =
            GlUtilities::buildShaderProgram(
                m_shaderProgram,
                ":/Resources/shaders/WaterDataCalculator.vert",
                ":/Resources/shaders/WaterVertexHeightCalculator.frag" );
    if ( ! isBuildOk )
    {
        this->disposeOpenGlResources();
        return;
    }

    isBuildOk =
            GlUtilities::buildShaderProgram(
                m_waterCalmerProgram,
                ":/Resources/shaders/WaterDataCalculator.vert",
                ":/Resources/shaders/WaterCalmer.frag" );
    if ( ! isBuildOk )
    {
        qDebug().nospace().noquote() << QT_STRINGIFY2( WaterDataCalculator ) << ": water calmer shader program not built successfully.";
    }

    this->cacheUniformVariableLocations();

    m_shaderProgram.activate();

    this->updateOpenGlData();

    m_openGlFunctions->glUniform1i( this->uniformVariableCachedLocation( UniformVariableIndexOldHeightTexSampler ), 0 );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1i( this->uniformVariableCachedLocation( UniformVariableIndexOldVelocityTexSampler ), 1 );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();

    m_openGlFunctions->glGenFramebuffers( 1, & m_fboId );
    LOG_OPENGL_ERROR();

    FramebufferBinder fboBinder = this->fboBinder( true );
    fboBinder.bind();

    const GLint mipmapLevel = 0;

    m_openGlFunctions->glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_textures[ TextureIndexHeight0 ], mipmapLevel );
    LOG_OPENGL_ERROR();
    m_openGlFunctions->glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_textures[ TextureIndexVelocity0 ], mipmapLevel );
    LOG_OPENGL_ERROR();
    m_openGlFunctions->glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, m_textures[ TextureIndexHeight1 ], mipmapLevel );
    LOG_OPENGL_ERROR();
    m_openGlFunctions->glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, m_textures[ TextureIndexVelocity1 ], mipmapLevel );
    LOG_OPENGL_ERROR();

    const GLenum fboStatus = m_openGlFunctions->glCheckFramebufferStatus( GL_FRAMEBUFFER );
    LOG_OPENGL_ERROR();
    if ( fboStatus != GL_FRAMEBUFFER_COMPLETE )
    {
        this->disposeOpenGlResources();
        return;
    }

    fboBinder.unbind();
}

void WaterDataCalculator::stopWaterPropagation()
{
    if ( ! m_isPropagatingWater )
    {
        return;
    }

    m_isPropagatingWater = false;
    m_waterPropagationDeltaTime = QTime();
}

bool WaterDataCalculator::isPropagatingWater() const
{
    return m_isPropagatingWater;
}

void WaterDataCalculator::rippleWaterAtRandomPoint()
{
    if ( m_meshSize.isEmpty() )
    {
        return;
    }

    const int x = MathUtilities::randomNumber( m_meshSize.width() );
    const int y = MathUtilities::randomNumber( m_meshSize.height() );
    this->rippleWaterAt( x, y );
}

void WaterDataCalculator::rippleWaterAt( const int x, const int y )
{
    // disallow rippling at peripheral vertices
    if ( ! m_isPropagatingWater
         || x <= 0
         || x >= m_meshSize.width()
         || y <= 0
         || y >= m_meshSize.height()
         || this->isWaterRippleScheduled() )
    {
        return;
    }

    m_scheduledWaterRipplePoints.append( QPoint{ x, y } );
}

void WaterDataCalculator::calmWater()
{
    m_isWaterCalmScheduled = true;
}

GLuint WaterDataCalculator::heightTextureId() const
{
    return this->newHeightTextureId();
}

void WaterDataCalculator::propagateWater()
{
    if ( m_openGlFunctions == nullptr
         || m_meshSize.isEmpty()
         || ! m_shaderProgram.isValid()
         || ! CommonEntities::isUnsignedGlValueValid( m_fboId )
         || ! CommonEntities::isUnsignedGlValueValid( m_vaoId ) )
    {
        return;
    }

#ifdef USE_GPU_ACCELERATED_RIPPLE
    GLuint actualOldHeightTextureId = CommonEntities::s_invalidUnsignedGlValue;
#else
    const GLuint actualOldHeightTextureId = this->oldVelocityTextureId();
#endif

#ifdef USE_GPU_ACCELERATED_RIPPLE
    const GLuint oldHeightTextureId = this->oldHeightTextureId();
    if ( this->isWaterRippleScheduled() )
    {
        m_waterRippler.rippleWaterAt(
                    oldHeightTextureId,
                    true, // option #2: false,
                    m_scheduledWaterRipplePoints.takeFirst() );
        // option #2: actualOldHeightTextureId = m_waterRippler.rippledHeightTextureId();
        actualOldHeightTextureId = oldHeightTextureId;
    }
    else
    {
        actualOldHeightTextureId = oldHeightTextureId;
    }
#endif

    FramebufferBinder fboActivator = this->fboBinder( false );
    fboActivator.bind();

    ViewportAlternator vpAlternator = this->viewportAlternator( false );
    vpAlternator.change();

    OpenGlCapabilityEnabler cullFaceEnabler{
                m_openGlFunctions,
                GL_CULL_FACE,
                true,
                false };
    cullFaceEnabler.change();

    OpenGlObjectBinder cullFaceModeAlternator{
                m_openGlFunctions,
                OpenGlObjectBinder::ObjectType::CullFaceMode,
                GL_BACK,
                CommonEntities::invalidEnumGlValue(),
                false };
    cullFaceModeAlternator.bind();

    OpenGlObjectBinder vaoBinder = this->vaoBinder( false );
    vaoBinder.bind();

    static const QVector< GLenum > h0v0Pair = QVector< GLenum >()
                                              << GL_COLOR_ATTACHMENT0
                                              << GL_COLOR_ATTACHMENT1;

    static const QVector< GLenum > h1v1Pair = QVector< GLenum >()
                                              << GL_COLOR_ATTACHMENT2
                                              << GL_COLOR_ATTACHMENT3;

    if ( m_isWaterCalmScheduled )
    {
        const QVector< GLenum >& readBuffers = m_useH1V1AsDrawBuffers
                                               ? h0v0Pair
                                               : h1v1Pair;
        m_openGlFunctions->glDrawBuffers( readBuffers.size(), readBuffers.constData() );
        LOG_OPENGL_ERROR();

        m_waterCalmerProgram.activate();
        m_openGlFunctions->glDrawArrays( GL_TRIANGLE_FAN,
                                         0,
                                         4 );
        LOG_OPENGL_ERROR();
        m_waterCalmerProgram.deactivate();

        m_isWaterCalmScheduled = false;
    }

    m_shaderProgram.activate();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexTimeDifference ),
                                    m_waterPropagationDeltaTime.elapsed() / 1000.0f );
    LOG_OPENGL_ERROR();

    m_isPropagatingWater = true;
    if ( m_waterPropagationDeltaTime.isNull() )
    {
        m_waterPropagationDeltaTime.start();
    }
    else
    {
        m_waterPropagationDeltaTime.restart();
    }

    const QVector< GLenum >& drawBuffers = m_useH1V1AsDrawBuffers
                                           ? h1v1Pair
                                           : h0v0Pair;
    m_openGlFunctions->glDrawBuffers( drawBuffers.size(), drawBuffers.constData() );
    LOG_OPENGL_ERROR();

    OpenGlObjectBinder readHeightTextureUnitBinder = this->textureUnitBinder( GL_TEXTURE0, false );
    readHeightTextureUnitBinder.bind();

    OpenGlObjectBinder readHeightTextureBinder = this->textureBinder( actualOldHeightTextureId, false );
    readHeightTextureBinder.bind();

#ifndef USE_GPU_ACCELERATED_RIPPLE
    if ( this->isWaterRippleScheduled() )
    {
        const auto heightAlternator = [ this ]( const float height, const int x, const int y )
        {
            m_openGlFunctions->glTexSubImage2D( GL_TEXTURE_2D,
                                                0, // mipmap level
                                                x, // x offset
                                                y, // y offset
                                                1, // width
                                                1, // height
                                                GL_RED,
                                                GL_FLOAT,
                                                & height );
            LOG_OPENGL_ERROR();
        };

        const QPoint ripplePoint = m_scheduledWaterRipplePoints.takeFirst();
        m_waterRipplerCpu.rippleWaterAt( heightAlternator, ripplePoint );
    }
#endif

    OpenGlObjectBinder readVelocityTextureUnitBinder = this->textureUnitBinder( GL_TEXTURE1, false );
    readVelocityTextureUnitBinder.bind();

    OpenGlObjectBinder readVelocityTextureBinder = this->textureBinder( this->oldVelocityTextureId(), false );
    readVelocityTextureBinder.bind();

    m_openGlFunctions->glDrawArrays( GL_TRIANGLE_FAN,
                                     0,
                                     4 );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();

    readVelocityTextureBinder.unbind();
    readVelocityTextureUnitBinder.unbind();

    readHeightTextureBinder.unbind();
    readHeightTextureUnitBinder.unbind();

    vaoBinder.unbind();
    fboActivator.unbind();
    vpAlternator.rollback();
    cullFaceModeAlternator.unbind();
    cullFaceEnabler.rollback();

    m_useH1V1AsDrawBuffers = ! m_useH1V1AsDrawBuffers;

    emit updateRequested();
}

QByteArray WaterDataCalculator::uniformVariableName( const int index )
{
    switch ( index )
    {
        case UniformVariableIndexOldHeightTexSampler:
            return "oldHeightTexSampler";
        case UniformVariableIndexOldVelocityTexSampler:
            return "oldVelocityTexSampler";
        case UniformVariableIndexTimeDifference:
            return "dt";
        case UniformVariableIndexSpeed:
            return "speed";
        case UniformVariableIndexColumnWidth:
            return "colWidth";
        case UniformVariableIndexVelocityScale:
            return "velocityScale";
        case UniformVariableIndexColumnHeightMaxSlope:
            return "colHeightMaxSlope";
        case UniformVariableIndexMeshVertexMaxIndices:
            return "meshVertexMaxIndices";
        default:
            break;
    }

    return QByteArray();
}

bool WaterDataCalculator::isUniformVariableIndexValid( const int index )
{
    return 0 <= index && index < UniformVariablesCount;
}

GLint WaterDataCalculator::uniformVariableLocation( const int index ) const
{
    if ( ! WaterDataCalculator::isUniformVariableIndexValid( index )
         || ! m_shaderProgram.isValid() )
    {
        return CommonEntities::s_invalidSignedGlValue;
    }

    const QByteArray uniformVariableName = WaterDataCalculator::uniformVariableName( index );
    return uniformVariableName.isEmpty()
           ? CommonEntities::s_invalidSignedGlValue
           : m_shaderProgram.uniformVarLocation( uniformVariableName );
}

void WaterDataCalculator::cacheUniformVariableLocations()
{
    m_uniformVariableCachedLocations.clear();
    m_uniformVariableCachedLocations.reserve( UniformVariablesCount );
    for ( int i = 0; i < UniformVariablesCount; ++ i )
    {
        m_uniformVariableCachedLocations.insert( i, this->uniformVariableLocation( i ) );
    }
}

GLint WaterDataCalculator::uniformVariableCachedLocation( const int index ) const
{
    return m_uniformVariableCachedLocations.value( index, CommonEntities::s_invalidSignedGlValue );
}
