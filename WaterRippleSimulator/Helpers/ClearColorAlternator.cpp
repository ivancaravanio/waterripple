#include "ClearColorAlternator.h"

#include "../WaterRippleSimulator_namespace.h"
#include "OpenGlErrorLogger.h"

#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Helpers;

ClearColorAlternator::ClearColorAlternator()
    : m_openGlFunctions{ nullptr }
    , m_autoRollback{ false }
{
}

ClearColorAlternator::ClearColorAlternator(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        const QColor& aRequestedClearColor,
        const bool aAutoRollback )
    : m_openGlFunctions{ aOpenGlFunctions }
    , m_requestedClearColor{ aRequestedClearColor }
    , m_autoRollback{ aAutoRollback }
{
}

ClearColorAlternator::~ClearColorAlternator()
{
    if ( m_autoRollback )
    {
        this->rollback();
    }
}

bool ClearColorAlternator::isValid() const
{
    return m_openGlFunctions != nullptr
           && m_requestedClearColor.isValid();
}

const QColor& ClearColorAlternator::requestedClearColor() const
{
    return m_requestedClearColor;
}

QColor ClearColorAlternator::currentClearColor() const
{
    enum ColorComponentIndex
    {
        ColorComponentIndexRed   = 0,
        ColorComponentIndexGreen = 1,
        ColorComponentIndexBlue  = 2,
        ColorComponentIndexAlpha = 3,
        ColorComponentsCount     = 4,
    };

    GLfloat color[ ColorComponentsCount ] = { 0.0f };

    m_openGlFunctions->glGetFloatv( GL_COLOR_CLEAR_VALUE, color );
    LOG_OPENGL_ERROR();

    return QColor::fromRgbF( color[ ColorComponentIndexRed   ],
                             color[ ColorComponentIndexGreen ],
                             color[ ColorComponentIndexBlue  ],
                             color[ ColorComponentIndexAlpha ] );
}

void ClearColorAlternator::change( const QColor& clearColor )
{
    m_openGlFunctions->glClearColor(
                clearColor.redF(),
                clearColor.greenF(),
                clearColor.blueF(),
                clearColor.alphaF() );
    LOG_OPENGL_ERROR();
}

void ClearColorAlternator::change()
{
    if ( ! this->isValid() )
    {
        return;
    }

    const QColor currentClearColor = this->currentClearColor();
    if ( currentClearColor == m_requestedClearColor )
    {
        return;
    }

    m_previousClearColor = currentClearColor;
    this->change( m_requestedClearColor );
}

void ClearColorAlternator::rollback()
{
    if ( ! m_previousClearColor.isValid() )
    {
        return;
    }

    this->change( m_previousClearColor );

    m_previousClearColor = QColor();
}
