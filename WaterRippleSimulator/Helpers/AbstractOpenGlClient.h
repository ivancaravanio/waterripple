#ifndef WATERRIPPLESIMULATOR_HELPERS_ABSTRACTOPENGLCLIENT_H
#define WATERRIPPLESIMULATOR_HELPERS_ABSTRACTOPENGLCLIENT_H

#include "../WaterRippleSimulator_namespace.h"
#include "FramebufferBinder.h"
#include "OpenGlCapabilityEnabler.h"
#include "OpenGlObjectBinder.h"
#include "ViewportAlternator.h"

#include <qopengl.h>
#include <QtGlobal>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

class AbstractOpenGlClient
{
public:
    explicit AbstractOpenGlClient(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions );
    virtual ~AbstractOpenGlClient();

    virtual void init() = 0;

protected:
    OpenGlObjectBinder textureUnitBinder( const GLenum textureUnit, const bool autoUnbind = true ) const;
    OpenGlObjectBinder textureBinder( const GLuint textureId, const bool autoUnbind = true ) const;
    OpenGlObjectBinder vaoBinder( const GLuint vaoId, const bool autoUnbind = true ) const;
    OpenGlObjectBinder vboBinder( const GLuint vboId, const bool autoUnbind = true ) const;
    OpenGlObjectBinder veaBinder( const GLuint veaId, const bool autoUnbind = true ) const;
    OpenGlObjectBinder polygonModeAlternator( const GLenum face, const GLenum mode, const bool autoUnbind = true );
    FramebufferBinder fboBinder( const GLuint aRequestedFboId,
                                 const FramebufferBinder::BindingTarget aBindingTarget,
                                 const bool aAutoUnbind = true ) const;
    ViewportAlternator viewportAlternator( const QRect& aRequestedViewport, const bool autoRollback = true ) const;
    OpenGlCapabilityEnabler capabilityEnabler(
            const GLenum aCapability,
            const bool aShouldEnable,
            const bool aAutoRollback = true ) const;

protected:
    QOpenGLFunctions_3_3_Core* m_openGlFunctions;
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_ABSTRACTOPENGLCLIENT_H
