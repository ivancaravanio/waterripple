#ifndef WATERRIPPLESIMULATOR_HELPERS_OPENGLERRORLOGGER_H
#define WATERRIPPLESIMULATOR_HELPERS_OPENGLERRORLOGGER_H

#include <QString>
#include <QSurfaceFormat>
#include <QtGlobal>

#include <gl/GL.h>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

class OpenGlErrorLogger
{
public:
    ~OpenGlErrorLogger();

    static OpenGlErrorLogger& instance();

    void setOpenGlFunctions( QOpenGLFunctions_3_3_Core* const aOpenGlFunctions );
    QOpenGLFunctions_3_3_Core* openGlFunctions() const;

    void logOpenGlError(
            const QString& filePath,
            const int lineNumber ) const;
    static void logOpenGlError(
            const GLenum openGlErrorCode,
            const QString& filePath = QString(),
            const int lineNumber = -1 );

    static bool isOpenGlErrorCodeValid( const GLenum openGlErrorCode );
    static QString errorDescription( const GLenum openGlErrorCode );

private:
    OpenGlErrorLogger();

private:
    QOpenGLFunctions_3_3_Core* m_openGlFunctions;
};

}
}

#ifndef LOG_OPENGL_ERRORS
    #define LOG_OPENGL_ERRORS
#endif

#ifdef LOG_OPENGL_ERRORS
    #define LOG_OPENGL_ERROR() WaterRippleSimulator::Helpers::OpenGlErrorLogger::instance().logOpenGlError( __FILE__, __LINE__ )
#else
    #define LOG_OPENGL_ERROR()
#endif

#endif // WATERRIPPLESIMULATOR_HELPERS_OPENGLERRORLOGGER_H
