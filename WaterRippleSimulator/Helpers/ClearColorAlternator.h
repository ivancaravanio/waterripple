#ifndef WATERRIPPLESIMULATOR_HELPERS_CLEARCOLORALTERNATOR_H
#define WATERRIPPLESIMULATOR_HELPERS_CLEARCOLORALTERNATOR_H

#include <QColor>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

class ClearColorAlternator
{
public:
    ClearColorAlternator();
    ClearColorAlternator(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
            const QColor& aRequestedClearColor,
            const bool aAutoRollback = true );
    ~ClearColorAlternator();

    bool isValid() const;

    const QColor& requestedClearColor() const;

    QColor currentClearColor() const;

    void change();
    void rollback();

private:
    void change( const QColor& clearColor );

private:
    QOpenGLFunctions_3_3_Core* m_openGlFunctions;
    QColor m_requestedClearColor;
    QColor m_previousClearColor;
    bool m_autoRollback;
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_CLEARCOLORALTERNATOR_H
