#ifndef WATERRIPPLESIMULATOR_HELPERS_OPENGLOBJECTBINDER_H
#define WATERRIPPLESIMULATOR_HELPERS_OPENGLOBJECTBINDER_H

#include "../WaterRippleSimulator_namespace.h"

#include <QtGlobal>

#include <gl/GL.h>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

using namespace WaterRippleSimulator;

class OpenGlObjectBinder
{
public:
    enum class ObjectType
    {
        Invalid,
        TextureUnit,  // binding target: 1 , object name: 0
        Texture,      // binding target: 1 , object name: 1
        Buffer,       // binding target: 1 , object name: 1
        VertexArray,  // binding target: 0 , object name: 1
        Framebuffer,  // binding target: 1 , object name: 1
        Renderbuffer, // binding target: 1 , object name: 1
        PolygonMode,  // binding target: 1 , object name: 1
        CullFaceMode  // binding target: 1 , object name: 0
    };

public:
    OpenGlObjectBinder();
    OpenGlObjectBinder(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
            const ObjectType aObjectType,
            const GLenum aBindingTarget,
            const GLuint aObjectName,
            const bool aAutoUnbind = true );
    ~OpenGlObjectBinder();

    bool isValid() const;

    ObjectType objectType() const;
    GLenum bindingTarget() const;
    GLuint objectName() const;
    GLuint requestedData() const;

    void bind();
    void unbind();

    // can be binding target or object name
    GLuint boundData( bool* const ok = nullptr ) const;

private:
    static GLenum openGlBindingTargetCheck(
            const ObjectType objectType,
            const GLenum bindingTarget = CommonEntities::invalidEnumGlValue() );
    GLenum openGlBindingTargetCheck() const;

    bool isObjectNameUsedAsBoundData() const;

    void bind( const GLenum aBindingTarget,
               const GLuint aObjectName );

private:
    QOpenGLFunctions_3_3_Core* m_openGlFunctions;
    ObjectType                 m_objectType;

    GLenum                     m_bindingTarget;
    GLuint                     m_objectName;

    bool                       m_autoUnbind;

    bool                       m_isRequestedDataBound;
    GLuint                     m_previouslyBoundData;
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_OPENGLOBJECTBINDER_H
