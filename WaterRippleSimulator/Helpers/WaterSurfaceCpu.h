#ifndef WATERRIPPLESIMULATOR_HELPERS_WATERSURFACECPU_H
#define WATERRIPPLESIMULATOR_HELPERS_WATERSURFACECPU_H

#include "AbstractWaterSurface.h"
#include "OpenGlObjectBinder.h"
#include "ShaderProgram.h"
#include "WaterRipplerCpu.h"

#include <QHash>
#include <QList>
#include <QPair>
#include <QTime>
#include <QtGlobal>
#include <QVector>
#include <QVector2D>
#include <QVector3D>

#include <gl/GL.h>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

using namespace WaterRippleSimulator::Data;

class WaterSurfaceCpu : public AbstractWaterSurface
{
    Q_OBJECT

public:
    explicit WaterSurfaceCpu(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
            const QSurfaceFormat& aOpenGlFormat,
            QObject* const parent = nullptr );
    ~WaterSurfaceCpu() Q_DECL_OVERRIDE;

    void init() Q_DECL_OVERRIDE;
    void draw( const bool shouldPropagateWater ) Q_DECL_OVERRIDE;

    void setMeshSize( const QSize& aMeshSize ) Q_DECL_OVERRIDE;

    // wave propagation
    void setWavePropagationSpeed( const float aWavePropagationSpeed ) Q_DECL_OVERRIDE;
    float wavePropagationSpeed() const Q_DECL_OVERRIDE;

    void setWaveHeightDampingOverTimeFactor( const float aWaveHeightDampingOverTimeFactor ) Q_DECL_OVERRIDE;
    float waveHeightDampingOverTimeFactor() const Q_DECL_OVERRIDE;

    void setWaveHeightClampingFactor( const float aWaveHeightClampingFactor ) Q_DECL_OVERRIDE;
    float waveHeightClampingFactor() const Q_DECL_OVERRIDE;

    // water ripple
    void setWaterRippleMethod( const WaterSettings::WaterRippleMethod aWaterRippleMethod ) Q_DECL_OVERRIDE;
    WaterSettings::WaterRippleMethod waterRippleMethod() const Q_DECL_OVERRIDE;

    void setWaterRippleMagnitude( const float aWaterRippleMagnitude ) Q_DECL_OVERRIDE;
    float waterRippleMagnitude() const Q_DECL_OVERRIDE;

    void setWaterRippleRadiusFactor( const QVector2D& aWaterRippleRadiusFactor ) Q_DECL_OVERRIDE;
    QVector2D waterRippleRadiusFactor() const Q_DECL_OVERRIDE;

    void setWaterRippleRotationAngle( const float aWaterRippleRotationAngle ) Q_DECL_OVERRIDE;
    float waterRippleRotationAngle() const Q_DECL_OVERRIDE;

public slots:
    void rippleWaterAt( const int x, const int y ) Q_DECL_OVERRIDE;
    void propagateWater() Q_DECL_OVERRIDE;
    void calmWater() Q_DECL_OVERRIDE;

protected:
    QList< GLuint > allGenericVertexAttributeIndices() const Q_DECL_OVERRIDE;
    void applyMeshSize() Q_DECL_OVERRIDE;
    void applyMatrix( const CommonEntities::MatrixType matrixType ) Q_DECL_OVERRIDE;
    void applyLight() Q_DECL_OVERRIDE;
    void applyMaterial() Q_DECL_OVERRIDE;
    void applyObtainsColorFromTexture() Q_DECL_OVERRIDE;

private:
    enum GenericVertexAttributeIndex
    {
        GenericVertexAttributeIndexInvalid         = -1,
        GenericVertexAttributeIndexVertex2DPos     = 0,
        GenericVertexAttributeIndexVertexTexCoords = 1,
        GenericVertexAttributeIndexVertexHeight    = 2,
        GenericVertexAttributeIndexVertexNormal    = 3,
        GenericVertexAttributesCount               = 4
    };

    enum FragmentOutputVariableIndex
    {
        FragmentOutputVariableIndexInvalid = -1,
        FragmentOutputVariableIndexColor   = 0,
        FragmentOutputVariablesCount       = 1
    };

    enum UniformVariableIndex
    {
        UniformVariableIndexInvalid                     = -1,
        UniformVariableIndexMaterialColor               = 0,
        UniformVariableIndexMaterialReflectanceEmissive = 1,
        UniformVariableIndexMaterialReflectanceAmbient  = 2,
        UniformVariableIndexMaterialReflectanceDiffuse  = 3,
        UniformVariableIndexMaterialReflectanceSpecular = 4,
        UniformVariableIndexMaterialShininess           = 5,
        UniformVariableIndexLightColor                  = 6,
        UniformVariableIndexLightPos                    = 7,
        UniformVariableIndexLightAttenuationConstant    = 8,
        UniformVariableIndexLightAttenuationLinear      = 9,
        UniformVariableIndexLightAttenuationQuadratic   = 10,
        UniformVariableIndexLightIntensity              = 11,
        UniformVariableIndexModelMatrix                 = 12,
        UniformVariableIndexNormalMatrix                = 13,
        UniformVariableIndexViewMatrix                  = 14,
        UniformVariableIndexProjectionMatrix            = 15,
        UniformVariableIndexEyePos                      = 16,
        UniformVariableIndexObtainsColorFromTexture     = 17,
        UniformVariableIndexColorTexSampler             = 18,
        UniformVariablesCount                           = 19
    };

    enum Vbo
    {
        VboInvalid              = -1,
        VboVertexIndices        = 0,
        VboVertex2DPosTexCoords = 1,
        VboVertexHeights        = 2,
        VboVertexNormals        = 3,
        VbosCount               = 4
    };

private:
    static QByteArray uniformVariableName( const int index );
    static bool isUniformVariableIndexValid( const int index );
    GLint uniformVariableLocation( const int index ) const;
    void cacheUniformVariableLocations();
    GLint uniformVariableCachedLocation( const int index ) const;

    void applyMeshSizeInternal();
    void applyMatrixInternal( const CommonEntities::MatrixType matrixType );
    void applyLightInternal();
    void applyMaterialInternal();
    void applyObtainsColorFromTextureInternal();
    void applyPartialShaderData();

    void updateMeshSizeOpenGlData();
    void updateNormalsHeightsOpenGlData();

    void calcHeights();
    void calcNormals();

    using MeshTransformer = void ( WaterSurfaceCpu::* )( const int currentIndex,

                                                         const int bottomIndex,
                                                         const int topIndex,
                                                         const int leftIndex,
                                                         const int rightIndex,

                                                         const int bottomLeftIndex,
                                                         const int bottomRightIndex,
                                                         const int topRightIndex,
                                                         const int topLeftIndex );

    void transformMesh( const MeshTransformer& transformer, const bool avoidBounds );

    void calcHeight(
            const int currentIndex,

            const int bottomIndex,
            const int topIndex,
            const int leftIndex,
            const int rightIndex,

            const int bottomLeftIndex,
            const int bottomRightIndex,
            const int topRightIndex,
            const int topLeftIndex );
    void calcNormal(
            const int currentIndex,

            const int bottomIndex,
            const int topIndex,
            const int leftIndex,
            const int rightIndex,

            const int bottomLeftIndex,
            const int bottomRightIndex,
            const int topRightIndex,
            const int topLeftIndex );

    OpenGlObjectBinder vaoBinder( const bool autoUnbind = true ) const;
    OpenGlObjectBinder vertexIndicesBufferBinder( const bool autoUnbind = true ) const;
    OpenGlObjectBinder vertex2DPosTexCoordsBufferBinder( const bool autoUnbind = true ) const;
    OpenGlObjectBinder vertexHeightsBufferBinder( const bool autoUnbind = true ) const;
    OpenGlObjectBinder vertexNormalsBufferBinder( const bool autoUnbind = true ) const;

    void disposeOpenGlResources();
    QVector< GLfloat >& vertexPreviousHeights();
    QVector< GLfloat >& vertexNewHeights();

private:
    using Vertex2DPosTexCoordPair = QPair< QVector2D, QVector2D >;

private:
    QVector< GLuint >                  m_vertexIndices;
    QVector< Vertex2DPosTexCoordPair > m_vertex2DPositionsTexCoords;
    QVector< GLfloat >                 m_vertexVelocities;
    QVector< GLfloat >                 m_vertexHeights0;
    QVector< GLfloat >                 m_vertexHeights1;
    QVector< QVector3D >               m_vertexNormals;

    ShaderProgram m_shaderProgram;
    GLuint m_vaoId;
    QVector< GLuint > m_vbos;

    QHash< int, GLint > m_uniformVariableCachedLocations;

    float     m_wavePropagationSpeed;
    float     m_waveHeightDampingOverTimeFactor;
    float     m_waveHeightClampingFactor;

    WaterRipplerCpu m_waterRippler;

    WaterSettings::WaterRippleMethod m_waterRippleMethod;
    float     m_waterRippleMagnitude;
    QVector2D m_waterRippleRadiusFactor;
    float     m_waterRippleRotationAngle;

    QTime   m_dtMsecs;
    GLfloat m_dtSecs;
    bool    m_readFromFirstHeight;
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_WATERSURFACECPU_H
