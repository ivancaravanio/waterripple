#ifndef WATERRIPPLESIMULATOR_HELPERS_WATERRIPPLERCPU_H
#define WATERRIPPLESIMULATOR_HELPERS_WATERRIPPLERCPU_H

#include "../Data/WaterSettings.h"
#include "../Utilities/MathUtilities.h"
#include "../WaterRippleSimulator_namespace.h"

#include <QMatrix>
#include <QPointF>
#include <QSize>
#include <QtGlobal>
#include <QVector2D>

namespace WaterRippleSimulator {
namespace Helpers {

using namespace WaterRippleSimulator::Data;
using namespace WaterRippleSimulator::Utilities;

class WaterRipplerCpu
{
public:
    WaterRipplerCpu();
    ~WaterRipplerCpu();

    void setMeshSize( const QSize& aMeshSize );
    QSize meshSize() const;
    QSize verticesCount() const;

    void setMethod( const WaterSettings::WaterRippleMethod aMethod );
    WaterSettings::WaterRippleMethod method() const;

    void setMagnitude( const float aMagnitude );
    float magnitude() const;

    void setRadiusFactor( const QVector2D& aRadiusFactor );
    QVector2D radiusFactor() const;
    QVector2D radius() const;

    void setRotationAngle( const float aRotationAngle );
    float rotationAngle() const;

    template< typename T > void rippleWaterAt( const T& heightAlternator, const QPoint& pt );
    template< typename T > void rippleWaterAt( const T& heightAlternator, const int x, const int y );
    template< typename T > void rippleWaterAt( const T& heightAlternator, const float absX, const float absY );

private:
    template< typename T > void rippleWaterAtCircularTrigonometricApproach( const T& heightAlternator, const int x, const int y );
    template< typename T > void rippleWaterAtEllipticTrigonometricApproach( const T& heightAlternator, const int x, const int y );
    template< typename T > void rippleWaterAtEllipticGaussianApproach( const T& heightAlternator, const int x, const int y );

private:
    QSize     m_meshSize;
    WaterSettings::WaterRippleMethod m_method;
    float     m_magnitude;
    QVector2D m_radiusFactor;
    float     m_rotationAngle;
};

template< typename T >
void WaterRipplerCpu::rippleWaterAt( const T& heightAlternator, const QPoint& pt )
{
    this->rippleWaterAt( heightAlternator, pt.x(), pt.y() );
}

template< typename T >
void WaterRipplerCpu::rippleWaterAt( const T& heightAlternator, const int x, const int y )
{
    if ( m_meshSize.isEmpty() )
    {
        return;
    }

    const int xVertexMaxIndex = m_meshSize.width();
    const int yVertexMaxIndex = m_meshSize.height();
    if ( x < 0
         || x > xVertexMaxIndex
         || y < 0
         || y > yVertexMaxIndex )
    {
        return;
    }

    switch ( m_method )
    {
        case WaterSettings::WaterRippleMethodCircularTrigonometric:
            this->rippleWaterAtCircularTrigonometricApproach( heightAlternator, x, y );
            break;
        case WaterSettings::WaterRippleMethodEllipticTrigonometric:
            this->rippleWaterAtEllipticTrigonometricApproach( heightAlternator, x, y );
            break;
        case WaterSettings::WaterRippleMethodEllipticGaussian:
            this->rippleWaterAtEllipticGaussianApproach( heightAlternator, x, y );
            break;
        default:
            break;
    }
}

template< typename T >
void WaterRipplerCpu::rippleWaterAt( const T& heightAlternator, const float absX, const float absY )
{
    this->rippleWaterAt(
                heightAlternator,
                qRound( m_meshSize.width() * absX ),
                qRound( m_meshSize.height() * absY ) );
}

template< typename T >
void WaterRipplerCpu::rippleWaterAtCircularTrigonometricApproach(
        const T& heightAlternator,
        const int x,
        const int y )
{
    const QVector2D ripplePt{ static_cast< float >( x ),
                              static_cast< float >( y ) };
    const float radius = this->radius().x();

    const int xVertexMaxIndex = m_meshSize.width();
    const int yVertexMaxIndex = m_meshSize.height();
    for ( int j = 1; j < yVertexMaxIndex; ++ j )
    {
        for ( int i = 1; i < xVertexMaxIndex; ++ i )
        {
            const float distance = ( QVector2D{ static_cast< float >( i ),
                                                static_cast< float >( j ) } - ripplePt ).length();
            if ( distance > radius )
            {
                continue;
            }

            const float distanceFactor = distance / radius;
            const float heightOffset = m_magnitude * ( cos( distanceFactor * M_PI ) + 1.0f );
            heightAlternator( heightOffset, i, j );
        }
    }
}

template< typename T >
void WaterRipplerCpu::rippleWaterAtEllipticTrigonometricApproach(
        const T& heightAlternator,
        const int x,
        const int y )
{
    if ( m_meshSize.isEmpty() )
    {
        return;
    }

    const int xVertexMaxIndex = m_meshSize.width();
    const int yVertexMaxIndex = m_meshSize.height();

    const QVector2D ripplePt{ static_cast< float >( x ),
                              static_cast< float >( y ) };
    const QPointF ripplePtRaw{ ripplePt.x(), ripplePt.y() };

    // this custom ripple kernel generation has nothing to do with the Gaussian one:
    //     http://en.wikipedia.org/wiki/Gaussian_function
    // where:
    // "... then we rotate the blob by a clockwise angle \theta
    // (for counterclockwise rotation invert the signs in the b coefficient).
    // ---
    // the rotation of the ellipse should happen CCW
    // therfore rotate all vertices in the opposite direction
    // to simulate the abscence of rotation angle

    QMatrix m;
    m.rotate( - m_rotationAngle );

    const QVector2D xAxis{ 1.0f, 0.0 };
    const QVector2D waterRippleRadius = this->radius();
    const float xWaterRippleRadius = waterRippleRadius.x();
    const float xxWaterRippleRadius = xWaterRippleRadius * xWaterRippleRadius;
    const float yWaterRippleRadius = waterRippleRadius.y();
    const float yyWaterRippleRadius = yWaterRippleRadius * yWaterRippleRadius;
    const float xyWaterRippleRadius = xWaterRippleRadius * yWaterRippleRadius;

    for ( int j = 1; j < yVertexMaxIndex; ++ j )
    {
        for ( int i = 1; i < xVertexMaxIndex; ++ i )
        {
            const QVector2D centerDistanceVec{ m.map( QPointF{ static_cast< float >( i ),
                                                               static_cast< float >( j ) } - ripplePtRaw ) };
            const QVector2D centerDistanceVecNorm = centerDistanceVec.normalized();
            const float xRadiusParticipation = qAbs( QVector2D::dotProduct( centerDistanceVecNorm, xAxis ) );

            const float tanSquaredRadiusParticipation = ( 1.0f - xRadiusParticipation * xRadiusParticipation ) / ( xRadiusParticipation * xRadiusParticipation );
            const float sgn = ( xRadiusParticipation > 0.0f
                                || qFuzzyIsNull( xRadiusParticipation )
                                ? 1.0f
                                : -1.0f );
            const float ellipsePtX = sgn
                                     * xyWaterRippleRadius
                                     / sqrt( yyWaterRippleRadius + xxWaterRippleRadius * tanSquaredRadiusParticipation );
            const float ellipsePtY = sgn
                                     * xyWaterRippleRadius
                                     / sqrt( xxWaterRippleRadius + yyWaterRippleRadius / tanSquaredRadiusParticipation );
            const QVector2D ellipsePt{ ellipsePtX, ellipsePtY };

            const float distanceRatio = centerDistanceVec.length() / ellipsePt.length();
            if ( distanceRatio > 1.0f )
            {
                continue;
            }

            const float heightOffset = m_magnitude * ( cos( distanceRatio * M_PI ) + 1.0f );
            heightAlternator( heightOffset, i, j );
        }
    }
}

template< typename T >
void WaterRipplerCpu::rippleWaterAtEllipticGaussianApproach(
        const T& heightAlternator,
        const int x,
        const int y )
{
    if ( m_meshSize.isEmpty() )
    {
        return;
    }

    const int xVertexMaxIndex = m_meshSize.width();
    const int yVertexMaxIndex = m_meshSize.height();

    const QVector2D waterRippleRadius = this->radius();
    const float xWaterRippleRadius = waterRippleRadius.x();
    const float yWaterRippleRadius = waterRippleRadius.y();
    const float xDenominatorFactor = 2.0f * xWaterRippleRadius * xWaterRippleRadius;
    const float yDenominatorFactor = 2.0f * yWaterRippleRadius * yWaterRippleRadius;

    const float rotationAngleRadians = MathUtilities::radians( m_rotationAngle );
    const float sinThetha = sin( rotationAngleRadians );
    const float sinThethaSquare = sinThetha * sinThetha;
    const float cosThethaSquare = 1.0f - sinThethaSquare;

    const float sinDoubleThetha = sin( 2.0f * rotationAngleRadians );

    const float a = cosThethaSquare / xDenominatorFactor
                    + sinThethaSquare / yDenominatorFactor;
    const float b = sinDoubleThetha / 2.0f * ( 1.0f / yDenominatorFactor - 1.0f / xDenominatorFactor );
    const float c = sinThethaSquare / xDenominatorFactor - cosThethaSquare / xDenominatorFactor;
    for ( int j = 1; j < yVertexMaxIndex; ++ j )
    {
        const float yDiff = j - y;
        for ( int i = 1; i < xVertexMaxIndex; ++ i )
        {
            const float xDiff = i - x;
            const float heightOffset = m_magnitude * exp( - ( a * xDiff * xDiff + 2.0f * b * xDiff * yDiff + c * yDiff * yDiff ) );
//            const float heightOffset = m_waterRippleMagnitude * exp( - ( ( xDiff * xDiff ) / xDenominatorFactor
//                                                                         + yDiff * yDiff / yDenominatorFactor ) );
            if ( ! qFuzzyIsNull( heightOffset ) )
            {
                heightAlternator( heightOffset, i, j );
            }
        }
    }
}

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_WATERRIPPLERCPU_H
