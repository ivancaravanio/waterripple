#include "WaterSurfaceCpu.h"

#include "../Data/ShaderInfo.h"
#include "../Data/WaterSettings.h"
#include "../Utilities/GeneralUtilities.h"
#include "../Utilities/GlUtilities.h"
#include "../Utilities/ImageUtilities.h"
#include "../Utilities/MathUtilities.h"
#include "../WaterRippleSimulator_namespace.h"
#include "OpenGlErrorLogger.h"

#include <QDateTime>
#include <QOpenGLFunctions_3_3_Core>
#include <QtMath>

using namespace WaterRippleSimulator::Helpers;

using namespace WaterRippleSimulator::Utilities;
using namespace WaterRippleSimulator;

WaterSurfaceCpu::WaterSurfaceCpu(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        const QSurfaceFormat& aOpenGlFormat,
        QObject* const parent )
    : AbstractWaterSurface{ aOpenGlFunctions, parent }
    , m_shaderProgram{ aOpenGlFunctions, aOpenGlFormat }
    , m_vaoId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_vbos( VbosCount, CommonEntities::s_invalidUnsignedGlValue )

    // wave propagation
    , m_wavePropagationSpeed{ WaterSettings::s_wavePropagationSpeedDefault }
    , m_waveHeightDampingOverTimeFactor{ WaterSettings::s_waveHeightClampingFactorDefault }
    , m_waveHeightClampingFactor{ WaterSettings::s_waveHeightClampingFactorDefault }

    // water ripple
    , m_waterRippleMethod{ WaterSettings::s_waterRippleMethodDefault }
    , m_waterRippleMagnitude{ WaterSettings::s_waterRippleMagnitudeDefault }
    , m_waterRippleRadiusFactor{ WaterSettings::s_waterRipple2DRadiusFactorDefault }
    , m_waterRippleRotationAngle{ WaterSettings::s_waterRippleRotationAngleDefault }

    , m_dtSecs{ 0.0f }
    , m_readFromFirstHeight{ true }
{
    this->applyMeshSizeInternal();
    this->applyPartialShaderData();
}

WaterSurfaceCpu::~WaterSurfaceCpu()
{
    this->disposeOpenGlResources();
}

void WaterSurfaceCpu::transformMesh(
        const MeshTransformer& transformer,
        const bool avoidBounds )
{
    const QSize verticesCount = this->verticesCount();
    if ( verticesCount.isEmpty() )
    {
        return;
    }

    const int xVerticesCount = verticesCount.width();
    const int yVerticesCount = verticesCount.height();

    const int xVertexIndexMax = xVerticesCount - 1;
    const int yVertexIndexMax = yVerticesCount - 1;

    const int margin = avoidBounds
                       ? 1
                       : 0;
    const int xBeginIndex = margin;
    const int xEndIndex = xVertexIndexMax - margin;
    const int yBeginIndex = margin;
    const int yEndIndex = yVertexIndexMax - margin;

    for ( int y = yBeginIndex;
          y <= yEndIndex;
          ++ y )
    {
        const int bottomYIndex = qMax( y - 1, 0 );
        const int topYIndex = qMin( y + 1, yVertexIndexMax );
        const int currentYOffset = y * xVerticesCount;
        for ( int x = xBeginIndex;
              x <= xEndIndex;
              ++ x )
        {
            const int leftXIndex = qMax( x - 1, 0 );
            const int rightXIndex = qMin( x + 1, xVertexIndexMax );

            const int bottomIndex = bottomYIndex * xVerticesCount + x;
            const int topIndex    = topYIndex * xVerticesCount + x;
            const int leftIndex   = currentYOffset + leftXIndex;
            const int rightIndex  = currentYOffset + rightXIndex;

            const int bottomLeftIndex  = bottomYIndex * xVerticesCount + leftXIndex;
            const int bottomRightIndex = bottomYIndex * xVerticesCount + rightXIndex;
            const int topRightIndex    = topYIndex * xVerticesCount + rightXIndex;
            const int topLeftIndex     = topYIndex * xVerticesCount + leftXIndex;

            ( this->*transformer )( currentYOffset + x,

                                    bottomIndex,
                                    topIndex,
                                    leftIndex,
                                    rightIndex,

                                    bottomLeftIndex,
                                    bottomRightIndex,
                                    topRightIndex,
                                    topLeftIndex );
        }
    }
}

void WaterSurfaceCpu::calcHeights()
{
    this->transformMesh( & WaterSurfaceCpu::calcHeight, true );
}

void WaterSurfaceCpu::calcNormals()
{
    this->transformMesh( & WaterSurfaceCpu::calcNormal, false );
}

void WaterSurfaceCpu::calcHeight(
        const int currentIndex,

        const int bottomIndex,
        const int topIndex,
        const int leftIndex,
        const int rightIndex,

        const int bottomLeftIndex,
        const int bottomRightIndex,
        const int topRightIndex,
        const int topLeftIndex )
{
    if ( qFuzzyIsNull( m_dtSecs ) || m_dtSecs < 0.0f )
    {
        return;
    }

    static const GLfloat colWidth = 1.0f;
    const float maxSpeed = colWidth / m_dtSecs;
    if ( m_wavePropagationSpeed > maxSpeed )
    {
        return;
    }

//    qDebug() << "indices:" << endl
//             << "\tbottomIndex =" << bottomIndex << endl
//             << "\ttopIndex    =" << topIndex    << endl
//             << "\tleftIndex   =" << leftIndex   << endl
//             << "\trightIndex  =" << rightIndex  << endl
//             << "\tbottomLeftIndex  =" << bottomLeftIndex  << endl
//             << "\tbottomRightIndex =" << bottomRightIndex << endl
//             << "\ttopRightIndex    =" << topRightIndex    << endl
//             << "\ttopLeftIndex     =" << topLeftIndex;

    GLfloat& velocity = m_vertexVelocities[ currentIndex ];

    const QVector< GLfloat >& previousHeights = this->vertexPreviousHeights();
    const GLfloat currentHeight = previousHeights[ currentIndex ];

#ifndef DIFFUSION_METHOD
    #define DIFFUSION_METHOD 1
#endif
#if DIFFUSION_METHOD == 3
    const GLfloat horizVertNeighbourDiscreteDistance = colWidth;
    const GLfloat diagNeighbourDiscreteDistance = sqrt( colWidth * colWidth + colWidth * colWidth );
    GLfloat offset = (   previousHeights[ bottomIndex ]
                       + previousHeights[ topIndex ]
                       + previousHeights[ leftIndex ]
                       + previousHeights[ rightIndex ] ) / horizVertNeighbourDiscreteDistance
                     + (   previousHeights[ bottomLeftIndex ]
                         + previousHeights[ bottomRightIndex ]
                         + previousHeights[ topRightIndex ]
                         + previousHeights[ topLeftIndex ] ) / diagNeighbourDiscreteDistance
                     - 4.0f * ( 1.0f / horizVertNeighbourDiscreteDistance
                                + 1.0f / diagNeighbourDiscreteDistance ) * currentHeight;
#elif DIFFUSION_METHOD == 2
    const GLfloat horizVertNeighbourDiscreteDistance = colWidth;
    const GLfloat diagNeighbourDiscreteDistance = sqrt( colWidth * colWidth + colWidth * colWidth );
    GLfloat offset = (   previousHeights[ bottomIndex ]
                         + previousHeights[ topIndex ]
                         + previousHeights[ leftIndex ]
                         + previousHeights[ rightIndex ] ) / horizVertNeighbourDiscreteDistance
                     + (   previousHeights[ bottomLeftIndex ]
                           + previousHeights[ bottomRightIndex ]
                           + previousHeights[ topRightIndex ]
                           + previousHeights[ topLeftIndex ] ) / diagNeighbourDiscreteDistance
                     - 4.0f * ( horizVertNeighbourDiscreteDistance
                                + diagNeighbourDiscreteDistance ) * currentHeight;
#else
    Q_UNUSED( bottomLeftIndex )
    Q_UNUSED( bottomRightIndex )
    Q_UNUSED( topRightIndex )
    Q_UNUSED( topLeftIndex )

    GLfloat offset = (   previousHeights[ bottomIndex ]
                         + previousHeights[ topIndex ]
                         + previousHeights[ leftIndex ]
                         + previousHeights[ rightIndex ] )
                     - 4.0f * currentHeight;
#endif

    const GLfloat maxOffset = m_waveHeightClampingFactor * colWidth;
    offset = qBound( - maxOffset, offset, maxOffset );
    // if ( offset > maxOffset )
    // {
    //     offset -= maxOffset;
    // }
    // else if ( offset < -maxOffset )
    // {
    //     offset += maxOffset;
    // }

    const GLfloat force = m_wavePropagationSpeed * m_wavePropagationSpeed
                          * offset
                          / ( colWidth * colWidth );

    velocity += force * m_dtSecs;
    this->vertexNewHeights()[ currentIndex ] = currentHeight + velocity * m_dtSecs;
    velocity *= m_waveHeightDampingOverTimeFactor;
}

void WaterSurfaceCpu::calcNormal(
        const int currentIndex,

        const int bottomIndex,
        const int topIndex,
        const int leftIndex,
        const int rightIndex,

        const int bottomLeftIndex,
        const int bottomRightIndex,
        const int topRightIndex,
        const int topLeftIndex )
{
    Q_UNUSED( bottomLeftIndex )
    Q_UNUSED( bottomRightIndex )
    Q_UNUSED( topRightIndex )
    Q_UNUSED( topLeftIndex )

    const QVector< GLfloat >& heights = this->vertexNewHeights();
    m_vertexNormals[ currentIndex ] =
            QVector3D::normal(
                QVector3D{ 1.0f,
                           0.0f,
                           heights[ rightIndex ] - heights[ leftIndex ] },
                QVector3D{ 0.0f,
                           1.0f,
                           heights[ topIndex ] - heights[ bottomIndex ] } );
}

OpenGlObjectBinder WaterSurfaceCpu::vaoBinder( const bool autoUnbind ) const
{
    return AbstractWaterSurface::vaoBinder( m_vaoId, autoUnbind );
}

OpenGlObjectBinder WaterSurfaceCpu::vertexIndicesBufferBinder( const bool autoUnbind ) const
{
    return this->veaBinder( m_vbos[ VboVertexIndices ], autoUnbind );
}

OpenGlObjectBinder WaterSurfaceCpu::vertex2DPosTexCoordsBufferBinder( const bool autoUnbind ) const
{
    return this->vboBinder( m_vbos[ VboVertex2DPosTexCoords ], autoUnbind );
}

OpenGlObjectBinder WaterSurfaceCpu::vertexHeightsBufferBinder( const bool autoUnbind ) const
{
    return this->vboBinder( m_vbos[ VboVertexHeights ], autoUnbind );
}

OpenGlObjectBinder WaterSurfaceCpu::vertexNormalsBufferBinder( const bool autoUnbind ) const
{
    return this->vboBinder( m_vbos[ VboVertexNormals ], autoUnbind );
}

void WaterSurfaceCpu::disposeOpenGlResources()
{
    if ( CommonEntities::isUnsignedGlValueValid( m_vaoId ) )
    {
        m_openGlFunctions->glDeleteVertexArrays( 1, & m_vaoId );
        LOG_OPENGL_ERROR();

        m_vaoId = CommonEntities::s_invalidUnsignedGlValue;
    }

    QVector< GLuint > vbosToDispose;
    for ( GLuint& vboId : m_vbos )
    {
        if ( CommonEntities::isUnsignedGlValueValid( vboId ) )
        {
            vbosToDispose.append( vboId );
            vboId = CommonEntities::s_invalidUnsignedGlValue;
        }
    }

    const int vbosToDisposeCount = vbosToDispose.size();
    if ( vbosToDisposeCount > 0 )
    {
        m_openGlFunctions->glDeleteBuffers( vbosToDisposeCount, vbosToDispose.constData() );
        LOG_OPENGL_ERROR();
    }
}

QVector< GLfloat >& WaterSurfaceCpu::vertexPreviousHeights()
{
    return m_readFromFirstHeight
           ? m_vertexHeights0
           : m_vertexHeights1;
}

QVector< GLfloat >& WaterSurfaceCpu::vertexNewHeights()
{
    return m_readFromFirstHeight
           ? m_vertexHeights1
           : m_vertexHeights0;
}

void WaterSurfaceCpu::init()
{
    AbstractWaterSurface::init();

    if ( m_openGlFunctions == nullptr
         || CommonEntities::isUnsignedGlValueValid( m_vaoId ) )
    {
        return;
    }

    // setup Vertex Buffer Objects first

    m_openGlFunctions->glGenBuffers( m_vbos.size(), m_vbos.data() );
    LOG_OPENGL_ERROR();

    this->applyMeshSize();

    // setup Vertex Array Objects next
    m_openGlFunctions->glGenVertexArrays( 1, & m_vaoId );
    LOG_OPENGL_ERROR();

    OpenGlObjectBinder vaoBinder = this->vaoBinder( false );
    vaoBinder.bind();

    OpenGlObjectBinder vertexIndicesBufferBinder = this->vertexIndicesBufferBinder( false );
    vertexIndicesBufferBinder.bind();

    this->enableGenericVertexAttributeArrays( true );
    LOG_OPENGL_ERROR();

    OpenGlObjectBinder vertex2DPosTexCoordsBufferBinder = this->vertex2DPosTexCoordsBufferBinder( false );
    vertex2DPosTexCoordsBufferBinder.bind();
    m_openGlFunctions->glVertexAttribPointer( GenericVertexAttributeIndexVertex2DPos,
                                              MathUtilities::s_axesCount2D,
                                              GL_FLOAT,
                                              GL_FALSE,
                                              sizeof( Vertex2DPosTexCoordPair ),
                                              reinterpret_cast< const GLvoid* >( offsetof( Vertex2DPosTexCoordPair, first ) ) );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glVertexAttribPointer( GenericVertexAttributeIndexVertexTexCoords,
                                              MathUtilities::s_axesCount2D,
                                              GL_FLOAT,
                                              GL_FALSE,
                                              sizeof( Vertex2DPosTexCoordPair ),
                                              reinterpret_cast< const GLvoid* >( offsetof( Vertex2DPosTexCoordPair, second ) ) );
    LOG_OPENGL_ERROR();
    vertex2DPosTexCoordsBufferBinder.unbind();

    OpenGlObjectBinder vertexHeightsBufferBinder = this->vertexHeightsBufferBinder( false );
    vertexHeightsBufferBinder.bind();
    m_openGlFunctions->glVertexAttribPointer( GenericVertexAttributeIndexVertexHeight,
                                              MathUtilities::s_axesCount1D,
                                              GL_FLOAT,
                                              GL_FALSE,
                                              0,
                                              nullptr );
    LOG_OPENGL_ERROR();
    vertexHeightsBufferBinder.unbind();

    OpenGlObjectBinder vertexNormalsBufferBinder = this->vertexNormalsBufferBinder( false );
    vertexNormalsBufferBinder.bind();
    m_openGlFunctions->glVertexAttribPointer( GenericVertexAttributeIndexVertexNormal,
                                              MathUtilities::s_axesCount3D,
                                              GL_FLOAT,
                                              GL_FALSE,
                                              0,
                                              nullptr );
    LOG_OPENGL_ERROR();
    vertexNormalsBufferBinder.unbind();

    this->enableGenericVertexAttributeArrays( false );

    vaoBinder.unbind();
    vertexIndicesBufferBinder.unbind();

    const bool isBuiltOk = GlUtilities::buildShaderProgram(
                               m_shaderProgram,
                               ":/Resources/shaders/WaterSurface.vert",
                               ":/Resources/shaders/WaterSurface.frag" );
    if ( ! isBuiltOk )
    {
        return;
    }

    this->cacheUniformVariableLocations();

    m_shaderProgram.activate();
    m_openGlFunctions->glUniform1i( this->uniformVariableCachedLocation( UniformVariableIndexColorTexSampler ), 0 );
    LOG_OPENGL_ERROR();

    this->applyPartialShaderData();
    m_shaderProgram.deactivate();
}

void WaterSurfaceCpu::draw( const bool shouldPropagateWater )
{
    if ( m_openGlFunctions == nullptr
         || ! CommonEntities::isUnsignedGlValueValid( m_vaoId )
         || this->meshSize().isEmpty() )
    {
        return;
    }

    if ( shouldPropagateWater )
    {
        this->propagateWater();
    }

    OpenGlObjectBinder polygonModeAlternator = this->polygonModeAlternator( GL_FRONT_AND_BACK,
                                                                            this->shouldDrawAsWireframe()
                                                                            ? GL_LINE
                                                                            : GL_FILL,
                                                                            false );
    polygonModeAlternator.bind();

    m_shaderProgram.activate();

    OpenGlObjectBinder vaoBinder = this->vaoBinder( false );
    vaoBinder.bind();

    OpenGlObjectBinder videoFrameTextureUnitBinder = this->textureUnitBinder( GL_TEXTURE0, false );
    videoFrameTextureUnitBinder.bind();

    OpenGlObjectBinder videoFrameTextureBinder = this->videoFrameTextureBinder( false );
    videoFrameTextureBinder.bind();

    this->enableGenericVertexAttributeArrays( true );

    m_openGlFunctions->glDrawElements( GL_TRIANGLES,
                                       m_vertexIndices.size(),
                                       GL_UNSIGNED_INT,
                                       nullptr );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();

    this->enableGenericVertexAttributeArrays( false );

    vaoBinder.unbind();
    polygonModeAlternator.unbind();
    videoFrameTextureBinder.unbind();
    videoFrameTextureUnitBinder.unbind();
}

void WaterSurfaceCpu::setMeshSize( const QSize& aMeshSize )
{
    AbstractWaterSurface::setMeshSize( aMeshSize );
    m_waterRippler.setMeshSize( aMeshSize );
}

void WaterSurfaceCpu::applyMatrix(
        const CommonEntities::MatrixType matrixType )
{
    this->applyMatrixInternal( matrixType );
}

void WaterSurfaceCpu::applyMatrixInternal(
        const CommonEntities::MatrixType matrixType )
{
    int uniformVariableIndex = UniformVariableIndexInvalid;
    switch ( matrixType )
    {
        case CommonEntities::MatrixType::Model:
            uniformVariableIndex = UniformVariableIndexModelMatrix;
            break;
        case CommonEntities::MatrixType::Normal:
            uniformVariableIndex = UniformVariableIndexNormalMatrix;
            break;
        case CommonEntities::MatrixType::View:
            uniformVariableIndex = UniformVariableIndexViewMatrix;
            break;
        case CommonEntities::MatrixType::Projection:
            uniformVariableIndex = UniformVariableIndexProjectionMatrix;
            break;
        default:
            return;
    }

    const GLint location = this->uniformVariableCachedLocation( uniformVariableIndex );
    if ( matrixType == CommonEntities::MatrixType::Normal )
    {
        const QMatrix3x3 matrix = this->normalMatrix();
        m_shaderProgram.setUniformMatrix3fv(
                    location,
                    1,
                    GL_FALSE,
                    matrix.constData() );
    }
    else
    {
        const QMatrix4x4 matrix = this->matrix( matrixType );
        m_shaderProgram.setUniformMatrix4fv(
                    location,
                    1,
                    GL_FALSE,
                    matrix.constData() );
    }
}

void WaterSurfaceCpu::setWaterRippleMagnitude( const float aWaterRippleMagnitude )
{
    if ( ! WaterSettings::isWaterRippleMagnitudeValid( aWaterRippleMagnitude )
         || qFuzzyCompare( aWaterRippleMagnitude, m_waterRippleMagnitude ) )
    {
        return;
    }

    m_waterRippleMagnitude = aWaterRippleMagnitude;
    m_waterRippler.setMagnitude( aWaterRippleMagnitude );

    // not needed
    // emit updateRequested();
}

float WaterSurfaceCpu::waterRippleMagnitude() const
{
    return m_waterRippleMagnitude;
}

void WaterSurfaceCpu::setWaterRippleRadiusFactor( const QVector2D& aWaterRippleRadiusFactor )
{
    if ( ! WaterSettings::isWaterRippleRadiusFactorValid( aWaterRippleRadiusFactor )
         || aWaterRippleRadiusFactor == m_waterRippleRadiusFactor )
    {
        return;
    }

    m_waterRippleRadiusFactor = aWaterRippleRadiusFactor;
    m_waterRippler.setRadiusFactor( aWaterRippleRadiusFactor );

    // not needed
    // emit updateRequested();
}

QVector2D WaterSurfaceCpu::waterRippleRadiusFactor() const
{
    return m_waterRippleRadiusFactor;
}

void WaterSurfaceCpu::setWaterRippleRotationAngle( const float aWaterRippleRotationAngle )
{
    const float normalizedAngle = MathUtilities::to360DegreesAngle( aWaterRippleRotationAngle );
    if ( qFuzzyCompare( normalizedAngle, m_waterRippleRotationAngle ) )
    {
        return;
    }

    m_waterRippleRotationAngle = normalizedAngle;
    m_waterRippler.setRotationAngle( normalizedAngle );

    // not needed
    // emit updateRequested();
}

float WaterSurfaceCpu::waterRippleRotationAngle() const
{
    return m_waterRippleRotationAngle;
}

void WaterSurfaceCpu::setWaterRippleMethod( const WaterSettings::WaterRippleMethod aWaterRippleMethod )
{
    if ( ! WaterSettings::isWaterRippleMethodValid( aWaterRippleMethod )
         || aWaterRippleMethod == m_waterRippleMethod )
    {
        return;
    }

    m_waterRippleMethod = aWaterRippleMethod;
    m_waterRippler.setMethod( aWaterRippleMethod );

    // not needed
    // emit updateRequested();
}

WaterSettings::WaterRippleMethod WaterSurfaceCpu::waterRippleMethod() const
{
    return m_waterRippleMethod;
}

void WaterSurfaceCpu::setWavePropagationSpeed( const float aWavePropagationSpeed )
{
    if ( ! WaterSettings::isWavePropagationSpeedValid( aWavePropagationSpeed )
         || qFuzzyCompare( aWavePropagationSpeed, m_wavePropagationSpeed ) )
    {
        return;
    }

    m_wavePropagationSpeed = aWavePropagationSpeed;
    emit updateRequested();
}

float WaterSurfaceCpu::wavePropagationSpeed() const
{
    return m_wavePropagationSpeed;
}

void WaterSurfaceCpu::setWaveHeightDampingOverTimeFactor( const float aWaveHeightDampingOverTimeFactor )
{
    if ( ! WaterSettings::isWaveHeightDampingOverTimeFactorValid( aWaveHeightDampingOverTimeFactor )
         || qFuzzyCompare( aWaveHeightDampingOverTimeFactor, m_waveHeightDampingOverTimeFactor ) )
    {
        return;
    }

    m_waveHeightDampingOverTimeFactor = aWaveHeightDampingOverTimeFactor;
    emit updateRequested();
}

float WaterSurfaceCpu::waveHeightDampingOverTimeFactor() const
{
    return m_waveHeightDampingOverTimeFactor;
}

void WaterSurfaceCpu::setWaveHeightClampingFactor( const float aWaveHeightClampingFactor )
{
    if ( ! WaterSettings::isWaveHeightClampingFactorValid( aWaveHeightClampingFactor )
         || qFuzzyCompare( aWaveHeightClampingFactor, m_waveHeightClampingFactor ) )
    {
        return;
    }

    m_waveHeightClampingFactor = aWaveHeightClampingFactor;
    emit updateRequested();
}

float WaterSurfaceCpu::waveHeightClampingFactor() const
{
    return m_waveHeightClampingFactor;
}

void WaterSurfaceCpu::rippleWaterAt( const int x, const int y )
{
    const QSize meshSize = this->meshSize();
    const int xVertexMaxIndex = meshSize.width();
    const int yVertexMaxIndex = meshSize.height();
    if ( x < 0
         || x > xVertexMaxIndex
         || y < 0
         || y > yVertexMaxIndex )
    {
        return;
    }

    QVector< GLfloat >& previousHeights = this->vertexPreviousHeights();
    const int xVerticesCount = this->verticesCount().width();
    const auto heightAlternator = [ xVerticesCount, & previousHeights ]( const float height,
                                                                         const int x,
                                                                         const int y )
    {
        previousHeights[ y * xVerticesCount + x ] += height;
    };

    m_waterRippler.rippleWaterAt( heightAlternator, x, y );

    emit updateRequested();
}

void WaterSurfaceCpu::propagateWater()
{
    if ( m_dtMsecs.isNull() )
    {
        m_dtSecs = 0.0;
        m_dtMsecs.start();
    }
    else
    {
        const int msecs = m_dtMsecs.elapsed();
        m_dtSecs = msecs > 0
                   ? msecs / 1000.0f
                   : 0.0f;
        m_dtMsecs.restart();
    }

    if ( qFuzzyIsNull( m_dtSecs ) )
    {
        return;
    }

    this->calcHeights();
    this->calcNormals();

    m_readFromFirstHeight = ! m_readFromFirstHeight;
    this->updateNormalsHeightsOpenGlData();

    emit updateRequested();
}

void WaterSurfaceCpu::calmWater()
{
    QVector< GLfloat >& vertexHeights = this->vertexPreviousHeights();

    const int heightsCount = vertexHeights.size();
    const int velocitiesCount = m_vertexVelocities.size();

    const int maxCount = qMax( velocitiesCount, heightsCount );
    for ( int i = 0; i < maxCount; ++ i )
    {
        if ( i < heightsCount )
        {
            vertexHeights[ i ] = 0.0f;
        }

        if ( i < velocitiesCount )
        {
            m_vertexVelocities[ i ] = 0.0f;
        }
    }

    emit updateRequested();
}

QList< GLuint > WaterSurfaceCpu::allGenericVertexAttributeIndices() const
{
    static const QList< GLuint > indices =
            QList< GLuint >()
            << GenericVertexAttributeIndexVertex2DPos
            << GenericVertexAttributeIndexVertexTexCoords
            << GenericVertexAttributeIndexVertexHeight
            << GenericVertexAttributeIndexVertexNormal;

    return indices;
}

void WaterSurfaceCpu::applyMeshSize()
{
    this->applyMeshSizeInternal();
}

void WaterSurfaceCpu::applyMeshSizeInternal()
{
    const QSize verticesCount = this->verticesCount();
    const GLuint xVerticesCount = verticesCount.width();
    const GLuint yVerticesCount = verticesCount.height();

    const int totalVerticesCount = xVerticesCount * yVerticesCount;
    m_vertex2DPositionsTexCoords.clear();
    m_vertex2DPositionsTexCoords.reserve( totalVerticesCount );
    m_vertexVelocities.fill( 0.0f, totalVerticesCount );
    m_vertexHeights0.fill( 0.0f, totalVerticesCount );
    m_vertexHeights1.fill( 0.0f, totalVerticesCount );
    m_vertexNormals.fill( QVector3D{}, totalVerticesCount );

    const QRectF meshRect = this->meshRect();
    const QMatrix4x4 m = GlUtilities::qtToOpenGlXYPlaneTransform();

    const QPointF bottomLeft = m.map( meshRect.bottomLeft() );
    const QPointF topRight = m.map( meshRect.topRight() );

    const float meshLeft = bottomLeft.x();
    const float meshWidth = topRight.x() - meshLeft;
    const float meshBottom = bottomLeft.y();
    const float meshHeight = topRight.y() - meshBottom;

    const GLuint xVertexIndexMax = xVerticesCount - 1;
    const GLuint yVertexIndexMax = yVerticesCount - 1;

    m_vertexIndices.clear();
    m_vertexIndices.reserve( xVertexIndexMax * yVertexIndexMax * 2 * Triangle3::s_verticesCount );

    for ( GLuint y = 0; y <= yVertexIndexMax; ++ y )
    {
        const float yPosRelative = static_cast< float >( y ) / yVertexIndexMax;
        const float yPos = meshBottom + yPosRelative * meshHeight;
        const bool canCreateYIndex = y < yVertexIndexMax;

        for ( GLuint x = 0; x <= xVertexIndexMax; ++ x )
        {
            const float xPosRelative = static_cast< float >( x ) / xVertexIndexMax;
            const float xPos = meshLeft + xPosRelative * meshWidth;
            m_vertex2DPositionsTexCoords.append(
                        Vertex2DPosTexCoordPair{
                            QVector2D{ xPos, yPos },
                            QVector2D{ xPosRelative, 1.0f - yPosRelative } // flipped Y-axis
                        } );

            if ( canCreateYIndex && x < xVertexIndexMax )
            {
                const GLuint bottomLeftIndex  = ( y + 0u ) * xVerticesCount + ( x + 0u );
                const GLuint bottomRightIndex = ( y + 0u ) * xVerticesCount + ( x + 1u );
                const GLuint topRightIndex    = ( y + 1u ) * xVerticesCount + ( x + 1u );
                const GLuint topLeftIndex     = ( y + 1u ) * xVerticesCount + ( x + 0u );

                m_vertexIndices
                        // face 1
                        << bottomLeftIndex
                        << bottomRightIndex
                        << topRightIndex

                        // face 2
                        << bottomLeftIndex
                        << topRightIndex
                        << topLeftIndex;
            }
        }
    }

    this->calcNormals();

    this->updateMeshSizeOpenGlData();
}

void WaterSurfaceCpu::updateMeshSizeOpenGlData()
{
    if (    ! CommonEntities::isUnsignedGlValueValid( m_vbos[ VboVertexIndices ] )
         || ! CommonEntities::isUnsignedGlValueValid( m_vbos[ VboVertex2DPosTexCoords ] )
         || ! CommonEntities::isUnsignedGlValueValid( m_vbos[ VboVertexHeights ] )
         || ! CommonEntities::isUnsignedGlValueValid( m_vbos[ VboVertexNormals ] ) )
    {
        return;
    }

    OpenGlObjectBinder vertexIndicesBinder = this->vertexIndicesBufferBinder( false );
    vertexIndicesBinder.bind();
    m_openGlFunctions->glBufferData( GL_ELEMENT_ARRAY_BUFFER,
                                     m_vertexIndices.size() * sizeof( GLuint ),
                                     m_vertexIndices.constData(),
                                     GL_STATIC_DRAW );
    LOG_OPENGL_ERROR();
    vertexIndicesBinder.unbind();

    OpenGlObjectBinder vertex2DPosBufferBinder = this->vertex2DPosTexCoordsBufferBinder( false );
    vertex2DPosBufferBinder.bind();
    m_openGlFunctions->glBufferData( GL_ARRAY_BUFFER,
                                     m_vertex2DPositionsTexCoords.size() * sizeof( Vertex2DPosTexCoordPair ),
                                     m_vertex2DPositionsTexCoords.constData(),
                                     GL_STREAM_DRAW );
    LOG_OPENGL_ERROR();
    vertex2DPosBufferBinder.unbind();

    OpenGlObjectBinder vertexHeightsBufferBinder = this->vertexHeightsBufferBinder( false );
    vertexHeightsBufferBinder.bind();

    const QVector< GLfloat >& newHeights = this->vertexNewHeights();
    m_openGlFunctions->glBufferData( GL_ARRAY_BUFFER,
                                     newHeights.size() * sizeof( GLfloat ),
                                     newHeights.constData(),
                                     GL_STREAM_DRAW );
    LOG_OPENGL_ERROR();
    vertexHeightsBufferBinder.unbind();

    OpenGlObjectBinder vertexNormalsBufferBinder = this->vertexNormalsBufferBinder( false );
    vertexNormalsBufferBinder.bind();
    m_openGlFunctions->glBufferData( GL_ARRAY_BUFFER,
                                     m_vertexNormals.size() * sizeof( QVector3D ),
                                     m_vertexNormals.constData(),
                                     GL_STATIC_DRAW );
    LOG_OPENGL_ERROR();
    vertexNormalsBufferBinder.unbind();
}

void WaterSurfaceCpu::updateNormalsHeightsOpenGlData()
{
    if (    ! CommonEntities::isUnsignedGlValueValid( m_vbos[ VboVertexHeights ] )
         || ! CommonEntities::isUnsignedGlValueValid( m_vbos[ VboVertexNormals ] ) )
    {
        return;
    }

    OpenGlObjectBinder vertexHeightsBufferBinder = this->vertexHeightsBufferBinder( false );
    vertexHeightsBufferBinder.bind();

    const QVector< GLfloat >& newHeights = this->vertexNewHeights();
    m_openGlFunctions->glBufferSubData( GL_ARRAY_BUFFER,
                                        0,
                                        newHeights.size() * sizeof( GLfloat ),
                                        newHeights.constData() );
    LOG_OPENGL_ERROR();
    vertexHeightsBufferBinder.unbind();

    OpenGlObjectBinder vertexNormalsBufferBinder = this->vertexNormalsBufferBinder( false );
    vertexNormalsBufferBinder.bind();
    m_openGlFunctions->glBufferSubData( GL_ARRAY_BUFFER,
                                        0,
                                        m_vertexNormals.size() * sizeof( QVector3D ),
                                        m_vertexNormals.constData() );
    LOG_OPENGL_ERROR();
    vertexNormalsBufferBinder.unbind();
}

void WaterSurfaceCpu::applyLight()
{
    this->applyLightInternal();
}

void WaterSurfaceCpu::applyLightInternal()
{
    if ( ! m_shaderProgram.isValid() )
    {
        return;
    }

    m_shaderProgram.activate();

    const Light& light = this->light();
    const QColor& lightColor = light.color;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexLightColor ),
                                    lightColor.redF(),
                                    lightColor.greenF(),
                                    lightColor.blueF() );
    LOG_OPENGL_ERROR();

    const QVector3D& lightPos = light.pos;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexLightPos ),
                                    lightPos.x(),
                                    lightPos.y(),
                                    lightPos.z() );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexLightAttenuationConstant ),
                                    light.attenuation.constant );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexLightAttenuationLinear ),
                                    light.attenuation.linear );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexLightAttenuationQuadratic ),
                                    light.attenuation.quadratic );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexLightIntensity ),
                                    light.intensity );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();
}

void WaterSurfaceCpu::applyMaterial()
{
    this->applyMaterialInternal();
}

void WaterSurfaceCpu::applyMaterialInternal()
{
    if ( ! m_shaderProgram.isValid() )
    {
        return;
    }

    m_shaderProgram.activate();

    const Material& material = this->material();
    const QColor& color = material.color;
    m_openGlFunctions->glUniform4f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialColor ),
                                    color.redF(),
                                    color.greenF(),
                                    color.blueF(),
                                    color.alphaF() );
    LOG_OPENGL_ERROR();

    const QVector3D& emissiveReflectance = material.reflectance.emissive;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialReflectanceEmissive ),
                                    emissiveReflectance.x(),
                                    emissiveReflectance.y(),
                                    emissiveReflectance.z() );
    LOG_OPENGL_ERROR();

    const QVector3D& ambientReflectance = material.reflectance.ambient;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialReflectanceAmbient ),
                                    ambientReflectance.x(),
                                    ambientReflectance.y(),
                                    ambientReflectance.z() );
    LOG_OPENGL_ERROR();

    const QVector3D& diffuseReflectance = material.reflectance.diffuse;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialReflectanceDiffuse ),
                                    diffuseReflectance.x(),
                                    diffuseReflectance.y(),
                                    diffuseReflectance.z() );
    LOG_OPENGL_ERROR();

    const QVector3D& specularReflectance = material.reflectance.specular;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialReflectanceSpecular ),
                                    specularReflectance.x(),
                                    specularReflectance.y(),
                                    specularReflectance.z() );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialShininess ), material.shininess );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();
}

void WaterSurfaceCpu::applyObtainsColorFromTextureInternal()
{
    m_shaderProgram.setUniformVariable1i(
                this->uniformVariableCachedLocation( UniformVariableIndexObtainsColorFromTexture ),
                this->obtainsColorFromTexture()
                ? GL_TRUE
                : GL_FALSE );
}

void WaterSurfaceCpu::applyObtainsColorFromTexture()
{
    this->applyObtainsColorFromTextureInternal();
}

void WaterSurfaceCpu::applyPartialShaderData()
{
    this->applyMatrixInternal( CommonEntities::MatrixType::Model );
    this->applyMatrixInternal( CommonEntities::MatrixType::View );
    this->applyMatrixInternal( CommonEntities::MatrixType::Projection );
    this->applyLightInternal();
    this->applyMaterialInternal();
    this->applyObtainsColorFromTextureInternal();
}

QByteArray WaterSurfaceCpu::uniformVariableName( const int index )
{
    switch ( index )
    {
        case UniformVariableIndexMaterialColor:
            return "material.color";
        case UniformVariableIndexMaterialReflectanceEmissive:
            return "material.re";
        case UniformVariableIndexMaterialReflectanceAmbient:
            return "material.ra";
        case UniformVariableIndexMaterialReflectanceDiffuse:
            return "material.rd";
        case UniformVariableIndexMaterialReflectanceSpecular:
            return "material.rs";
        case UniformVariableIndexMaterialShininess:
            return "material.shininess";
        case UniformVariableIndexLightColor:
            return "light.color";
        case UniformVariableIndexLightPos:
            return "light.pos";
        case UniformVariableIndexLightAttenuationConstant:
            return "light.kc";
        case UniformVariableIndexLightAttenuationLinear:
            return "light.kl";
        case UniformVariableIndexLightAttenuationQuadratic:
            return "light.kq";
        case UniformVariableIndexLightIntensity:
            return "light.intensity";
        case UniformVariableIndexModelMatrix:
            return "modelMatrix";
        case UniformVariableIndexNormalMatrix:
            return "normalMatrix";
        case UniformVariableIndexViewMatrix:
            return "viewMatrix";
        case UniformVariableIndexProjectionMatrix:
            return "projectionMatrix";
        case UniformVariableIndexEyePos:
            return "eyePos";
        case UniformVariableIndexObtainsColorFromTexture:
            return "obtainsColorFromTexture";
        case UniformVariableIndexColorTexSampler:
            return "colorTexSampler";
    }

    return QByteArray();
}

bool WaterSurfaceCpu::isUniformVariableIndexValid( const int index )
{
    return 0 <= index && index < UniformVariablesCount;
}

GLint WaterSurfaceCpu::uniformVariableLocation( const int index ) const
{
    if ( ! WaterSurfaceCpu::isUniformVariableIndexValid( index )
         || ! m_shaderProgram.isValid() )
    {
        return CommonEntities::s_invalidSignedGlValue;
    }

    const QByteArray uniformVariableName = WaterSurfaceCpu::uniformVariableName( index );
    return uniformVariableName.isEmpty()
           ? CommonEntities::s_invalidSignedGlValue
           : m_shaderProgram.uniformVarLocation( uniformVariableName );
}

void WaterSurfaceCpu::cacheUniformVariableLocations()
{
    m_uniformVariableCachedLocations.clear();
    m_uniformVariableCachedLocations.reserve( UniformVariablesCount );
    for ( int i = 0; i < UniformVariablesCount; ++ i )
    {
        m_uniformVariableCachedLocations.insert( i, this->uniformVariableLocation( i ) );
    }
}

GLint WaterSurfaceCpu::uniformVariableCachedLocation( const int index ) const
{
    return m_uniformVariableCachedLocations.value( index, CommonEntities::s_invalidSignedGlValue );
}
