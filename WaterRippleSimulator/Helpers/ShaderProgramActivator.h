#ifndef WATERRIPPLESIMULATOR_HELPERS_SHADERPROGRAMACTIVATOR_H
#define WATERRIPPLESIMULATOR_HELPERS_SHADERPROGRAMACTIVATOR_H

#include <QSurfaceFormat>
#include <QtGlobal>

#include <gl/GL.h>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

class ShaderProgramActivator
{
public:
    ShaderProgramActivator();
    ShaderProgramActivator(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
            const QSurfaceFormat& aOpenGlFormat,
            const GLuint aShaderProgramId );
    ~ShaderProgramActivator();

    bool isValid() const;

    GLuint activeShaderProgramId() const;

    void activate();
    void deactivate();

private:
    QOpenGLFunctions_3_3_Core* m_openGlFunctions;
    QSurfaceFormat m_openGlFormat;
    GLuint m_shaderProgramId;
    GLuint m_previousShaderProgramId;
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_SHADERPROGRAMACTIVATOR_H
