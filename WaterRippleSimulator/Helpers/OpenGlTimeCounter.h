#ifndef WATERRIPPLESIMULATOR_HELPERS_OPENGLTIMECOUNTER_H
#define WATERRIPPLESIMULATOR_HELPERS_OPENGLTIMECOUNTER_H

#include <QOpenGLContext>
#include <QtGlobal>
#include <QVector>

#include <gl/GL.h>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

class OpenGlTimeCounter
{
public:
    explicit OpenGlTimeCounter( QOpenGLFunctions_3_3_Core* const aOpenGlFunctions );
    ~OpenGlTimeCounter();

    void start();
    void stop();
    bool isRunning() const;

    GLuint64 timeNanosecs() const;
    qreal timeMillisecs() const;

    void dumpTime() const;

private:
    enum TimerQueryIndex
    {
        TimerQueryIndexStart = 0,
        TimerQueryIndexEnd   = 1,
        TimerQueriesCount    = 2
    };

private:
    QOpenGLFunctions_3_3_Core* m_openGlFunctions;
    QVector< GLuint >          m_timerQueries;
    GLuint64                   m_timeNanosecs;
    bool                       m_isRunning;
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_OPENGLTIMECOUNTER_H
