#include "WaterRippler.h"

#include "../Data/ShaderInfo.h"
#include "../Data/WaterSettings.h"
#include "../Utilities/GeneralUtilities.h"
#include "../Utilities/GlUtilities.h"
#include "../Utilities/ImageUtilities.h"
#include "../Utilities/MathUtilities.h"
#include "../WaterRippleSimulator_namespace.h"
#include "ClearColorAlternator.h"
#include "OpenGlCapabilityEnabler.h"
#include "OpenGlErrorLogger.h"
#include "OpenGlObjectBinder.h"
#include "ViewportAlternator.h"

#include <QDateTime>
#include <QMatrix4x4>
#include <QOpenGLFunctions_3_3_Core>

#include <cstdlib>
#include <limits>

using namespace WaterRippleSimulator::Helpers;

using namespace WaterRippleSimulator::Utilities;
using namespace WaterRippleSimulator;

const QPoint WaterRippler::s_noWaterRippleScheduledPt{ -1, -1 };
const QColor WaterRippler::s_clearColor{ 0, 0, 0, 0 };

WaterRippler::WaterRippler(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        const QSurfaceFormat& aOpenGlFormat,

        const WaterSettings::WaterRippleMethod aMethod,
        const float aMagnitude,
        const QVector2D& aRadiusFactor,
        const float aRotationAngle,

        QObject* const parent )
    : QObject{ parent }
    , AbstractOpenGlClient{ aOpenGlFunctions }
    , m_shaderProgram{ aOpenGlFunctions, aOpenGlFormat }
    , m_fboId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_vaoId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_rippledHeightTextureId{ CommonEntities::s_invalidUnsignedGlValue }

    , m_method{ WaterSettings::s_waterRippleMethodDefault }
    , m_magnitude{ WaterSettings::s_waterRippleMagnitudeDefault }
    , m_radiusFactor{ WaterSettings::s_waterRipple2DRadiusFactorDefault }
    , m_rotationAngle{ WaterSettings::s_waterRippleRotationAngleDefault }

    , m_radius{ 0.0f, 0.0f }
{
    this->setMethod( aMethod, false );
    this->setMagnitude( aMagnitude, false );
    this->setRadiusFactor( aRadiusFactor );
    this->setRotationAngle( aRotationAngle, false );
    this->updateOpenGlData();
}

WaterRippler::~WaterRippler()
{
    this->disposeOpenGlResources();
}

void WaterRippler::setMethod( const WaterSettings::WaterRippleMethod aMethod )
{
    if ( this->setMethod( aMethod, true ) )
    {
        emit updateRequested();
    }
}

WaterSettings::WaterRippleMethod WaterRippler::method() const
{
    return m_method;
}

void WaterRippler::setRadiusFactor( const QVector2D& aRadiusFactor )
{
    if ( ! WaterSettings::isWaterRippleRadiusFactorValid( aRadiusFactor )
         || qFuzzyCompare( aRadiusFactor, m_radiusFactor ) )
    {
        return;
    }

    m_radiusFactor = aRadiusFactor;
}

QVector2D WaterRippler::radiusFactor() const
{
    return m_radiusFactor;
}

void WaterRippler::setRotationAngle( const float aRotationAngle )
{
    if ( this->setRotationAngle( aRotationAngle, true ) )
    {
        emit updateRequested();
    }
}

float WaterRippler::rotationAngle() const
{
    return m_rotationAngle;
}

void WaterRippler::setMagnitude( const float aMagnitude )
{
    if ( this->setMagnitude( aMagnitude, true ) )
    {
        emit updateRequested();
    }
}

float WaterRippler::magnitude() const
{
    return m_magnitude;
}

void WaterRippler::disposeOpenGlResources()
{
    if ( CommonEntities::isUnsignedGlValueValid( m_fboId ) )
    {
        m_openGlFunctions->glDeleteFramebuffers( 1, & m_fboId );
        LOG_OPENGL_ERROR();

        m_fboId = CommonEntities::s_invalidUnsignedGlValue;
    }

    if ( CommonEntities::isUnsignedGlValueValid( m_vaoId ) )
    {
        m_openGlFunctions->glDeleteVertexArrays( 1, & m_vaoId );
        LOG_OPENGL_ERROR();

        m_vaoId = CommonEntities::s_invalidUnsignedGlValue;
    }

    if ( CommonEntities::isUnsignedGlValueValid( m_rippledHeightTextureId ) )
    {
        m_openGlFunctions->glDeleteTextures( 1, & m_rippledHeightTextureId );
        LOG_OPENGL_ERROR();

        m_rippledHeightTextureId = CommonEntities::s_invalidUnsignedGlValue;
    }
}

float WaterRippler::randomPositiveNormalizedRealValue()
{
    const QDateTime currentDateTime = QDateTime::currentDateTime();
    const qint64 msecsSinceEpoch = currentDateTime.toMSecsSinceEpoch();
    qsrand( msecsSinceEpoch % static_cast< qint64 >( std::numeric_limits< uint >::max() ) );

    return static_cast< float >( qrand() ) / RAND_MAX;
}

FramebufferBinder WaterRippler::fboBinder( const bool autoUnbind ) const
{
    return AbstractOpenGlClient::fboBinder(
                m_fboId,
                FramebufferBinder::BindingTarget::Both,
                autoUnbind );
}

void WaterRippler::setupFbo()
{
    if ( m_openGlFunctions == nullptr
         || ! CommonEntities::isUnsignedGlValueValid( m_fboId )
         || ! CommonEntities::isUnsignedGlValueValid( m_rippledHeightTextureId )
         || m_textureSize.isEmpty() ) // texture size ( 1, 1 ) and above is allowed
    {
        return;
    }

    FramebufferBinder fboBinder = this->fboBinder( true );
    fboBinder.bind();

    const GLint mipmapLevel = 0;
    m_openGlFunctions->glFramebufferTexture2D(
                GL_FRAMEBUFFER,
                GL_COLOR_ATTACHMENT0,
                GL_TEXTURE_2D,
                m_rippledHeightTextureId,
                mipmapLevel );
    LOG_OPENGL_ERROR();

    const GLenum fboStatus = m_openGlFunctions->glCheckFramebufferStatus( GL_FRAMEBUFFER );
    LOG_OPENGL_ERROR();
    if ( fboStatus != GL_FRAMEBUFFER_COMPLETE )
    {
        qDebug().nospace().noquote() << QT_STRINGIFY2( WaterRippler ) << ": framebuffer not complete!";
    }

    /* void glDrawBuffers(GLsizei n, const GLenum *buffers);
     * ---
     * Selects the color buffers enabled for writing or clearing and disables
     * buffers enabled by previous calls to glDrawBuffer() or glDrawBuffers().
     * More than one buffer may be enabled at one time. The value of mode can
     * be one of the following:
     *
     * GL_FRONT GL_FRONT_LEFT GL_NONE
     * GL_BACK GL_FRONT_RIGHT GL_FRONT_AND_BACK
     * GL_LEFT GL_BACK_LEFT GL_COLOR_ATTACHMENTi
     * GL_RIGHT GL_BACK_RIGHT
     *
     * If mode, or the entries in buffers is not one of the above, a
     * GL_INVALID_ENUM error is generated. Additionally, if a framebuffer     <-----
     * object is bound that is not the default framebuffer, then only GL_NONE
     * and GL_COLOR_ATTACHMENTi are accepted, otherwise a
     * GL_INVALID_ENUM error is generated.
     */

    const GLenum drawBuffer = GL_COLOR_ATTACHMENT0;
    m_openGlFunctions->glDrawBuffers( 1, & drawBuffer );
    LOG_OPENGL_ERROR();
}

void WaterRippler::init()
{
    if ( ! m_uniformVariableCachedLocations.isEmpty() )
    {
        return;
    }

    m_openGlFunctions->glGenTextures( 1, & m_rippledHeightTextureId );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glGenVertexArrays( 1, & m_vaoId );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glGenFramebuffers( 1, & m_fboId );
    LOG_OPENGL_ERROR();

    const bool isBuiltOk = GlUtilities::buildShaderProgram(
                               m_shaderProgram,
                               ":/Resources/shaders/WaterDataCalculator.vert",
                               ":/Resources/shaders/WaterRippler.frag" );
    if ( ! isBuiltOk )
    {
        this->disposeOpenGlResources();
        return;
    }

    this->cacheUniformVariableLocations();

    m_shaderProgram.activate();

    this->updateOpenGlData();
    this->setupFbo();

    m_openGlFunctions->glUniform1i( this->uniformVariableCachedLocation( UniformVariableIndexOrgHeightTexSampler ), 0 );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();
}

void WaterRippler::rippleWaterAtRandomPoint(
        const GLuint orgHeightTextureId,
        const bool modifySource )
{
    this->rippleWaterAt( orgHeightTextureId,
                         modifySource,
                         WaterRippler::randomPositiveNormalizedRealValue(),
                         WaterRippler::randomPositiveNormalizedRealValue() );
}

void WaterRippler::rippleWaterAt(
        const GLuint orgHeightTextureId,
        const bool modifySource,
        const QPoint& pt )
{
    this->rippleWaterAt(
                orgHeightTextureId,
                modifySource,
                pt.x(), pt.y() );
}

void WaterRippler::rippleWaterAt(
        const GLuint orgHeightTextureId,
        const bool modifySource,
        const int x,
        const int y )
{
    if ( ! CommonEntities::isUnsignedGlValueValid( orgHeightTextureId )
         || ! CommonEntities::isUnsignedGlValueValid( m_fboId )
         || ! CommonEntities::isUnsignedGlValueValid( m_vaoId )
         || ! CommonEntities::isUnsignedGlValueValid( m_rippledHeightTextureId )
         || ! m_shaderProgram.isValid() )
    {
        return;
    }

    const QSize newTextureSize = GlUtilities::texture2DSize( m_openGlFunctions, orgHeightTextureId );
    if ( newTextureSize.isEmpty() )
    {
        return;
    }

    const bool shouldSetupFbo = ! m_textureSize.isValid();
    this->setTextureSize( newTextureSize );
    if ( shouldSetupFbo )
    {
        this->setupFbo();
    }

    FramebufferBinder fboBinder = this->fboBinder( false );
    fboBinder.bind();

    ClearColorAlternator clearColorAlternator{ m_openGlFunctions,
                                               s_clearColor,
                                               false };
    clearColorAlternator.change();

    m_openGlFunctions->glClear( GL_COLOR_BUFFER_BIT );
    LOG_OPENGL_ERROR();

    const QRect viewport{ QPoint{ 0,0 }, newTextureSize };
    ViewportAlternator vpAlternator{ m_openGlFunctions, viewport, false };
    vpAlternator.change();

    m_shaderProgram.activate();

    OpenGlCapabilityEnabler faceCullingEnabler{ m_openGlFunctions, GL_CULL_FACE, false, false };
    faceCullingEnabler.change();

    OpenGlCapabilityEnabler depthTestEnabler{ m_openGlFunctions, GL_DEPTH_TEST, false, false };
    depthTestEnabler.change();

    OpenGlObjectBinder vaoBinder{ m_openGlFunctions,
                                  OpenGlObjectBinder::ObjectType::VertexArray,
                                  CommonEntities::invalidEnumGlValue(),
                                  m_vaoId,
                                  false };
    vaoBinder.bind();

    OpenGlObjectBinder readTextureUnitBinder{ m_openGlFunctions,
                                              OpenGlObjectBinder::ObjectType::TextureUnit,
                                              GL_TEXTURE0,
                                              CommonEntities::s_invalidUnsignedGlValue,
                                              false };
    readTextureUnitBinder.bind();

    OpenGlObjectBinder readTextureBinder{ m_openGlFunctions,
                                          OpenGlObjectBinder::ObjectType::Texture,
                                          GL_TEXTURE_2D,
                                          orgHeightTextureId,
                                          false };
    readTextureBinder.bind();

    this->setEpicenter( QPoint{ x, y } );
    this->setRadius( m_radiusFactor * qMin( newTextureSize.width(), newTextureSize.height() ) );

    m_openGlFunctions->glDrawArrays( GL_TRIANGLE_FAN,
                                     0,
                                     4 );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();

    vaoBinder.unbind();

    if ( modifySource )
    {
        // https://www.opengl.org/wiki/Texture_Storage#Texture_copy
        // Framebuffer copy creation
        // When creating storage for a texture, you can also get the pixel data from the Framebuffer currently bound to the GL_READ_FRAMEBUFFER​ target.
        // It will use the current read buffer of that framebuffer (for color reads), so make sure to use glReadBuffer​ to set it properly beforehand.
        // Also, the framebuffer must be complete.

        // These functions act like a combination of glReadPixels​ followed by glTexImage*​ for the appropriate type.
        // Since framebuffers are two-dimensional, only 1D and 2D copy creation are allowed:

        // void glCopyTexImage1D​(GLenum target​, GLint level​, GLenum internalformat​, GLint x​, GLint y​, GLsizei width​, GLint border​);
        // void glCopyTexImage2D​(GLenum target​, GLint level​, GLenum internalformat​, GLint x​, GLint y​, GLsizei width​, GLsizei height​, GLint border​);
        // The width​ and height​ define the size of the given mipmap level​. They also define the size of the region taken from the framebuffer.
        // x​ and y​ define the bottom-left corner where the read starts (remember: OpenGL puts the origin of the framebuffer at the bottom-left).

        // Note: Copies to multisample textures are not allowed. This is because glReadPixels​ forces a multisample resolve.
        // You can copy from multisample images, but this will do a resolve.
        // If you want to preserve the sample count, you must blit it into an already existing multisample texture's storage.
        // Which buffer is copied from depends on the type of internalformat​.
        // If internalformat​ is a color format, then the current read buffer specified by glReadBuffer​ is used.
        // If internalformat​ has a depth component, the depth buffer is used (an error occurs if there is no depth buffer).
        // If it has a stencil component, the stencil buffer is used (an error occurs if there is no stencil buffer).
        // If internalformat​ has both depth and stencil, then both are used as the source.

        const GLenum buffer = GL_COLOR_ATTACHMENT0;
        m_openGlFunctions->glReadBuffer( buffer );
        LOG_OPENGL_ERROR();

        // the last bound texture texture is the original unrippled one
        m_openGlFunctions->glCopyTexImage2D(
                    GL_TEXTURE_2D,
                    0,
                    GL_R32F,
                    viewport.x(), viewport.y(),
                    viewport.width(), viewport.height(),
                    0 );
        LOG_OPENGL_ERROR();

        m_openGlFunctions->glDrawBuffers( 1, & buffer );
        LOG_OPENGL_ERROR();
    }

    readTextureBinder.unbind();
    readTextureUnitBinder.unbind();

    fboBinder.unbind();
    vpAlternator.rollback();
    faceCullingEnabler.rollback();
    depthTestEnabler.rollback();
    clearColorAlternator.rollback();
}

void WaterRippler::rippleWaterAt(
        const GLuint orgHeightTextureId,
        const bool modifySource,
        const float absX,
        const float absY )
{
    if ( ! MathUtilities::fuzzyContainedInRegion( 0.0f, absX, 1.0f )
         || ! MathUtilities::fuzzyContainedInRegion( 0.0f, absY, 1.0f ) )
    {
        return;
    }

    const QSize texSize = GlUtilities::texture2DSize( m_openGlFunctions, orgHeightTextureId );
    if ( texSize.isEmpty() )
    {
        return;
    }

    this->rippleWaterAt( orgHeightTextureId,
                         modifySource,
                         absX * texSize.width(),
                         absY * texSize.height() );
}

GLuint WaterRippler::rippledHeightTextureId() const
{
    return m_rippledHeightTextureId;
}

QByteArray WaterRippler::uniformVariableName( const int index )
{
    switch ( index )
    {
        case UniformVariableIndexOrgHeightTexSampler:
            return "orgHeightTexSampler";
        case UniformVariableIndexOrgHeightTexMaxIndices:
            return "orgHeightTexMaxIndices";

        case UniformVariableIndexMethod:
            return "method";
        case UniformVariableIndexMagnitude:
            return "magnitude";
        case UniformVariableIndexRadius:
            return "radius";
        case UniformVariableIndexRotationMatrix:
            return "rotationMatrix";

        case UniformVariableIndexEpicenter:
            return "epicenter";
        default:
            break;
    }

    return QByteArray();
}

bool WaterRippler::isUniformVariableIndexValid( const int index )
{
    return 0 <= index && index < UniformVariablesCount;
}

GLint WaterRippler::uniformVariableLocation( const int index ) const
{
    if ( ! WaterRippler::isUniformVariableIndexValid( index )
         || ! m_shaderProgram.isValid() )
    {
        return CommonEntities::s_invalidSignedGlValue;
    }

    const QByteArray uniformVariableName = WaterRippler::uniformVariableName( index );
    return uniformVariableName.isEmpty()
           ? CommonEntities::s_invalidSignedGlValue
           : m_shaderProgram.uniformVarLocation( uniformVariableName );
}

void WaterRippler::cacheUniformVariableLocations()
{
    m_uniformVariableCachedLocations.clear();
    m_uniformVariableCachedLocations.reserve( UniformVariablesCount );
    for ( int i = 0; i < UniformVariablesCount; ++ i )
    {
        m_uniformVariableCachedLocations.insert( i, this->uniformVariableLocation( i ) );
    }
}

GLint WaterRippler::uniformVariableCachedLocation( const int index ) const
{
    return m_uniformVariableCachedLocations.value( index, CommonEntities::s_invalidSignedGlValue );
}

void WaterRippler::setTextureSize( const QSize& aTextureSize )
{
    if ( ! aTextureSize.isValid()
         || aTextureSize == m_textureSize )
    {
        return;
    }

    m_textureSize = aTextureSize;
    this->updateTextureSizeOpenGlData();
}

void WaterRippler::updateTextureSizeOpenGlData()
{
    if ( ! CommonEntities::isUnsignedGlValueValid( m_rippledHeightTextureId )
         || ! m_shaderProgram.isValid()
         || ! m_textureSize.isValid() )
    {
        return;
    }

    const GLint textureMaxIndicesUniformVarLocation = this->uniformVariableCachedLocation( UniformVariableIndexOrgHeightTexMaxIndices );
    if ( ! CommonEntities::isSignedGlValueValid( textureMaxIndicesUniformVarLocation ) )
    {
        return;
    }

    const int w = m_textureSize.width();
    const int h = m_textureSize.height();

    // texture size ( 0, 0 ) is allowed
    if ( w < 0 || h < 0 )
    {
        return;
    }

    OpenGlObjectBinder writeTextureBinder = this->textureBinder( m_rippledHeightTextureId, false );
    writeTextureBinder.bind();

    GlUtilities::setTexture( m_openGlFunctions,
                             nullptr,
                             w,
                             h,
                             GL_R32F,
                             GL_RED,
                             GL_FLOAT,
                             GlUtilities::TextureFilter::Nearest,
                             GlUtilities::TextureFilter::Nearest,
                             GlUtilities::TextureWrapMode::ClampToEdge,
                             GlUtilities::TextureWrapMode::ClampToEdge,
                             false );
    writeTextureBinder.unbind();

    m_shaderProgram.setUniformVariable2i(
                textureMaxIndicesUniformVarLocation,
                w - 1,
                h - 1 );
}

bool WaterRippler::setMethod(
        const WaterSettings::WaterRippleMethod aMethod,
        const bool shouldUpdateOpenGlData )
{
    if ( ! WaterSettings::isWaterRippleMethodValid( aMethod )
         || aMethod == m_method )
    {
        return false;
    }

    m_method = aMethod;
    if ( shouldUpdateOpenGlData )
    {
        this->updateMethodOpenGlData();
    }

    return true;
}

void WaterRippler::updateMethodOpenGlData()
{
    m_shaderProgram.setUniformVariable1i(
                this->uniformVariableCachedLocation( UniformVariableIndexMethod ),
                static_cast< GLint >( m_method ) );
}

bool WaterRippler::setMagnitude( const float aMagnitude, const bool shouldUpdateOpenGlData )
{
    if ( ! WaterSettings::isWaterRippleMagnitudeValid( aMagnitude )
         || qFuzzyCompare( aMagnitude, m_magnitude ) )
    {
        return false;
    }

    m_magnitude = aMagnitude;
    if ( shouldUpdateOpenGlData )
    {
        this->updateMagnitudeOpenGlData();
    }

    return true;
}

void WaterRippler::updateMagnitudeOpenGlData()
{
    m_shaderProgram.setUniformVariable1f(
                this->uniformVariableCachedLocation( UniformVariableIndexMagnitude ),
                m_magnitude );
}

void WaterRippler::setEpicenter( const QPoint& aEpicenter )
{
    if ( aEpicenter == m_epicenter )
    {
        return;
    }

    m_epicenter = aEpicenter;
    this->updateEpicenterOpenGlData();
}

void WaterRippler::updateEpicenterOpenGlData()
{
    m_shaderProgram.setUniformVariable2i(
                this->uniformVariableCachedLocation( UniformVariableIndexEpicenter ),
                m_epicenter.x(),
                m_epicenter.y() );
}

void WaterRippler::setRadius( const QVector2D& aRadius )
{
    const float eps = std::numeric_limits< float >::epsilon();
    if ( aRadius.x() < eps
         || aRadius.y() < eps
         || qFuzzyCompare( aRadius, m_radius ) )
    {
        return;
    }

    m_radius = aRadius;
    this->updateRadiusOpenGlData();
}

void WaterRippler::updateRadiusOpenGlData()
{
    m_shaderProgram.setUniformVariable2f(
                this->uniformVariableCachedLocation( UniformVariableIndexRadius ),
                m_radius.x(),
                m_radius.y() );
}

bool WaterRippler::setRotationAngle( const float aRotationAngle, const bool shouldUpdateOpenGlData )
{
    if ( ! WaterSettings::isWaterRippleRotationAngleValid( aRotationAngle )
         || qFuzzyCompare( aRotationAngle, m_rotationAngle ) )
    {
        return false;
    }

    m_rotationAngle = aRotationAngle;
    if ( shouldUpdateOpenGlData )
    {
        this->updateRotationAngleOpenGlData();
    }

    return true;
}

void WaterRippler::updateRotationAngleOpenGlData()
{
    QMatrix4x4 m;
    m.rotate( - m_rotationAngle, 0.0f, 0.0f, 1.0f );

    m_shaderProgram.setUniformMatrix4fv(
                this->uniformVariableCachedLocation( UniformVariableIndexRotationMatrix ),
                1,
                GL_FALSE,
                m.data() );
}

void WaterRippler::updateOpenGlData()
{
    this->updateTextureSizeOpenGlData();

    this->updateMethodOpenGlData();
    this->updateMagnitudeOpenGlData();
    this->updateRadiusOpenGlData();
    this->updateRotationAngleOpenGlData();

    this->updateEpicenterOpenGlData();
}
