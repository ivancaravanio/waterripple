#ifndef WATERRIPPLESIMULATOR_HELPERS_WATERDATACALCULATOR_H
#define WATERRIPPLESIMULATOR_HELPERS_WATERDATACALCULATOR_H

#include "../Data/Light.h"
#include "../Data/Material.h"
#include "../Data/Triangle3.h"
#include "../Data/VertexData.h"
#include "../WaterRippleSimulator_namespace.h"
#include "AbstractOpenGlClient.h"
#include "FramebufferBinder.h"
#include "OpenGlObjectBinder.h"
#include "ShaderProgram.h"
#include "ViewportAlternator.h"
#include "WaterRippler.h"
#include "WaterRipplerCpu.h"

#include <QHash>
#include <QLinkedList>
#include <QList>
#include <QObject>
#include <QPair>
#include <QRectF>
#include <QSize>
#include <QtGlobal>
#include <QTime>
#include <QVector>
#include <QVector3D>

#include <gl/GL.h>

#define USE_GPU_ACCELERATED_RIPPLE

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

using namespace WaterRippleSimulator::Data;

class WaterDataCalculator : public QObject, public AbstractOpenGlClient
{
    Q_OBJECT

public:
    explicit WaterDataCalculator(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
            const QSurfaceFormat& aOpenGlFormat,
            const QSize& aMeshSize,
            QObject* const parent = nullptr );
    ~WaterDataCalculator() Q_DECL_OVERRIDE;

    void setMeshSize( const QSize& aMeshSize );
    QSize meshSize() const;
    QSize verticesCount() const;

    // wave propagation
    void setWavePropagationSpeed( const float aWavePropagationSpeed );
    float wavePropagationSpeed() const;

    void setWaveHeightDampingOverTimeFactor( const float aWaveHeightDampingOverTimeFactor );
    float waveHeightDampingOverTimeFactor() const;

    void setWaveHeightClampingFactor( const float aWaveHeightClampingFactor );
    float waveHeightClampingFactor() const;

    // water ripple
    void setWaterRippleMethod( const WaterSettings::WaterRippleMethod aWaterRippleMethod );
    WaterSettings::WaterRippleMethod waterRippleMethod() const;

    void setWaterRippleMagnitude( const float aWaterRippleMagnitude );
    float waterRippleMagnitude() const;

    void setWaterRippleRadiusFactor( const QVector2D& aWaterRippleRadiusFactor );
    QVector2D waterRippleRadiusFactor() const;

    void setWaterRippleRotationAngle( const float aWaterRippleRotationAngle );
    float waterRippleRotationAngle() const;

    void init() Q_DECL_OVERRIDE;

    void propagateWater();
    void stopWaterPropagation();
    bool isPropagatingWater() const;

    void rippleWaterAtRandomPoint();
    void rippleWaterAt( const int x, const int y );
    void calmWater();

    GLuint heightTextureId() const;

signals:
    void updateRequested();

private:
    // should be the same as location
    enum FragmentOutputVariableIndex
    {
        FragmentOutputVariableIndexInvalid  = -1,
        FragmentOutputVariableIndexHeight   = 0,
        FragmentOutputVariableIndexVelocity = 1,
        FragmentOutputVariablesCount        = 2
    };

    enum UniformVariableIndex
    {
        UniformVariableIndexInvalid               = -1,
        UniformVariableIndexOldHeightTexSampler   = 0,
        UniformVariableIndexOldVelocityTexSampler = 1,
        UniformVariableIndexTimeDifference        = 2,
        UniformVariableIndexSpeed                 = 3,
        UniformVariableIndexColumnWidth           = 4,
        UniformVariableIndexVelocityScale         = 5,
        UniformVariableIndexColumnHeightMaxSlope  = 6,
        UniformVariableIndexMeshVertexMaxIndices  = 7,
        UniformVariablesCount                     = 8
    };

    enum TextureIndex
    {
        TextureIndexHeight0   = 0,
        TextureIndexVelocity0 = 1,
        TextureIndexHeight1   = 2,
        TextureIndexVelocity1 = 3,
        TexturesCount         = 4
    };

private:
    static QByteArray uniformVariableName( const int index );
    static bool isUniformVariableIndexValid( const int index );
    GLint uniformVariableLocation( const int index ) const;
    void cacheUniformVariableLocations();
    GLint uniformVariableCachedLocation( const int index ) const;

    bool setMeshSizeInternal( const QSize& aMeshSize, const bool shouldUpdateOpenGlData );
    void updateMeshOpenGlData();

    void applyWavePropagationSpeed();
    void applyWaveHeightDampingOverTimeFactor();
    void applyWaveHeightClampingFactor();

    void updateOpenGlData();

    void disposeOpenGlResources();
    bool isWaterRippleScheduled() const;

    GLuint oldHeightTextureId() const;
    GLuint newHeightTextureId() const;
    GLuint oldVelocityTextureId() const;
    GLuint newVelocityTextureId() const;

    FramebufferBinder fboBinder( const bool autoUnbind ) const;
    ViewportAlternator viewportAlternator( const bool autoRollback ) const;
    OpenGlObjectBinder vaoBinder( const bool autoUnbind = true ) const;
    OpenGlObjectBinder textureUnitBinder( const GLenum textureUnit, const bool autoUnbind = true ) const;
    OpenGlObjectBinder textureBinder( const GLuint textureId, const bool autoUnbind = true ) const;

private:
    QSize m_meshSize;

    ShaderProgram m_shaderProgram;
    ShaderProgram m_waterCalmerProgram;

    QVector< GLuint > m_textures;
    GLuint m_fboId;
    GLuint m_vaoId;

    // 2: all shaders common variable - per vertex and per fragment invariant
    QHash< int, GLint > m_uniformVariableCachedLocations;
    bool m_useH1V1AsDrawBuffers;
    QLinkedList< QPoint > m_scheduledWaterRipplePoints;
    bool m_isWaterCalmScheduled;

    bool m_isPropagatingWater;
    float m_wavePropagationSpeed;
    float m_waveHeightDampingOverTimeFactor;
    float m_waveHeightClampingFactor;

    WaterRipplerCpu m_waterRipplerCpu;
    WaterRippler    m_waterRippler;

    QTime m_waterPropagationDeltaTime;
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_WATERDATACALCULATOR_H
