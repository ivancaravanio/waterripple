#include "OpenGlErrorLogger.h"

#include "../WaterRippleSimulator_namespace.h"

#include <QDebug>
#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Helpers;

OpenGlErrorLogger::OpenGlErrorLogger()
    : m_openGlFunctions( 0 )
{
}

OpenGlErrorLogger::~OpenGlErrorLogger()
{
}

OpenGlErrorLogger& OpenGlErrorLogger::instance()
{
    static OpenGlErrorLogger logger;
    return logger;
}

void OpenGlErrorLogger::setOpenGlFunctions( QOpenGLFunctions_3_3_Core* const aOpenGlFunctions )
{
    m_openGlFunctions = aOpenGlFunctions;
}

QOpenGLFunctions_3_3_Core* OpenGlErrorLogger::openGlFunctions() const
{
    return m_openGlFunctions;
}

void OpenGlErrorLogger::logOpenGlError( const QString& filePath, const int lineNumber ) const
{
    if ( m_openGlFunctions == nullptr )
    {
        return;
    }

    const GLenum openGlErrorCode = m_openGlFunctions->glGetError();
    OpenGlErrorLogger::logOpenGlError( openGlErrorCode, filePath, lineNumber );
}

void OpenGlErrorLogger::logOpenGlError(
        const GLenum openGlErrorCode,
        const QString& filePath,
        const int lineNumber )
{
    if ( ! OpenGlErrorLogger::isOpenGlErrorCodeValid( openGlErrorCode )
         || openGlErrorCode == GL_NO_ERROR )
    {
        return;
    }

    qDebug().noquote() << "OpenGL error:";
    if ( ! filePath.isEmpty() )
    {
        qDebug().noquote() << "\tfile:" << filePath;
    }

    if ( lineNumber >= 0 )
    {
        qDebug().noquote() << "\tline:" << lineNumber;
    }

    qDebug().noquote() << "\tcode:" << QString::number( openGlErrorCode, 16 ) << endl
                       << "\tdescription:" << OpenGlErrorLogger::errorDescription( openGlErrorCode );
}

bool OpenGlErrorLogger::isOpenGlErrorCodeValid( const GLenum openGlErrorCode )
{
    return openGlErrorCode == GL_INVALID_ENUM
           || openGlErrorCode == GL_INVALID_VALUE
           || openGlErrorCode == GL_INVALID_OPERATION
           || openGlErrorCode == GL_STACK_OVERFLOW
           || openGlErrorCode == GL_STACK_UNDERFLOW
           || openGlErrorCode == GL_OUT_OF_MEMORY;
}

QString OpenGlErrorLogger::errorDescription( const GLenum openGlErrorCode )
{
    switch ( openGlErrorCode )
    {
        case GL_INVALID_ENUM:
            return QT_STRINGIFY2( GL_INVALID_ENUM );
        case GL_INVALID_VALUE:
            return QT_STRINGIFY2( GL_INVALID_VALUE );
        case GL_INVALID_OPERATION:
            return QT_STRINGIFY2( GL_INVALID_OPERATION );
        case GL_STACK_OVERFLOW:
            return QT_STRINGIFY2( GL_STACK_OVERFLOW );
        case GL_STACK_UNDERFLOW:
            return QT_STRINGIFY2( GL_STACK_UNDERFLOW );
        case GL_OUT_OF_MEMORY:
            return QT_STRINGIFY2( GL_OUT_OF_MEMORY );
        default:
            break;
    }

    return QString();
}
