#ifndef WATERRIPPLESIMULATOR_HELPERS_WATERPERVERTEXDATACALCULATOR_H
#define WATERRIPPLESIMULATOR_HELPERS_WATERPERVERTEXDATACALCULATOR_H

#include "../WaterRippleSimulator_namespace.h"
#include "AbstractOpenGlClient.h"
#include "FramebufferBinder.h"
#include "OpenGlObjectBinder.h"
#include "ShaderProgram.h"
#include "ViewportAlternator.h"

#include <QHash>
#include <QObject>
#include <QSize>
#include <QtGlobal>
#include <QVector>

#include <gl/GL.h>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

using namespace WaterRippleSimulator::Data;

class WaterPerVertexDataCalculator : public QObject, public AbstractOpenGlClient
{
    Q_OBJECT

public:
    explicit WaterPerVertexDataCalculator(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
            const QSurfaceFormat& aOpenGlFormat,
            QObject* const parent = nullptr );
    ~WaterPerVertexDataCalculator() Q_DECL_OVERRIDE;

    void setVertexHeightsTextureId( const GLuint aVertexHeightsTextureId );
    GLuint vertexHeightsTextureId() const;

    void setMeshRect( const QRectF& aMeshRect );
    QRectF meshRect() const;

    void init() Q_DECL_OVERRIDE;
    void calc();

    GLuint vertexPosTextureId() const;
    GLuint vertexNormalsTextureId() const;
    GLuint vertexTextureCoordsTextureId() const;

signals:
    void updateRequested();

private:
    // should be the same as location
    enum FragmentOutputVariableIndex
    {
        FragmentOutputVariableIndexInvalid = -1,
        FragmentOutputVariableVertexPos    = 0,
        FragmentOutputVariableVertexNormal = 1,
        FragmentOutputVariablesCount       = 2
    };

    enum UniformVariableIndex
    {
        UniformVariableIndexInvalid             = -1,
        UniformVariableIndexHeightTexSampler    = 0,
        UniformVariableIndexHeightTexMaxIndices = 1,
        UniformVariableIndexMeshRectBottomLeft  = 2,
        UniformVariableIndexMeshRectTopRight    = 3,
        UniformVariablesCount                   = 4
    };

    enum TextureIndex
    {
        TextureIndexVertexInvalid = -1,
        TextureIndexVertexPos     = 0,
        TextureIndexVertexNormals = 1,
        TexturesCount             = 2
    };

private:
    static QByteArray uniformVariableName( const int index );
    static bool isUniformVariableIndexValid( const int index );
    GLint uniformVariableLocation( const int index ) const;
    void cacheUniformVariableLocations();
    GLint uniformVariableCachedLocation( const int index ) const;

    bool setMeshRectInternal( const QRectF& aMeshRect );
    void updateMeshRectOpenGlData();
    void setupFbo();

    void setVerticesCount( const QSize& aVerticesCount );
    QSize verticesCount() const;
    void updateVerticesCountOpenGlData();

    void disposeOpenGlResources();

    FramebufferBinder fboBinder( const bool autoUnbind ) const;
    ViewportAlternator viewportAlternator( const bool autoRollback ) const;
    OpenGlObjectBinder vaoBinder( const bool autoUnbind = true ) const;

private:
    ShaderProgram m_shaderProgram;

    GLuint m_vertexHeightsTextureId;
    QSize m_verticesCount;
    QRectF m_meshRect;

    QVector< GLuint > m_outputTextures;
    GLuint m_fboId;
    GLuint m_vaoId;

    // 2: all shaders common variable - per vertex and per fragment invariant
    QHash< int, GLint > m_uniformVariableCachedLocations;
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_WATERPERVERTEXDATACALCULATOR_H
