#ifndef WATERRIPPLESIMULATOR_HELPERS_WATERSURFACEGPU_H
#define WATERRIPPLESIMULATOR_HELPERS_WATERSURFACEGPU_H

#include "../Data/VertexDataGpu.h"
#include "../WaterRippleSimulator_namespace.h"
#include "AbstractWaterSurface.h"
#include "OpenGlObjectBinder.h"
#include "ShaderProgram.h"
#include "WaterDataCalculator.h"

#include <QHash>
#include <QList>
#include <QMatrix4x4>
#include <QtGlobal>
#include <QVector>
#include <QVector3D>

#include <gl/GL.h>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

using namespace WaterRippleSimulator::Data;

class WaterSurfaceGpu : public AbstractWaterSurface
{
    Q_OBJECT

public:
    explicit WaterSurfaceGpu(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
            const QSurfaceFormat& aOpenGlFormat,
            QObject* const parent = nullptr );
    ~WaterSurfaceGpu() Q_DECL_OVERRIDE;

    void init() Q_DECL_OVERRIDE;
    void draw( const bool shouldPropagateWater ) Q_DECL_OVERRIDE;

    // wave propagation
    void setWavePropagationSpeed( const float aWavePropagationSpeed ) Q_DECL_OVERRIDE;
    float wavePropagationSpeed() const Q_DECL_OVERRIDE;

    void setWaveHeightDampingOverTimeFactor( const float aWaveHeightDampingOverTimeFactor ) Q_DECL_OVERRIDE;
    float waveHeightDampingOverTimeFactor() const Q_DECL_OVERRIDE;

    void setWaveHeightClampingFactor( const float aWaveHeightClampingFactor ) Q_DECL_OVERRIDE;
    float waveHeightClampingFactor() const Q_DECL_OVERRIDE;

    // water ripple
    void setWaterRippleMethod( const WaterSettings::WaterRippleMethod aWaterRippleMethod ) Q_DECL_OVERRIDE;
    WaterSettings::WaterRippleMethod waterRippleMethod() const Q_DECL_OVERRIDE;

    void setWaterRippleMagnitude( const float aWaterRippleMagnitude ) Q_DECL_OVERRIDE;
    float waterRippleMagnitude() const Q_DECL_OVERRIDE;

    void setWaterRippleRadiusFactor( const QVector2D& aWaterRippleRadiusFactor ) Q_DECL_OVERRIDE;
    QVector2D waterRippleRadiusFactor() const Q_DECL_OVERRIDE;

    void setWaterRippleRotationAngle( const float aWaterRippleRotationAngle ) Q_DECL_OVERRIDE;
    float waterRippleRotationAngle() const Q_DECL_OVERRIDE;

public slots:
    void rippleWaterAt( const int x, const int y ) Q_DECL_OVERRIDE;
    void propagateWater() Q_DECL_OVERRIDE;
    void stopWaterPropagation();

protected:
    QList< GLuint > allGenericVertexAttributeIndices() const Q_DECL_OVERRIDE;
    void applyMeshSize() Q_DECL_OVERRIDE;
    void applyMatrix( const CommonEntities::MatrixType matrixType ) Q_DECL_OVERRIDE;
    void applyLight() Q_DECL_OVERRIDE;
    void applyMaterial() Q_DECL_OVERRIDE;
    void applyObtainsColorFromTexture() Q_DECL_OVERRIDE;
    void calmWater() Q_DECL_OVERRIDE;

private:
    enum GenericVertexAttributeIndex
    {
        GenericVertexAttributeIndexInvalid        = -1,
        GenericVertexAttributeIndexVertexTexCoord = 0,
        GenericVertexAttributesCount              = 1
    };

    enum FragmentOutputVariableIndex
    {
        FragmentOutputVariableIndexInvalid = -1,
        FragmentOutputVariableIndexColor   = 0,
        FragmentOutputVariablesCount       = 1
    };

    enum UniformVariableIndex
    {
        UniformVariableIndexInvalid                     = -1,
        UniformVariableIndexMaterialColor               = 0,
        UniformVariableIndexMaterialReflectanceEmissive = 1,
        UniformVariableIndexMaterialReflectanceAmbient  = 2,
        UniformVariableIndexMaterialReflectanceDiffuse  = 3,
        UniformVariableIndexMaterialReflectanceSpecular = 4,
        UniformVariableIndexMaterialShininess           = 5,
        UniformVariableIndexLightColor                  = 6,
        UniformVariableIndexLightPos                    = 7,
        UniformVariableIndexLightAttenuationConstant    = 8,
        UniformVariableIndexLightAttenuationLinear      = 9,
        UniformVariableIndexLightAttenuationQuadratic   = 10,
        UniformVariableIndexLightIntensity              = 11,
        UniformVariableIndexModelMatrix                 = 12,
        UniformVariableIndexNormalMatrix                = 13,
        UniformVariableIndexViewMatrix                  = 14,
        UniformVariableIndexProjectionMatrix            = 15,
        UniformVariableIndexEyePos                      = 16,

        UniformVariableIndexHeightSampler               = 17,

        UniformVariableIndexMeshVertexMaxIndices        = 18,
        UniformVariableIndexMeshBottomLeft              = 19,
        UniformVariableIndexMeshTopRight                = 20,
        UniformVariableIndexObtainsColorFromTexture     = 21,
        UniformVariableIndexColorTexSampler             = 22,
        UniformVariablesCount                           = 23
    };

private:
    static QByteArray uniformVariableName( const int index );
    static bool isUniformVariableIndexValid( const int index );
    GLint uniformVariableLocation( const int index ) const;
    void cacheUniformVariableLocations();
    GLint uniformVariableCachedLocation( const int index ) const;

    void applyMeshSizeInternal();
    void applyMatrixInternal( const CommonEntities::MatrixType matrixType );
    void applyLightInternal();
    void applyMaterialInternal();
    void applyObtainsColorFromTextureInternal();
    void applyPartialShaderData();

    void updateMeshOpenGlData();
    void disposeOpenGlResources();

    OpenGlObjectBinder vaoBinder( const bool autoUnbind = true ) const;
    OpenGlObjectBinder vboBinder( const bool autoUnbind = true ) const;
    OpenGlObjectBinder veaBinder( const bool autoUnbind = true ) const;
    OpenGlObjectBinder heightTextureUnitBinder( const bool autoUnbind = true ) const;
    OpenGlObjectBinder heightTextureBinder( const bool autoUnbind = true ) const;

private:
    QVector< VertexDataGpu > m_vertices;
    QVector< GLuint > m_vertexIndices;

    ShaderProgram m_shaderProgram;
    GLuint m_vaoId;
    GLuint m_veaId;

    // 0: generic vertex attributes (vertex shader input variables)
    GLuint m_vboId;

    // 1: fragment shader output variables

    // 2: all shaders common variable - per vertex and per fragment invariant
    QHash< int, GLint > m_uniformVariableCachedLocations;

    WaterDataCalculator m_waterDataCalc;
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_WATERSURFACEGPU_H
