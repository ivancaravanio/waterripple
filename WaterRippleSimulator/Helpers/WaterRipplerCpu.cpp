#include "WaterRipplerCpu.h"

using namespace WaterRippleSimulator::Helpers;

WaterRipplerCpu::WaterRipplerCpu()
    : m_method{ WaterSettings::s_waterRippleMethodDefault }
    , m_magnitude{ WaterSettings::s_waterRippleMagnitudeDefault }
    , m_radiusFactor{ WaterSettings::s_waterRipple2DRadiusFactorDefault }
    , m_rotationAngle{ WaterSettings::s_waterRippleRotationAngleDefault }
{
}

WaterRipplerCpu::~WaterRipplerCpu()
{
}

void WaterRipplerCpu::setMeshSize( const QSize& aMeshSize )
{
    if ( aMeshSize.isEmpty() )
    {
        return;
    }

    m_meshSize = aMeshSize;
}

QSize WaterRipplerCpu::meshSize() const
{
    return m_meshSize;
}

QSize WaterRipplerCpu::verticesCount() const
{
    return MathUtilities::squaresToVerticesCount( m_meshSize );
}

void WaterRipplerCpu::setMethod( const WaterSettings::WaterRippleMethod aMethod )
{
    if ( WaterSettings::isWaterRippleMethodValid( aMethod ) )
    {
        m_method = aMethod;
    }
}

WaterSettings::WaterRippleMethod WaterRipplerCpu::method() const
{
    return m_method;
}

void WaterRipplerCpu::setMagnitude( const float aMagnitude )
{
    if ( WaterSettings::isWaterRippleMagnitudeValid( aMagnitude ) )
    {
        m_magnitude = aMagnitude;
    }
}

float WaterRipplerCpu::magnitude() const
{
    return m_magnitude;
}

void WaterRipplerCpu::setRadiusFactor( const QVector2D& aRadiusFactor )
{
    if ( WaterSettings::isWaterRippleRadiusFactorValid( aRadiusFactor ) )
    {
        m_radiusFactor = aRadiusFactor;
    }
}

QVector2D WaterRipplerCpu::radiusFactor() const
{
    return m_radiusFactor;
}

QVector2D WaterRipplerCpu::radius() const
{
    const QSize verticesCount = this->verticesCount();

    return m_radiusFactor * qMin( verticesCount.width(), verticesCount.height() );
}

void WaterRipplerCpu::setRotationAngle( const float aRotationAngle )
{
    const float normalizedAngle = MathUtilities::to360DegreesAngle( aRotationAngle );
    if ( WaterSettings::isWaterRippleRotationAngleValid( normalizedAngle ) )
    {
        m_rotationAngle = normalizedAngle;
    }
}

float WaterRipplerCpu::rotationAngle() const
{
    return m_rotationAngle;
}
