#include "CoordinateSystemIndicator.h"

#include "../Utilities/GeneralUtilities.h"
#include "../Utilities/GlUtilities.h"
#include "../Utilities/MathUtilities.h"
#include "../WaterRippleSimulator_namespace.h"
#include "OpenGlCapabilityEnabler.h"
#include "OpenGlErrorLogger.h"

#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Helpers;

using namespace WaterRippleSimulator::Utilities;
using namespace WaterRippleSimulator;

const QList< GLuint > CoordinateSystemIndicator::s_allOrderedGenericVertexAttributeIndices =
        QList< GLuint >()
        << CoordinateSystemIndicator::GenericVertexAttributeIndexVertexPos;

CoordinateSystemIndicator::CoordinateSystemIndicator(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        const QSurfaceFormat& aOpenGlFormat,
        QObject* const parent )
    : QObject{ parent }
    , AbstractOpenGlClient{ aOpenGlFunctions }
    , m_shaderProgram{ aOpenGlFunctions, aOpenGlFormat }
    , m_vao{ CommonEntities::s_invalidUnsignedGlValue }
    , m_vbo{ CommonEntities::s_invalidUnsignedGlValue }
{
}

CoordinateSystemIndicator::~CoordinateSystemIndicator()
{
    this->disposeOpenGlResources();
}

void CoordinateSystemIndicator::disposeOpenGlResources()
{
    if ( CommonEntities::isUnsignedGlValueValid( m_vao ) )
    {
        m_openGlFunctions->glDeleteVertexArrays( 1, & m_vao );
        LOG_OPENGL_ERROR();

        m_vao = CommonEntities::s_invalidUnsignedGlValue;
    }

    QVector< GLuint > vbosToDispose;
    if ( CommonEntities::isUnsignedGlValueValid( m_vbo ) )
    {
        vbosToDispose.append( m_vbo );
        m_vao = CommonEntities::s_invalidUnsignedGlValue;
    }

    const int vbosToDisposeCount = vbosToDispose.size();
    if ( vbosToDisposeCount > 0 )
    {
        m_openGlFunctions->glDeleteBuffers( vbosToDisposeCount, vbosToDispose.constData() );
        LOG_OPENGL_ERROR();
    }
}

OpenGlObjectBinder CoordinateSystemIndicator::vaoBinder( const bool autoUnbind ) const
{
    return AbstractOpenGlClient::vaoBinder( m_vao, autoUnbind );
}

void CoordinateSystemIndicator::init()
{
    if ( m_openGlFunctions == nullptr
         || ( CommonEntities::isUnsignedGlValueValid( m_vao )
              && CommonEntities::isUnsignedGlValueValid( m_vbo )
              && ! m_uniformVariableCachedLocations.isEmpty() ) )
    {
        return;
    }

    const bool isBuiltOk = GlUtilities::buildShaderProgram(
                               m_shaderProgram,
                               ":/Resources/shaders/CoordinateSystemIndicator.vert",
                               ":/Resources/shaders/CoordinateSystemIndicator.frag" );
    if ( ! isBuiltOk )
    {
        return;
    }

    this->cacheUniformVariableLocations();

#ifdef USE_OVERLAPPING_TRIANGLES_APPROACH
    AxisDrawData& frontTriangleData = m_axesDrawData[ TriangleFront ];
    frontTriangleData.color.setRgb( 255, 0, 0 );
    frontTriangleData.vertices
            << QVector3D( 0.0f, 0.0f, 0.9f )
            << QVector3D( 1.0f, 0.0f, 0.9f )
            << QVector3D( 0.0f, 0.5f, 0.9f );

    AxisDrawData& backTriangleData = m_axesDrawData[ TriangleBack ];
    backTriangleData.color.setRgb( 0, 255, 0 );
    backTriangleData.vertices
            << QVector3D( 0.0f, 0.0f, -0.5f )
            << QVector3D( 1.0f, 0.0f, -0.5f )
            << QVector3D( 0.0f, 0.5f, -0.5f );

#else
    AxisDrawData& xAxisDrawData = m_axesDrawData[ AxisX ];
    QVector< QVector3D >& xAxisVertices = xAxisDrawData.vertices;
    xAxisVertices
            // x
            << QVector3D( 0.0f, 0.0f, 0.0f )
            << QVector3D( 1.0f, 0.0f, 0.0f )

            << QVector3D( 1.0f, 0.0f, 0.0f )
            << QVector3D( 0.8f, 0.1f, 0.0f )
            << QVector3D( 1.0f, 0.0f, 0.0f )
            << QVector3D( 0.8f, -0.1f, 0.0f )

            << QVector3D( 1.0f, 0.0f, 0.0f )
            << QVector3D( 0.8f, 0.0f, 0.1f )
            << QVector3D( 1.0f, 0.0f, 0.0f )
            << QVector3D( 0.8f, 0.0f, -0.1f )

            << QVector3D( 0.7f, -0.1f, 0.0f )
            << QVector3D( 0.9f, -0.3f, 0.0f )

            << QVector3D( 0.7f, -0.3f, 0.0f )
            << QVector3D( 0.9f, -0.1f, 0.0f );

    xAxisDrawData.color.setRgb( 255, 0, 0 );

    AxisDrawData& yAxisDrawData = m_axesDrawData[ AxisY ];
    QVector< QVector3D >& yAxisVertices = yAxisDrawData.vertices;
    yAxisVertices
            // y
            << QVector3D( 0.0f, 0.0f, 0.0f )
            << QVector3D( 0.0f, 1.0f, 0.0f )

            << QVector3D( 0.0f, 1.0f, 0.0f )
            << QVector3D( 0.1f, 0.8f, 0.0f )
            << QVector3D( 0.0f, 1.0f, 0.0f )
            << QVector3D( -0.1f, 0.8f, 0.0f )

            << QVector3D( 0.0f, 1.0f, 0.0f )
            << QVector3D( 0.0f, 0.8f, 0.1f )
            << QVector3D( 0.0f, 1.0f, 0.0f )
            << QVector3D( 0.0f, 0.8f, -0.1f )

            << QVector3D( 0.1f, 0.9f, 0.0f )
            << QVector3D( 0.2f, 0.8f, 0.0f )

            << QVector3D( 0.3f, 0.9f, 0.0f )
            << QVector3D( 0.1f, 0.7f, 0.0f );

    yAxisDrawData.color.setRgb( 0, 255, 0 );

    AxisDrawData& zAxisDrawData = m_axesDrawData[ AxisZ ];
    QVector< QVector3D >& zAxisVertices = zAxisDrawData.vertices;
    zAxisVertices
            // z
            << QVector3D( 0.0f, 0.0f, 0.0f )
            << QVector3D( 0.0f, 0.0f, 1.0f )

            << QVector3D( 0.0f, 0.0f, 1.0f )
            << QVector3D( 0.1f, 0.0f, 0.8f )
            << QVector3D( 0.0f, 0.0f, 1.0f )
            << QVector3D( -0.1f, 0.0f, 0.8f )

            << QVector3D( 0.0f, 0.0f, 1.0f )
            << QVector3D( 0.0f, 0.1f, 0.8f )
            << QVector3D( 0.0f, 0.0f, 1.0f )
            << QVector3D( 0.0f, -0.1f, 0.8f )

            << QVector3D( 0.0f, 0.2f, 0.7f )
            << QVector3D( 0.0f, 0.2f, 0.9f )

            << QVector3D( 0.0f, 0.4f, 0.7f )
            << QVector3D( 0.0f, 0.4f, 0.9f )

            << QVector3D( 0.0f, 0.2f, 0.9f )
            << QVector3D( 0.0f, 0.4f, 0.7f );

    zAxisDrawData.color.setRgb( 0, 0, 255 );
#endif

    m_openGlFunctions->glGenBuffers( 1, & m_vbo );
    LOG_OPENGL_ERROR();

    OpenGlObjectBinder vboBinder{ m_openGlFunctions,
                                  OpenGlObjectBinder::ObjectType::Buffer,
                                  GL_ARRAY_BUFFER,
                                  m_vbo,
                                  false };
    vboBinder.bind();

    QVector< QVector3D > allVertices;
    allVertices
#ifdef USE_OVERLAPPING_TRIANGLES_APPROACH
            << frontTriangleData.vertices
            << backTriangleData.vertices
#else
            << xAxisVertices
            << yAxisVertices
            << zAxisVertices
#endif
               ;

    m_openGlFunctions->glBufferData( GL_ARRAY_BUFFER,
                                     allVertices.size() * sizeof( QVector3D ),
                                     allVertices.constData(),
                                     GL_STATIC_DRAW );
    LOG_OPENGL_ERROR();

    // setup Vertex Array Objects next
    m_openGlFunctions->glGenVertexArrays( 1, & m_vao );
    LOG_OPENGL_ERROR();

    OpenGlObjectBinder vaoBinder = this->vaoBinder( false );
    vaoBinder.bind();

    this->enableGenericVertexAttributeArrays( true );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glVertexAttribPointer( GenericVertexAttributeIndexVertexPos,
                                              MathUtilities::s_axesCount3D,
                                              GL_FLOAT,
                                              GL_FALSE,
                                              sizeof( QVector3D ),
                                              nullptr );
    LOG_OPENGL_ERROR();

    vaoBinder.unbind();
    vboBinder.unbind();

    this->enableGenericVertexAttributeArrays( false );
    LOG_OPENGL_ERROR();
}

void CoordinateSystemIndicator::draw()
{
    if ( m_openGlFunctions == nullptr
         || ! CommonEntities::isUnsignedGlValueValid( m_vao )
         || ! CommonEntities::isUnsignedGlValueValid( m_vbo ) )
    {
        return;
    }

    m_shaderProgram.activate();

    OpenGlCapabilityEnabler cullFaceEnabler{ m_openGlFunctions, GL_CULL_FACE, true, false };
    cullFaceEnabler.change();

    OpenGlObjectBinder cullFaceModeAlternator{
        m_openGlFunctions,
        OpenGlObjectBinder::ObjectType::CullFaceMode,
        GL_BACK,
        CommonEntities::invalidEnumGlValue(),
        false };
    cullFaceModeAlternator.bind();

    OpenGlObjectBinder vaoBinder = this->vaoBinder( false );
    vaoBinder.bind();

    this->enableGenericVertexAttributeArrays( true );

    GLint vertexStartIndex = 0;
    const GLint colorUniformVarLoc = this->uniformVariableCachedLocation( UniformVariableIndexColor );
    for ( int i = 0; i <
      #ifdef USE_OVERLAPPING_TRIANGLES_APPROACH
          TrianglesCount
      #else
          AxesCount
      #endif
          ; ++ i )
    {
        AxisDrawData& axisDrawData = m_axesDrawData[ i ];
        const int verticesCount = axisDrawData.vertices.size();

        const QColor& color = axisDrawData.color;
        m_shaderProgram.setUniformVariable4f( colorUniformVarLoc,
                                              color.redF(),
                                              color.greenF(),
                                              color.blueF(),
                                              color.alphaF() );

        m_openGlFunctions->glDrawArrays(
            #ifdef USE_OVERLAPPING_TRIANGLES_APPROACH
                GL_TRIANGLES
            #else
                GL_LINES
            #endif
                    ,
                    vertexStartIndex,
                    verticesCount );
        LOG_OPENGL_ERROR();

        vertexStartIndex += verticesCount;
    }

    vaoBinder.unbind();

    cullFaceModeAlternator.unbind();
    cullFaceEnabler.rollback();

    this->enableGenericVertexAttributeArrays( false );

    m_shaderProgram.deactivate();
}

void CoordinateSystemIndicator::setMatrix(
        const QMatrix4x4& matrix )
{
    m_shaderProgram.setUniformMatrix4fv(
                this->uniformVariableCachedLocation( UniformVariableIndexMatrix ),
                1,
                GL_FALSE,
                matrix.constData() );
}

void CoordinateSystemIndicator::enableGenericVertexAttributeArrays( const bool enable )
{
    for ( const GLuint genericVertexAttributeIndex : s_allOrderedGenericVertexAttributeIndices )
    {
        if ( enable )
        {
            m_openGlFunctions->glEnableVertexAttribArray( genericVertexAttributeIndex );
            LOG_OPENGL_ERROR();
        }
        else
        {
            m_openGlFunctions->glDisableVertexAttribArray( genericVertexAttributeIndex );
            LOG_OPENGL_ERROR();
        }
    }
}

QByteArray CoordinateSystemIndicator::uniformVariableName( const int index )
{
    switch ( index )
    {
        case UniformVariableIndexColor:
            return "color";
        case UniformVariableIndexMatrix:
            return "matrix";
    }

    return QByteArray();
}

bool CoordinateSystemIndicator::isUniformVariableIndexValid( const int index )
{
    return 0 <= index && index < UniformVariablesCount;
}

GLint CoordinateSystemIndicator::uniformVariableLocation( const int index ) const
{
    if ( ! CoordinateSystemIndicator::isUniformVariableIndexValid( index )
         || ! m_shaderProgram.isValid() )
    {
        return CommonEntities::s_invalidSignedGlValue;
    }

    const QByteArray uniformVariableName = CoordinateSystemIndicator::uniformVariableName( index );
    return uniformVariableName.isEmpty()
           ? CommonEntities::s_invalidSignedGlValue
           : m_shaderProgram.uniformVarLocation( uniformVariableName );
}

void CoordinateSystemIndicator::cacheUniformVariableLocations()
{
    m_uniformVariableCachedLocations.clear();
    m_uniformVariableCachedLocations.reserve( UniformVariablesCount );
    for ( int i = 0; i < UniformVariablesCount; ++ i )
    {
        m_uniformVariableCachedLocations.insert( i, this->uniformVariableLocation( i ) );
    }
}

GLint CoordinateSystemIndicator::uniformVariableCachedLocation( const int index ) const
{
    return m_uniformVariableCachedLocations.value( index, CommonEntities::s_invalidSignedGlValue );
}
