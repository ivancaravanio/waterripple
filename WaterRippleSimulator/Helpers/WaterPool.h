#ifndef WATERRIPPLESIMULATOR_HELPERS_WATERPOOL_H
#define WATERRIPPLESIMULATOR_HELPERS_WATERPOOL_H

#include "../Data/Light.h"
#include "../Data/Material.h"
#include "../WaterRippleSimulator_namespace.h"
#include "AbstractOpenGlClient.h"
#include "OpenGlObjectBinder.h"
#include "ShaderProgram.h"

#include <QHash>
#include <QList>
#include <QMatrix4x4>
#include <QObject>
#include <QRectF>
#include <QtGlobal>
#include <QVector>
#include <QVector3D>

#include <gl/GL.h>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

using namespace WaterRippleSimulator::Data;

class WaterPool : public QObject, public AbstractOpenGlClient
{
    Q_OBJECT

public:
    explicit WaterPool(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
            const QSurfaceFormat& aOpenGlFormat,
            const QRectF& aWaterRect,
            QObject* const parent = nullptr );
    ~WaterPool() Q_DECL_OVERRIDE;

    // the water rect at (X, Y) plane
    // the rect's center should be at (X=0, Y=0)
    void setWaterRect( const QRectF& aWaterRect );
    QRectF waterRect() const;

    void init() Q_DECL_OVERRIDE;
    void draw();

    void setMatrix( const CommonEntities::MatrixType matrixType, const QMatrix4x4& matrix );
    void setLight( const Light& aLight );
    void setMaterial( const Material& aMaterial );

signals:
    void updateRequested();

private:
    enum GenericVertexAttributeIndex
    {
        GenericVertexAttributeIndexInvalid        = -1,
        GenericVertexAttributeIndexVertexPos      = 0,
        GenericVertexAttributeIndexVertexTexCoord = 1,
        GenericVertexAttributeIndexVertexNormal   = 2,
        GenericVertexAttributesCount              = 3
    };

    enum FragmentOutputVariableIndex
    {
        FragmentOutputVariableIndexInvalid = -1,
        FragmentOutputVariableIndexColor   = 0,
        FragmentOutputVariablesCount       = 1
    };

    enum UniformVariableIndex
    {
        UniformVariableIndexInvalid                     = -1,
        UniformVariableIndexColorTexSampler             = 0,
        UniformVariableIndexMaterialReflectanceEmissive = 1,
        UniformVariableIndexMaterialReflectanceAmbient  = 2,
        UniformVariableIndexMaterialReflectanceDiffuse  = 3,
        UniformVariableIndexMaterialReflectanceSpecular = 4,
        UniformVariableIndexMaterialShininess           = 5,
        UniformVariableIndexLightColor                  = 6,
        UniformVariableIndexLightPos                    = 7,
        UniformVariableIndexLightAttenuationConstant    = 8,
        UniformVariableIndexLightAttenuationLinear      = 9,
        UniformVariableIndexLightAttenuationQuadratic   = 10,
        UniformVariableIndexLightIntensity              = 11,
        UniformVariableIndexModelMatrix                 = 12,
        UniformVariableIndexNormalMatrix                = 13,
        UniformVariableIndexViewMatrix                  = 14,
        UniformVariableIndexProjectionMatrix            = 15,
        UniformVariableIndexEyePos                      = 16,
        UniformVariablesCount                           = 17
    };

    struct VertexData
    {
        QVector3D pos;
        QVector2D texCoord;
        QVector3D normal;
    };

    enum VertexIndex
    {
        VertexIndexRoofBottomLeft   = 0,
        VertexIndexRoofBottomRight  = 1,
        VertexIndexRoofTopRight     = 2,
        VertexIndexRoofTopLeft      = 3,
        VertexIndexFloorBottomLeft  = 4,
        VertexIndexFloorBottomRight = 5,
        VertexIndexFloorTopRight    = 6,
        VertexIndexFloorTopLeft     = 7,
        VerticesCount               = 8
    };

private:
    static const QList< GLuint > s_allOrderedGenericVertexAttributeIndices;

private:
    static QByteArray uniformVariableName( const int index );
    static bool isUniformVariableIndexValid( const int index );
    GLint uniformVariableLocation( const int index ) const;
    void cacheUniformVariableLocations();
    GLint uniformVariableCachedLocation( const int index ) const;

    void enableGenericVertexAttributeArrays( const bool enable );

    void updateMeshOpenGlData();
    void updateLightOpenGlData();
    void updateMaterialOpenGlData();

    void setWaterRectInternal( const QRectF& aWaterRect );
    void setLightInternal( const Light& aLight );
    void setMaterialInternal( const Material& aMaterial );

    OpenGlObjectBinder vaoBinder( const bool autoUnbind = true ) const;
    OpenGlObjectBinder vboBinder( const bool autoUnbind = true ) const;
    OpenGlObjectBinder textureUnitBinder( const bool autoUnbind = true ) const;
    OpenGlObjectBinder textureBinder( const bool autoUnbind = true ) const;

    void disposeOpenGlResources();

private:
    QVector< VertexData > m_verticesData;
    QRectF m_waterRect;

    ShaderProgram m_shaderProgram;
    GLuint m_vaoId;
    GLuint m_vboId;
    GLuint m_textureId;

    // 2: all shaders common variable - per vertex and per fragment invariant
    QHash< int, GLint > m_uniformVariableCachedLocations;

    Light m_light;
    BaseMaterial m_material;
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_WATERPOOL_H
