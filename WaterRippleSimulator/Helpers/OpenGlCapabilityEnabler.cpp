#include "OpenGlCapabilityEnabler.h"

#include "../WaterRippleSimulator_namespace.h"
#include "OpenGlErrorLogger.h"

#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Helpers;

OpenGlCapabilityEnabler::OpenGlCapabilityEnabler()
    : m_openGlFunctions{ nullptr }
    , m_capability{ CommonEntities::invalidEnumGlValue() }
    , m_shouldEnable{ false }
    , m_wasChanged{ false }
    , m_autoRollback{ false }
{
}

OpenGlCapabilityEnabler::OpenGlCapabilityEnabler(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        const GLenum aCapability,
        const bool aShouldEnable,
        const bool aAutoRollback )
    : m_openGlFunctions{ aOpenGlFunctions }
    , m_capability{ CommonEntities::isEnumGlValueValid( aCapability )
                    ? aCapability
                    : CommonEntities::invalidEnumGlValue() }
    , m_shouldEnable{ aShouldEnable }
    , m_wasChanged{ false }
    , m_autoRollback{ aAutoRollback }
{
}

OpenGlCapabilityEnabler::~OpenGlCapabilityEnabler()
{
    if ( m_autoRollback )
    {
        this->rollback();
    }
}

bool OpenGlCapabilityEnabler::isValid() const
{
    return m_openGlFunctions != nullptr
           && CommonEntities::isEnumGlValueValid( m_capability );
}

void OpenGlCapabilityEnabler::change( const bool shouldEnable )
{
    if ( ! this->isValid() )
    {
        return;
    }

    if ( shouldEnable )
    {
        m_openGlFunctions->glEnable( m_capability );
        LOG_OPENGL_ERROR();
    }
    else
    {
        m_openGlFunctions->glDisable( m_capability );
        LOG_OPENGL_ERROR();
    }
}

void OpenGlCapabilityEnabler::change()
{
    if ( ! this->isValid() )
    {
        return;
    }

    const bool isEnabled = this->isEnabled();
    if ( isEnabled == m_shouldEnable )
    {
        return;
    }

    m_wasChanged = true;
    this->change( m_shouldEnable );
}

void OpenGlCapabilityEnabler::rollback()
{
    if ( ! m_wasChanged
         || ! this->isValid() )
    {
        return;
    }

    this->change( ! m_shouldEnable );
}

GLenum OpenGlCapabilityEnabler::capability() const
{
    return m_capability;
}

bool OpenGlCapabilityEnabler::shouldEnable() const
{
    return m_shouldEnable;
}

bool OpenGlCapabilityEnabler::isEnabled() const
{
    if ( ! this->isValid() )
    {
        return false;
    }

    const GLboolean isEnabled = m_openGlFunctions->glIsEnabled( m_capability );
    LOG_OPENGL_ERROR();

    return isEnabled == GL_TRUE;
}
