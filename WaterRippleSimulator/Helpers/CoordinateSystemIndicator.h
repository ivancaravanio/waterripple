#ifndef WATERRIPPLESIMULATOR_HELPERS_COORDINATESYSTEMINDICATOR_H
#define WATERRIPPLESIMULATOR_HELPERS_COORDINATESYSTEMINDICATOR_H

#include "../WaterRippleSimulator_namespace.h"
#include "AbstractOpenGlClient.h"
#include "OpenGlObjectBinder.h"
#include "ShaderProgram.h"

#include <QColor>
#include <QHash>
#include <QList>
#include <QMatrix4x4>
#include <QObject>
#include <QtGlobal>
#include <QVector>
#include <QVector3D>

#include <gl/GL.h>

// #define USE_OVERLAPPING_TRIANGLES_APPROACH

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

using namespace WaterRippleSimulator::Data;

class CoordinateSystemIndicator : public QObject, public AbstractOpenGlClient
{
    Q_OBJECT

public:
    explicit CoordinateSystemIndicator(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
            const QSurfaceFormat& aOpenGlFormat,
            QObject* const parent = nullptr );
    ~CoordinateSystemIndicator() Q_DECL_OVERRIDE;

    void init() Q_DECL_OVERRIDE;
    void draw();

    void setMatrix( const QMatrix4x4& matrix );

signals:
    void updateRequested();

private:
    enum GenericVertexAttributeIndex
    {
        GenericVertexAttributeIndexInvalid   = -1,
        GenericVertexAttributeIndexVertexPos = 0,
        GenericVertexAttributesCount         = 1
    };

    enum FragmentOutputVariableIndex
    {
        FragmentOutputVariableIndexInvalid = -1,
        FragmentOutputVariableIndexColor   = 0,
        FragmentOutputVariablesCount       = 1
    };

    enum UniformVariableIndex
    {
        UniformVariableIndexInvalid = -1,
        UniformVariableIndexColor   = 0,
        UniformVariableIndexMatrix  = 1,
        UniformVariablesCount       = 2
    };

    struct AxisDrawData
    {
        QVector< QVector3D > vertices;
        QColor color;
    };

#ifdef USE_OVERLAPPING_TRIANGLES_APPROACH
    enum Triangle
    {
        TriangleFront  = 0,
        TriangleBack   = 1,
        TrianglesCount = 2
    };
#else
    enum Axis
    {
        AxisX     = 0,
        AxisY     = 1,
        AxisZ     = 2,
        AxesCount = 3
    };
#endif

private:
    static const QList< GLuint > s_allOrderedGenericVertexAttributeIndices;

private:
    static QByteArray uniformVariableName( const int index );
    static bool isUniformVariableIndexValid( const int index );
    GLint uniformVariableLocation( const int index ) const;
    void cacheUniformVariableLocations();
    GLint uniformVariableCachedLocation( const int index ) const;

    void enableGenericVertexAttributeArrays( const bool enable );

    void disposeOpenGlResources();

    OpenGlObjectBinder vaoBinder( const bool autoUnbind = true ) const;

private:
    ShaderProgram m_shaderProgram;
    GLuint m_vao;
    GLuint m_vbo;

    // 2: all shaders common variable - per vertex and per fragment invariant
    QHash< int, GLint > m_uniformVariableCachedLocations;

#ifdef USE_OVERLAPPING_TRIANGLES_APPROACH
    AxisDrawData m_axesDrawData[ TrianglesCount ];
#else
    AxisDrawData m_axesDrawData[ AxesCount ];
#endif
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_COORDINATESYSTEMINDICATOR_H
