#include "WaterPerVertexDataCalculator.h"

#include "../Data/ShaderInfo.h"
#include "../Data/WaterSettings.h"
#include "../Utilities/GeneralUtilities.h"
#include "../Utilities/GlUtilities.h"
#include "../Utilities/ImageUtilities.h"
#include "../Utilities/MathUtilities.h"
#include "../WaterRippleSimulator_namespace.h"
#include "OpenGlCapabilityEnabler.h"
#include "OpenGlErrorLogger.h"

#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Helpers;

using namespace WaterRippleSimulator::Utilities;
using namespace WaterRippleSimulator;

WaterPerVertexDataCalculator::WaterPerVertexDataCalculator(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        const QSurfaceFormat& aOpenGlFormat,
        QObject* const parent )
    : QObject{ parent }
    , AbstractOpenGlClient{ aOpenGlFunctions }
    , m_shaderProgram{ aOpenGlFunctions, aOpenGlFormat }
    , m_vertexHeightsTextureId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_outputTextures( TexturesCount, CommonEntities::s_invalidUnsignedGlValue )
    , m_fboId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_vaoId{ CommonEntities::s_invalidUnsignedGlValue }
{
}

WaterPerVertexDataCalculator::~WaterPerVertexDataCalculator()
{
    this->disposeOpenGlResources();
}

void WaterPerVertexDataCalculator::setVertexHeightsTextureId( const GLuint aVertexHeightsTextureId )
{
    if ( ! CommonEntities::isUnsignedGlValueValid( aVertexHeightsTextureId ) )
    {
        return;
    }

    // don't perform checks
    m_vertexHeightsTextureId = aVertexHeightsTextureId;

    this->setVerticesCount( GlUtilities::texture2DSize( m_openGlFunctions, m_vertexHeightsTextureId ) );
}

GLuint WaterPerVertexDataCalculator::vertexHeightsTextureId() const
{
    return m_vertexHeightsTextureId;
}

void WaterPerVertexDataCalculator::setMeshRect( const QRectF& aMeshRect )
{
    if ( this->setMeshRectInternal( aMeshRect ) )
    {
        emit updateRequested();
    }
}

QRectF WaterPerVertexDataCalculator::meshRect() const
{
    return m_meshRect;
}

void WaterPerVertexDataCalculator::updateVerticesCountOpenGlData()
{
    if ( m_openGlFunctions == nullptr
         || ! CommonEntities::isUnsignedGlValueValid( m_vertexHeightsTextureId )
         || ! m_shaderProgram.isValid() )
    {
        return;
    }

    const int xVerticesCount = m_verticesCount.width();
    const int yVerticesCount = m_verticesCount.height();
    for ( int i = 0; i < TexturesCount; ++i )
    {
        const GLuint outputTextureId = m_outputTextures[ i ];
        if ( ! CommonEntities::isUnsignedGlValueValid( outputTextureId ) )
        {
            continue;
        }

        OpenGlObjectBinder textureBinder = this->textureBinder( outputTextureId );
        textureBinder.bind();

        GlUtilities::setTexture( m_openGlFunctions,
                                 nullptr,
                                 xVerticesCount,
                                 yVerticesCount,
                                 GL_RGB32F,
                                 GL_RGB,
                                 GL_FLOAT,
                                 GlUtilities::TextureFilter::Nearest,
                                 GlUtilities::TextureFilter::Nearest,
                                 GlUtilities::TextureWrapMode::ClampToEdge,
                                 GlUtilities::TextureWrapMode::ClampToEdge,
                                 false );
    }

    this->setupFbo();
    m_shaderProgram.setUniformVariable2i(
                this->uniformVariableCachedLocation( UniformVariableIndexHeightTexMaxIndices ),
                xVerticesCount - 1,
                yVerticesCount - 1 );
}

void WaterPerVertexDataCalculator::disposeOpenGlResources()
{
    if ( CommonEntities::isUnsignedGlValueValid( m_fboId ) )
    {
        m_openGlFunctions->glDeleteFramebuffers( 1, & m_fboId );
        LOG_OPENGL_ERROR();

        m_fboId = CommonEntities::s_invalidUnsignedGlValue;
    }

    QVector< GLuint > texturesIdsToDispose;
    for ( int i = 0; i < TexturesCount; ++i )
    {
        GLuint& textureId = m_outputTextures[ i ];
        if ( CommonEntities::isUnsignedGlValueValid( textureId ) )
        {
            texturesIdsToDispose.append( textureId );
            textureId = CommonEntities::s_invalidUnsignedGlValue;
        }
    }

    const int texturesIdsToDisposeCount = texturesIdsToDispose.size();
    if ( texturesIdsToDisposeCount > 0 )
    {
        m_openGlFunctions->glDeleteTextures( texturesIdsToDisposeCount, texturesIdsToDispose.constData() );
        LOG_OPENGL_ERROR();
    }
}

FramebufferBinder WaterPerVertexDataCalculator::fboBinder( const bool autoUnbind ) const
{
    return AbstractOpenGlClient::fboBinder(
                m_fboId,
                FramebufferBinder::BindingTarget::Both,
                autoUnbind );
}

ViewportAlternator WaterPerVertexDataCalculator::viewportAlternator( const bool autoRollback ) const
{
    return AbstractOpenGlClient::viewportAlternator(
                QRect{ QPoint{ 0, 0 }, m_verticesCount },
                autoRollback );
}

OpenGlObjectBinder WaterPerVertexDataCalculator::vaoBinder( const bool autoUnbind ) const
{
    return AbstractOpenGlClient::vaoBinder( m_vaoId, autoUnbind );
}

void WaterPerVertexDataCalculator::init()
{
    if ( ! m_uniformVariableCachedLocations.isEmpty() )
    {
        return;
    }

    m_openGlFunctions->glGenTextures( m_outputTextures.size(), m_outputTextures.data() );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glGenVertexArrays( 1, & m_vaoId );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glGenFramebuffers( 1, & m_fboId );
    LOG_OPENGL_ERROR();

    const bool isBuiltOk = GlUtilities::buildShaderProgram(
                               m_shaderProgram,
                               ":/Resources/shaders/WaterDataCalculator.vert",
                               ":/Resources/shaders/WaterPerVertexDataCalculator.frag" );
    if ( ! isBuiltOk )
    {
        this->disposeOpenGlResources();
        return;
    }

    this->cacheUniformVariableLocations();
    m_shaderProgram.setUniformVariable1i( this->uniformVariableCachedLocation( UniformVariableIndexHeightTexSampler ), 0 );
    this->updateVerticesCountOpenGlData();
    this->updateMeshRectOpenGlData();
}

void WaterPerVertexDataCalculator::calc()
{
    if ( m_openGlFunctions == nullptr
         || m_verticesCount.isEmpty()
         || ! m_shaderProgram.isValid()
         || ! CommonEntities::isUnsignedGlValueValid( m_fboId )
         || ! CommonEntities::isUnsignedGlValueValid( m_vaoId ) )
    {
        return;
    }

    FramebufferBinder fboActivator = this->fboBinder( false );
    fboActivator.bind();

    ViewportAlternator vpAlternator = this->viewportAlternator( false );
    vpAlternator.change();

    m_shaderProgram.activate();

    OpenGlCapabilityEnabler cullFaceEnabler{
                m_openGlFunctions,
                GL_CULL_FACE,
                true,
                false };
    cullFaceEnabler.change();

    OpenGlObjectBinder cullFaceModeAlternator{
                m_openGlFunctions,
                OpenGlObjectBinder::ObjectType::CullFaceMode,
                GL_BACK,
                CommonEntities::invalidEnumGlValue(),
                false };
    cullFaceModeAlternator.bind();

    OpenGlObjectBinder vaoBinder = this->vaoBinder( false );
    vaoBinder.bind();

    OpenGlObjectBinder readHeightTextureUnitBinder = this->textureUnitBinder( GL_TEXTURE0, false );
    readHeightTextureUnitBinder.bind();

    OpenGlObjectBinder readHeightTextureBinder = this->textureBinder( m_vertexHeightsTextureId, false );
    readHeightTextureBinder.bind();

    m_openGlFunctions->glDrawArrays( GL_TRIANGLE_FAN,
                                     0,
                                     4 );
    LOG_OPENGL_ERROR();

    static bool readVertices = false;
    if ( readVertices )
    {
        m_openGlFunctions->glReadBuffer( GL_COLOR_ATTACHMENT0 );
        LOG_OPENGL_ERROR();

        const QSize verticesCount = this->verticesCount();
        const int xVerticesCount = verticesCount.width();
        const int yVerticesCount = verticesCount.height();
        QVector< QVector3D > vertices( xVerticesCount * yVerticesCount );

        m_openGlFunctions->glReadPixels( 0, 0, xVerticesCount, yVerticesCount, GL_RGB, GL_FLOAT, vertices.data() );
        LOG_OPENGL_ERROR();

        qDebug() << "v:" << vertices;

        static const QVector< GLenum > drawBuffers = QVector< GLenum >()
                                                     << GL_COLOR_ATTACHMENT0
                                                     << GL_COLOR_ATTACHMENT1;
        m_openGlFunctions->glDrawBuffers( drawBuffers.size(), drawBuffers.constData() );
        LOG_OPENGL_ERROR();

        readVertices = false;
    }

    m_shaderProgram.deactivate();

    vaoBinder.unbind();

    readHeightTextureBinder.unbind();
    readHeightTextureUnitBinder.unbind();
    fboActivator.unbind();
    vpAlternator.rollback();
    cullFaceModeAlternator.unbind();
    cullFaceEnabler.rollback();

    emit updateRequested();
}

GLuint WaterPerVertexDataCalculator::vertexPosTextureId() const
{
    return m_outputTextures[ TextureIndexVertexPos ];
}

GLuint WaterPerVertexDataCalculator::vertexNormalsTextureId() const
{
    return m_outputTextures[ TextureIndexVertexNormals ];
}

QByteArray WaterPerVertexDataCalculator::uniformVariableName( const int index )
{
    switch ( index )
    {
        case UniformVariableIndexHeightTexSampler:
            return "heightTexSampler";
        case UniformVariableIndexHeightTexMaxIndices:
            return "heightTexMaxIndices";
        case UniformVariableIndexMeshRectBottomLeft:
            return "meshRectBottomLeft";
        case UniformVariableIndexMeshRectTopRight:
            return "meshRectTopRight";
        default:
            break;
    }

    return QByteArray();
}

bool WaterPerVertexDataCalculator::isUniformVariableIndexValid( const int index )
{
    return 0 <= index && index < UniformVariablesCount;
}

GLint WaterPerVertexDataCalculator::uniformVariableLocation( const int index ) const
{
    if ( ! WaterPerVertexDataCalculator::isUniformVariableIndexValid( index )
         || ! m_shaderProgram.isValid() )
    {
        return CommonEntities::s_invalidSignedGlValue;
    }

    const QByteArray uniformVariableName = WaterPerVertexDataCalculator::uniformVariableName( index );
    return uniformVariableName.isEmpty()
           ? CommonEntities::s_invalidSignedGlValue
           : m_shaderProgram.uniformVarLocation( uniformVariableName );
}

void WaterPerVertexDataCalculator::cacheUniformVariableLocations()
{
    m_uniformVariableCachedLocations.clear();
    m_uniformVariableCachedLocations.reserve( UniformVariablesCount );
    for ( int i = 0; i < UniformVariablesCount; ++ i )
    {
        m_uniformVariableCachedLocations.insert( i, this->uniformVariableLocation( i ) );
    }
}

GLint WaterPerVertexDataCalculator::uniformVariableCachedLocation( const int index ) const
{
    return m_uniformVariableCachedLocations.value( index, CommonEntities::s_invalidSignedGlValue );
}

bool WaterPerVertexDataCalculator::setMeshRectInternal( const QRectF& aMeshRect )
{
    if ( aMeshRect.isEmpty()
         || aMeshRect == m_meshRect )
    {
        return false;
    }

    m_meshRect = aMeshRect;
    this->updateMeshRectOpenGlData();

    return true;
}

void WaterPerVertexDataCalculator::updateMeshRectOpenGlData()
{
    if ( ! m_shaderProgram.isValid() )
    {
        return;
    }

    const QMatrix4x4 m = GlUtilities::qtToOpenGlXYPlaneTransform();

    const QPointF bottomLeft = m.map( m_meshRect.bottomLeft() );
    m_shaderProgram.setUniformVariable2f(
                this->uniformVariableCachedLocation( UniformVariableIndexMeshRectBottomLeft ),
                bottomLeft.x(),
                bottomLeft.y() );

    const QPointF topRight = m.map( m_meshRect.topRight() );
    m_shaderProgram.setUniformVariable2f(
                this->uniformVariableCachedLocation( UniformVariableIndexMeshRectTopRight ),
                topRight.x(),
                topRight.y() );
}

void WaterPerVertexDataCalculator::setupFbo()
{
    if ( m_verticesCount.isEmpty()
         || ! CommonEntities::isUnsignedGlValueValid( m_fboId ) )
    {
        return;
    }

    FramebufferBinder fboBinder = this->fboBinder( true );
    fboBinder.bind();

    const GLint mipmapLevel = 0;

    m_openGlFunctions->glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->vertexPosTextureId(), mipmapLevel );
    LOG_OPENGL_ERROR();
    m_openGlFunctions->glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, this->vertexNormalsTextureId(), mipmapLevel );
    LOG_OPENGL_ERROR();

    const GLenum fboStatus = m_openGlFunctions->glCheckFramebufferStatus( GL_FRAMEBUFFER );
    LOG_OPENGL_ERROR();
    if ( fboStatus != GL_FRAMEBUFFER_COMPLETE )
    {
        this->disposeOpenGlResources();
        return;
    }

    static const QVector< GLenum > drawBuffers = QVector< GLenum >()
                                                 << GL_COLOR_ATTACHMENT0
                                                 << GL_COLOR_ATTACHMENT1;
    m_openGlFunctions->glDrawBuffers( drawBuffers.size(), drawBuffers.constData() );
    LOG_OPENGL_ERROR();

    fboBinder.unbind();
}

void WaterPerVertexDataCalculator::setVerticesCount( const QSize& aVerticesCount )
{
    if ( aVerticesCount.isEmpty() )
    {
        return;
    }

    if ( aVerticesCount == m_verticesCount )
    {
        return;
    }

    m_verticesCount = aVerticesCount;
    this->updateVerticesCountOpenGlData();
}

QSize WaterPerVertexDataCalculator::verticesCount() const
{
    return m_verticesCount;
}
