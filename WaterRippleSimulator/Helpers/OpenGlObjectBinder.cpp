#include "OpenGlObjectBinder.h"

#include "OpenGlErrorLogger.h"

#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Helpers;

OpenGlObjectBinder::OpenGlObjectBinder()
    : m_openGlFunctions{ nullptr }
    , m_objectType{ ObjectType::Invalid }
    , m_bindingTarget{ CommonEntities::invalidEnumGlValue() }
    , m_objectName{ CommonEntities::s_invalidUnsignedGlValue }
    , m_autoUnbind{ false }
    , m_isRequestedDataBound{ false }
    , m_previouslyBoundData{ CommonEntities::s_invalidUnsignedGlValue }
{
}

OpenGlObjectBinder::OpenGlObjectBinder(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        const ObjectType aObjectType,
        const GLenum aBindingTarget,
        const GLuint aObjectName,
        const bool aAutoUnbind )
    : m_openGlFunctions{ aOpenGlFunctions }
    , m_objectType{ aObjectType }
    , m_bindingTarget{ aBindingTarget }
    , m_objectName{ aObjectName }
    , m_autoUnbind{ aAutoUnbind }
    , m_isRequestedDataBound{ false }
    , m_previouslyBoundData{ CommonEntities::s_invalidUnsignedGlValue }
{
}

OpenGlObjectBinder::~OpenGlObjectBinder()
{
    if ( m_autoUnbind )
    {
        this->unbind();
    }
}

bool OpenGlObjectBinder::isValid() const
{
    return m_openGlFunctions != nullptr
           && m_objectType != ObjectType::Invalid;
}

OpenGlObjectBinder::ObjectType OpenGlObjectBinder::objectType() const
{
    return m_objectType;
}

GLenum OpenGlObjectBinder::bindingTarget() const
{
    return m_bindingTarget;
}

GLuint OpenGlObjectBinder::objectName() const
{
    return m_objectName;
}

GLuint OpenGlObjectBinder::requestedData() const
{
    return this->isObjectNameUsedAsBoundData()
           ? m_objectName
           : m_bindingTarget;
}

GLenum OpenGlObjectBinder::openGlBindingTargetCheck(
        const OpenGlObjectBinder::ObjectType objectType,
        const GLenum bindingTarget )
{
    switch ( objectType )
    {
        case ObjectType::TextureUnit:
            return GL_ACTIVE_TEXTURE;
        case ObjectType::Texture:
            switch ( bindingTarget )
            {
                case GL_TEXTURE_1D:
                    return GL_TEXTURE_BINDING_1D;
                case GL_TEXTURE_2D:
                    return GL_TEXTURE_BINDING_2D;
                case GL_TEXTURE_3D:
                    return GL_TEXTURE_BINDING_3D;
                case GL_TEXTURE_1D_ARRAY:
                    return GL_TEXTURE_BINDING_1D_ARRAY;
                case GL_TEXTURE_2D_ARRAY:
                    return GL_TEXTURE_BINDING_2D_ARRAY;
                case GL_TEXTURE_RECTANGLE:
                    return GL_TEXTURE_BINDING_RECTANGLE;
                case GL_TEXTURE_CUBE_MAP:
                    return GL_TEXTURE_BINDING_CUBE_MAP;
                case GL_TEXTURE_2D_MULTISAMPLE:
                    return GL_TEXTURE_BINDING_2D_MULTISAMPLE;
                case GL_TEXTURE_2D_MULTISAMPLE_ARRAY:
                    return GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY;
                case GL_TEXTURE_BUFFER:
                    return GL_TEXTURE_BINDING_BUFFER;
                case GL_TEXTURE_CUBE_MAP_ARRAY:
                    return GL_TEXTURE_BINDING_CUBE_MAP_ARRAY;
            }

            break;
        case ObjectType::Buffer:
            switch ( bindingTarget )
            {
                case GL_ARRAY_BUFFER:
                    return GL_ARRAY_BUFFER_BINDING;
                case GL_ATOMIC_COUNTER_BUFFER:
                    return GL_ATOMIC_COUNTER_BUFFER_BINDING;
                case GL_COPY_READ_BUFFER:
                    return GL_COPY_READ_BUFFER_BINDING;
                case GL_COPY_WRITE_BUFFER:
                    return GL_COPY_WRITE_BUFFER_BINDING;
                case GL_DRAW_INDIRECT_BUFFER:
                    return GL_DRAW_INDIRECT_BUFFER_BINDING;
                case GL_DISPATCH_INDIRECT_BUFFER:
                    return GL_DISPATCH_INDIRECT_BUFFER_BINDING;
                case GL_ELEMENT_ARRAY_BUFFER:
                    return GL_ELEMENT_ARRAY_BUFFER_BINDING;
                case GL_PIXEL_PACK_BUFFER:
                    return GL_PIXEL_PACK_BUFFER_BINDING;
                case GL_PIXEL_UNPACK_BUFFER:
                    return GL_PIXEL_UNPACK_BUFFER_BINDING;
                case GL_QUERY_BUFFER:
                    return GL_QUERY_BUFFER_BINDING;
                case GL_SHADER_STORAGE_BUFFER:
                    return GL_SHADER_STORAGE_BUFFER_BINDING;
                case GL_TEXTURE_BUFFER:
                    return GL_TEXTURE_BUFFER_BINDING;
                case GL_TRANSFORM_FEEDBACK_BUFFER:
                    return GL_TRANSFORM_FEEDBACK_BUFFER_BINDING;
                case GL_UNIFORM_BUFFER:
                    return GL_UNIFORM_BUFFER_BINDING;
            }

            break;
        case ObjectType::VertexArray:
            return GL_VERTEX_ARRAY_BINDING;
        case ObjectType::Framebuffer:
            switch ( bindingTarget )
            {
                case GL_FRAMEBUFFER:
                    return GL_FRAMEBUFFER_BINDING;
                case GL_DRAW_FRAMEBUFFER:
                    return GL_DRAW_FRAMEBUFFER_BINDING;
                case GL_READ_FRAMEBUFFER:
                    return GL_READ_FRAMEBUFFER_BINDING;
            }

            break;
        case ObjectType::Renderbuffer:
            return GL_RENDERBUFFER_BINDING;
        case ObjectType::PolygonMode:
            return GL_POLYGON_MODE;
        case ObjectType::CullFaceMode:
            return GL_CULL_FACE_MODE;
        default:
            break;
    }

    return CommonEntities::invalidEnumGlValue();
}

GLenum OpenGlObjectBinder::openGlBindingTargetCheck() const
{
    return OpenGlObjectBinder::openGlBindingTargetCheck( m_objectType, m_bindingTarget );
}

bool OpenGlObjectBinder::isObjectNameUsedAsBoundData() const
{
    return    m_objectType == ObjectType::Texture
           || m_objectType == ObjectType::Buffer
           || m_objectType == ObjectType::VertexArray
           || m_objectType == ObjectType::Renderbuffer
           || m_objectType == ObjectType::PolygonMode;
}

void OpenGlObjectBinder::bind( const GLenum aBindingTarget,
                               const GLuint aObjectName )
{
    switch ( m_objectType )
    {
        case ObjectType::TextureUnit:
            m_openGlFunctions->glActiveTexture( aBindingTarget );
            LOG_OPENGL_ERROR();
            break;
        case ObjectType::Texture:
            m_openGlFunctions->glBindTexture( aBindingTarget, aObjectName );
            LOG_OPENGL_ERROR();
            break;
        case ObjectType::Buffer:
            m_openGlFunctions->glBindBuffer( aBindingTarget, aObjectName );
            LOG_OPENGL_ERROR();
            break;
        case ObjectType::VertexArray:
            m_openGlFunctions->glBindVertexArray( aObjectName );
            LOG_OPENGL_ERROR();
            break;
        case ObjectType::Framebuffer:
            m_openGlFunctions->glBindFramebuffer( aBindingTarget, aObjectName );
            LOG_OPENGL_ERROR();
            break;
        case ObjectType::Renderbuffer:
            m_openGlFunctions->glBindRenderbuffer( aBindingTarget, aObjectName );
            LOG_OPENGL_ERROR();
            break;
        case ObjectType::PolygonMode:
            m_openGlFunctions->glPolygonMode( aBindingTarget, aObjectName );
            LOG_OPENGL_ERROR();
            break;
        case ObjectType::CullFaceMode:
            m_openGlFunctions->glCullFace( aBindingTarget );
            LOG_OPENGL_ERROR();
            break;
        default:
            break;
    }
}

void OpenGlObjectBinder::bind()
{
    if ( ! this->isValid() )
    {
        return;
    }

    bool ok = false;
    const GLuint boundData = this->boundData( & ok );
    const GLuint reqData = this->requestedData();
    if ( ! ok || boundData == reqData )
    {
        return;
    }

    m_isRequestedDataBound = true;
    m_previouslyBoundData = boundData;
    this->bind( m_bindingTarget,
                this->isObjectNameUsedAsBoundData()
                ? m_objectName
                : CommonEntities::s_invalidUnsignedGlValue );
}

void OpenGlObjectBinder::unbind()
{
    if ( ! m_isRequestedDataBound )
    {
        return;
    }

    const bool isObjectNameUsedAsBoundData = this->isObjectNameUsedAsBoundData();
    this->bind( isObjectNameUsedAsBoundData
                ? m_bindingTarget
                : m_previouslyBoundData,
                isObjectNameUsedAsBoundData
                ? m_previouslyBoundData
                : CommonEntities::s_invalidUnsignedGlValue );

    m_previouslyBoundData = CommonEntities::s_invalidUnsignedGlValue;
    m_isRequestedDataBound = false;
}

GLuint OpenGlObjectBinder::boundData( bool* const ok ) const
{
    if ( ! this->isValid() )
    {
        if ( ok != nullptr )
        {
            *ok = false;
        }

        return CommonEntities::s_invalidUnsignedGlValue;
    }

    GLint data[ 2 ] = { static_cast< GLint >( CommonEntities::s_invalidUnsignedGlValue ),
                        static_cast< GLint >( CommonEntities::s_invalidUnsignedGlValue ) };

    // GL_POLYGON_MODE fails on AMD/ATi chips:
    // http://devgurus.amd.com/thread/169728
    m_openGlFunctions->glGetIntegerv( this->openGlBindingTargetCheck(), data );
    const GLenum openGlErrorCode = m_openGlFunctions->glGetError();
    if ( openGlErrorCode != GL_NO_ERROR )
    {
        // OpenGlErrorLogger::logOpenGlError( openGlErrorCode, __FILE__, __LINE__ );
        if ( ok != nullptr )
        {
            *ok = false;
        }

        return CommonEntities::s_invalidUnsignedGlValue;
    }

    if ( ok != nullptr )
    {
        *ok = true;
    }

    return static_cast< GLuint >(
                data[ m_objectType == ObjectType::PolygonMode
                      && m_bindingTarget == GL_BACK
                      ? 1
                      : 0 ] );
}
