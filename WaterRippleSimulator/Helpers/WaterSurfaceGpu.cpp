#include "WaterSurfaceGpu.h"

#include "../Data/ShaderInfo.h"
#include "../Utilities/GeneralUtilities.h"
#include "../Utilities/GlUtilities.h"
#include "../Utilities/ImageUtilities.h"
#include "../Utilities/MathUtilities.h"
#include "../WaterRippleSimulator_namespace.h"
#include "OpenGlCapabilityEnabler.h"
#include "OpenGlErrorLogger.h"

#include <QDateTime>
#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Helpers;

using namespace WaterRippleSimulator::Utilities;
using namespace WaterRippleSimulator;

WaterSurfaceGpu::WaterSurfaceGpu(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        const QSurfaceFormat& aOpenGlFormat,
        QObject* const parent )
    : AbstractWaterSurface{ aOpenGlFunctions, parent }
    , m_shaderProgram{ aOpenGlFunctions, aOpenGlFormat }
    , m_vaoId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_veaId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_vboId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_waterDataCalc{ aOpenGlFunctions, aOpenGlFormat, this->meshSize() }
{
    this->applyMeshSizeInternal();
    this->applyPartialShaderData();

    WaterSurfaceGpu::connect( & m_waterDataCalc, SIGNAL(updateRequested()), this, SIGNAL(updateRequested()) );
}

WaterSurfaceGpu::~WaterSurfaceGpu()
{
    this->disposeOpenGlResources();
}

void WaterSurfaceGpu::applyMeshSize()
{
    this->applyMeshSizeInternal();
}

void WaterSurfaceGpu::applyMeshSizeInternal()
{
    m_waterDataCalc.setMeshSize( this->meshSize() );

    const QSize verticesCount = this->verticesCount();
    const int xVerticesCount = verticesCount.width();
    const int yVerticesCount = verticesCount.height();

    m_vertices.clear();
    m_vertices.reserve( xVerticesCount * yVerticesCount );
    for ( int y = 0; y < yVerticesCount; ++ y )
    {
        for ( int x = 0; x < xVerticesCount; ++ x )
        {
            m_vertices.append( VertexDataGpu{ QPoint{ x, y } } );
        }
    }

    m_vertexIndices.clear();
    const GLuint yVertexMaxIndex = yVerticesCount - 1;
    const GLuint xVertexMaxIndex = xVerticesCount - 1;
    m_vertexIndices.reserve( yVertexMaxIndex * xVertexMaxIndex * 2 * Triangle3::s_verticesCount );
    for ( GLuint y = 0u; y < yVertexMaxIndex; ++ y )
    {
        for ( GLuint x = 0u; x < xVertexMaxIndex; ++ x )
        {
            const GLuint bottomLeftIndex  = ( y + 0u ) * xVerticesCount + ( x + 0u );
            const GLuint bottomRightIndex = ( y + 0u ) * xVerticesCount + ( x + 1u );
            const GLuint topRightIndex    = ( y + 1u ) * xVerticesCount + ( x + 1u );
            const GLuint topLeftIndex     = ( y + 1u ) * xVerticesCount + ( x + 0u );

            m_vertexIndices
                    // face 1
                    << bottomLeftIndex
                    << bottomRightIndex
                    << topRightIndex

                    // face 2
                    << bottomLeftIndex
                    << topRightIndex
                    << topLeftIndex;
        }
    }

    this->updateMeshOpenGlData();
}

void WaterSurfaceGpu::disposeOpenGlResources()
{
    if ( CommonEntities::isUnsignedGlValueValid( m_vaoId ) )
    {
        m_openGlFunctions->glDeleteVertexArrays( 1, & m_vaoId );
        LOG_OPENGL_ERROR();

        m_vaoId = CommonEntities::s_invalidUnsignedGlValue;
    }

    QVector< GLuint > vbosToDispose;
    if ( CommonEntities::isUnsignedGlValueValid( m_veaId ) )
    {
        vbosToDispose.append( m_veaId );
        m_veaId = CommonEntities::s_invalidUnsignedGlValue;
    }

    if ( CommonEntities::isUnsignedGlValueValid( m_vboId ) )
    {
        vbosToDispose.append( m_vboId );
        m_vboId = CommonEntities::s_invalidUnsignedGlValue;
    }

    const int vbosToDisposeCount = vbosToDispose.size();
    if ( vbosToDisposeCount > 0 )
    {
        m_openGlFunctions->glDeleteBuffers( vbosToDisposeCount, vbosToDispose.constData() );
        LOG_OPENGL_ERROR();
    }
}

OpenGlObjectBinder WaterSurfaceGpu::vaoBinder( const bool autoUnbind ) const
{
    return AbstractWaterSurface::vaoBinder( m_vaoId, autoUnbind );
}

OpenGlObjectBinder WaterSurfaceGpu::vboBinder( const bool autoUnbind ) const
{
    return AbstractWaterSurface::vboBinder( m_vboId, autoUnbind );
}

OpenGlObjectBinder WaterSurfaceGpu::veaBinder( const bool autoUnbind ) const
{
    return AbstractWaterSurface::veaBinder( m_veaId, autoUnbind );
}

OpenGlObjectBinder WaterSurfaceGpu::heightTextureUnitBinder( const bool autoUnbind ) const
{
    return this->textureUnitBinder( GL_TEXTURE0, autoUnbind );
}

OpenGlObjectBinder WaterSurfaceGpu::heightTextureBinder( const bool autoUnbind ) const
{
    return this->textureBinder( m_waterDataCalc.heightTextureId(), autoUnbind );
}

void WaterSurfaceGpu::init()
{
    AbstractWaterSurface::init();

    if ( CommonEntities::isUnsignedGlValueValid( m_vaoId )
         && CommonEntities::isUnsignedGlValueValid( m_veaId )
         && CommonEntities::isUnsignedGlValueValid( m_vboId )
         && ! m_uniformVariableCachedLocations.isEmpty() )
    {
        return;
    }

    m_waterDataCalc.init();

    const bool isBuiltOk = GlUtilities::buildShaderProgram(
                               m_shaderProgram,
                               ":/Resources/shaders/WaterSurfaceGpu.vert",
                               ":/Resources/shaders/WaterSurface.frag" );
    if ( ! isBuiltOk )
    {
        return;
    }

    this->cacheUniformVariableLocations();

    m_shaderProgram.activate();

    this->applyPartialShaderData();

    m_openGlFunctions->glUniform1i( this->uniformVariableCachedLocation( UniformVariableIndexHeightSampler ), 0 );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1i( this->uniformVariableCachedLocation( UniformVariableIndexColorTexSampler ), 1 );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();

    // setup Vertex Buffer Objects first

    m_openGlFunctions->glGenBuffers( 1, & m_veaId );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glGenBuffers( 1, & m_vboId );
    LOG_OPENGL_ERROR();

    this->updateMeshOpenGlData();

    // setup Vertex Array Objects next
    m_openGlFunctions->glGenVertexArrays( 1, & m_vaoId );
    LOG_OPENGL_ERROR();

    OpenGlObjectBinder vaoBinder = this->vaoBinder( false );
    vaoBinder.bind();

    OpenGlObjectBinder veaBinder = this->veaBinder( false );
    veaBinder.bind();

    this->enableGenericVertexAttributeArrays( true );
    LOG_OPENGL_ERROR();

    OpenGlObjectBinder vboBinder = this->vboBinder( false );
    vboBinder.bind();

    m_openGlFunctions->glVertexAttribIPointer( GenericVertexAttributeIndexVertexTexCoord,
                                               MathUtilities::s_axesCount2D,
                                               GL_UNSIGNED_INT,
                                               sizeof( VertexDataGpu ),
                                               nullptr );
    LOG_OPENGL_ERROR();

    this->enableGenericVertexAttributeArrays( false );
    LOG_OPENGL_ERROR();

    vaoBinder.unbind();
    veaBinder.unbind();
    vboBinder.unbind();
}

void WaterSurfaceGpu::draw( const bool shouldPropagateWater )
{
    const QSize& meshSize = this->meshSize();
    if ( m_openGlFunctions == nullptr
         || meshSize.isEmpty()
         || ! m_shaderProgram.isValid()
         || ! CommonEntities::isUnsignedGlValueValid( m_vaoId )
         || ! CommonEntities::isUnsignedGlValueValid( m_veaId )
         || ! CommonEntities::isUnsignedGlValueValid( m_vboId ) )
    {
        return;
    }

    if ( shouldPropagateWater )
    {
        m_waterDataCalc.propagateWater();
    }

    OpenGlCapabilityEnabler cullFaceEnabler = this->capabilityEnabler( GL_CULL_FACE, false );
    cullFaceEnabler.change();

    m_shaderProgram.activate();
    OpenGlObjectBinder vaoBinder = this->vaoBinder( false );
    vaoBinder.bind();

    OpenGlObjectBinder heightTextureUnitBinder = this->heightTextureUnitBinder( false );
    heightTextureUnitBinder.bind();

    OpenGlObjectBinder heightTextureBinder = this->heightTextureBinder( false );
    heightTextureBinder.bind();

    OpenGlObjectBinder videoFrameTextureUnitBinder = this->textureUnitBinder( GL_TEXTURE1, false );
    videoFrameTextureUnitBinder.bind();

    OpenGlObjectBinder videoFrameTextureBinder = this->videoFrameTextureBinder( false );
    videoFrameTextureBinder.bind();

    OpenGlObjectBinder polygonModeAlternator = this->polygonModeAlternator( GL_FRONT_AND_BACK,
                                                                            this->shouldDrawAsWireframe()
                                                                            ? GL_LINE
                                                                            : GL_FILL,
                                                                            false );
    polygonModeAlternator.bind();

    this->enableGenericVertexAttributeArrays( true );

    m_openGlFunctions->glDrawElements(
                GL_TRIANGLES,
                m_vertexIndices.size(),
                GL_UNSIGNED_INT,
                nullptr );
    LOG_OPENGL_ERROR();

    this->enableGenericVertexAttributeArrays( false );

    polygonModeAlternator.unbind();

    m_shaderProgram.deactivate();

    vaoBinder.unbind();
    videoFrameTextureBinder.unbind();
    videoFrameTextureUnitBinder.unbind();
    heightTextureBinder.unbind();
    heightTextureUnitBinder.unbind();
    cullFaceEnabler.rollback();
}

void WaterSurfaceGpu::applyMatrix(
        const CommonEntities::MatrixType matrixType )
{
    this->applyMatrixInternal( matrixType );
}

void WaterSurfaceGpu::applyMatrixInternal(
        const CommonEntities::MatrixType matrixType )
{
    int uniformVariableIndex = UniformVariableIndexInvalid;
    switch ( matrixType )
    {
        case CommonEntities::MatrixType::Model:
            uniformVariableIndex = UniformVariableIndexModelMatrix;
            break;
        case CommonEntities::MatrixType::Normal:
            uniformVariableIndex = UniformVariableIndexNormalMatrix;
            break;
        case CommonEntities::MatrixType::View:
            uniformVariableIndex = UniformVariableIndexViewMatrix;
            break;
        case CommonEntities::MatrixType::Projection:
            uniformVariableIndex = UniformVariableIndexProjectionMatrix;
            break;
        default:
            return;
    }

    const GLint location = this->uniformVariableCachedLocation( uniformVariableIndex );
    if ( matrixType == CommonEntities::MatrixType::Normal )
    {
        const QMatrix3x3 matrix = this->normalMatrix();
        m_shaderProgram.setUniformMatrix3fv(
                    location,
                    1,
                    GL_FALSE,
                    matrix.constData() );
    }
    else
    {
        const QMatrix4x4 matrix = this->matrix( matrixType );
        m_shaderProgram.setUniformMatrix4fv(
                    location,
                    1,
                    GL_FALSE,
                    matrix.constData() );
    }
}

void WaterSurfaceGpu::propagateWater()
{
    m_waterDataCalc.propagateWater();
}

void WaterSurfaceGpu::stopWaterPropagation()
{
    m_waterDataCalc.stopWaterPropagation();
}

void WaterSurfaceGpu::setWaterRippleMagnitude( const float aWaterRippleMagnitude )
{
    m_waterDataCalc.setWaterRippleMagnitude( aWaterRippleMagnitude );
}

float WaterSurfaceGpu::waterRippleMagnitude() const
{
    return m_waterDataCalc.waterRippleMagnitude();
}

void WaterSurfaceGpu::setWaterRippleRadiusFactor( const QVector2D& aWaterRippleRadiusFactor )
{
    m_waterDataCalc.setWaterRippleRadiusFactor( aWaterRippleRadiusFactor );
}

QVector2D WaterSurfaceGpu::waterRippleRadiusFactor() const
{
    return m_waterDataCalc.waterRippleRadiusFactor();
}

void WaterSurfaceGpu::setWaterRippleRotationAngle( const float aWaterRippleRotationAngle )
{
    m_waterDataCalc.setWaterRippleRotationAngle( aWaterRippleRotationAngle );
}

float WaterSurfaceGpu::waterRippleRotationAngle() const
{
    return m_waterDataCalc.waterRippleRotationAngle();
}

void WaterSurfaceGpu::setWavePropagationSpeed( const float aWavePropagationSpeed )
{
    m_waterDataCalc.setWavePropagationSpeed( aWavePropagationSpeed );
}

float WaterSurfaceGpu::wavePropagationSpeed() const
{
    return m_waterDataCalc.wavePropagationSpeed();
}

void WaterSurfaceGpu::setWaveHeightDampingOverTimeFactor( const float aWaveHeightDampingOverTimeFactor )
{
    m_waterDataCalc.setWaveHeightDampingOverTimeFactor( aWaveHeightDampingOverTimeFactor );
}

float WaterSurfaceGpu::waveHeightDampingOverTimeFactor() const
{
    return m_waterDataCalc.waveHeightDampingOverTimeFactor();
}

void WaterSurfaceGpu::setWaveHeightClampingFactor( const float aWaveHeightClampingFactor )
{
    m_waterDataCalc.setWaveHeightClampingFactor( aWaveHeightClampingFactor );
}

float WaterSurfaceGpu::waveHeightClampingFactor() const
{
    return m_waterDataCalc.waveHeightClampingFactor();
}

void WaterSurfaceGpu::setWaterRippleMethod( const WaterSettings::WaterRippleMethod aWaterRippleMethod )
{
    m_waterDataCalc.setWaterRippleMethod( aWaterRippleMethod );
}

WaterSettings::WaterRippleMethod WaterSurfaceGpu::waterRippleMethod() const
{
    return m_waterDataCalc.waterRippleMethod();
}

void WaterSurfaceGpu::rippleWaterAt( const int x, const int y )
{
    const QSize& meshSize = this->meshSize();

    // disallow rippling at peripheral vertices
    if ( x <= 0
         || x >= meshSize.width()
         || y <= 0
         || y >= meshSize.height() )
    {
        return;
    }

    m_waterDataCalc.rippleWaterAt( x, y );
}

QList< GLuint > WaterSurfaceGpu::allGenericVertexAttributeIndices() const
{
    static const QList< GLuint > indices =
            QList< GLuint >()
            << WaterSurfaceGpu::GenericVertexAttributeIndexVertexTexCoord;

    return indices;
}

void WaterSurfaceGpu::updateMeshOpenGlData()
{
    if ( ! m_shaderProgram.isValid() )
    {
        return;
    }

    OpenGlObjectBinder veaBinder = this->veaBinder( false );
    veaBinder.bind();
    m_openGlFunctions->glBufferData( GL_ELEMENT_ARRAY_BUFFER,
                                     m_vertexIndices.size() * sizeof( GLuint ),
                                     m_vertexIndices.constData(),
                                     GL_STATIC_DRAW );
    LOG_OPENGL_ERROR();
    veaBinder.unbind();

    OpenGlObjectBinder vboBinder = this->vboBinder( false );
    vboBinder.bind();
    m_openGlFunctions->glBufferData( GL_ARRAY_BUFFER,
                                     m_vertices.size() * sizeof( VertexDataGpu ),
                                     m_vertices.constData(),
                                     GL_STATIC_DRAW );
    LOG_OPENGL_ERROR();
    vboBinder.unbind();

    m_shaderProgram.activate();

    const QSize& meshSize = this->meshSize();
    m_openGlFunctions->glUniform2i( this->uniformVariableCachedLocation( UniformVariableIndexMeshVertexMaxIndices ),
                                    meshSize.width(),
                                    meshSize.height() );
    LOG_OPENGL_ERROR();

    const QMatrix4x4 m = GlUtilities::qtToOpenGlXYPlaneTransform();
    const QRectF meshRect = this->meshRect();

    const QPointF bottomLeft = m.map( meshRect.bottomLeft() );
    m_openGlFunctions->glUniform2f( this->uniformVariableCachedLocation( UniformVariableIndexMeshBottomLeft ),
                                    bottomLeft.x(),
                                    bottomLeft.y() );
    LOG_OPENGL_ERROR();

    const QPointF topRight = m.map( meshRect.topRight() );
    m_openGlFunctions->glUniform2f( this->uniformVariableCachedLocation( UniformVariableIndexMeshTopRight ),
                                    topRight.x(),
                                    topRight.y() );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();
}

void WaterSurfaceGpu::applyLight()
{
    this->applyLightInternal();
}

void WaterSurfaceGpu::applyLightInternal()
{
    if ( ! m_shaderProgram.isValid() )
    {
        return;
    }

    m_shaderProgram.activate();

    const Light& light = this->light();
    const QColor& lightColor = light.color;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexLightColor ),
                                    lightColor.redF(),
                                    lightColor.greenF(),
                                    lightColor.blueF() );
    LOG_OPENGL_ERROR();

    const QVector3D& lightPos = light.pos;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexLightPos ),
                                    lightPos.x(),
                                    lightPos.y(),
                                    lightPos.z() );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexLightAttenuationConstant ),
                                    light.attenuation.constant );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexLightAttenuationLinear ),
                                    light.attenuation.linear );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexLightAttenuationQuadratic ),
                                    light.attenuation.quadratic );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexLightIntensity ),
                                    light.intensity );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();
}

void WaterSurfaceGpu::applyMaterial()
{
    this->applyMaterialInternal();
}

void WaterSurfaceGpu::applyObtainsColorFromTexture()
{
    this->applyObtainsColorFromTextureInternal();
}

void WaterSurfaceGpu::calmWater()
{
    m_waterDataCalc.calmWater();
}

void WaterSurfaceGpu::applyMaterialInternal()
{
    if ( ! m_shaderProgram.isValid() )
    {
        return;
    }

    m_shaderProgram.activate();

    const Material& material = this->material();
    const QColor& color = material.color;
    m_openGlFunctions->glUniform4f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialColor ),
                                    color.redF(),
                                    color.greenF(),
                                    color.blueF(),
                                    color.alphaF() );
    LOG_OPENGL_ERROR();

    const QVector3D& emissiveReflectance = material.reflectance.emissive;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialReflectanceEmissive ),
                                    emissiveReflectance.x(),
                                    emissiveReflectance.y(),
                                    emissiveReflectance.z() );
    LOG_OPENGL_ERROR();

    const QVector3D& ambientReflectance = material.reflectance.ambient;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialReflectanceAmbient ),
                                    ambientReflectance.x(),
                                    ambientReflectance.y(),
                                    ambientReflectance.z() );
    LOG_OPENGL_ERROR();

    const QVector3D& diffuseReflectance = material.reflectance.diffuse;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialReflectanceDiffuse ),
                                    diffuseReflectance.x(),
                                    diffuseReflectance.y(),
                                    diffuseReflectance.z() );
    LOG_OPENGL_ERROR();

    const QVector3D& specularReflectance = material.reflectance.specular;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialReflectanceSpecular ),
                                    specularReflectance.x(),
                                    specularReflectance.y(),
                                    specularReflectance.z() );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialShininess ), material.shininess );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();
}

void WaterSurfaceGpu::applyObtainsColorFromTextureInternal()
{
    m_shaderProgram.setUniformVariable1i(
                this->uniformVariableCachedLocation( UniformVariableIndexObtainsColorFromTexture ),
                this->obtainsColorFromTexture()
                ? GL_TRUE
                : GL_FALSE );
}

void WaterSurfaceGpu::applyPartialShaderData()
{
    this->applyMatrixInternal( CommonEntities::MatrixType::Model );
    this->applyMatrixInternal( CommonEntities::MatrixType::View );
    this->applyMatrixInternal( CommonEntities::MatrixType::Projection );
    this->applyLightInternal();
    this->applyMaterialInternal();
    this->applyObtainsColorFromTextureInternal();
}

QByteArray WaterSurfaceGpu::uniformVariableName( const int index )
{
    switch ( index )
    {
        case UniformVariableIndexMaterialColor:
            return "material.color";
        case UniformVariableIndexMaterialReflectanceEmissive:
            return "material.re";
        case UniformVariableIndexMaterialReflectanceAmbient:
            return "material.ra";
        case UniformVariableIndexMaterialReflectanceDiffuse:
            return "material.rd";
        case UniformVariableIndexMaterialReflectanceSpecular:
            return "material.rs";
        case UniformVariableIndexMaterialShininess:
            return "material.shininess";
        case UniformVariableIndexLightColor:
            return "light.color";
        case UniformVariableIndexLightPos:
            return "light.pos";
        case UniformVariableIndexLightAttenuationConstant:
            return "light.kc";
        case UniformVariableIndexLightAttenuationLinear:
            return "light.kl";
        case UniformVariableIndexLightAttenuationQuadratic:
            return "light.kq";
        case UniformVariableIndexLightIntensity:
            return "light.intensity";
        case UniformVariableIndexModelMatrix:
            return "modelMatrix";
        case UniformVariableIndexNormalMatrix:
            return "normalMatrix";
        case UniformVariableIndexViewMatrix:
            return "viewMatrix";
        case UniformVariableIndexProjectionMatrix:
            return "projectionMatrix";
        case UniformVariableIndexEyePos:
            return "eyePos";
        case UniformVariableIndexHeightSampler:
            return "heightSampler";
        case UniformVariableIndexMeshVertexMaxIndices:
            return "meshVertexMaxIndices";
        case UniformVariableIndexMeshBottomLeft:
            return "meshBottomLeft";
        case UniformVariableIndexMeshTopRight:
            return "meshTopRight";
        case UniformVariableIndexObtainsColorFromTexture:
            return "obtainsColorFromTexture";
        case UniformVariableIndexColorTexSampler:
            return "colorTexSampler";
    }

    return QByteArray();
}

bool WaterSurfaceGpu::isUniformVariableIndexValid( const int index )
{
    return 0 <= index && index < UniformVariablesCount;
}

GLint WaterSurfaceGpu::uniformVariableLocation( const int index ) const
{
    if ( ! WaterSurfaceGpu::isUniformVariableIndexValid( index )
         || ! m_shaderProgram.isValid() )
    {
        return CommonEntities::s_invalidSignedGlValue;
    }

    const QByteArray uniformVariableName = WaterSurfaceGpu::uniformVariableName( index );
    return uniformVariableName.isEmpty()
           ? CommonEntities::s_invalidSignedGlValue
           : m_shaderProgram.uniformVarLocation( uniformVariableName );
}

void WaterSurfaceGpu::cacheUniformVariableLocations()
{
    m_uniformVariableCachedLocations.clear();
    m_uniformVariableCachedLocations.reserve( UniformVariablesCount );
    for ( int i = 0; i < UniformVariablesCount; ++ i )
    {
        m_uniformVariableCachedLocations.insert( i, this->uniformVariableLocation( i ) );
    }
}

GLint WaterSurfaceGpu::uniformVariableCachedLocation( const int index ) const
{
    return m_uniformVariableCachedLocations.value( index, CommonEntities::s_invalidSignedGlValue );
}
