#ifndef WATERRIPPLESIMULATOR_HELPERS_FRAMEBUFFERBINDER_H
#define WATERRIPPLESIMULATOR_HELPERS_FRAMEBUFFERBINDER_H

#include <QtGlobal>

#include <gl/GL.h>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

class FramebufferBinder
{
public:
    enum class BindingTarget
    {
        Draw,
        Read,
        Both,
        /*
         * https://www.opengl.org/wiki/Framebuffer :
         *     glBindFramebuffer( GLenum target, GLuint framebuffer ) :
         *         Binding to the GL_FRAMEBUFFER​ target is equivalent to binding that framebuffer to *both* GL_DRAW_FRAMEBUFFER​ and GL_READ_FRAMEBUFFER​.
         *         Note that most other uses of GL_FRAMEBUFFER​ mean the draw framebuffer; this is the case when it means both.
         * https://www.opengl.org/wiki/Framebuffer_Object :
         *     Framebuffer Object Structure :
         *         glBindFramebuffer( GLenum target, GLuint framebuffer ) :
         *             The target​ parameter for this object can take one of 3 values: GL_FRAMEBUFFER​, GL_READ_FRAMEBUFFER​, or GL_DRAW_FRAMEBUFFER​.
         *             The last two allow you to bind an FBO so that reading commands (glReadPixels​, etc) and
         *             writing commands (all rendering commands) can happen to two different framebuffers.
         *             The GL_FRAMEBUFFER​ binding target simply sets both the read and the write to the same FBO.
         *     Attaching Images :
         *         void glFramebufferTexture1D​(GLenum target​, GLenum attachment​, GLenum textarget​, GLuint texture​, GLint level​);
         *         void glFramebufferTexture2D​(GLenum target​, GLenum attachment​, GLenum textarget​, GLuint texture​, GLint level​);
         *         void glFramebufferTextureLayer​(GLenum target​, GLenum attachment​, GLuint texture​, GLint level​, GLint layer​);
         *         ---
         *             The target​ parameter here is the same as the one for bind.
         *             However, GL_FRAMEBUFFER doesn't mean both read and draw (as that would make no sense);
         *             instead, it is the same as GL_DRAW_FRAMEBUFFER.
         *             The attachment​ parameter is one of the above attachment points.
         *
         *     operation                                   | GL_FRAMEBUFFER meaning
         *     ============================================|=================================
         *     glBindFramebuffer( GLenum target, ... )     | GL_DRAW_BUFFER && GL_READ_BUFFER
         *     --------------------------------------------|---------------------------------
         *     glFramebufferTexture1D(GLenum target​, ... ) | GL_DRAW_BUFFER
         *     glFramebufferTexture2D(GLenum target​, ... ) | GL_DRAW_BUFFER
         *     glFramebufferTexture2D(GLenum target​, ... ) | GL_DRAW_BUFFER
         */
    };

public:
    FramebufferBinder();
    FramebufferBinder(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
            const GLuint aRequestedFboId,
            const BindingTarget aBindingTarget,
            const bool aAutoUnbind = true );
    ~FramebufferBinder();

    bool isValid() const;

    GLuint requestedFboId() const;
    BindingTarget bindingTarget() const;

    void bind();
    void unbind();
    GLuint boundFboId() const;

private:
    static GLenum openGlFboBindingTarget( const BindingTarget bindingTarget );
    GLenum openGlFboBindingTarget() const;

    static GLenum openGlFboBindingTargetCheck( const BindingTarget bindingTarget );
    GLenum openGlFboBindingTargetCheck() const;

    void bind( const GLuint fboId );

private:
    QOpenGLFunctions_3_3_Core* m_openGlFunctions;
    GLuint m_requestedFboId;
    BindingTarget m_bindingTarget;

    GLuint m_previouslyBoundFboId;
    bool m_isRequestedFboBound;
    bool m_autoUnbind;
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_FRAMEBUFFERBINDER_H
