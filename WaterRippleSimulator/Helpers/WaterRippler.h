#ifndef WATERRIPPLESIMULATOR_HELPERS_WATERRIPPLER_H
#define WATERRIPPLESIMULATOR_HELPERS_WATERRIPPLER_H

#include "../Data/WaterSettings.h"
#include "../WaterRippleSimulator_namespace.h"
#include "AbstractOpenGlClient.h"
#include "FramebufferBinder.h"
#include "ShaderProgram.h"

#include <QColor>
#include <QHash>
#include <QList>
#include <QObject>
#include <QPair>
#include <QSize>
#include <QtGlobal>
#include <QVector2D>

#include <gl/GL.h>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Helpers {

using namespace WaterRippleSimulator::Data;

class WaterRippler : public QObject, public AbstractOpenGlClient
{
    Q_OBJECT

public:
    explicit WaterRippler(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
            const QSurfaceFormat& aOpenGlFormat,
            const WaterSettings::WaterRippleMethod aMethod,
            const float aMagnitude,
            const QVector2D& aRadiusFactor, // factor relative to the texture's minimum dimension
            const float aRotationAngle,
            QObject* const parent = nullptr );
    ~WaterRippler() Q_DECL_OVERRIDE;

    void setMethod( const WaterSettings::WaterRippleMethod aMethod );
    WaterSettings::WaterRippleMethod method() const;

    void setMagnitude( const float aMagnitude );
    float magnitude() const;

    void setRadiusFactor( const QVector2D& aRadiusFactor );
    QVector2D radiusFactor() const;

    void setRotationAngle( const float aRotationAngle );
    float rotationAngle() const;

    void init() Q_DECL_OVERRIDE;

    void rippleWaterAtRandomPoint( const GLuint orgHeightTextureId, const bool modifySource );
    void rippleWaterAt( const GLuint orgHeightTextureId, const bool modifySource, const QPoint& pt );
    void rippleWaterAt( const GLuint orgHeightTextureId, const bool modifySource, const int x, const int y );
    void rippleWaterAt( const GLuint orgHeightTextureId, const bool modifySource, const float absX, const float absY );

    GLuint rippledHeightTextureId() const;

signals:
    void updateRequested();

private:
    // should be the same as location
    enum FragmentOutputVariableIndex
    {
        FragmentOutputVariableIndexInvalid       = -1,
        FragmentOutputVariableIndexRippledHeight = 0,
        FragmentOutputVariablesCount             = 1
    };

    enum UniformVariableIndex
    {
        UniformVariableIndexInvalid                = -1,

        UniformVariableIndexOrgHeightTexSampler    = 0,
        UniformVariableIndexOrgHeightTexMaxIndices = 1,

        UniformVariableIndexMethod                 = 2,
        UniformVariableIndexMagnitude              = 3,
        UniformVariableIndexRadius                 = 4,
        UniformVariableIndexRotationMatrix         = 5,

        UniformVariableIndexEpicenter              = 6,

        UniformVariablesCount                      = 7
    };

private:
    static const QPoint s_noWaterRippleScheduledPt;
    static const QColor s_clearColor;

private:
    static QByteArray uniformVariableName( const int index );
    static bool isUniformVariableIndexValid( const int index );
    GLint uniformVariableLocation( const int index ) const;
    void cacheUniformVariableLocations();
    GLint uniformVariableCachedLocation( const int index ) const;

    void setTextureSize( const QSize& aTextureSize );
    void updateTextureSizeOpenGlData();

    bool setMethod( const WaterSettings::WaterRippleMethod aMethod,
                    const bool shouldUpdateOpenGlData );
    void updateMethodOpenGlData();

    bool setMagnitude( const float aMagnitude, const bool shouldUpdateOpenGlData );
    void updateMagnitudeOpenGlData();

    void setRadius( const QVector2D& aRadius );
    void updateRadiusOpenGlData();

    bool setRotationAngle( const float aRotationAngle, const bool shouldUpdateOpenGlData );
    void updateRotationAngleOpenGlData();

    void setEpicenter( const QPoint& aEpicenter );
    void updateEpicenterOpenGlData();

    void updateOpenGlData();
    void disposeOpenGlResources();

    static float randomPositiveNormalizedRealValue();

    FramebufferBinder fboBinder( const bool autoUnbind ) const;
    void setupFbo();

private:
    ShaderProgram m_shaderProgram;

    GLuint m_fboId;
    GLuint m_vaoId;
    GLuint m_rippledHeightTextureId;

    // 2: all shaders common variable - per vertex and per fragment invariant
    QHash< int, GLint > m_uniformVariableCachedLocations;

    WaterSettings::WaterRippleMethod m_method;
    float m_magnitude;
    QVector2D m_radiusFactor;
    float m_rotationAngle;

    QVector2D m_radius;

    QSize m_textureSize;
    QPoint m_epicenter;
};

}
}

#endif // WATERRIPPLESIMULATOR_HELPERS_WATERRIPPLER_H
