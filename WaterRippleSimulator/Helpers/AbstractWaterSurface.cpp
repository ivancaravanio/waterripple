#include "AbstractWaterSurface.h"

#include "../Data/ShaderInfo.h"
#include "../Data/WaterSettings.h"
#include "../Utilities/GlUtilities.h"
#include "../Utilities/MathUtilities.h"
#include "../WaterRippleSimulator_namespace.h"
#include "OpenGlErrorLogger.h"

#include <QAbstractVideoBuffer>
#include <QDateTime>
#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Helpers;

using namespace WaterRippleSimulator::Utilities;
using namespace WaterRippleSimulator;

AbstractWaterSurface::AbstractWaterSurface(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        QObject* const parent )
    : QObject{ parent }
    , AbstractOpenGlClient{ aOpenGlFunctions }
    , m_meshSize{ WaterSettings::s_meshDimensionDefault, WaterSettings::s_meshDimensionDefault }
    , m_shouldDrawAsWireframe{ WaterSettings::s_shouldDrawAsWireframeDefault }
    , m_matrices( MatricesCount )
    , m_obtainsColorFromTexture{ false }
    , m_videoFrameTextureId{ CommonEntities::s_invalidUnsignedGlValue }
{
}

AbstractWaterSurface::~AbstractWaterSurface()
{
}

void AbstractWaterSurface::setMeshSize( const QSize& aMeshSize )
{
    if ( this->setMeshSizeInternal( aMeshSize ) )
    {
        emit updateRequested();
    }
}

const QSize& AbstractWaterSurface::meshSize() const
{
    return m_meshSize;
}

QSize AbstractWaterSurface::verticesCount() const
{
    return MathUtilities::squaresToVerticesCount( m_meshSize );
}

void AbstractWaterSurface::setShouldDrawAsWireframe( const bool aShouldDrawAsWireframe )
{
    if ( this->setShouldDrawAsWireframeInternal( aShouldDrawAsWireframe ) )
    {
        emit updateRequested();
    }
}

bool AbstractWaterSurface::setShouldDrawAsWireframeInternal( const bool aShouldDrawAsWireframe )
{
    if ( aShouldDrawAsWireframe == m_shouldDrawAsWireframe )
    {
        return false;
    }

    m_shouldDrawAsWireframe = aShouldDrawAsWireframe;
    this->applyShouldDrawAsWireframe();

    return true;
}

bool AbstractWaterSurface::setMatrixInternal(
        const CommonEntities::MatrixType matrixType,
        const QMatrix4x4& matrix )
{
    const int matrixIndex = AbstractWaterSurface::matrixIndexFromType( matrixType );
    if ( ! AbstractWaterSurface::isMatrixIndexValid( matrixIndex ) )
    {
        return false;
    }

    QMatrix4x4& currentMatrix = m_matrices[ matrixIndex ];
    if ( matrix == currentMatrix )
    {
        return false;
    }

    currentMatrix = matrix;
    this->applyMatrix( matrixType );
    if ( matrixType == CommonEntities::MatrixType::Model )
    {
        this->applyMatrix( CommonEntities::MatrixType::Normal );
    }

    return true;
}

bool AbstractWaterSurface::shouldDrawAsWireframe() const
{
    return m_shouldDrawAsWireframe;
}

void AbstractWaterSurface::init()
{
    if ( m_openGlFunctions == nullptr
         || CommonEntities::isUnsignedGlValueValid( m_videoFrameTextureId ) )
    {
        return;
    }

    m_openGlFunctions->glGenTextures( 1, & m_videoFrameTextureId );
    LOG_OPENGL_ERROR();
}

bool AbstractWaterSurface::setMeshSizeInternal( const QSize& aMeshSize )
{
    if ( aMeshSize.isEmpty()
         || aMeshSize == m_meshSize )
    {
        return false;
    }

    m_meshSize = aMeshSize;
    this->applyMeshSize();

    return true;
}

bool AbstractWaterSurface::setLightInternal( const Light& aLight )
{
    if ( aLight == m_light )
    {
        return false;
    }

    m_light = aLight;
    this->applyLight();

    return true;
}

bool AbstractWaterSurface::setMaterialInternal( const Material& aMaterial )
{
    if ( aMaterial == m_material )
    {
        return false;
    }

    m_material = aMaterial;
    this->applyMaterial();

    return true;
}

bool AbstractWaterSurface::obtainColorFromTextureInternal( const bool aObtainsColorFromTexture )
{
    if ( aObtainsColorFromTexture == m_obtainsColorFromTexture )
    {
        return false;
    }

    m_obtainsColorFromTexture = aObtainsColorFromTexture;
    this->applyObtainsColorFromTexture();

    return true;
}

int AbstractWaterSurface::matrixIndexFromType( const CommonEntities::MatrixType matrixType )
{
    switch ( matrixType )
    {
        case CommonEntities::MatrixType::Model:
            return MatrixIndexModel;
        case CommonEntities::MatrixType::View:
            return MatrixIndexView;
        case CommonEntities::MatrixType::Projection:
            return MatrixIndexProjection;
        default:
            break;
    }

    return MatrixIndexInvalid;
}

int AbstractWaterSurface::isMatrixIndexValid( const int matrixType )
{
    return 0 <= matrixType && matrixType < MatricesCount;
}

void AbstractWaterSurface::setMatrix(
        const CommonEntities::MatrixType matrixType,
        const QMatrix4x4& matrix )
{
    if ( this->setMatrixInternal( matrixType, matrix ) )
    {
        emit updateRequested();
    }
}

QMatrix4x4 AbstractWaterSurface::matrix( const CommonEntities::MatrixType matrixType ) const
{
    const int matrixIndex = AbstractWaterSurface::matrixIndexFromType( matrixType );
    return AbstractWaterSurface::isMatrixIndexValid( matrixIndex )
           ? m_matrices[ matrixIndex ]
           : QMatrix4x4();
}

QMatrix3x3 AbstractWaterSurface::normalMatrix() const
{
    // http://www.arcsynthesis.org/gltut/Illumination/Tut09%20Normal%20Transformation.html
    return m_matrices[ MatrixIndexModel ].normalMatrix();
}

void AbstractWaterSurface::setLight( const Light& aLight )
{
    if ( this->setLightInternal( aLight ) )
    {
        emit updateRequested();
    }
}

const Light& AbstractWaterSurface::light() const
{
    return m_light;
}

void AbstractWaterSurface::setMaterial( const Material& aMaterial )
{
    if ( this->setMaterialInternal( aMaterial ) )
    {
        emit updateRequested();
    }
}

const Material& AbstractWaterSurface::material() const
{
    return m_material;
}

QRectF AbstractWaterSurface::drawableAreaRect() const
{
    return QRectF{ QPointF{ CommonEntities::s_drawingVolumeBoundsXMin,
                            CommonEntities::s_drawingVolumeBoundsYMin },
                   QPointF{ CommonEntities::s_drawingVolumeBoundsXMax,
                            CommonEntities::s_drawingVolumeBoundsYMax } };
}

QList< Triangle3 > AbstractWaterSurface::drawableAreaRectTriangulated() const
{
    return AbstractWaterSurface::triangulatedRect( this->drawableAreaRect() );
}

QList< Triangle3 > AbstractWaterSurface::triangulatedRect( const QRectF& rect )
{
    QList< QVector3D > vertices;
    vertices.reserve( 4 );
    vertices << QVector3D{ rect.bottomLeft() }
             << QVector3D{ rect.bottomRight() }
             << QVector3D{ rect.topRight() }
             << QVector3D{ rect.topLeft() };

    const QMatrix4x4 m = GlUtilities::qtToOpenGlXYPlaneTransform();
    for ( QVector3D& v : vertices )
    {
        v = m.map( v );
    }

    const QVector3D& bottomLeft = vertices[ 0 ];
    const QVector3D& topRight = vertices[ 2 ];

    return QList< Triangle3 >()
           << Triangle3{ bottomLeft, vertices[ 1 ], topRight }
           << Triangle3{ topRight, vertices[ 3 ], bottomLeft };
}

QRectF AbstractWaterSurface::meshRect() const
{
    if ( m_meshSize.isEmpty() )
    {
        return QRectF{};
    }

    const QRectF drawableAreaRect = this->drawableAreaRect();
    const qreal minSideLength = qMin( drawableAreaRect.width(), drawableAreaRect.height() );

    const int meshXUnitsCount = m_meshSize.width();
    const int meshYUnitsCount = m_meshSize.height();

    const qreal meshUnitSideLength = sqrt( minSideLength * minSideLength / ( meshXUnitsCount * meshXUnitsCount
                                                                             + meshYUnitsCount * meshYUnitsCount ) );
    const qreal meshXLength = meshXUnitsCount * meshUnitSideLength;
    const qreal meshYLength = meshYUnitsCount * meshUnitSideLength;

    QRectF meshRect{ QPointF{ 0.0, 0.0 }, QSizeF{ meshXLength, meshYLength } };
    meshRect.moveCenter( drawableAreaRect.center() );

    return meshRect;
}

QList< Triangle3 > AbstractWaterSurface::meshRectTriangulated() const
{
    return AbstractWaterSurface::triangulatedRect( this->meshRect() );
}

QVector2D AbstractWaterSurface::waterRippleRadius() const
{
    const QSize verticesCount = this->verticesCount();
    return this->waterRippleRadiusFactor()
           * qMin( verticesCount.width(), verticesCount.height() );
}

void AbstractWaterSurface::obtainColorFromTexture( const bool aObtainsColorFromTexture )
{
    if ( this->obtainColorFromTextureInternal( aObtainsColorFromTexture ) )
    {
        emit updateRequested();
    }
}

bool AbstractWaterSurface::obtainsColorFromTexture() const
{
    return m_obtainsColorFromTexture;
}

void AbstractWaterSurface::rippleWaterAtRandomPoint()
{
    if ( m_meshSize.isEmpty() )
    {
        return;
    }

    const int x = MathUtilities::randomNumber( m_meshSize.width() );
    const int y = MathUtilities::randomNumber( m_meshSize.height() );
    this->rippleWaterAt( x, y );
}

void AbstractWaterSurface::rippleWaterAt( const QVector3D& pt )
{
    if ( m_meshSize.isEmpty() )
    {
        return;
    }

    const QRectF meshRect = this->meshRect();
    const qreal x = pt.x();
    const qreal y = pt.y();
    if ( ! meshRect.contains( QPointF{ x, y } ) )
    {
        return;
    }

    const qreal relX = x - meshRect.left();
    const qreal relY = y - meshRect.top();

    const int vertexXIndex = qRound( m_meshSize.width() * ( relX / meshRect.width() ) );
    const int vertexYIndex = qRound( m_meshSize.height() * ( relY / meshRect.height() ) );
    this->rippleWaterAt( vertexXIndex,
                         vertexYIndex );
}

void AbstractWaterSurface::displayVideoFrame( const QVideoFrame& videoFrame )
{
    if ( ! this->obtainsColorFromTexture()
         || ! CommonEntities::isUnsignedGlValueValid( m_videoFrameTextureId )
         || ! videoFrame.isValid()
         || m_openGlFunctions == nullptr )
    {
        return;
    }

    QVideoFrame videoFrameExplicitCopy = videoFrame;
    if ( ! videoFrameExplicitCopy.map( QAbstractVideoBuffer::ReadOnly ) )
    {
        return;
    }

    OpenGlObjectBinder videoFrameTextureBinder{
            m_openGlFunctions,
            OpenGlObjectBinder::ObjectType::Texture,
            GL_TEXTURE_2D,
            m_videoFrameTextureId,
            true };
    videoFrameTextureBinder.bind();

    const QSize newVideoFrameSize = videoFrame.size();
    if ( newVideoFrameSize.isEmpty() )
    {
        videoFrameExplicitCopy.unmap();
        return;
    }

    if ( newVideoFrameSize != m_videoFrameSize )
    {
        m_videoFrameSize = newVideoFrameSize;
        GlUtilities::setTexture(
                    m_openGlFunctions,
                    videoFrame.bits(),
                    m_videoFrameSize.width(),
                    m_videoFrameSize.height(),
                    GL_RGB5_A1,
                    GL_BGRA,
                    GL_UNSIGNED_SHORT_1_5_5_5_REV,
                    GlUtilities::TextureFilter::Nearest,
                    GlUtilities::TextureFilter::Nearest,
                    GlUtilities::TextureWrapMode::ClampToEdge,
                    GlUtilities::TextureWrapMode::ClampToEdge,
                    true );
    }
    else
    {
        m_openGlFunctions->glTexSubImage2D(
                    GL_TEXTURE_2D,
                    0, // level
                    0, // x-offset
                    0, // y-offset
                    m_videoFrameSize.width(),
                    m_videoFrameSize.height(),
                    GL_BGRA,
                    GL_UNSIGNED_SHORT_1_5_5_5_REV,
                    videoFrame.bits() );
        LOG_OPENGL_ERROR();
    }

    videoFrameExplicitCopy.unmap();

    emit updateRequested();
}

QList< GLuint > AbstractWaterSurface::allGenericVertexAttributeIndices() const
{
    return QList< GLuint >();
}

void AbstractWaterSurface::enableGenericVertexAttributeArrays( const bool enable )
{
    for ( const GLuint genericVertexAttributeIndex : this->allGenericVertexAttributeIndices() )
    {
        if ( enable )
        {
            m_openGlFunctions->glEnableVertexAttribArray( genericVertexAttributeIndex );
            LOG_OPENGL_ERROR();
        }
        else
        {
            m_openGlFunctions->glDisableVertexAttribArray( genericVertexAttributeIndex );
            LOG_OPENGL_ERROR();
        }
    }
}

void AbstractWaterSurface::applyShouldDrawAsWireframe()
{
}

OpenGlObjectBinder AbstractWaterSurface::videoFrameTextureBinder( const bool autoUnbind ) const
{
    return this->textureBinder( m_videoFrameTextureId, autoUnbind );
}
