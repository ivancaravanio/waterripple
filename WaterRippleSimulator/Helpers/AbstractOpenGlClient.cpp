#include "AbstractOpenGlClient.h"

#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Helpers;

AbstractOpenGlClient::AbstractOpenGlClient(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions )
    : m_openGlFunctions{ aOpenGlFunctions }
{
}

AbstractOpenGlClient::~AbstractOpenGlClient()
{
}

OpenGlObjectBinder AbstractOpenGlClient::textureUnitBinder(
        const GLenum textureUnit,
        const bool autoUnbind ) const
{
    return OpenGlObjectBinder{ m_openGlFunctions,
                               OpenGlObjectBinder::ObjectType::TextureUnit,
                               textureUnit,
                               CommonEntities::s_invalidUnsignedGlValue,
                               autoUnbind };
}

OpenGlObjectBinder AbstractOpenGlClient::textureBinder(
        const GLuint textureId,
        const bool autoUnbind ) const
{
    return CommonEntities::isUnsignedGlValueValid( textureId )
           ? OpenGlObjectBinder{ m_openGlFunctions,
                                 OpenGlObjectBinder::ObjectType::Texture,
                                 GL_TEXTURE_2D,
                                 textureId,
                                 autoUnbind }
           : OpenGlObjectBinder();
}

OpenGlObjectBinder AbstractOpenGlClient::vaoBinder( const GLuint vaoId, const bool autoUnbind ) const
{
    return CommonEntities::isUnsignedGlValueValid( vaoId )
           ? OpenGlObjectBinder{ m_openGlFunctions,
                                 OpenGlObjectBinder::ObjectType::VertexArray,
                                 CommonEntities::invalidEnumGlValue(),
                                 vaoId,
                                 autoUnbind }
           : OpenGlObjectBinder();
}

OpenGlObjectBinder AbstractOpenGlClient::vboBinder( const GLuint vboId, const bool autoUnbind ) const
{
    return CommonEntities::isUnsignedGlValueValid( vboId )
           ? OpenGlObjectBinder{ m_openGlFunctions,
                                 OpenGlObjectBinder::ObjectType::Buffer,
                                 GL_ARRAY_BUFFER,
                                 vboId,
                                 autoUnbind }
           : OpenGlObjectBinder();
}

OpenGlObjectBinder AbstractOpenGlClient::veaBinder( const GLuint veaId, const bool autoUnbind ) const
{
    return CommonEntities::isUnsignedGlValueValid( veaId )
           ? OpenGlObjectBinder{ m_openGlFunctions,
                                 OpenGlObjectBinder::ObjectType::Buffer,
                                 GL_ELEMENT_ARRAY_BUFFER,
                                 veaId,
                                 autoUnbind }
           : OpenGlObjectBinder();
}

OpenGlObjectBinder AbstractOpenGlClient::polygonModeAlternator(
        const GLenum face,
        const GLenum mode,
        const bool autoUnbind )
{
    return OpenGlObjectBinder{
                m_openGlFunctions,
                OpenGlObjectBinder::ObjectType::PolygonMode,
                face,
                static_cast< GLuint >( mode ),
                autoUnbind };
}

FramebufferBinder AbstractOpenGlClient::fboBinder(
        const GLuint aRequestedFboId,
        const FramebufferBinder::BindingTarget aBindingTarget,
        const bool aAutoUnbind ) const
{
    return FramebufferBinder(
                m_openGlFunctions,
                aRequestedFboId,
                aBindingTarget,
                aAutoUnbind );
}

ViewportAlternator AbstractOpenGlClient::viewportAlternator( const QRect& aRequestedViewport, const bool autoRollback ) const
{
    return ViewportAlternator{
                m_openGlFunctions,
                aRequestedViewport,
                autoRollback };
}

OpenGlCapabilityEnabler AbstractOpenGlClient::capabilityEnabler(
        const GLenum aCapability,
        const bool aShouldEnable,
        const bool aAutoRollback ) const
{
    return OpenGlCapabilityEnabler{
                m_openGlFunctions,
                aCapability,
                aShouldEnable,
                aAutoRollback };
}
