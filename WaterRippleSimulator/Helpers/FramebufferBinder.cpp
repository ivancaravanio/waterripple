#include "FramebufferBinder.h"

#include "../WaterRippleSimulator_namespace.h"
#include "OpenGlErrorLogger.h"

#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Helpers;

FramebufferBinder::FramebufferBinder()
    : m_openGlFunctions{ nullptr }
    , m_requestedFboId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_bindingTarget{ BindingTarget::Both }
    , m_previouslyBoundFboId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_isRequestedFboBound{ false }
    , m_autoUnbind{ false }
{
}

FramebufferBinder::FramebufferBinder(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        const GLuint aRequestedFboId,
        const BindingTarget aBindingTarget,
        const bool aAutoUnbind )
    : m_openGlFunctions{ aOpenGlFunctions }
    , m_requestedFboId{ aRequestedFboId }
    , m_bindingTarget{ aBindingTarget }
    , m_previouslyBoundFboId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_isRequestedFboBound{ false }
    , m_autoUnbind{ aAutoUnbind }
{
}

FramebufferBinder::~FramebufferBinder()
{
    if ( m_autoUnbind )
    {
        this->unbind();
    }
}

bool FramebufferBinder::isValid() const
{
    return m_openGlFunctions != nullptr
           // 0 - the index of the default framebuffer - the screen
           // https://www.opengl.org/wiki/Default_Framebuffer
           // default framebuffer can differ from the OpenGL's default one.
           // Qt 5.4's QOpenGLWindow and QOpenGLWidget classes use a dedicated one.
           // && CommonEntities::isUnsignedGlValueValid( m_requestedFboId )
                                ;
}

GLuint FramebufferBinder::requestedFboId() const
{
    return m_requestedFboId;
}

FramebufferBinder::BindingTarget FramebufferBinder::bindingTarget() const
{
    return m_bindingTarget;
}

GLuint FramebufferBinder::boundFboId() const
{
    if ( ! this->isValid() )
    {
        return CommonEntities::s_invalidUnsignedGlValue;
    }

    // 0 is the default FBO -> screen
    // CAUTION: Qt's QOpenGL(Widget|Window) use a proxy framebuffer that differs from the screen's one:
    //
    // http://doc-snapshot.qt-project.org/qt5-5.4/qopenglwindow.html :
    //     QOpenGLWindow supports multiple update behaviors.
    //     The default, NoPartialUpdate is equivalent to a regular, OpenGL-based QWindow or the legacy QGLWidget.
    //     In contrast, PartialUpdateBlit and PartialUpdateBlend are more in line with QOpenGLWidget's way of working, where there is always an extra, dedicated framebuffer object present.
    //     These modes allow, by sacrificing some performance, redrawing only a smaller area on each paint and having the rest of the content preserved from of the previous frame.
    //     This is useful for applications than render incrementally using QPainter, because this way they do not have to redraw the entire window content on each paintGL() call.
    //
    // http://doc-snapshot.qt-project.org/qt5-5.4/qopenglwidget.html :
    //     All rendering happens into an OpenGL framebuffer object.
    //     makeCurrent() ensure that it is bound in the context.
    //     Keep this in mind when creating and binding additional framebuffer objects in the rendering code in paintGL().
    //     Never re-bind the framebuffer with ID 0.
    //     Instead, call defaultFramebufferObject() to get the ID that should be bound.

    GLint currentlyBoundFbo = CommonEntities::s_invalidUnsignedGlValue;
    m_openGlFunctions->glGetIntegerv( this->openGlFboBindingTargetCheck(), & currentlyBoundFbo );
    LOG_OPENGL_ERROR();

    return currentlyBoundFbo;
}

GLenum FramebufferBinder::openGlFboBindingTarget( const FramebufferBinder::BindingTarget bindingTarget )
{
    switch ( bindingTarget )
    {
        case BindingTarget::Draw:
            return GL_DRAW_FRAMEBUFFER;
        case BindingTarget::Read:
            return GL_READ_FRAMEBUFFER;
        case BindingTarget::Both:
            return GL_FRAMEBUFFER;
    }

    return GL_FRAMEBUFFER;
}

GLenum FramebufferBinder::openGlFboBindingTarget() const
{
    return FramebufferBinder::openGlFboBindingTarget( m_bindingTarget );
}

GLenum FramebufferBinder::openGlFboBindingTargetCheck( const FramebufferBinder::BindingTarget bindingTarget )
{
    switch ( bindingTarget )
    {
        case BindingTarget::Draw:
            return GL_DRAW_FRAMEBUFFER_BINDING;
        case BindingTarget::Read:
            return GL_READ_FRAMEBUFFER_BINDING;
        case BindingTarget::Both:
            return GL_FRAMEBUFFER_BINDING;
    }

    return GL_FRAMEBUFFER_BINDING;
}

GLenum FramebufferBinder::openGlFboBindingTargetCheck() const
{
    return FramebufferBinder::openGlFboBindingTargetCheck( m_bindingTarget );
}

void FramebufferBinder::bind( const GLuint fboId )
{
    m_openGlFunctions->glBindFramebuffer( this->openGlFboBindingTarget(), fboId );
    LOG_OPENGL_ERROR();
}

void FramebufferBinder::bind()
{
    if ( ! this->isValid() )
    {
        return;
    }

    const GLuint boundFboId = this->boundFboId();
    if ( boundFboId == m_requestedFboId )
    {
        return;
    }

    m_previouslyBoundFboId = boundFboId;
    m_isRequestedFboBound = true;
    this->bind( m_requestedFboId );
}

void FramebufferBinder::unbind()
{
    if ( ! m_isRequestedFboBound )
    {
        return;
    }

    this->bind( m_previouslyBoundFboId );

    m_previouslyBoundFboId = CommonEntities::s_invalidUnsignedGlValue;
    m_isRequestedFboBound = false;
}
