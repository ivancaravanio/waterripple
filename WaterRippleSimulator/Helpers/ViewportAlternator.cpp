#include "ViewportAlternator.h"

#include "../WaterRippleSimulator_namespace.h"
#include "OpenGlErrorLogger.h"

#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Helpers;

ViewportAlternator::ViewportAlternator()
    : m_openGlFunctions{ nullptr }
    , m_autoRollback{ false }
{
}

ViewportAlternator::ViewportAlternator(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        const QRect& aRequestedViewport,
        const bool aAutoRollback )
    : m_openGlFunctions{ aOpenGlFunctions }
    , m_requestedViewport{ aRequestedViewport }
    , m_autoRollback{ aAutoRollback }
{
}

ViewportAlternator::~ViewportAlternator()
{
    if ( m_autoRollback )
    {
        this->rollback();
    }
}

bool ViewportAlternator::isValid() const
{
    return m_openGlFunctions != nullptr
           && m_requestedViewport.isValid();
}

const QRect& ViewportAlternator::requestedViewport() const
{
    return m_requestedViewport;
}

QRect ViewportAlternator::currentViewport() const
{
    // viewport is 2D and its Y axis is pointing upwards
    enum ViewportCoord
    {
        ViewportCoordBottomLeftX = 0,
        ViewportCoordBottomLeftY = 1,
        ViewportCoordWidth       = 2,
        ViewportCoordHeight      = 3,
        ViewportCoordsCount      = 4,
    };

    GLint viewport[ ViewportCoordsCount ] = { 0 };

    m_openGlFunctions->glGetIntegerv( GL_VIEWPORT, viewport );
    LOG_OPENGL_ERROR();

    return QRect( viewport[ ViewportCoordBottomLeftX ],
                  viewport[ ViewportCoordBottomLeftY ],
                  viewport[ ViewportCoordWidth       ],
                  viewport[ ViewportCoordHeight      ] );
}

void ViewportAlternator::change( const QRect& viewport )
{
    m_openGlFunctions->glViewport(
                viewport.x(),
                viewport.y(),
                viewport.width(),
                viewport.height() );
    LOG_OPENGL_ERROR();
}

void ViewportAlternator::change()
{
    if ( ! this->isValid() )
    {
        return;
    }

    const QRect currentVp = this->currentViewport();
    if ( currentVp == m_requestedViewport )
    {
        return;
    }

    m_previousViewport = currentVp;
    this->change( m_requestedViewport );
}

void ViewportAlternator::rollback()
{
    if ( m_previousViewport.isNull() )
    {
        return;
    }

    this->change( m_previousViewport );

    m_previousViewport = QRect();
}
