#include "WaterPool.h"

#include "../Data/Mesh3.h"
#include "../Data/ShaderInfo.h"
#include "../Utilities/GeneralUtilities.h"
#include "../Utilities/GlUtilities.h"
#include "../Utilities/ImageUtilities.h"
#include "../Utilities/MathUtilities.h"
#include "../WaterRippleSimulator_namespace.h"
#include "OpenGlCapabilityEnabler.h"
#include "OpenGlErrorLogger.h"

#include <QDateTime>
#include <QGLWidget>
#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Helpers;

using namespace WaterRippleSimulator::Utilities;
using namespace WaterRippleSimulator;

const QList< GLuint > WaterPool::s_allOrderedGenericVertexAttributeIndices =
        QList< GLuint >()
        << WaterPool::GenericVertexAttributeIndexVertexPos
        << WaterPool::GenericVertexAttributeIndexVertexTexCoord
        << WaterPool::GenericVertexAttributeIndexVertexNormal;

WaterPool::WaterPool(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        const QSurfaceFormat& aOpenGlFormat,
        const QRectF& aWaterRect,
        QObject* const parent )
    : QObject{ parent }
    , AbstractOpenGlClient{ aOpenGlFunctions }
    , m_shaderProgram{ aOpenGlFunctions, aOpenGlFormat }
    , m_vaoId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_vboId{ CommonEntities::s_invalidUnsignedGlValue }
    , m_textureId{ CommonEntities::s_invalidUnsignedGlValue }
{
    this->setWaterRectInternal( aWaterRect );
}

WaterPool::~WaterPool()
{
    this->disposeOpenGlResources();
}

void WaterPool::setWaterRect( const QRectF& aWaterRect )
{
    this->setWaterRectInternal( aWaterRect );
    emit updateRequested();
}

QRectF WaterPool::waterRect() const
{
    return m_waterRect;
}

void WaterPool::setWaterRectInternal( const QRectF& aWaterRect )
{
    if ( aWaterRect.isEmpty()
         || aWaterRect == m_waterRect )
    {
        return;
    }

    m_waterRect = aWaterRect;

    m_verticesData.clear();

    QList< QVector3D > vertices;
    vertices.reserve( VerticesCount );
    vertices << QVector3D{ m_waterRect.topLeft() }
             << QVector3D{ m_waterRect.topRight() }
             << QVector3D{ m_waterRect.bottomRight() }
             << QVector3D{ m_waterRect.bottomLeft() };

    static const float overWaterOffset = 0.2f;
    static const float underWaterOffset = 0.5f;

    const int verticesCount = vertices.size();
    for ( int i = 0; i < verticesCount; ++ i )
    {
        QVector3D& v = vertices[ i ];
        QVector3D underWaterVertex = v;
        v.setZ( v.z() + overWaterOffset );
        underWaterVertex.setZ( underWaterVertex.z() - underWaterOffset );
        vertices.append( underWaterVertex );
    }

    const QVector2D vertexTexCoordBottomLeft{ 0.0f, 0.0f };
    const QVector2D vertexTexCoordBottomRight{ 1.0f, 0.0f };
    const QVector2D vertexTexCoordTopRight{ 1.0f, 1.0f };
    const QVector2D vertexTexCoordTopLeft{ 0.0f, 1.0f };

    const Triangle2 bottomRightTexCoordHalf{
                vertexTexCoordBottomLeft,
                vertexTexCoordBottomRight,
                vertexTexCoordTopRight };
    const Triangle2 topLeftTexCoordHalf{
                vertexTexCoordTopRight,
                vertexTexCoordTopLeft,
                vertexTexCoordBottomLeft };

    QList< QPair< QList< int >, bool > > triPosTexPairs;
    triPosTexPairs.reserve( VerticesCount );

    // wall - bottom - face 1
    triPosTexPairs << QPair< QList< int >, bool >(
                QList< int >()
                << VertexIndexFloorBottomRight
                << VertexIndexFloorBottomLeft
                << VertexIndexRoofBottomLeft,
                true );

    // wall - bottom - face 2
    triPosTexPairs << QPair< QList< int >, bool >(
                QList< int >()
                << VertexIndexRoofBottomLeft
                << VertexIndexRoofBottomRight
                << VertexIndexFloorBottomRight,
                false );

    // wall - right - face 1
    triPosTexPairs << QPair< QList< int >, bool >(
                QList< int >()
                << VertexIndexFloorTopRight
                << VertexIndexFloorBottomRight
                << VertexIndexRoofBottomRight,
                true );

    // wall - right - face 2
    triPosTexPairs << QPair< QList< int >, bool >(
                QList< int >()
                << VertexIndexRoofBottomRight
                << VertexIndexRoofTopRight
                << VertexIndexFloorTopRight,
                false );

    // wall - top - face 1
    triPosTexPairs << QPair< QList< int >, bool >(
                QList< int >()
                << VertexIndexFloorTopLeft
                << VertexIndexFloorTopRight
                << VertexIndexRoofTopRight,
                true );

    // wall - top - face 2
    triPosTexPairs << QPair< QList< int >, bool >(
                QList< int >()
                << VertexIndexRoofTopRight
                << VertexIndexRoofTopLeft
                << VertexIndexFloorTopLeft,
                false );

    // wall - left - face 1
    triPosTexPairs << QPair< QList< int >, bool >(
                QList< int >()
                << VertexIndexFloorBottomLeft
                << VertexIndexFloorTopLeft
                << VertexIndexRoofTopLeft,
                true );

    // wall - left - face 2
    triPosTexPairs << QPair< QList< int >, bool >(
                QList< int >()
                << VertexIndexRoofTopLeft
                << VertexIndexRoofBottomLeft
                << VertexIndexFloorBottomLeft,
                false );

    // floor - face 1
    triPosTexPairs << QPair< QList< int >, bool >(
                QList< int >()
                << VertexIndexFloorBottomLeft
                << VertexIndexFloorBottomRight
                << VertexIndexFloorTopRight,
                true );

    // floor - face 2
    triPosTexPairs << QPair< QList< int >, bool >(
                QList< int >()
                << VertexIndexFloorTopRight
                << VertexIndexFloorTopLeft
                << VertexIndexFloorBottomLeft,
                false );

    QList< QPair< Triangle3, Triangle2 > > posTexCoordsTriangles;
    posTexCoordsTriangles.reserve( 5 * 2 );

    for ( const QPair< QList< int >, bool >& triPosTexPair : triPosTexPairs )
    {
        const QPair< Triangle3, Triangle2 > posTexCoordsTriangle{
            Triangle3{ vertices[ triPosTexPair.first[ 0 ] ],
                       vertices[ triPosTexPair.first[ 1 ] ],
                       vertices[ triPosTexPair.first[ 2 ] ] },
            triPosTexPair.second
            ? bottomRightTexCoordHalf
            : topLeftTexCoordHalf };

        posTexCoordsTriangles << posTexCoordsTriangle;
    }

    const Mesh3 mesh{ posTexCoordsTriangles };

    QList< QVector3D > vertexNormals;
    vertexNormals.reserve( VerticesCount );

    for ( const QVector3D& v : vertices )
    {
        const QVector3D n = mesh.normalAtVertex( v );
        vertexNormals.append( n );
    }

    const int trianglesCount = posTexCoordsTriangles.size();
    m_verticesData.reserve( trianglesCount * Triangle3::s_verticesCount );
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        const QPair< Triangle3, Triangle2 >& posTexCoordTriangle = posTexCoordsTriangles[ i ];
        const QList< int >& vertexIndices = triPosTexPairs[ i ].first;
        for ( int j = 0; j < Triangle3::s_verticesCount; ++ j )
        {
            const VertexData vertexData{
                        posTexCoordTriangle.first.vertexAtIndex( j ),
                        posTexCoordTriangle.second.vertexAtIndex( j ),
                        vertexNormals[ vertexIndices[ j ] ] };
            m_verticesData.append( vertexData );
        }
    }

    this->updateMeshOpenGlData();
}

void WaterPool::setLightInternal( const Light& aLight )
{
    m_light = aLight;
    this->updateLightOpenGlData();
}

void WaterPool::setMaterialInternal( const Material& aMaterial )
{
    m_material = aMaterial;
    this->updateMaterialOpenGlData();
}

OpenGlObjectBinder WaterPool::vaoBinder( const bool autoUnbind ) const
{
    return AbstractOpenGlClient::vaoBinder( m_vaoId, autoUnbind );
}

OpenGlObjectBinder WaterPool::vboBinder( const bool autoUnbind ) const
{
    return AbstractOpenGlClient::vboBinder( m_vboId, autoUnbind );
}

OpenGlObjectBinder WaterPool::textureUnitBinder( const bool autoUnbind ) const
{
    return AbstractOpenGlClient::textureUnitBinder( GL_TEXTURE0, autoUnbind );
}

OpenGlObjectBinder WaterPool::textureBinder( const bool autoUnbind ) const
{
    return AbstractOpenGlClient::textureBinder( m_textureId, autoUnbind );
}

void WaterPool::disposeOpenGlResources()
{
    if ( CommonEntities::isUnsignedGlValueValid( m_vaoId ) )
    {
        m_openGlFunctions->glDeleteVertexArrays( 1, & m_vaoId );
        LOG_OPENGL_ERROR();

        m_vaoId = CommonEntities::s_invalidUnsignedGlValue;
    }

    QVector< GLuint > vbosToDispose;
    if ( CommonEntities::isUnsignedGlValueValid( m_vboId ) )
    {
        vbosToDispose.append( m_vboId );
        m_vaoId = CommonEntities::s_invalidUnsignedGlValue;
    }

    const int vbosToDisposeCount = vbosToDispose.size();
    if ( vbosToDisposeCount > 0 )
    {
        m_openGlFunctions->glDeleteBuffers( vbosToDisposeCount, vbosToDispose.constData() );
        LOG_OPENGL_ERROR();
    }

    if ( CommonEntities::isUnsignedGlValueValid( m_textureId ) )
    {
        m_openGlFunctions->glDeleteTextures( 1, & m_textureId );
        LOG_OPENGL_ERROR();
    }
}

void WaterPool::init()
{
    if ( m_openGlFunctions == nullptr
         || ( CommonEntities::isUnsignedGlValueValid( m_vaoId )
              && CommonEntities::isUnsignedGlValueValid( m_vboId )
              && ! m_uniformVariableCachedLocations.isEmpty() ) )
    {
        return;
    }

    const bool isBuiltOk = GlUtilities::buildShaderProgram(
                               m_shaderProgram,
                               ":/Resources/shaders/WaterPool.vert",
                               ":/Resources/shaders/WaterPool.frag" );
    if ( ! isBuiltOk )
    {
        return;
    }

    this->cacheUniformVariableLocations();

    m_shaderProgram.activate();

    this->updateLightOpenGlData();
    this->updateMaterialOpenGlData();

    static const GLint textureUnit0Index = 0;
    m_openGlFunctions->glUniform1i( this->uniformVariableCachedLocation( UniformVariableIndexColorTexSampler ), textureUnit0Index );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();

    // setup Vertex Buffer Objects first

    m_openGlFunctions->glGenBuffers( 1, & m_vboId );
    LOG_OPENGL_ERROR();

    this->updateMeshOpenGlData();

    // setup Vertex Array Objects next
    m_openGlFunctions->glGenVertexArrays( 1, & m_vaoId );
    LOG_OPENGL_ERROR();

    OpenGlObjectBinder vaoBinder = this->vaoBinder( false );
    vaoBinder.bind();

    OpenGlObjectBinder vboBinder = this->vboBinder( false );
    vboBinder.bind();

    this->enableGenericVertexAttributeArrays( true );
    LOG_OPENGL_ERROR();

    const GLsizei stride = sizeof( VertexData );
    m_openGlFunctions->glVertexAttribPointer( GenericVertexAttributeIndexVertexPos,
                                              MathUtilities::s_axesCount3D,
                                              GL_FLOAT,
                                              GL_FALSE,
                                              stride,
                                              reinterpret_cast< const GLvoid* >( offsetof( VertexData, pos ) ) );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glVertexAttribPointer( GenericVertexAttributeIndexVertexTexCoord,
                                              MathUtilities::s_axesCount2D,
                                              GL_FLOAT,
                                              GL_FALSE,
                                              stride,
                                              reinterpret_cast< const GLvoid* >( offsetof( VertexData, texCoord ) ) );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glVertexAttribPointer( GenericVertexAttributeIndexVertexNormal,
                                              MathUtilities::s_axesCount3D,
                                              GL_FLOAT,
                                              GL_FALSE,
                                              stride,
                                              reinterpret_cast< const GLvoid* >( offsetof( VertexData, normal ) ) );
    LOG_OPENGL_ERROR();

    vboBinder.unbind();

    this->enableGenericVertexAttributeArrays( false );
    LOG_OPENGL_ERROR();

    vaoBinder.unbind();

    m_openGlFunctions->glGenTextures( 1, & m_textureId );
    LOG_OPENGL_ERROR();

    OpenGlObjectBinder textureUnitBinder = this->textureUnitBinder( false );
    textureUnitBinder.bind();

    OpenGlObjectBinder textureBinder = this->textureBinder( false );
    textureBinder.bind();

    const QImage wallImage( ":/Resources/bath_1.png" );
    const QImage wallTexture = QGLWidget::convertToGLFormat( wallImage );
    GlUtilities::setTexture( m_openGlFunctions,
                             wallTexture.constBits(),
                             wallTexture.width(),
                             wallTexture.height(),
                             GL_RGBA,
                             GL_RGBA,
                             GL_UNSIGNED_BYTE,
                             GlUtilities::s_textureDefaultMinificationFilter,
                             GlUtilities::s_textureDefaultMagnificationFilter,
                             GlUtilities::TextureWrapMode::Repeat,
                             GlUtilities::TextureWrapMode::Repeat );

    textureBinder.unbind();
    textureUnitBinder.unbind();
}

void WaterPool::draw()
{
    if ( m_openGlFunctions == nullptr
         || ! CommonEntities::isUnsignedGlValueValid( m_vaoId )
         || ! CommonEntities::isUnsignedGlValueValid( m_vboId )
         || ! CommonEntities::isUnsignedGlValueValid( m_textureId )
         || m_waterRect.isEmpty() )
    {
        return;
    }

    OpenGlCapabilityEnabler cullFaceEnabler{ m_openGlFunctions, GL_CULL_FACE, true, false };
    cullFaceEnabler.change();

    m_shaderProgram.activate();

    OpenGlObjectBinder vaoBinder = this->vaoBinder( false );
    vaoBinder.bind();

    this->enableGenericVertexAttributeArrays( true );

    OpenGlObjectBinder textureUnitBinder = this->textureUnitBinder( false );
    textureUnitBinder.bind();

    OpenGlObjectBinder textureBinder = this->textureBinder( false );
    textureBinder.bind();

    m_openGlFunctions->glDrawArrays(
                GL_TRIANGLES,
                0,
                m_verticesData.size() );
    LOG_OPENGL_ERROR();

    vaoBinder.unbind();

    this->enableGenericVertexAttributeArrays( false );

    textureBinder.unbind();
    textureUnitBinder.unbind();

    m_shaderProgram.deactivate();

    cullFaceEnabler.rollback();
}

void WaterPool::setMatrix(
        const CommonEntities::MatrixType matrixType,
        const QMatrix4x4& matrix )
{
    int uniformVariableIndex = UniformVariableIndexInvalid;
    switch ( matrixType )
    {
        case CommonEntities::MatrixType::Model:
            uniformVariableIndex = UniformVariableIndexModelMatrix;
            break;
        case CommonEntities::MatrixType::View:
            uniformVariableIndex = UniformVariableIndexViewMatrix;
            break;
        case CommonEntities::MatrixType::Projection:
            uniformVariableIndex = UniformVariableIndexProjectionMatrix;
            break;
        default:
            return;
    }

    m_shaderProgram.activate();

    m_openGlFunctions->glUniformMatrix4fv( this->uniformVariableCachedLocation( uniformVariableIndex ), 1, GL_FALSE, matrix.constData() );
    LOG_OPENGL_ERROR();

    if ( matrixType == CommonEntities::MatrixType::Model )
    {
        // http://www.arcsynthesis.org/gltut/Illumination/Tut09%20Normal%20Transformation.html
        const QMatrix3x3 normalMatrix = matrix.normalMatrix();
        m_openGlFunctions->glUniformMatrix3fv( this->uniformVariableCachedLocation( UniformVariableIndexNormalMatrix ), 1, GL_FALSE, normalMatrix.constData() );
        LOG_OPENGL_ERROR();
    }

    m_shaderProgram.deactivate();
}

void WaterPool::setLight( const Light& aLight )
{
    this->setLightInternal( aLight );
    emit updateRequested();
}

void WaterPool::setMaterial( const Material& aMaterial )
{
    this->setMaterialInternal( aMaterial );
    emit updateRequested();
}

void WaterPool::enableGenericVertexAttributeArrays( const bool enable )
{
    for ( const GLuint genericVertexAttributeIndex : s_allOrderedGenericVertexAttributeIndices )
    {
        if ( enable )
        {
            m_openGlFunctions->glEnableVertexAttribArray( genericVertexAttributeIndex );
            LOG_OPENGL_ERROR();
        }
        else
        {
            m_openGlFunctions->glDisableVertexAttribArray( genericVertexAttributeIndex );
            LOG_OPENGL_ERROR();
        }
    }
}

void WaterPool::updateMeshOpenGlData()
{
    if ( ! CommonEntities::isUnsignedGlValueValid( m_vboId ) )
    {
        return;
    }

    OpenGlObjectBinder vboBinder = this->vboBinder( false );
    vboBinder.bind();

    m_openGlFunctions->glBufferData( GL_ARRAY_BUFFER,
                                     m_verticesData.size() * sizeof( VertexData ),
                                     m_verticesData.constData(),
                                     GL_STATIC_DRAW );
    LOG_OPENGL_ERROR();

    vboBinder.unbind();
}

void WaterPool::updateLightOpenGlData()
{
    if ( ! m_shaderProgram.isValid() )
    {
        return;
    }

    m_shaderProgram.activate();

    const QColor& lightColor = m_light.color;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexLightColor ),
                                    lightColor.redF(),
                                    lightColor.greenF(),
                                    lightColor.blueF() );
    LOG_OPENGL_ERROR();

    const QVector3D& lightPos = m_light.pos;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexLightPos ),
                                    lightPos.x(),
                                    lightPos.y(),
                                    lightPos.z() );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexLightAttenuationConstant ),
                                    m_light.attenuation.constant );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexLightAttenuationLinear ),
                                    m_light.attenuation.linear );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexLightAttenuationQuadratic ),
                                    m_light.attenuation.quadratic );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexLightIntensity ),
                                    m_light.intensity );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();
}

void WaterPool::updateMaterialOpenGlData()
{
    if ( ! m_shaderProgram.isValid() )
    {
        return;
    }

    m_shaderProgram.activate();

    const QVector3D& emissiveReflectance = m_material.reflectance.emissive;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialReflectanceEmissive ),
                                    emissiveReflectance.x(),
                                    emissiveReflectance.y(),
                                    emissiveReflectance.z() );
    LOG_OPENGL_ERROR();

    const QVector3D& ambientReflectance = m_material.reflectance.ambient;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialReflectanceAmbient ),
                                    ambientReflectance.x(),
                                    ambientReflectance.y(),
                                    ambientReflectance.z() );
    LOG_OPENGL_ERROR();

    const QVector3D& diffuseReflectance = m_material.reflectance.diffuse;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialReflectanceDiffuse ),
                                    diffuseReflectance.x(),
                                    diffuseReflectance.y(),
                                    diffuseReflectance.z() );
    LOG_OPENGL_ERROR();

    const QVector3D& specularReflectance = m_material.reflectance.specular;
    m_openGlFunctions->glUniform3f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialReflectanceSpecular ),
                                    specularReflectance.x(),
                                    specularReflectance.y(),
                                    specularReflectance.z() );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glUniform1f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialShininess ), m_material.shininess );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();
}

QByteArray WaterPool::uniformVariableName( const int index )
{
    switch ( index )
    {
        case UniformVariableIndexColorTexSampler:
            return "colorTexSampler";
        case UniformVariableIndexMaterialReflectanceEmissive:
            return "material.re";
        case UniformVariableIndexMaterialReflectanceAmbient:
            return "material.ra";
        case UniformVariableIndexMaterialReflectanceDiffuse:
            return "material.rd";
        case UniformVariableIndexMaterialReflectanceSpecular:
            return "material.rs";
        case UniformVariableIndexMaterialShininess:
            return "material.shininess";
        case UniformVariableIndexLightColor:
            return "light.color";
        case UniformVariableIndexLightPos:
            return "light.pos";
        case UniformVariableIndexLightAttenuationConstant:
            return "light.kc";
        case UniformVariableIndexLightAttenuationLinear:
            return "light.kl";
        case UniformVariableIndexLightAttenuationQuadratic:
            return "light.kq";
        case UniformVariableIndexLightIntensity:
            return "light.intensity";
        case UniformVariableIndexModelMatrix:
            return "modelMatrix";
        case UniformVariableIndexNormalMatrix:
            return "normalMatrix";
        case UniformVariableIndexViewMatrix:
            return "viewMatrix";
        case UniformVariableIndexProjectionMatrix:
            return "projectionMatrix";
        case UniformVariableIndexEyePos:
            return "eyePos";
    }

    return QByteArray();
}

bool WaterPool::isUniformVariableIndexValid( const int index )
{
    return 0 <= index && index < UniformVariablesCount;
}

GLint WaterPool::uniformVariableLocation( const int index ) const
{
    if ( ! WaterPool::isUniformVariableIndexValid( index )
         || ! m_shaderProgram.isValid() )
    {
        return CommonEntities::s_invalidSignedGlValue;
    }

    const QByteArray uniformVariableName = WaterPool::uniformVariableName( index );
    return uniformVariableName.isEmpty()
           ? CommonEntities::s_invalidSignedGlValue
           : m_shaderProgram.uniformVarLocation( uniformVariableName );
}

void WaterPool::cacheUniformVariableLocations()
{
    m_uniformVariableCachedLocations.clear();
    m_uniformVariableCachedLocations.reserve( UniformVariablesCount );
    for ( int i = 0; i < UniformVariablesCount; ++ i )
    {
        m_uniformVariableCachedLocations.insert( i, this->uniformVariableLocation( i ) );
    }
}

GLint WaterPool::uniformVariableCachedLocation( const int index ) const
{
    return m_uniformVariableCachedLocations.value( index, CommonEntities::s_invalidSignedGlValue );
}
