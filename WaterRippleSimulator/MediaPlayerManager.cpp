#include "MediaPlayerManager.h"

#include "VideoSurface.h"

#include <QApplication>

#include <QDir>
#include <QStringBuilder>
#include <QTime>
#include <QTimer>
#include <QtMath>

#if QT_VERSION >= QT_VERSION_CHECK( 5, 0, 0 )
    #include <QStandardPaths>
#else
    #include <QDesktopServices>
#endif

#include <limits>

using namespace WaterRippleSimulator;

const int MediaPlayerManager::s_volumeLevelMin     = 0;
const int MediaPlayerManager::s_volumeLevelInitial = 20;
const int MediaPlayerManager::s_volumeLevelMax     = 100;

const qreal MediaPlayerManager::s_playbackRateStep = 0.05;
const qreal MediaPlayerManager::s_playbackRateMin  = MediaPlayerManager::s_playbackRateStep;
const qreal MediaPlayerManager::s_playbackRateMax  = 10.0;

const QList< QMediaPlayer::State > MediaPlayerManager::s_playerStates =
        QList< QMediaPlayer::State >()
        << QMediaPlayer::PlayingState
        << QMediaPlayer::PausedState
        << QMediaPlayer::StoppedState;

const QStringList MediaPlayerManager::s_allowedFileExtensions =
        QStringList()
        << "avi" << "mov"  << "wmv"
        << "mp4" << "mkv"  << "3gp"
        << "mpg" << "mpeg" << "flv";

MediaPlayerManager::MediaPlayerManager( QObject* parent )
    : QObject( parent )
    , m_mediaPlayer( nullptr )
    , m_mediaStatusUpdateTimer( nullptr )
{
    this->resetMediaPlayer();
    this->resetVideoOutput();
}

MediaPlayerManager::~MediaPlayerManager()
{
}

void MediaPlayerManager::setMediaUrl( const QUrl& aMediaUrl )
{
    if ( aMediaUrl == m_mediaPlayer->media().canonicalUrl() )
    {
        return;
    }

    m_mediaPlayer->setMedia( aMediaUrl.isValid()
                             ? QMediaContent( aMediaUrl )
                             : QMediaContent() );
}

void MediaPlayerManager::setMediaFilePath( const QString& aMediaFilePath )
{
    this->setMediaUrl( QUrl::fromLocalFile( aMediaFilePath ) );
}

void MediaPlayerManager::clearMediaFilePath()
{
    this->setMediaUrl( QUrl() );
}

void MediaPlayerManager::setPlayerState( const QMediaPlayer::State aPlayerState )
{
    switch ( aPlayerState )
    {
        case QMediaPlayer::PlayingState:
            m_mediaPlayer->play();
            break;
        case QMediaPlayer::PausedState:
            m_mediaPlayer->pause();
            break;
        case QMediaPlayer::StoppedState:
            m_mediaPlayer->stop();
            break;
        default:
            break;
    }
}

void MediaPlayerManager::play()
{
    this->setPlayerState( QMediaPlayer::PlayingState );
    QTimer::singleShot( 5000, this, SLOT(changePlaybackRate()) );
}

void MediaPlayerManager::pause()
{
    this->setPlayerState( QMediaPlayer::PausedState );
}

void MediaPlayerManager::stop()
{
    this->setPlayerState( QMediaPlayer::StoppedState );
}

void MediaPlayerManager::setIsMuted( const bool aIsMuted )
{
    m_mediaPlayer->setMuted( aIsMuted );
}

void MediaPlayerManager::setVolumeLevel( const int aVolumeLevel )
{
    m_mediaPlayer->setVolume( aVolumeLevel );
}

void MediaPlayerManager::setPlaybackRate( const qreal& aPlaybackRate )
{
    m_mediaPlayer->setPlaybackRate( aPlaybackRate );
}

void MediaPlayerManager::onMediaStatusChanged()
{
    if ( m_mediaStatusUpdateTimer == nullptr )
    {
        m_mediaStatusUpdateTimer = new QTimer( this );
        connect( m_mediaStatusUpdateTimer, SIGNAL(timeout()), this, SLOT(onMediaStatusChanged()) );
        m_mediaStatusUpdateTimer->setInterval( 200 );
    }

    if ( m_mediaStatusUpdateTimer != nullptr
         && this->sender() == m_mediaStatusUpdateTimer )
    {
        if ( ! this->isMediaValid()
             && ! m_mediaPlayer->media().isNull() )
        {
            // if QMediaPlayer doesn't recognize the input media file or it is corrupted,
            // clearing the content of the media player object by setting it an empty one,
            // doesn't reset its state to a neutral one
            // it is even impossible to subsequently load a valid content
            // force clear the state
            this->resetMediaPlayer();
            return;
        }
    }
    else
    {
        m_mediaStatusUpdateTimer->start();
        return;
    }
}

void MediaPlayerManager::onPlayerStateDataChanged( const QMediaPlayer::State aPlayerState )
{
    emit playerStateChanged( aPlayerState );

    switch ( aPlayerState )
    {
        case QMediaPlayer::StoppedState:
            emit stopped();
            break;
        case QMediaPlayer::PlayingState:
            emit startedPlaying();
            break;
        case QMediaPlayer::PausedState:
            emit paused();
            break;
    }
}

void MediaPlayerManager::changePlaybackRate()
{
    this->setPlaybackRate( 6.0 );
}

void MediaPlayerManager::step( const qint64& stepSize )
{
    const qint64 minPosition = Q_INT64_C( 0 );
    const qint64 maxPosition = m_mediaPlayer->duration();
    const qint64 currentPosition = m_mediaPlayer->position();
    if ( currentPosition == minPosition
         || currentPosition == maxPosition )
    {
        return;
    }

    const qint64 newPosition = qBound( minPosition, currentPosition + stepSize, maxPosition );
    m_mediaPlayer->setPosition( newPosition );
}

QMediaPlayer::State MediaPlayerManager::playerState() const
{
    return m_mediaPlayer->state();
}

bool MediaPlayerManager::isPlaying() const
{
    return this->playerState() == QMediaPlayer::PlayingState;
}

bool MediaPlayerManager::isPaused() const
{
    return this->playerState() == QMediaPlayer::PausedState;
}

bool MediaPlayerManager::isStopped() const
{
    return this->playerState() == QMediaPlayer::StoppedState;
}

int MediaPlayerManager::isMuted() const
{
    return m_mediaPlayer->isMuted();
}

int MediaPlayerManager::volumeLevel() const
{
    return m_mediaPlayer->volume();
}

qreal MediaPlayerManager::playbackRate() const
{
    return m_mediaPlayer->playbackRate();
}

void MediaPlayerManager::setVideoOutput( QVideoWidget* const aVideoOutput )
{
    m_mediaPlayer->setVideoOutput( aVideoOutput );

    m_videoOutputVideoWidget = aVideoOutput;
    m_videoOutputGraphicsVideoItem.clear();
    m_videoOutputAbstractVideoSurface.clear();
}

void MediaPlayerManager::setVideoOutput( QGraphicsVideoItem* const aVideoOutput )
{
    m_mediaPlayer->setVideoOutput( aVideoOutput );

    m_videoOutputVideoWidget.clear();
    m_videoOutputGraphicsVideoItem = aVideoOutput;
    m_videoOutputAbstractVideoSurface.clear();
}

void MediaPlayerManager::setVideoOutput( QAbstractVideoSurface* const aVideoOutput )
{
    m_mediaPlayer->setVideoOutput( aVideoOutput );

    m_videoOutputVideoWidget.clear();
    m_videoOutputGraphicsVideoItem.clear();
    m_videoOutputAbstractVideoSurface = aVideoOutput;
}

void MediaPlayerManager::resetMediaPlayer()
{
    if ( m_mediaPlayer != nullptr )
    {
        disconnect( m_mediaPlayer, SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(onPlayerStateDataChanged(QMediaPlayer::State)) );
        disconnect( m_mediaPlayer, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)), this, SLOT(onMediaStatusChanged()) );
        m_mediaPlayer->deleteLater();
    }

    m_mediaPlayer = new QMediaPlayer( this, QMediaPlayer::VideoSurface );
    connect( m_mediaPlayer, SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(onPlayerStateDataChanged(QMediaPlayer::State)) );
    connect( m_mediaPlayer, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)), this, SLOT(onMediaStatusChanged()) );

    if ( ! m_videoOutputVideoWidget.isNull() )
    {
        m_mediaPlayer->setVideoOutput( m_videoOutputVideoWidget.data() );
    }

    if ( ! m_videoOutputGraphicsVideoItem.isNull() )
    {
        m_mediaPlayer->setVideoOutput( m_videoOutputGraphicsVideoItem.data() );
    }

    if ( ! m_videoOutputAbstractVideoSurface.isNull() )
    {
        m_mediaPlayer->setVideoOutput( m_videoOutputAbstractVideoSurface.data() );
    }
}

void MediaPlayerManager::resetVideoOutput()
{
    VideoSurface* const videoSurface = new VideoSurface( this );
    connect( videoSurface, SIGNAL(frameReceived(QVideoFrame)), this, SIGNAL(frameReceived(QVideoFrame)) );
    this->setVideoOutput( videoSurface );
}

QUrl MediaPlayerManager::mediaUrl() const
{
    return m_mediaPlayer->media().canonicalUrl();
}

bool MediaPlayerManager::isMediaValid() const
{
    const QMediaPlayer::MediaStatus mediaStatus = m_mediaPlayer->mediaStatus();
    return mediaStatus != QMediaPlayer::UnknownMediaStatus
           && mediaStatus != QMediaPlayer::NoMedia
           && mediaStatus != QMediaPlayer::InvalidMedia
           && m_mediaPlayer->error() == QMediaPlayer::NoError
           && m_mediaPlayer->duration() > Q_INT64_C( 0 )
           && ! m_mediaPlayer->media().isNull()
           && ! m_mediaPlayer->currentMedia().isNull();
}

QUrl MediaPlayerManager::urlFromMimeData( const QMimeData* const mimeData )
{
    if ( mimeData == nullptr )
    {
        return QUrl();
    }

    QUrl url;
    if ( mimeData->hasUrls() )
    {
        const QList< QUrl > urls = mimeData->urls();
        if ( ! urls.isEmpty() )
        {
            url = urls.first();
        }
    }

    if ( url.isEmpty() && mimeData->hasText() )
    {
        const QUrl urlCandidate( mimeData->text() );
        if ( urlCandidate.isValid() )
        {
            url = urlCandidate;
        }
    }

    if ( url.isEmpty() )
    {
        return url;
    }

    if ( url.isLocalFile() )
    {
        const QString localFile = url.toLocalFile();
        const QFileInfo localFileInfo( localFile );
        if ( ! s_allowedFileExtensions.contains( localFileInfo.suffix(), Qt::CaseInsensitive ) )
        {
            return QUrl();
        }
    }

    return url;
}
