#include "MainController.h"

#include "MainWindow.h"

#include <QApplication>
#include <QFile>

using namespace WaterRippleSimulator;

MainController::MainController( QObject* parent )
    : QObject( parent )
{
}

MainController::~MainController()
{
}

void MainController::initialize()
{
    if ( ! m_mainWindow.isNull() )
    {
        return;
    }

    MainController::loadStyleSheet();

    qApp->setApplicationDisplayName( tr( "Water Ripple Simulator" ) );

    m_mainWindow.reset( new MainWindow );
    m_mainWindow->showMaximized();
}

void MainController::setMediaFilePath( const QString& aMediaFilePath )
{
    if ( m_mainWindow.isNull() )
    {
        return;
    }

    m_mainWindow->setMediaFilePath( aMediaFilePath );
}

void MainController::loadStyleSheet()
{
    QFile styleSheetFile( ":/Resources/style.qss" );
    if ( ! styleSheetFile.open( QIODevice::ReadOnly ) )
    {
        return;
    }

    qApp->setStyleSheet( styleSheetFile.readAll() );
}


