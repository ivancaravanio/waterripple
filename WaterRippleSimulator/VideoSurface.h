#ifndef WATERRIPPLESIMULATOR_VIDEOSURFACE_H
#define WATERRIPPLESIMULATOR_VIDEOSURFACE_H

#include <QAbstractVideoSurface>
#include <QVideoFrame>

namespace WaterRippleSimulator {

class VideoSurface : public QAbstractVideoSurface
{
    Q_OBJECT

public:
    explicit VideoSurface( QObject* parent = 0 );
    virtual ~VideoSurface();

    virtual QList< QVideoFrame::PixelFormat > supportedPixelFormats(
            QAbstractVideoBuffer::HandleType handleType = QAbstractVideoBuffer::NoHandle ) const;
    virtual bool isFormatSupported( const QVideoSurfaceFormat& format ) const;

    virtual bool start( const QVideoSurfaceFormat& format );
    virtual void stop();

    virtual bool present( const QVideoFrame& frame );

signals:
    void frameReceived( const QVideoFrame& frame );
};

}

#endif // WATERRIPPLESIMULATOR_VIDEOSURFACE_H
