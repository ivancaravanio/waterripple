#include "MainController.h"

#include <QApplication>
#include <QCommandLineOption>
#include <QCommandLineParser>

using namespace WaterRippleSimulator;

int main( int argc, char* argv[] )
{
    QApplication a( argc, argv );

    MainController mainController;
    mainController.initialize();

    QCommandLineParser parser;

    const QCommandLineOption mediaFilePathOption( "m",
                                                  "Media file path.",
                                                  "The path to the media file." );
    parser.addOption( mediaFilePathOption );
    parser.process( a );

    const QString mediaFilePath = parser.value( mediaFilePathOption );
    mainController.setMediaFilePath( mediaFilePath );

    return a.exec();
}
