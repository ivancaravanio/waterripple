#include "GeneralUtilities.h"

#include <QFile>

#if QT_VERSION >= QT_VERSION_CHECK( 5, 0, 0 )
    #include <QGuiApplication>
    #include <QScreen>
#else
    #include <QApplication>
    #include <QDesktopWidget>
#endif

using namespace WaterRippleSimulator::Utilities;

QRect GeneralUtilities::primaryDesktopRect()
{
#if QT_VERSION >= QT_VERSION_CHECK( 5, 0, 0 )
    if ( qGuiApp == 0 )
    {
        return QRect();
    }

    return qGuiApp->primaryScreen()->geometry();
#else
    if ( qApp == 0 )
    {
        return QRect();
    }

    return qApp->desktop()->screenGeometry();
#endif
}

QByteArray GeneralUtilities::fileContents( const QString& fileName )
{
    QFile file( fileName );
    if ( ! file.open( QIODevice::ReadOnly ) )
    {
        return QByteArray();
    }

    return file.readAll();
}
