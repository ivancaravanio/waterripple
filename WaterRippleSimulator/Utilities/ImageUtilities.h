#ifndef WATERRIPPLESIMULATOR_UTILITIES_IMAGEUTILITIES_H
#define WATERRIPPLESIMULATOR_UTILITIES_IMAGEUTILITIES_H

#include <QColor>
#include <QImage>
#include <QVector3D>
#include <QVector4D>

namespace WaterRippleSimulator {
namespace Utilities {

class ImageUtilities
{
public:
    enum ColorComponentType
    {
        ColorComponentTypeRed,
        ColorComponentTypeGreen,
        ColorComponentTypeBlue
    };

public:
    static int intColorComponentMin();
    static int intColorComponentMax();

    static float realFromIntColorComponent( const int intColorComponent );
    static int intFromRealColorComponent( const float realColorComponent );

    static QVector4D realFromIntColor( const QColor& intColor );
    static QColor intFromRealColor( const QVector4D& realColor );

    static QImage toGrayscale( const QImage& image );
    static QImage toNormalMap( const QImage& image, const qreal scaleFactor );
    static QImage toEnhancedNormalMap( const QImage& image, const qreal scaleFactor );

    static QVector3D normalFromColor( const QColor& color );
    static QColor colorFromNormal( const QVector3D& point );
    static int colorComponentFromGeometricComponent( const float geometricComponent );
    static float geometricComponentFromColorComponent( const int colorComponent );

    static QImage blurredImage( const QImage& image,
                                const qreal radius,
                                const bool quality = true,
                                const bool alphaOnly = false,
                                const int transposed = 0 );
    static QImage overlayImages( const QImage& imageFore,
                                 const QImage& imageBack );

private:
    ImageUtilities();
};

}
}

#endif // WATERRIPPLESIMULATOR_UTILITIES_IMAGEUTILITIES_H
