#ifndef WATERRIPPLESIMULATOR_UTILITIES_GLUTILITIES_H
#define WATERRIPPLESIMULATOR_UTILITIES_GLUTILITIES_H

#include "../Helpers/ShaderProgram.h"

#include <QMatrix4x4>
#include <QSize>
#include <QSurfaceFormat>
#include <QtGlobal>

#include <gl/GL.h>

QT_FORWARD_DECLARE_CLASS(QOpenGLFunctions_3_3_Core)

namespace WaterRippleSimulator {
namespace Utilities {

using namespace WaterRippleSimulator::Helpers;

class GlUtilities
{
public:
    enum class TextureWrapMode
    {
        Invalid,
        Repeat,
        Clamp,
        ClampToEdge,
        ClampToBorder
    };

    // The process of calculating color fragments from
    // a stretched or shrunken texture map is called texture filtering.
    enum class TextureFilter
    {
        Invalid,
        Nearest,              // Returns the value of the texture element that is nearest (in Manhattan distance) to the center of the pixel being textured.
        Linear,               // Returns the weighted average of the four texture elements that are closest to the center of the pixel being textured.
        NearestMipmapNearest, // Chooses the mipmap that most closely matches the size of the pixel being textured and uses the GL_NEAREST criterion (the texture element nearest to the center of the pixel) to produce a texture value.
        LinearMipmapNearest,  // Chooses the mipmap that most closely matches the size of the pixel being textured and uses the GL_LINEAR criterion (a weighted average of the four texture elements that are closest to the center of the pixel) to produce a texture value.
        NearestMipmapLinear,  // Chooses the two mipmaps that most closely match the size of the pixel being textured and uses the GL_NEAREST criterion (the texture element nearest to the center of the pixel) to produce a texture value from each mipmap. The final texture value is a weighted average of those two values.
        LinearMipmapLinear    // Chooses the two mipmaps that most closely match the size of the pixel being textured and uses the GL_LINEAR criterion (a weighted average of the four texture elements that are closest to the center of the pixel) to produce a texture value from each mipmap. The final texture value is a weighted average of those two values.
    };

    // The naming format of the mipmap texture filters is described as follows:
    // GL_<FILTER>_MIPMAP_<SELECTOR>:
    // - <FILTER>: the type of the filter that should be applied on the selected mip level: 0, 1, 2, ..., n
    // - <SELECTOR>: specifies how the mip level is selected;
    //               for example, GL_NEAREST selects the nearest matching mip level.
    //               Using GL_LINEAR for the selector creates a linear interpolation between
    //               the two nearest mip levels, which is again filtered by the chosen texture filter.

    // Nearest neighbor filtering is the simplest and fastest filtering method you can choose.
    // Texture coordinates are evaluated and plotted against a texture’s texels, and whichever
    // texel the coordinate falls in, that color is used for the fragment texture color. Nearest
    // neighbor filtering is characterized by large blocky pixels when the texture is stretched
    // especially large.

    // Linear filtering works by not taking the nearest texel to the texture coordinate, but by
    // applying the weighted average of the texels surrounding the texture coordinate (a linear
    // interpolation). For this interpolated fragment to match the texel color exactly, the texture
    // coordinate needs to fall directly in the center of the texel. Linear filtering is characterized
    // by "fuzzy" graphics when a texture is stretched. This fuzziness, however, often lends a
    // more realistic and less artificial look than the jagged blocks of the nearest neighbor filtering
    // mode.

public:
    static const GLint                        s_textureDefaultDestinationFormat;
    static const GLenum                       s_textureDefaultSourceFormat;
    static const GLenum                       s_textureDefaultSourcePixelComponentsSize;
    static const GlUtilities::TextureFilter   s_textureDefaultMinificationFilter;
    static const GlUtilities::TextureFilter   s_textureDefaultMagnificationFilter;
    static const GlUtilities::TextureWrapMode s_textureDefaultHorizontalWrapMode;
    static const GlUtilities::TextureWrapMode s_textureDefaultVerticalWrapMode;
    static const bool                         s_textureDefaultApplyAnisotropicFiltering;

public:
    static GLuint activeShaderProgramId(
            QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
            const QSurfaceFormat& aOpenGlFormat );

    static QMatrix4x4 qtToOpenGlXYPlaneTransform();

    static bool isTextureWrapModeValid( const TextureWrapMode textureWrapMode );
    static GLenum textureWrapModeToOpenGlOne( const TextureWrapMode textureWrapMode );
    static TextureWrapMode textureWrapModeFromOpenGlOne( const GLenum openGlTextureWrapMode );

    static bool isTextureFilterValid( const TextureFilter textureFilter );
    static GLenum textureFilterToOpenGlOne( const TextureFilter textureFilter );
    static TextureFilter textureFilterFromOpenGlOne( const GLenum openGlTextureFilter );
    static bool isMipMapTextureFilter( const TextureFilter textureFilter );

    static void setTexture(
            QOpenGLFunctions_3_3_Core* const openGlFunctions,
            const GLvoid* const              pixelColorComponentValues,
            const int                        pixelsPerRowCount,
            const int                        pixelsPerColumnCount,
            const GLint                      destinationFormat         = s_textureDefaultDestinationFormat,
            const GLenum                     sourceFormat              = s_textureDefaultSourceFormat,
            const GLenum                     sourcePixelComponentsSize = s_textureDefaultSourcePixelComponentsSize,
            const TextureFilter              minificationFilter        = s_textureDefaultMinificationFilter,
            const TextureFilter              magnificationFilter       = s_textureDefaultMagnificationFilter,
            const TextureWrapMode            horizontalWrapMode        = s_textureDefaultHorizontalWrapMode,
            const TextureWrapMode            verticalWrapMode          = s_textureDefaultVerticalWrapMode,
            const bool                       applyAnisotropicFiltering = s_textureDefaultApplyAnisotropicFiltering );

    static QSize texture2DSize(
            QOpenGLFunctions_3_3_Core* const openGlFunctions,
            const GLuint textureId );

    static ShaderProgram builtShaderProgram(
            QOpenGLFunctions_3_3_Core* const openGlFunctions,
            const QSurfaceFormat& aOpenGlFormat,
            const QString& vertexShaderFilePath,
            const QString& fragmentShaderFilePath );

    static bool buildShaderProgram(
            ShaderProgram& shaderProgram,
            const QString& vertexShaderFilePath,
            const QString& fragmentShaderFilePath );

private:
    // do not define
    GlUtilities();
};

}
}

#endif // WATERRIPPLESIMULATOR_UTILITIES_GLUTILITIES_H
