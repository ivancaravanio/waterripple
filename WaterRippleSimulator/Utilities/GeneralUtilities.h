#ifndef WATERRIPPLESIMULATOR_UTILITIES_GENERALUTILITIES_H
#define WATERRIPPLESIMULATOR_UTILITIES_GENERALUTILITIES_H

#include <QByteArray>
#include <QRect>
#include <QString>

namespace WaterRippleSimulator {
namespace Utilities {

class GeneralUtilities
{
public:
    static QRect primaryDesktopRect();
    static QByteArray fileContents( const QString& fileName );

private:
    // do not define
    GeneralUtilities();
};

}
}

#endif // WATERRIPPLESIMULATOR_UTILITIES_GENERALUTILITIES_H
