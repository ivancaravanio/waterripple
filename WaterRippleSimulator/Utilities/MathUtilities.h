#ifndef WATERRIPPLESIMULATOR_UTILITIES_MATHUTILITIES_H
#define WATERRIPPLESIMULATOR_UTILITIES_MATHUTILITIES_H

#include "../Data/Triangle2.h"
#include "../Data/Triangle3.h"

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <QList>
#include <QRect>
#include <QtGlobal>
#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>

namespace WaterRippleSimulator {
namespace Utilities {

using namespace WaterRippleSimulator::Data;

class MathUtilities
{
public:
    static const int s_axesCount1D;
    static const int s_axesCount2D;
    static const int s_axesCount3D;
    static const int s_axesCount4D;

public:
    static QRect scaledConcentricRect( const QRect& rect, const qreal scaleFactor );

    static qreal to360DegreesAngle( const qreal angleDegrees );
    static int boundedRandom( const int min, const int max );

    static void convertDoubleToSinglePrecisionV( float* const singlePrecision, const double* const doublePrecision, const int length );

    static QList< QVector3D > gramSchmidtOrthogonalization( const QList< QVector3D >& linearlyIndependentVectors );

    static bool epsilonEqual( const QVector2D& v1, const QVector2D& v2 );
    static bool epsilonEqual( const QVector3D& v1, const QVector3D& v2 );
    static bool epsilonEqual( const QVector4D& v1, const QVector4D& v2 );

    static bool isUnit( const QVector2D& v );
    static bool isUnit( const QVector3D& v );
    static bool isUnit( const QVector4D& v );

    static bool isInf( const QVector2D& v );
    static bool isInf( const QVector3D& v );
    static bool isInf( const QVector4D& v );

    static bool isNan( const QVector2D& v );
    static bool isNan( const QVector3D& v );
    static bool isNan( const QVector4D& v );

    static int vertexIndex( const QList< QVector2D >& vertices, const QVector2D& v );
    static int vertexIndex( const QList< QVector3D >& vertices, const QVector3D& v );
    static int vertexIndex( const QList< QVector4D >& vertices, const QVector4D& v );

    template< typename T > static bool fuzzyContainedInRegion( const T& minValue, const T& value, const T& maxValue );

    static float degrees( const float radians );
    static float radians( const float degrees );

    static float angle( const QVector2D& v1, const QVector2D& v2, const bool useDegrees = true );
    static float angle( const QVector3D& v1, const QVector3D& v2, const bool useDegrees = true );
    static float angle( const QVector4D& v1, const QVector4D& v2, const bool useDegrees = true );

    static QVector2D projection( const QVector2D& v1, const QVector2D& v2 );
    static QVector3D projection( const QVector3D& v1, const QVector3D& v2 );
    static QVector4D projection( const QVector4D& v1, const QVector4D& v2 );

    static QVector< float > coordinates( const QVector2D& v );
    static QVector< float > coordinates( const QVector3D& v );
    static QVector< float > coordinates( const QVector4D& v );
    static QVector< float > coordinates( const Triangle2& t );
    static QVector< float > coordinates( const Triangle3& t );

    static QVector< float > coordinates( const QList< QVector2D >& vectors );
    static QVector< float > coordinates( const QList< QVector3D >& vectors );
    static QVector< float > coordinates( const QList< QVector4D >& vectors );
    static QVector< float > coordinates( const QList< Triangle2 >& triangles );
    static QVector< float > coordinates( const QList< Triangle3 >& triangles );

    static int digitsCount( const int number );
    static int rangeRand( const int minValue, const int maxValue );

    static QVector2D qtFromGlmVec( const glm::vec2& glmVec );
    static QVector3D qtFromGlmVec( const glm::vec3& glmVec );
    static QVector4D qtFromGlmVec( const glm::vec4& glmVec );

    static glm::vec2 glmFromQtVec( const QVector2D& glmVec );
    static glm::vec3 glmFromQtVec( const QVector3D& glmVec );
    static glm::vec4 glmFromQtVec( const QVector4D& glmVec );

    static glm::vec2 safeNormalize( const glm::vec2& v );
    static glm::vec3 safeNormalize( const glm::vec3& v );
    static glm::vec4 safeNormalize( const glm::vec4& v );

    static bool epsilonEqual( const glm::vec2& v1, const glm::vec2& v2 );
    static bool epsilonEqual( const glm::vec3& v1, const glm::vec3& v2 );
    static bool epsilonEqual( const glm::vec4& v1, const glm::vec4& v2 );

    static bool hasZeroLength( const glm::vec2& v );
    static bool hasZeroLength( const glm::vec3& v );
    static bool hasZeroLength( const glm::vec4& v );

    static bool isUnit( const glm::vec2& v );
    static bool isUnit( const glm::vec3& v );
    static bool isUnit( const glm::vec4& v );

    static QSize squaresToVerticesCount( const QSize& squaresCount );
    static QSize verticesToSquaresCount( const QSize& verticesCount );

    static int randomNumber( const int max );

private:
    MathUtilities();
};

template< typename T > bool MathUtilities::fuzzyContainedInRegion(
        const T& minValue,
        const T& value,
        const T& maxValue )
{
    return qFuzzyCompare( value, minValue )
           || ( minValue <= value && value <= maxValue )
           || qFuzzyCompare( value, maxValue );
}

}
}

#endif // WATERRIPPLESIMULATOR_UTILITIES_MATHUTILITIES_H
