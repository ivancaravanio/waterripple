#include "GlUtilities.h"

#include "../Helpers/OpenGlErrorLogger.h"
#include "../Helpers/OpenGlObjectBinder.h"
#include "../WaterRippleSimulator_namespace.h"
#include "GeneralUtilities.h"

#include <qopenglext.h>
#include <QOpenGLFunctions_3_3_Core>

using namespace WaterRippleSimulator::Utilities;

using namespace WaterRippleSimulator::Helpers;
using namespace WaterRippleSimulator;

const GLint                        GlUtilities::s_textureDefaultDestinationFormat         = GL_RGBA;
const GLenum                       GlUtilities::s_textureDefaultSourceFormat              = GL_RGBA;
const GLenum                       GlUtilities::s_textureDefaultSourcePixelComponentsSize = GL_UNSIGNED_BYTE;
const GlUtilities::TextureFilter   GlUtilities::s_textureDefaultMinificationFilter        = GlUtilities::TextureFilter::LinearMipmapLinear;
const GlUtilities::TextureFilter   GlUtilities::s_textureDefaultMagnificationFilter       = GlUtilities::TextureFilter::Linear; // use nearest when having small images
const GlUtilities::TextureWrapMode GlUtilities::s_textureDefaultHorizontalWrapMode        = GlUtilities::TextureWrapMode::ClampToEdge;
const GlUtilities::TextureWrapMode GlUtilities::s_textureDefaultVerticalWrapMode          = GlUtilities::TextureWrapMode::ClampToEdge;
const bool                         GlUtilities::s_textureDefaultApplyAnisotropicFiltering = true;

GLuint GlUtilities::activeShaderProgramId(
        QOpenGLFunctions_3_3_Core* const aOpenGlFunctions,
        const QSurfaceFormat& aOpenGlFormat )
{
    if ( aOpenGlFunctions == nullptr )
    {
        return CommonEntities::s_invalidUnsignedGlValue;
    }

    // http://cu-droplet.googlecode.com/git-history/5dd30c9749a595441098aa1d448445075b6f363d/DropletSimDemos/DropletRenderer/src/RenderableDroplet.cpp
    // OpenGL 3.3:  GL_CURRENT_PROGRAM
    // OpenGL 4.2+: GL_ACTIVE_PROGRAM

    GLint activeShaderProgramId = static_cast< GLint >( CommonEntities::s_invalidUnsignedGlValue );

    const QPair< int, int > openGlVersion = aOpenGlFormat.version();
    aOpenGlFunctions->glGetIntegerv(
                openGlVersion.first >= 4
                && openGlVersion.second >= 2
                ? GL_ACTIVE_PROGRAM
                : GL_CURRENT_PROGRAM,
                & activeShaderProgramId );
    LOG_OPENGL_ERROR();

    return static_cast< GLuint >( activeShaderProgramId );
}

QMatrix4x4 GlUtilities::qtToOpenGlXYPlaneTransform()
{
    QMatrix4x4 m;
    m.rotate( 180.0f, 1.0f, 0.0f );

    return m;
}

void GlUtilities::setTexture(
        QOpenGLFunctions_3_3_Core* const openGlFunctions,
        const GLvoid* const pixelColorComponentValues,
        const int pixelsPerRowCount,
        const int pixelsPerColumnCount,
        const GLint destinationFormat,
        const GLenum sourceFormat,
        const GLenum sourcePixelComponentsSize,
        const GlUtilities::TextureFilter minificationFilter,
        const GlUtilities::TextureFilter magnificationFilter,
        const GlUtilities::TextureWrapMode horizontalWrapMode,
        const GlUtilities::TextureWrapMode verticalWrapMode,
        const bool applyAnisotropicFiltering )
{
    if ( pixelsPerRowCount < 0
         || pixelsPerColumnCount < 0
         // || pixelColorComponentValues == nullptr
         // allow null data, so that to it is possible to only allocate memory for of the texture or
         // to transfer data between it and the PBO for instance
         || ! GlUtilities::isTextureFilterValid( minificationFilter )
         || ! GlUtilities::isTextureFilterValid( magnificationFilter )
         || ! GlUtilities::isTextureWrapModeValid( horizontalWrapMode )
         || ! GlUtilities::isTextureWrapModeValid( verticalWrapMode ) )
    {
        return;
    }

    openGlFunctions->glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
    LOG_OPENGL_ERROR();

    openGlFunctions->glTexImage2D(
                GL_TEXTURE_2D,
                0, // the mip level which we are providing an image for;
                   // we should either provide images for all other mip levels or let OpenGL generate them by itself
                destinationFormat,
                pixelsPerRowCount,
                pixelsPerColumnCount,
                0,
                sourceFormat,
                sourcePixelComponentsSize,
                pixelColorComponentValues );
    LOG_OPENGL_ERROR();

    openGlFunctions->glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GlUtilities::textureWrapModeToOpenGlOne( horizontalWrapMode ) );
    LOG_OPENGL_ERROR();
    openGlFunctions->glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GlUtilities::textureWrapModeToOpenGlOne( verticalWrapMode ) );
    LOG_OPENGL_ERROR();

    openGlFunctions->glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GlUtilities::textureFilterToOpenGlOne( minificationFilter ) );
    LOG_OPENGL_ERROR();
    openGlFunctions->glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GlUtilities::textureFilterToOpenGlOne( magnificationFilter ) );
    LOG_OPENGL_ERROR();

    if ( applyAnisotropicFiltering )
    {
        GLfloat maxSupportedAnisotropicFiltering = -1.0;
        openGlFunctions->glGetFloatv( GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, & maxSupportedAnisotropicFiltering );
        LOG_OPENGL_ERROR();
        if ( openGlFunctions->glGetError() == GL_NO_ERROR
             && maxSupportedAnisotropicFiltering > 0.0 )
        {
            openGlFunctions->glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxSupportedAnisotropicFiltering );
            LOG_OPENGL_ERROR();
        }
    }

    // When the mipmapped texture filter is set, it is selected only for the minification filter.
    // This is typically the case because after OpenGL selects the largest available mip level, no
    // larger levels are available to select from. Essentially, this is to say that after a certain
    // threshold is passed, the largest available texture image is used, and there are no additional
    // mipmap levels to choose from.
    if ( GlUtilities::isMipMapTextureFilter( minificationFilter )
         || GlUtilities::isMipMapTextureFilter( magnificationFilter ) )
    {
        // For the very best visual quality (as well as for consistency), you should load your own pregenerated mipmaps.
        // While having precomputed mip levels for your textures yields the very best results,
        // it is convenient and somewhat common to have OpenGL generate the textures for you.
        openGlFunctions->glGenerateMipmap( GL_TEXTURE_2D );
        LOG_OPENGL_ERROR();
    }
}

QSize GlUtilities::texture2DSize(
        QOpenGLFunctions_3_3_Core* const openGlFunctions,
        const GLuint textureId )
{
    if ( openGlFunctions == nullptr
         || ! CommonEntities::isUnsignedGlValueValid( textureId ) )
    {
        return QSize();
    }

    const GLint mipmapLevel = 0;

    OpenGlObjectBinder textureBinder{
        openGlFunctions,
        OpenGlObjectBinder::ObjectType::Texture,
        GL_TEXTURE_2D,
        textureId };
    textureBinder.bind();

    GLint w = 0;
    openGlFunctions->glGetTexLevelParameteriv( GL_TEXTURE_2D, mipmapLevel, GL_TEXTURE_WIDTH, & w );
    LOG_OPENGL_ERROR();

    GLint h = 0;
    openGlFunctions->glGetTexLevelParameteriv( GL_TEXTURE_2D, mipmapLevel, GL_TEXTURE_HEIGHT, & h );
    LOG_OPENGL_ERROR();

    return QSize( w, h );
}

ShaderProgram GlUtilities::builtShaderProgram(
        QOpenGLFunctions_3_3_Core* const openGlFunctions,
        const QSurfaceFormat& aOpenGlFormat,
        const QString& vertexShaderFilePath,
        const QString& fragmentShaderFilePath )
{
    ShaderProgram program(
                openGlFunctions,
                aOpenGlFormat );
    GlUtilities::buildShaderProgram( program, vertexShaderFilePath, fragmentShaderFilePath );

    return program.isValid()
           ? program
           : ShaderProgram();
}

bool GlUtilities::buildShaderProgram(
        ShaderProgram& shaderProgram,
        const QString& vertexShaderFilePath,
        const QString& fragmentShaderFilePath )
{
    const ShaderInfo vertexShaderInfo(
                ShaderInfo::ShaderTypeVertex,
                GeneralUtilities::fileContents( vertexShaderFilePath ) );

    const ShaderInfo fragmentShaderInfo(
                ShaderInfo::ShaderTypeFragment,
                GeneralUtilities::fileContents( fragmentShaderFilePath ) );

    shaderProgram.setShaders(
                QList< ShaderInfo >()
                << vertexShaderInfo
                << fragmentShaderInfo );

    return shaderProgram.build();
}

bool GlUtilities::isTextureWrapModeValid( const GlUtilities::TextureWrapMode textureWrapMode )
{
    return textureWrapMode == TextureWrapMode::Repeat
           || textureWrapMode == TextureWrapMode::Clamp
           || textureWrapMode == TextureWrapMode::ClampToEdge
           || textureWrapMode == TextureWrapMode::ClampToBorder;
}

GLenum GlUtilities::textureWrapModeToOpenGlOne( const GlUtilities::TextureWrapMode textureWrapMode )
{
    switch ( textureWrapMode )
    {
        case TextureWrapMode::Repeat:
            return GL_REPEAT;
        case TextureWrapMode::Clamp:
            return GL_CLAMP;
        case TextureWrapMode::ClampToEdge:
            return GL_CLAMP_TO_EDGE;
        case TextureWrapMode::ClampToBorder:
            return GL_CLAMP_TO_BORDER;
        default:
            break;
    }

    return CommonEntities::invalidEnumGlValue();
}

GlUtilities::TextureWrapMode GlUtilities::textureWrapModeFromOpenGlOne( const GLenum openGlTextureWrapMode )
{
    switch ( openGlTextureWrapMode )
    {
        case GL_REPEAT:
            return TextureWrapMode::Repeat;
        case GL_CLAMP:
            return TextureWrapMode::Clamp;
        case GL_CLAMP_TO_EDGE:
            return TextureWrapMode::ClampToEdge;
        case GL_CLAMP_TO_BORDER:
            return TextureWrapMode::ClampToBorder;
    }

    return TextureWrapMode::Invalid;
}

bool GlUtilities::isTextureFilterValid( const GlUtilities::TextureFilter textureFilter )
{
    return textureFilter == TextureFilter::Nearest
           || textureFilter == TextureFilter::Linear
           || GlUtilities::isMipMapTextureFilter( textureFilter );
}

GLenum GlUtilities::textureFilterToOpenGlOne( const GlUtilities::TextureFilter textureFilter )
{
    switch ( textureFilter )
    {
        case TextureFilter::Nearest:
             return GL_NEAREST;
        case TextureFilter::Linear:
             return GL_LINEAR;
        case TextureFilter::NearestMipmapNearest:
             return GL_NEAREST_MIPMAP_NEAREST;
        case TextureFilter::LinearMipmapNearest:
             return GL_LINEAR_MIPMAP_NEAREST;
        case TextureFilter::NearestMipmapLinear:
             return GL_NEAREST_MIPMAP_LINEAR;
        case TextureFilter::LinearMipmapLinear:
             return GL_LINEAR_MIPMAP_LINEAR;
        default:
            break;
    }

    return CommonEntities::invalidEnumGlValue();
}

GlUtilities::TextureFilter GlUtilities::textureFilterFromOpenGlOne( const GLenum openGlTextureFilter )
{
    switch ( openGlTextureFilter )
    {
        case GL_NEAREST:
             return TextureFilter::Nearest;
        case GL_LINEAR:
             return TextureFilter::Linear;
        case GL_NEAREST_MIPMAP_NEAREST:
             return TextureFilter::NearestMipmapNearest;
        case GL_LINEAR_MIPMAP_NEAREST:
             return TextureFilter::LinearMipmapNearest;
        case GL_NEAREST_MIPMAP_LINEAR:
             return TextureFilter::NearestMipmapLinear;
        case GL_LINEAR_MIPMAP_LINEAR:
             return TextureFilter::LinearMipmapLinear;
    }

    return TextureFilter::Invalid;
}

bool GlUtilities::isMipMapTextureFilter( const GlUtilities::TextureFilter textureFilter )
{
    return textureFilter == TextureFilter::NearestMipmapNearest
           || textureFilter == TextureFilter::LinearMipmapNearest
           || textureFilter == TextureFilter::NearestMipmapLinear
           || textureFilter == TextureFilter::LinearMipmapLinear;
}
