#include "MathUtilities.h"

#include <glm/gtc/epsilon.hpp>

#include <QDateTime>
#include <QMatrix>
#include <QPoint>
#include <QtMath>

#include <cmath>
#include <limits>

using namespace WaterRippleSimulator::Utilities;

const int MathUtilities::s_axesCount1D = 1;
const int MathUtilities::s_axesCount2D = 2;
const int MathUtilities::s_axesCount3D = 3;
const int MathUtilities::s_axesCount4D = 4;

// don't define
// MathUtilities::MathUtilities()
// {
// }

QRect MathUtilities::scaledConcentricRect(
        const QRect& rect,
        const qreal scaleFactor )
{
    if ( ! rect.isValid()
         || ( scaleFactor < 0.0
              || scaleFactor > 1.0 ) )
    {
        return QRect();
    }

    const QPoint center = rect.center();
    QMatrix m;
    m.translate( center.x(), center.y() );
    m.scale( scaleFactor,
             scaleFactor );
    m.translate( -center.x(), -center.y() );

    return m.mapRect( rect );
}

qreal MathUtilities::to360DegreesAngle( const qreal angleDegrees )
{
    qreal angle360Degrees = angleDegrees;
    if ( qFuzzyCompare( qAbs( angleDegrees ), 360.0 )
         || qAbs( angleDegrees ) > 360.0 )
    {
        angle360Degrees = int( qAbs( angleDegrees ) ) % 360 * angle360Degrees / qAbs( angleDegrees );
    }

    return angle360Degrees;
}

int MathUtilities::boundedRandom( const int min, const int max )
{
    int randomNumber = 0;
    if ( min < max )
    {
        return qrand() % ( max - min + 1 ) + min;
    }
    else if ( min == max )
    {
        randomNumber = min;
    }

    return randomNumber;
}

void MathUtilities::convertDoubleToSinglePrecisionV( float* const singlePrecision, const double* const doublePrecision, const int length )
{
    for ( int i = 0; i < length; ++ i )
    {
        singlePrecision[ i ] = static_cast< float > ( doublePrecision[ i ] );
    }
}

QList< QVector3D > MathUtilities::gramSchmidtOrthogonalization( const QList< QVector3D >& linearlyIndependentVectors )
{
    const int linearlyIndependentVectorsCount = linearlyIndependentVectors.size();
    if ( linearlyIndependentVectorsCount == 0 )
    {
        return QList< QVector3D >();
    }

    QList< QVector3D > orthogonalizedVectors;
    orthogonalizedVectors.reserve( linearlyIndependentVectorsCount );
    orthogonalizedVectors.append( linearlyIndependentVectors.first() );
    for ( int i = 1; i < linearlyIndependentVectorsCount; ++ i )
    {
        const QVector3D& linearlyIndependentVector = linearlyIndependentVectors[ i ];
        QVector3D orthogonalizedVector = linearlyIndependentVector;
        for ( int j = 0; j < i; ++ j )
        {
            const QVector3D projectionVector = MathUtilities::projection( linearlyIndependentVector, orthogonalizedVectors[ j ] );
            if ( ! MathUtilities::isNan( projectionVector ) && ! MathUtilities::isInf( projectionVector ) )
            {
                orthogonalizedVector -= projectionVector;
            }
        }

        orthogonalizedVectors.append( orthogonalizedVector );
    }

    return orthogonalizedVectors;
}

bool MathUtilities::epsilonEqual( const QVector2D& v1, const QVector2D& v2 )
{
    return    qFuzzyCompare( v1.x(), v2.x() )
           && qFuzzyCompare( v1.y(), v2.y() );
}

bool MathUtilities::epsilonEqual( const QVector3D& v1, const QVector3D& v2 )
{
    return    qFuzzyCompare( v1.x(), v2.x() )
           && qFuzzyCompare( v1.y(), v2.y() )
           && qFuzzyCompare( v1.z(), v2.z() );
}

bool MathUtilities::epsilonEqual( const QVector4D& v1, const QVector4D& v2 )
{
    return    qFuzzyCompare( v1.x(), v2.x() )
           && qFuzzyCompare( v1.y(), v2.y() )
           && qFuzzyCompare( v1.z(), v2.z() )
           && qFuzzyCompare( v1.w(), v2.w() );
}

bool MathUtilities::isUnit( const QVector2D& v )
{
    return v.length() <= 1.0f;
}

bool MathUtilities::isUnit( const QVector3D& v )
{
    return v.length() <= 1.0f;
}

bool MathUtilities::isUnit( const QVector4D& v )
{
    return v.length() <= 1.0f;
}

bool MathUtilities::isInf( const QVector2D& v )
{
    return    std::isinf( v.x() )
           || std::isinf( v.y() );
}

bool MathUtilities::isInf( const QVector3D& v )
{
    return    std::isinf( v.x() )
           || std::isinf( v.y() )
           || std::isinf( v.z() );
}

bool MathUtilities::isInf( const QVector4D& v )
{
    return    std::isinf( v.x() )
           || std::isinf( v.y() )
           || std::isinf( v.z() )
           || std::isinf( v.w() );
}

bool MathUtilities::isNan( const QVector2D& v )
{
    return    std::isnan( v.x() )
           || std::isnan( v.y() );
}

bool MathUtilities::isNan( const QVector3D& v )
{
    return    std::isnan( v.x() )
           || std::isnan( v.y() )
           || std::isnan( v.z() );
}

bool MathUtilities::isNan( const QVector4D& v )
{
    return    std::isnan( v.x() )
           || std::isnan( v.y() )
           || std::isnan( v.z() )
           || std::isnan( v.w() );
}

int MathUtilities::vertexIndex( const QList< QVector2D >& vertices, const QVector2D& v )
{
    const int verticesCount = vertices.size();
    for ( int i = 0; i < verticesCount; ++ i )
    {
        if ( MathUtilities::epsilonEqual( vertices[ i ], v ) )
        {
            return i;
        }
    }

    return -1;
}

int MathUtilities::vertexIndex( const QList< QVector3D >& vertices, const QVector3D& v )
{
    const int verticesCount = vertices.size();
    for ( int i = 0; i < verticesCount; ++ i )
    {
        if ( MathUtilities::epsilonEqual( vertices[ i ], v ) )
        {
            return i;
        }
    }

    return -1;
}

int MathUtilities::vertexIndex( const QList< QVector4D >& vertices, const QVector4D& v )
{
    const int verticesCount = vertices.size();
    for ( int i = 0; i < verticesCount; ++ i )
    {
        if ( MathUtilities::epsilonEqual( vertices[ i ], v ) )
        {
            return i;
        }
    }

    return -1;
}

float MathUtilities::degrees( const float radians )
{
    return radians / M_PI * 180.0f;
}

float MathUtilities::radians( const float degrees )
{
    return degrees / 180.0f * M_PI;
}

float MathUtilities::angle( const QVector2D& v1, const QVector2D& v2, const bool useDegrees )
{
    const float radiansAngle = acosf( QVector2D::dotProduct( v1.normalized(), v2.normalized() ) );
    return useDegrees
           ? MathUtilities::degrees( radiansAngle )
           : radiansAngle;
}

float MathUtilities::angle( const QVector3D& v1, const QVector3D& v2, const bool useDegrees )
{
    const float radiansAngle = acosf( QVector3D::dotProduct( v1.normalized(), v2.normalized() ) );
    return useDegrees
           ? MathUtilities::degrees( radiansAngle )
           : radiansAngle;
}

float MathUtilities::angle( const QVector4D& v1, const QVector4D& v2, const bool useDegrees )
{
    const float radiansAngle = acosf( QVector4D::dotProduct( v1.normalized(), v2.normalized() ) );
    return useDegrees
           ? MathUtilities::degrees( radiansAngle )
           : radiansAngle;
}

QVector2D MathUtilities::projection( const QVector2D& v1, const QVector2D& v2 )
{
    return QVector2D::dotProduct( v1, v2 ) / v2.lengthSquared() * v2;
}

QVector3D MathUtilities::projection( const QVector3D& v1, const QVector3D& v2 )
{
    return QVector3D::dotProduct( v1, v2 ) / v2.lengthSquared() * v2;
}

QVector4D MathUtilities::projection( const QVector4D& v1, const QVector4D& v2 )
{
    return QVector4D::dotProduct( v1, v2 ) / v2.lengthSquared() * v2;
}

QVector< float > MathUtilities::coordinates( const QVector2D& v )
{
    return MathUtilities::coordinates( QList< QVector2D >() << v );
}

QVector< float > MathUtilities::coordinates( const QVector3D& v )
{
    return MathUtilities::coordinates( QList< QVector3D >() << v );
}

QVector< float > MathUtilities::coordinates( const QVector4D& v )
{
    return MathUtilities::coordinates( QList< QVector4D >() << v );
}

QVector< float > MathUtilities::coordinates( const Triangle2& t )
{
    return MathUtilities::coordinates( QList< Triangle2 >() << t );
}

QVector< float > MathUtilities::coordinates( const Triangle3& t )
{
    return MathUtilities::coordinates( QList< Triangle3 >() << t );
}

QVector< float > MathUtilities::coordinates( const QList< QVector2D >& vectors )
{
    QVector< float > coords;
    const int vectorsCount = vectors.size();
    if ( vectorsCount > 0 )
    {
        coords.reserve( vectorsCount * s_axesCount2D );
        for ( int i = 0; i < vectorsCount; ++ i )
        {
            const QVector2D& vector = vectors[ i ];
            coords << vector.x()
                   << vector.y();
        }
    }

    return coords;
}

QVector< float > MathUtilities::coordinates( const QList< QVector3D >& vectors )
{
    QVector< float > coords;
    const int vectorsCount = vectors.size();
    if ( vectorsCount > 0 )
    {
        coords.reserve( vectorsCount * s_axesCount3D );
        for ( int i = 0; i < vectorsCount; ++ i )
        {
            const QVector3D& vector = vectors[ i ];
            coords << vector.x()
                   << vector.y()
                   << vector.z();
        }
    }

    return coords;
}

QVector< float > MathUtilities::coordinates( const QList< QVector4D >& vectors )
{
    QVector< float > coords;
    const int vectorsCount = vectors.size();
    if ( vectorsCount > 0 )
    {
        coords.reserve( vectorsCount * s_axesCount4D );
        for ( int i = 0; i < vectorsCount; ++ i )
        {
            const QVector4D& vector = vectors[ i ];
            coords << vector.x()
                   << vector.y()
                   << vector.z()
                   << vector.w();
        }
    }

    return coords;
}

QVector< float > MathUtilities::coordinates( const QList< Triangle2 >& triangles )
{
    QVector< float > coords;
    const int trianglesCount = triangles.size();
    if ( trianglesCount > 0 )
    {
        coords.reserve( trianglesCount * Triangle2::s_verticesCount * s_axesCount2D );
        for ( int i = 0; i < trianglesCount; ++ i )
        {
            coords << MathUtilities::coordinates( triangles[ i ].vertices() );
        }
    }

    return coords;
}

QVector< float > MathUtilities::coordinates( const QList< Triangle3 >& triangles )
{
    QVector< float > coords;
    const int trianglesCount = triangles.size();
    if ( trianglesCount > 0 )
    {
        coords.reserve( trianglesCount * Triangle3::s_verticesCount * s_axesCount3D );
        for ( int i = 0; i < trianglesCount; ++ i )
        {
            coords << MathUtilities::coordinates( triangles[ i ].vertices() );
        }
    }

    return coords;
}

int MathUtilities::digitsCount( const int number )
{
    return qRound( log10( number ) ) + 1;
}

int MathUtilities::rangeRand( const int minValue, const int maxValue )
{
    const int diff = maxValue - minValue;
    if ( diff <= 0 )
    {
        return minValue;
    }

    return minValue + qrand() % ( diff + 1 );
}

QVector2D MathUtilities::qtFromGlmVec( const glm::vec2& glmVec )
{
    return QVector2D( glmVec.x, glmVec.y );
}

QVector3D MathUtilities::qtFromGlmVec( const glm::vec3& glmVec )
{
    return QVector3D( glmVec.x, glmVec.y, glmVec.z );
}

QVector4D MathUtilities::qtFromGlmVec( const glm::vec4& glmVec )
{
    return QVector4D( glmVec.x, glmVec.y, glmVec.z, glmVec.w );
}

glm::vec2 MathUtilities::glmFromQtVec( const QVector2D& qtVec )
{
    return glm::vec2( qtVec.x(), qtVec.y() );
}

glm::vec3 MathUtilities::glmFromQtVec( const QVector3D& qtVec )
{
    return glm::vec3( qtVec.x(), qtVec.y(), qtVec.z() );
}

glm::vec4 MathUtilities::glmFromQtVec( const QVector4D& qtVec )
{
    return glm::vec4( qtVec.x(), qtVec.y(), qtVec.z(), qtVec.w() );
}

glm::vec2 MathUtilities::safeNormalize( const glm::vec2& v )
{
    return MathUtilities::hasZeroLength( v )
           || glm::any( glm::isinf( v ) )
           || glm::any( glm::isnan( v ) )
           ? v
           : glm::normalize( v );
}

glm::vec3 MathUtilities::safeNormalize( const glm::vec3& v )
{
    return MathUtilities::hasZeroLength( v )
           || glm::any( glm::isinf( v ) )
           || glm::any( glm::isnan( v ) )
           ? v
           : glm::normalize( v );
}

glm::vec4 MathUtilities::safeNormalize( const glm::vec4& v )
{
    return MathUtilities::hasZeroLength( v )
           || glm::any( glm::isinf( v ) )
           || glm::any( glm::isnan( v ) )
           ? v
           : glm::normalize( v );
}

bool MathUtilities::epsilonEqual( const glm::vec2& v1, const glm::vec2& v2 )
{
    return glm::all( glm::epsilonEqual( v1, v2, glm::epsilon< glm::vec2::value_type >() ) );
}

bool MathUtilities::epsilonEqual( const glm::vec3& v1, const glm::vec3& v2 )
{
    return glm::all( glm::epsilonEqual( v1, v2, glm::epsilon< glm::vec3::value_type >() ) );
}

bool MathUtilities::epsilonEqual( const glm::vec4& v1, const glm::vec4& v2 )
{
    return glm::all( glm::epsilonEqual( v1, v2, glm::epsilon< glm::vec4::value_type >() ) );
}

bool MathUtilities::hasZeroLength( const glm::vec2& v )
{
    return MathUtilities::epsilonEqual( v, glm::vec2( 0.0f ) );
}

bool MathUtilities::hasZeroLength( const glm::vec3& v )
{
    return MathUtilities::epsilonEqual( v, glm::vec3( 0.0f ) );
}

bool MathUtilities::hasZeroLength( const glm::vec4& v )
{
    return MathUtilities::epsilonEqual( v, glm::vec4( 0.0f ) );
}

bool MathUtilities::isUnit( const glm::vec2& v )
{
    return glm::length( v ) <= 1.0f;
}

bool MathUtilities::isUnit( const glm::vec3& v )
{
    return glm::length( v ) <= 1.0f;
}

bool MathUtilities::isUnit( const glm::vec4& v )
{
    return glm::length( v ) <= 1.0f;
}

QSize MathUtilities::squaresToVerticesCount( const QSize& squaresCount )
{
    return squaresCount.isEmpty()
           ? squaresCount
           : squaresCount + QSize( 1, 1 );
}

QSize MathUtilities::verticesToSquaresCount( const QSize& verticesCount )
{
    return verticesCount.isEmpty()
           ? verticesCount
           : verticesCount - QSize( 1, 1 );
}

int MathUtilities::randomNumber( const int max )
{
    if ( max < 0 )
    {
        return -1;
    }

    if ( max == 0 )
    {
        return 0;
    }

    const QDateTime currentDateTime = QDateTime::currentDateTime();
    const qint64 msecsSinceEpoch = currentDateTime.toMSecsSinceEpoch();
    return static_cast< int >( ( static_cast< qint64 >( qrand() )
                                 + msecsSinceEpoch )
                               % static_cast< qint64 >( max ) );
}
