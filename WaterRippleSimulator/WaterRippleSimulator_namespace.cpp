#include "WaterRippleSimulator_namespace.h"

#include <qopenglext.h>

#include <QStringBuilder>

using namespace WaterRippleSimulator;

const GLuint CommonEntities::s_invalidUnsignedGlValue = 0U;
const GLint CommonEntities::s_invalidSignedGlValue    = -1;
const int CommonEntities::s_axesCount2D = 2;
const int CommonEntities::s_axesCount3D = 3;
const int CommonEntities::s_axesCount4D = 4;

const float CommonEntities::s_drawingVolumeBoundsXMin = -1.0f;
const float CommonEntities::s_drawingVolumeBoundsXMax = 1.0f;
const float CommonEntities::s_drawingVolumeBoundsYMin = -1.0f;
const float CommonEntities::s_drawingVolumeBoundsYMax = 1.0f;
const float CommonEntities::s_drawingVolumeBoundsZMin = -1.0f;
const float CommonEntities::s_drawingVolumeBoundsZMax = 1.0f;

QString CommonEntities::vecToString( const QVector2D& v )
{
    return QString( "%1( %2, %3 )" )
           .arg( QT_STRINGIFY2( QVector2D ) )
           .arg( v.x(), 0, 'f', 3 )
           .arg( v.y(), 0, 'f', 3 );
}

QString CommonEntities::vecToString( const QVector3D& v )
{
    return QString( "%1( %2, %3, %4 )" )
           .arg( QT_STRINGIFY2( QVector3D ) )
           .arg( v.x(), 0, 'f', 3 )
           .arg( v.y(), 0, 'f', 3 )
           .arg( v.z(), 0, 'f', 3 );
}

QString CommonEntities::vecToString( const QVector4D& v )
{
    return QString( "%1( %2, %3, %4, %5 )" )
            .arg( QT_STRINGIFY2( QVector4D ) )
            .arg( v.x(), 0, 'f', 3 )
            .arg( v.y(), 0, 'f', 3 )
            .arg( v.z(), 0, 'f', 3 )
            .arg( v.w(), 0, 'f', 3 );
}

bool CommonEntities::isTextureIndexValid( const CommonEntities::TextureIndex textureIndex )
{
    return    textureIndex == TextureIndexNone
           || textureIndex == TextureIndexVideoFrameColor;
}

bool CommonEntities::isTextureUnitIndexValid( const CommonEntities::TextureUnitIndex textureUnitIndex )
{
    return    textureUnitIndex == TextureUnitIndexNone
           || textureUnitIndex == TextureUnitIndexVideoFrameColor;
}

CommonEntities::TextureUnitIndex CommonEntities::textureUnitIndexFromTextureIndex( const CommonEntities::TextureIndex textureIndex )
{
    switch ( textureIndex )
    {
        case TextureIndexNone:
            return TextureUnitIndexNone;
        case TextureIndexVideoFrameColor:
            return TextureUnitIndexVideoFrameColor;
        default:
            break;
    }

    return TextureUnitIndexInvalid;
}

GLenum CommonEntities::textureUnitOpenGlDescriptorFromTextureUnitIndex( const CommonEntities::TextureUnitIndex textureUnitIndex )
{
    switch ( textureUnitIndex )
    {
        case TextureUnitIndexNone:
            return GL_TEXTURE0;
        case TextureUnitIndexVideoFrameColor:
            return GL_TEXTURE0;
        default:
            break;
    }

    return CommonEntities::s_invalidUnsignedGlValue;
}

QString CommonEntities::textureUnitGlSlNameFromTextureUnitIndex( const CommonEntities::TextureUnitIndex textureUnitIndex )
{
    switch ( textureUnitIndex )
    {
        case TextureUnitIndexVideoFrameColor:
            return "colorTextureSampler";
        default:
            break;
    }

    return QString();
}

QList< CommonEntities::TextureIndex > CommonEntities::textureIndicesFromTextureUnitIndex( const CommonEntities::TextureUnitIndex textureUnitIndex )
{
    QList< CommonEntities::TextureIndex > textureIndices;
    switch ( textureUnitIndex )
    {
        case TextureUnitIndexNone:
            textureIndices << TextureIndexNone;
        case TextureUnitIndexVideoFrameColor:
            textureIndices << TextureIndexVideoFrameColor;
            break;
        default:
            break;
    }

    return textureIndices;
}

GLenum CommonEntities::invalidEnumGlValue()
{
    return std::numeric_limits< GLenum >::is_signed
           ? CommonEntities::s_invalidSignedGlValue
           : CommonEntities::s_invalidUnsignedGlValue;
}

bool CommonEntities::isUnsignedGlValueValid( const GLuint unsignedGlValue )
{
    return unsignedGlValue > 0U;
}

bool CommonEntities::isSignedGlValueValid( const GLint signedGlValue )
{
    return signedGlValue >= 0;
}

bool CommonEntities::isEnumGlValueValid( const GLenum enumGlValue )
{
    return enumGlValue > CommonEntities::invalidEnumGlValue();
}
