#include "VideoDisplay.h"

#include "Helpers/CoordinateSystemIndicator.h"
#include "Helpers/OpenGlErrorLogger.h"
#include "Helpers/OpenGlTimeCounter.h"
#include "Helpers/WaterPool.h"
#include "Helpers/WaterSurfaceGpu.h"
#include "glext.h"
#include "Utilities/GeneralUtilities.h"
#include "Utilities/ImageUtilities.h"
#include "Utilities/MathUtilities.h"

#include <QDebug>
#include <QGLWidget>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QOpenGLFunctions_3_3_Core>
#include <QScopedArrayPointer>
#include <QString>
#include <QTime>
#include <QTimer>
#include <QVector3D>

#include <limits>

using namespace VideoEffects;

const QList< VideoDisplay::GenericVertexAttributeIndex > VideoDisplay::s_allOrderedGenericVertexAttributeIndices =
        QList< VideoDisplay::GenericVertexAttributeIndex >()
        << VideoDisplay::GenericVertexAttributeIndexVertexPos
        << VideoDisplay::GenericVertexAttributeIndexVertexTexCoord;

const QColor VideoDisplay::s_defaultClearColor{ 255, 255, 255, 255 };
const int VideoDisplay::s_randomRipplePeriodStep = 200;

VideoDisplay::VideoDisplay( QWidget* parent, Qt::WindowFlags f )
    : QOpenGLWidget( parent, f )
    , m_aspectRatioMode{ Qt::KeepAspectRatio }
    , m_openGlFunctions{ nullptr }
    , m_currentPboIndex{ -1 }
    , m_textureChangeTimer{ nullptr }
    , m_isPlayingVideo{ false }
    , m_videoFrameDisplayMode{ VideoFrameDisplayMode::Normal }
    , m_alteredMatrixType{ CommonEntities::MatrixType::Invalid }
    , m_hasMouseDragStarted{ false }
    , m_randomRippleTimer{ nullptr }
    , m_waterPropagationTimer{ nullptr }
    , m_randomWaterRippleScheduled{ false }
    , m_sceneBackgroundColor{ s_defaultClearColor }
    , m_framesCount{ 0 }
    , m_fpsTimer{ nullptr }
    , m_fps{ 0.0 }
{
    QSurfaceFormat surfaceFormat;
    surfaceFormat.setRenderableType( QSurfaceFormat::OpenGL );
    surfaceFormat.setVersion( 3, 3 );
    surfaceFormat.setProfile( QSurfaceFormat::CoreProfile );
    this->setFormat( surfaceFormat );

    this->clearGlData();

    static const float screenWidth = 1.0f;
    static const float halfModelWidth = screenWidth * 2.0f / 4.0f;

    VaoData screenData;

    const QVector3D bottomLeftPos( - halfModelWidth, -halfModelWidth, 0.0 );
    const QVector3D bottomRightPos( halfModelWidth, -halfModelWidth, 0.0 );
    const QVector3D topRightPos( halfModelWidth, halfModelWidth, 0.0 );
    const QVector3D topLeftPos( - halfModelWidth, halfModelWidth, 0.0 );
    screenData.geometry
            << Triangle3( bottomLeftPos,
                          bottomRightPos,
                          topRightPos )
            << Triangle3( topRightPos,
                          topLeftPos,
                          bottomLeftPos );

    const QVector3D bottomLeftColor  = ImageUtilities::realFromIntColor( QColor( 255, 0, 0 ) ).toVector3D();
    const QVector3D bottomRightColor = ImageUtilities::realFromIntColor( QColor( 0, 255, 0 ) ).toVector3D();
    const QVector3D topRightColor    = ImageUtilities::realFromIntColor( QColor( 0, 0, 255 ) ).toVector3D();
    const QVector3D topLeftColor     = ImageUtilities::realFromIntColor( QColor( 255, 255, 0 ) ).toVector3D();
    screenData.color
            << Triangle3( bottomLeftColor,
                          bottomRightColor,
                          topRightColor )
            << Triangle3( topRightColor,
                          topLeftColor,
                          bottomLeftColor );

    const QVector2D bottomLeftTexCoord( 0.25f, 0.25f );
    const QVector2D bottomRightTexCoord( 0.75f, 0.25f );
    const QVector2D topRightTexCoord( 0.75f, 0.75f );
    const QVector2D topLeftTexCoord( 0.25, 0.75f );
    screenData.textureCoordinates
            << Triangle2( bottomLeftTexCoord,
                          bottomRightTexCoord,
                          topRightTexCoord )
            << Triangle2( topRightTexCoord,
                          topLeftTexCoord,
                          bottomLeftTexCoord );
    m_vaosData.append( screenData );

    m_fpsTimer = new QTimer( this );
    m_fpsTimer->setInterval( 1000 );
    connect( m_fpsTimer, SIGNAL(timeout()), this, SLOT(calcFps()) );

    m_waterPropagationTimer = new QTimer( this );

    const int desiredFps = 0;
    const int fps = qMax( 0, desiredFps );
    m_waterPropagationTimer->setInterval( fps == 0
                                          ? 0
                                          : qRound( 1000.0 / fps ) );
    connect( m_waterPropagationTimer,
             SIGNAL(timeout()),
             this,
             SLOT(update()) );
}

VideoDisplay::~VideoDisplay()
{
    this->clearGlData();
}

void VideoDisplay::setAspectRatioMode( const Qt::AspectRatioMode aAspectRatioMode )
{
    if ( ! VideoDisplay::isAspectRatioModeValid( aAspectRatioMode )
         || aAspectRatioMode == m_aspectRatioMode )
    {
        return;
    }

    m_aspectRatioMode = aAspectRatioMode;
    this->resizeGL( this->width(), this->height() );
}

Qt::AspectRatioMode VideoDisplay::aspectRatioMode() const
{
    return m_aspectRatioMode;
}

bool VideoDisplay::isAspectRatioModeValid( const Qt::AspectRatioMode aAspectRatioMode )
{
    return aAspectRatioMode == Qt::IgnoreAspectRatio
           || aAspectRatioMode == Qt::KeepAspectRatio
           || aAspectRatioMode == Qt::KeepAspectRatioByExpanding;
}

void VideoDisplay::setVideoFrameDisplayMode( const VideoDisplay::VideoFrameDisplayMode aVideoFrameDisplayMode )
{
    if ( aVideoFrameDisplayMode == m_videoFrameDisplayMode )
    {
        return;
    }

    this->clear();
    m_videoFrameDisplayMode = aVideoFrameDisplayMode;
    m_framesCount = 0;
    this->update();
}

VideoDisplay::VideoFrameDisplayMode VideoDisplay::videoFrameDisplayMode() const
{
    return m_videoFrameDisplayMode;
}

void VideoDisplay::setSceneBackgroundColor( const QColor& aSceneBackgroundColor )
{
    if ( ! aSceneBackgroundColor.isValid()
         || aSceneBackgroundColor == m_sceneBackgroundColor )
    {
        return;
    }

    m_sceneBackgroundColor = aSceneBackgroundColor;
    this->update();
}

QColor VideoDisplay::sceneBackgroundColor() const
{
    return m_sceneBackgroundColor;
}

WaterSettings VideoDisplay::waterSettings() const
{
    return m_waterSettings;
}

qreal VideoDisplay::fps() const
{
    return m_fps;
}

void VideoDisplay::setWaterSettings( const WaterSettings& aWaterSettings )
{
    const WaterSettings::WaterProperties changedProperties = aWaterSettings.diff( m_waterSettings );
    m_waterSettings = aWaterSettings;
    if ( changedProperties.testFlag( WaterSettings::WaterPropertyMeshSize ) )
    {
        this->applyMeshSize();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyShouldDrawAsWireframe ) )
    {
        this->applyShouldDrawAsWireframe();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyWavePropagationMethod ) )
    {
        this->applyWavePropagationMethod();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyWaterRippleMagnitude ) )
    {
        this->applyWaterRippleMagnitude();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyWaterRippleAbsRadius ) )
    {
        this->applyWaterRippleAbsRadius();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyWavePropagationSpeed ) )
    {
        this->applyWavePropagationSpeed();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyWaveHeightDampingOverTimeFactor ) )
    {
        this->applyWaveHeightDampingOverTimeFactor();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyWaveHeightClampingFactor ) )
    {
        this->applyWaveHeightClampingFactor();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyShouldPropagateWaves ) )
    {
        this->applyShouldPropagateWaves();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyShouldSimulateRain ) )
    {
        this->applyShouldSimulateRain();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyRainDropsFallTimeInterval ) )
    {
        this->applyRainDropsFallTimeInterval();
    }

    if ( changedProperties.testFlag( WaterSettings::WaterPropertyRainDropsCountPerFall ) )
    {
        this->applyRainDropsCountPerFall();
    }
}

void VideoDisplay::clear()
{
    m_currentPboIndex = -1;
    m_isPlayingVideo = false;
    m_framesCount = 0;
    m_fpsTimer->stop();
    this->setVideoFrameSize( QSize() );

    const int w = this->width();
    const int h = this->height();
    const int pixelComponentsCount = w * h * CommonEntities::s_axesCount4D;
    QVector< GLubyte > clearTexture( pixelComponentsCount );
    const QVector4D clearColor = ImageUtilities::realFromIntColor( s_defaultClearColor );
    for ( int i = 0; i < pixelComponentsCount; ++ i )
    {
        clearTexture[ i ] = clearColor[ i % CommonEntities::s_axesCount4D ];
    }

    this->setTexture( CommonEntities::TextureIndexVideoFrameColor,
                      clearTexture.data(),
                      w,
                      h,
                      GL_RGBA,
                      GL_BGRA,
                      GL_UNSIGNED_INT_8_8_8_8_REV,
                      GlUtilities::TextureFilter::Linear,
                      GlUtilities::TextureFilter::Linear,
                      GlUtilities::TextureWrapMode::Clamp,
                      GlUtilities::TextureWrapMode::Clamp,
                      true,
                      true );
}

void VideoDisplay::displayVideoFrame( const QVideoFrame& videoFrame )
{
    m_isPlayingVideo = true;

    // OpenGlTimeCounter t( m_openGlFunctions );
    // t.start();
    switch ( m_videoFrameDisplayMode )
    {
        case VideoFrameDisplayMode::Normal:
            this->displayVideoFrameUsingNormalApproach( videoFrame );
            break;
        case VideoFrameDisplayMode::SinglePbo:
            this->displayVideoFrameUsingSinglePbo( videoFrame );
            break;
        case VideoFrameDisplayMode::SinglePbo2:
            this->displayVideoFrameUsingSinglePbo2( videoFrame );
            break;
        case VideoFrameDisplayMode::MultiplePbo:
            this->displayVideoFrameUsingMultiplePbo( videoFrame );
            break;
    }

    // t.stop();
    // t.dumpTime();
}

void VideoDisplay::initializeGL()
{
    m_openGlFunctions = this->context()->versionFunctions< QOpenGLFunctions_3_3_Core >();
    if ( m_openGlFunctions == nullptr )
    {
        const QSurfaceFormat surfaceFormat = this->format();
        qDebug() << QString( "OpenGL version %1.%2 (%3 profile) is not supported." )
                    .arg( surfaceFormat.majorVersion() )
                    .arg( surfaceFormat.minorVersion() )
                    .arg( surfaceFormat.profile() == QSurfaceFormat::CoreProfile
                          ? "Core"
                          : "Compatibility" );
        return;
    }

    if ( ! m_openGlFunctions->initializeOpenGLFunctions() )
    {
        return;
    }

    OpenGlErrorLogger::instance().setOpenGlFunctions( m_openGlFunctions );

    m_openGlFunctions->glEnable( GL_LINE_SMOOTH );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glEnable( GL_POLYGON_SMOOTH );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glEnable( GL_DEPTH_TEST );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glClearColor( m_sceneBackgroundColor.redF(),
                                     m_sceneBackgroundColor.greenF(),
                                     m_sceneBackgroundColor.blueF(),
                                     m_sceneBackgroundColor.alphaF() );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
    LOG_OPENGL_ERROR();

    CoordinateSystemIndicator* const coordinateSystemIndicatorRaw = new CoordinateSystemIndicator( m_openGlFunctions, this->format() );
    m_coordinateSystemIndicator.reset( coordinateSystemIndicatorRaw );
    m_coordinateSystemIndicator->init();

    WaterSurfaceGpu* const waterSurfaceRaw = new WaterSurfaceGpu( m_openGlFunctions, this->format() );
    m_waterSurface.reset( waterSurfaceRaw );
    m_waterSurface->init();

    WaterPool* const waterPoolRaw = new WaterPool( m_openGlFunctions, this->format(), waterSurfaceRaw->meshRect() );
    m_waterPool.reset( waterPoolRaw );
    m_waterPool->init();

    Light light;
    light.pos = QVector3D( -1.0f, 0.5f, 0.5f );
    light.color.setRgb( 255, 255, 255 );
    light.attenuation.constant  = 0.8f;
    light.attenuation.linear    = 0.1f;
    light.attenuation.quadratic = 0.001f;
//    light.attenuation.constant  = 0.0f;
//    light.attenuation.linear    = 0.0f;
//    light.attenuation.quadratic = 0.0f;
    light.intensity             = 3.5f;

    m_waterSurface->setLight( light );
    m_waterPool->setLight( light );

    Material material;
    material.color.setRgb( 0x1C, 0x6B, 0xA0 );
    material.reflectance.emissive = QVector3D( 0.0f, 0.0f, 0.0f );
    material.reflectance.ambient = QVector3D( 0.9f, 0.9f, 0.9f );
    material.reflectance.diffuse = QVector3D( 0.5f, 0.5f, 0.5f );
    material.reflectance.specular = QVector3D( 0.1f, 0.1f, 0.1f );
//    material.reflectance.ambient = QVector3D( 1.0f, 1.0f, 1.0f );
//    material.reflectance.diffuse = QVector3D( 1.0f, 1.0f, 1.0f );
//    material.reflectance.specular = QVector3D( 1.0f, 1.0f, 1.0f );
    material.shininess = 100.0f;

    m_waterSurface->setMaterial( material );
    m_waterPool->setMaterial( material );

    // connect( m_waterSurface.data(), SIGNAL(updateRequested()), this, SLOT(update()) );
    // connect( m_waterPool.data(), SIGNAL(updateRequested()), this, SLOT(update()) );
    // connect( m_coordinateSystemIndicator.data(), SIGNAL(updateRequested()), this, SLOT(update()) );

    m_randomRippleTimer = new QTimer( this );
    m_randomRippleTimer->setInterval( WaterSettings::s_rainDropsFallTimeIntervalDefault );
    connect( m_randomRippleTimer, SIGNAL(timeout()), this, SLOT(scheduleWaterRippleAtRandomPoint()) );

    this->clearGlData();

    const ShaderInfo vertexShaderInfo(
                ShaderInfo::ShaderTypeVertex,
                GeneralUtilities::fileContents( ":/Resources/shaders/VideoDisplay.vert" ) );

    const ShaderInfo fragmentShaderInfo(
                ShaderInfo::ShaderTypeFragment,
                GeneralUtilities::fileContents( ":/Resources/shaders/VideoDisplay.frag" ) );

    m_shaderProgram.setShaders( QList< ShaderInfo >()
                                << vertexShaderInfo
                                << fragmentShaderInfo );
    m_shaderProgram.setOpenGlFunctionsFormat( m_openGlFunctions, this->format() );
    if ( ! m_shaderProgram.build() )
    {
        return;
    }

    m_shaderProgram.activate();

    const int shaderProgramId = m_shaderProgram.id();

    m_uniformVariableLocations.clear();
    if ( UniformVariablesCount > 0 )
    {
        m_uniformVariableLocations.reserve( UniformVariablesCount );
        for ( int i = 0; i < UniformVariablesCount; ++ i )
        {
            m_uniformVariableLocations.append(
                        m_openGlFunctions->glGetUniformLocation(
                            shaderProgramId,
                            VideoDisplay::uniformVariableName( static_cast< UniformVariableIndex >( i ) ).constData() ) );
            LOG_OPENGL_ERROR();
        }
    }

//    QMatrix4x4 m;
//    m( 1, 1 ) = -1;
//    this->setMatrix( m );

    m_textureUnitGlSlIds.clear();
    m_textureUnitGlSlIds.fill( CommonEntities::s_invalidSignedGlValue, CommonEntities::TextureUnitsCount );
    for ( int textureUnitIndex = CommonEntities::TextureUnitIndexVideoFrameColor;
          textureUnitIndex < CommonEntities::TextureUnitsCount;
          ++ textureUnitIndex )
    {
        const CommonEntities::TextureUnitIndex textureUnitIndexIntrinsic = static_cast< CommonEntities::TextureUnitIndex > ( textureUnitIndex );
        m_openGlFunctions->glActiveTexture( CommonEntities::textureUnitOpenGlDescriptorFromTextureUnitIndex( textureUnitIndexIntrinsic ) );
        LOG_OPENGL_ERROR();

        const QString samplerUniformVarName = CommonEntities::textureUnitGlSlNameFromTextureUnitIndex( textureUnitIndexIntrinsic );
        const GLint textureUnitGlSlDescriptor = m_openGlFunctions->glGetUniformLocation(
                                                    shaderProgramId,
                                                    samplerUniformVarName.toLatin1().constData() );
        LOG_OPENGL_ERROR();

        if ( CommonEntities::isSignedGlValueValid( textureUnitGlSlDescriptor ) )
        {
            m_textureUnitGlSlIds[ textureUnitIndex ] = textureUnitGlSlDescriptor;
            m_openGlFunctions->glUniform1i( textureUnitGlSlDescriptor, static_cast< GLint > ( textureUnitIndexIntrinsic ) );
            LOG_OPENGL_ERROR();
        }
        else
        {
            qDebug().nospace() << "sampler " << samplerUniformVarName << " not found";
        }
    }

    m_textureIds.clear();
    m_textureIds.fill( CommonEntities::s_invalidUnsignedGlValue, CommonEntities::TexturesCount );
    m_openGlFunctions->glGenTextures( m_textureIds.size(), m_textureIds.data() );
    LOG_OPENGL_ERROR();

    m_vbos.fill( CommonEntities::s_invalidUnsignedGlValue, VbosCount );
    m_openGlFunctions->glGenBuffers( m_vbos.size(), m_vbos.data() );
    LOG_OPENGL_ERROR();

    for ( int i = 0; i < VaosCount; ++ i )
    {
        const VaoIndex vaoIndex = static_cast< VaoIndex > ( i );
        const VaoData& vaoData = m_vaosData[ i ];

        const GLuint vertexPositionVbo = m_vbos[ this->vboIndex( vaoIndex, GenericVertexAttributeIndexVertexPos ) ];
        m_openGlFunctions->glBindBuffer( GL_ARRAY_BUFFER, vertexPositionVbo );
        LOG_OPENGL_ERROR();

        const QVector< float > verticesPosRaw = MathUtilities::coordinates( vaoData.geometry );
        m_openGlFunctions->glBufferData( GL_ARRAY_BUFFER,
                                         verticesPosRaw.size() * sizeof( float ),
                                         reinterpret_cast< const void* > ( verticesPosRaw.constData() ),
                                         GL_STATIC_DRAW );
        LOG_OPENGL_ERROR();

        const GLuint vertexTexCoordVbo = m_vbos[ this->vboIndex( vaoIndex, GenericVertexAttributeIndexVertexTexCoord ) ];
        m_openGlFunctions->glBindBuffer( GL_ARRAY_BUFFER, vertexTexCoordVbo );
        LOG_OPENGL_ERROR();

        const QVector< float > texCoordsRaw = MathUtilities::coordinates( vaoData.textureCoordinates );
        m_openGlFunctions->glBufferData( GL_ARRAY_BUFFER,
                                         texCoordsRaw.size() * sizeof( float ),
                                         reinterpret_cast< const void* > ( texCoordsRaw.constData() ),
                                         GL_STATIC_DRAW );
        LOG_OPENGL_ERROR();
    }

    m_openGlFunctions->glBindBuffer( GL_ARRAY_BUFFER, CommonEntities::s_invalidUnsignedGlValue );
    LOG_OPENGL_ERROR();

    m_vaos.fill( CommonEntities::s_invalidUnsignedGlValue, VaosCount );
    m_openGlFunctions->glGenVertexArrays( m_vaos.size(), m_vaos.data() );
    LOG_OPENGL_ERROR();

    for ( int i = 0; i < VaosCount; ++ i )
    {
        m_openGlFunctions->glBindVertexArray( m_vaos[ i ] );
        LOG_OPENGL_ERROR();

        this->enableGenericVertexAttributeArrays( true );

        const VaoIndex vaoIndex = static_cast< VaoIndex > ( i );

        const GLuint vertexPositionVbo = m_vbos[ this->vboIndex( vaoIndex, GenericVertexAttributeIndexVertexPos ) ];
        m_openGlFunctions->glBindBuffer( GL_ARRAY_BUFFER, vertexPositionVbo );
        LOG_OPENGL_ERROR();
        m_openGlFunctions->glVertexAttribPointer( GenericVertexAttributeIndexVertexPos,
                                                  MathUtilities::s_axesCount3D,
                                                  GL_FLOAT,
                                                  GL_FALSE,
                                                  0,
                                                  nullptr );
        LOG_OPENGL_ERROR();

        const GLuint vertexTexCoordVbo = m_vbos[ this->vboIndex( vaoIndex, GenericVertexAttributeIndexVertexTexCoord ) ];
        m_openGlFunctions->glBindBuffer( GL_ARRAY_BUFFER, vertexTexCoordVbo );
        LOG_OPENGL_ERROR();
        m_openGlFunctions->glVertexAttribPointer( GenericVertexAttributeIndexVertexTexCoord,
                                                  MathUtilities::s_axesCount2D,
                                                  GL_FLOAT,
                                                  GL_FALSE,
                                                  0,
                                                  nullptr );
        LOG_OPENGL_ERROR();

        m_openGlFunctions->glBindBuffer( GL_ARRAY_BUFFER, CommonEntities::s_invalidUnsignedGlValue );
        LOG_OPENGL_ERROR();

        this->enableGenericVertexAttributeArrays( false );
    }

    m_openGlFunctions->glBindVertexArray( CommonEntities::s_invalidUnsignedGlValue );
    LOG_OPENGL_ERROR();

    m_pbos.fill( CommonEntities::s_invalidUnsignedGlValue, PbosCount );
    m_openGlFunctions->glGenBuffers( m_pbos.size(), m_pbos.data() );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();

    this->applyMeshSize();
    this->applyShouldDrawAsWireframe();
    this->applyWavePropagationMethod();
    this->applyWaterRippleMagnitude();
    this->applyWaterRippleAbsRadius();
    this->applyWavePropagationSpeed();
    this->applyWaveHeightDampingOverTimeFactor();
    this->applyWaveHeightClampingFactor();
    this->applyShouldPropagateWaves();
    this->applyShouldSimulateRain();
    this->applyRainDropsFallTimeInterval();
    this->applyRainDropsCountPerFall();
}

bool VideoDisplay::isGLInitialized() const
{
    return m_openGlFunctions != nullptr;
}

void VideoDisplay::paintGL()
{
    if ( m_openGlFunctions == nullptr )
    {
        return;
    }

    // if ( ! m_fpsTimer->isActive() )
    // {
    //     m_fpsTimer->start();
    // }

    // needed since prior to calling paintGL, QOpenGL(Widget/Window) call glViewport
    this->resizeGL( this->width(), this->height() );

    m_openGlFunctions->glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    LOG_OPENGL_ERROR();

//    if ( m_shaderProgram.isValid() )
//    {
//        m_shaderProgram.activate();
//        for ( int i = 0; i < VaosCount; ++ i )
//        {
//            m_openGlFunctions->glBindVertexArray( m_vaos[ i ] );
//            LOG_OPENGL_ERROR();

//            this->enableGenericVertexAttributeArrays( true );

//            this->bindTexturesForVaoIndex( static_cast< VaoIndex > ( i ) );

//            m_openGlFunctions->glDrawArrays( GL_TRIANGLES, 0, m_vaosData[ i ].geometry.size() * Triangle3::s_verticesCount );
//            LOG_OPENGL_ERROR();

//            for ( int textureUnitIndex = 0;
//                  textureUnitIndex < CommonEntities::TextureUnitsCount;
//                  ++ textureUnitIndex )
//            {
//                this->bindTextureForTextureUnit( static_cast< CommonEntities::TextureUnitIndex >( i ),
//                                                 CommonEntities::s_invalidUnsignedGlValue );
//            }

//            this->enableGenericVertexAttributeArrays( false );
//        }

//        m_openGlFunctions->glBindVertexArray( CommonEntities::s_invalidUnsignedGlValue );
//        LOG_OPENGL_ERROR();

//        m_shaderProgram.deactivate();
//    }

    if ( m_randomWaterRippleScheduled )
    {
        m_waterSurface->rippleWaterAtRandomPoint();
        m_randomWaterRippleScheduled = false;
    }

    m_waterSurface->draw( m_waterSettings.shouldPropagateWaves() );
    m_waterPool->draw();
    m_coordinateSystemIndicator->draw();

    if ( ! m_fpsTimer->isActive() )
    {
        m_fpsTimer->start();
    }

    ++ m_framesCount;
    // if ( m_isPlayingVideo )
    // {
    //     ++ m_framesCount;
    // }
}

void VideoDisplay::resizeGL( int w, int h )
{
    if ( m_openGlFunctions == nullptr )
    {
        return;
    }

    // const QSize displaySize = this->size();
    // const QSize viewportSize = ( m_videoFrameSize.isValid()
    //                              ? m_videoFrameSize.scaled( displaySize, m_aspectRatioMode )
    //                              : displaySize )* this->devicePixelRatio();

    // const QSize offset = ( QSizeF( displaySize - viewportSize ) / 2.0 ).toSize();

    const QRect vp = this->viewport( w, h );
    m_openGlFunctions->glViewport(
                vp.x(),
                vp.y(),
                vp.width(),
                vp.height() );
    LOG_OPENGL_ERROR();
}

void VideoDisplay::keyPressEvent( QKeyEvent* event )
{
    QOpenGLWidget::keyPressEvent( event );

    static const QList< VideoFrameDisplayMode > modes =
            QList< VideoFrameDisplayMode >()
            << VideoFrameDisplayMode::Normal
            << VideoFrameDisplayMode::SinglePbo
            << VideoFrameDisplayMode::SinglePbo2
            << VideoFrameDisplayMode::MultiplePbo;
    const int key = event->key();

    const int modeIndex = key - Qt::Key_0;
    if ( 0 <= modeIndex && modeIndex < modes.size() )
    {
        this->setVideoFrameDisplayMode( modes[ modeIndex ] );
        return;
    }

    if ( key == Qt::Key_Left
         || key == Qt::Key_Right
         || key == Qt::Key_Up
         || key == Qt::Key_Down )
    {
        static const float angleStep = 5.0f;
        float yRot = 0.0f;
        float xRot = 0.0f;
        switch( key )
        {
            case Qt::Key_Left:
                yRot = - angleStep;
                break;
            case Qt::Key_Right:
                yRot = angleStep;
                break;
            case Qt::Key_Up:
                xRot = - angleStep;
                break;
            case Qt::Key_Down:
                xRot = angleStep;
                break;
        }

        QMatrix4x4 keyMatrix;
        if ( ! qFuzzyIsNull( xRot ) )
        {
            keyMatrix.rotate( xRot, 1.0f, 0.0f );
        }

        if ( ! qFuzzyIsNull( yRot ) )
        {
            keyMatrix.rotate( yRot, 0.0f, 1.0f );
        }

        m_modelMatrix = keyMatrix * m_modelMatrix;

        this->applyMatrix( CommonEntities::MatrixType::Model, m_mouseCacheMatrix * m_modelMatrix, true );
    }
    else if ( key == Qt::Key_Space )
    {
        const bool shouldPropagateWater = ! m_waterSettings.shouldPropagateWaves();
        m_waterSettings.setShouldPropagateWaves( shouldPropagateWater );

        emit shouldPropagateWaterChanged( shouldPropagateWater );

        this->update();
    }
    else if ( key == Qt::Key_R )
    {
        if ( m_randomRippleTimer->isActive() )
        {
            m_randomRippleTimer->stop();
        }
        else
        {
            m_randomRippleTimer->start();
        }
    }
    else if ( key == Qt::Key_Plus
              || key == Qt::Key_Minus )
    {
        m_randomRippleTimer->setInterval( qMax( m_randomRippleTimer->interval()
                                                + ( key == Qt::Key_Minus ? 1 : -1 )
                                                * s_randomRipplePeriodStep,
                                                s_randomRipplePeriodStep ) );
    }
}

void VideoDisplay::mousePressEvent( QMouseEvent* event )
{
    QOpenGLWidget::mousePressEvent( event );

    if ( ! this->shouldHandleMouseEvent( event ) )
    {
        return;
    }

    const QPoint mousePos = event->pos();
    if ( event->buttons().testFlag( Qt::RightButton ) )
    {
        this->scheduleWaterRippleAtViewportPt( mousePos );
    }
    else
    {
        m_hasMouseDragStarted = true;
        m_mousePressInitialPos = mousePos;
        m_alteredMatrixType = CommonEntities::MatrixType::Model;
    }
}

void VideoDisplay::mouseMoveEvent( QMouseEvent* event )
{
    QOpenGLWidget::mouseMoveEvent( event );

    if ( ! this->shouldHandleMouseEvent( event ) )
    {
        return;
    }

    if ( event->buttons().testFlag( Qt::RightButton ) )
    {
        this->scheduleWaterRippleAtViewportPt( event->pos() );
    }
    else
    {
        this->updateMouseMatrix( event );
    }
}

void VideoDisplay::mouseReleaseEvent( QMouseEvent* event )
{
    QOpenGLWidget::mouseReleaseEvent( event );

    this->updateMouseMatrix( event );
    m_modelMatrix = m_mouseCacheMatrix * m_modelMatrix;
    m_mouseCacheMatrix.setToIdentity();

    m_hasMouseDragStarted = false;
    m_alteredMatrixType = CommonEntities::MatrixType::Invalid;
    m_mousePressInitialPos = QPoint();
}

#ifndef QT_NO_WHEELEVENT
void VideoDisplay::wheelEvent( QWheelEvent* event )
{
    QOpenGLWidget::wheelEvent( event );
}
#endif

VideoDisplay::VboIndex VideoDisplay::vboIndex(
        const VideoDisplay::VaoIndex vaoIndex,
        const VideoDisplay::GenericVertexAttributeIndex genericVertexAttributeIndex )
{
    switch ( vaoIndex )
    {
        case VaoIndexScreen:
            switch ( genericVertexAttributeIndex )
            {
                case GenericVertexAttributeIndexVertexPos:
                    return VboIndexScreenVertexPos;
                case GenericVertexAttributeIndexVertexTexCoord:
                    return VboIndexScreenVertexTexCoord;
                default:
                    break;
            }

            break;
        default:
            break;
    }

    return VboIndexInvalid;
}

void VideoDisplay::clearGlData()
{
    if ( m_openGlFunctions == 0 )
    {
        return;
    }

    m_shaderProgram.clear();

    const int vaosCount = m_vaos.size();
    if ( vaosCount > 0 )
    {
        m_openGlFunctions->glDeleteVertexArrays( vaosCount, m_vaos.constData() );
        LOG_OPENGL_ERROR();

        m_vaos.clear();
    }

    const int vbosCount = m_vbos.size();
    if ( vbosCount > 0 )
    {
        m_openGlFunctions->glDeleteBuffers( vbosCount, m_vbos.constData() );
        LOG_OPENGL_ERROR();

        m_vbos.clear();
    }

    const int pbosCount = m_pbos.size();
    if ( pbosCount > 0 )
    {
        m_openGlFunctions->glDeleteBuffers( pbosCount, m_pbos.constData() );
        LOG_OPENGL_ERROR();

        m_pbos.clear();
    }

    m_uniformVariableLocations.clear();

    m_textureUnitGlSlIds.clear();

    const int textureIdsCount = m_textureIds.size();
    if ( textureIdsCount > 0 )
    {
        m_openGlFunctions->glDeleteTextures( textureIdsCount, m_textureIds.data() );
        LOG_OPENGL_ERROR();
        m_textureIds.clear();
    }
}

void VideoDisplay::enableGenericVertexAttributeArrays( const bool enable )
{
    foreach ( const GenericVertexAttributeIndex genericVertexAttributeIndex, s_allOrderedGenericVertexAttributeIndices )
    {
        if ( enable )
        {
            m_openGlFunctions->glEnableVertexAttribArray( genericVertexAttributeIndex );
            LOG_OPENGL_ERROR();
        }
        else
        {
            m_openGlFunctions->glDisableVertexAttribArray( genericVertexAttributeIndex );
            LOG_OPENGL_ERROR();
        }
    }
}

QByteArray VideoDisplay::genericVertexAttributeName( const VideoDisplay::GenericVertexAttributeIndex index )
{
    switch ( index )
    {
        case GenericVertexAttributeIndexVertexPos:
            return "vVertexPos";
        case GenericVertexAttributeIndexVertexTexCoord:
            return "vVertexTexCoord";
        default:
            break;
    }

    return QByteArray();
}

QByteArray VideoDisplay::fragmentOutputVariableName( const VideoDisplay::FragmentOutputVariableIndex index )
{
    switch ( index )
    {
        case FragmentOutputVariableIndexColor:
            return "fColor";
        default:
            break;
    }

    return QByteArray();
}

QByteArray VideoDisplay::uniformVariableName( const VideoDisplay::UniformVariableIndex index )
{
    switch ( index )
    {
        case UniformVariableIndexModelMatrix:
            return "modelMatrix";
        case UniformVariableIndexViewMatrix:
            return "viewMatrix";
        case UniformVariableIndexProjectionMatrix:
            return "projectionMatrix";
    }

    return QByteArray();
}

void VideoDisplay::setTexture(
        const CommonEntities::TextureIndex textureIndex,
        const GLubyte* const pixelColorComponentValues,
        const int pixelsPerRowCount,
        const int pixelsPerColumnCount,
        const GLint destinationFormat,
        const GLenum sourceFormat,
        const GLenum sourcePixelComponentsSize,
        const GlUtilities::TextureFilter minificationFilter,
        const GlUtilities::TextureFilter magnificationFilter,
        const GlUtilities::TextureWrapMode horizontalWrapMode,
        const GlUtilities::TextureWrapMode verticalWrapMode,
        const bool applyAnisotropicFiltering,
        const bool updateUi )
{
    if ( ! CommonEntities::isTextureIndexValid( textureIndex )
         || textureIndex == CommonEntities::TextureIndexNone
         // || pixelColorComponentValues == nullptr
         || pixelsPerRowCount <= 0
         || pixelsPerColumnCount <= 0
         || ! GlUtilities::isTextureFilterValid( minificationFilter )
         || ! GlUtilities::isTextureFilterValid( magnificationFilter )
         || ! GlUtilities::isTextureWrapModeValid( horizontalWrapMode )
         || ! GlUtilities::isTextureWrapModeValid( verticalWrapMode ) )
    {
        return;
    }

    this->bindTextureForTextureIndex( textureIndex );

    GlUtilities::setTexture(
                m_openGlFunctions,
                pixelColorComponentValues,
                pixelsPerRowCount,
                pixelsPerColumnCount,
                destinationFormat,
                sourceFormat,
                sourcePixelComponentsSize,
                minificationFilter,
                magnificationFilter,
                horizontalWrapMode,
                verticalWrapMode,
                applyAnisotropicFiltering );

    this->bindTextureForTextureIndex( CommonEntities::TextureIndexNone );

    if ( updateUi )
    {
        this->update();
    }
}

void VideoDisplay::applyMatrix(
        const CommonEntities::MatrixType matrixType,
        const QMatrix4x4& matrix,
        const bool updateUi )
{
    if ( m_openGlFunctions == nullptr )
    {
        return;
    }

    if ( ! m_shaderProgram.isValid() )
    {
        return;
    }

    // TODO: later on, change to view matrix, when view matrix handling is implemented
    if ( matrixType == CommonEntities::MatrixType::Model )
    {
        m_coordinateSystemIndicator->setMatrix( matrix );
    }

    m_waterSurface->setMatrix( matrixType, matrix );
    m_waterPool->setMatrix( matrixType, matrix );
    m_shaderProgram.activate();

    const GLint matrixUniformVarLocation = m_uniformVariableLocations[ this->matrixUniformVarIndexFromType( matrixType ) ];
    if ( ! CommonEntities::isSignedGlValueValid( matrixUniformVarLocation ) )
    {
        return;
    }

    m_openGlFunctions->glUniformMatrix4fv( matrixUniformVarLocation, 1, GL_FALSE, matrix.constData() );
    LOG_OPENGL_ERROR();

    m_shaderProgram.deactivate();

    if ( updateUi )
    {
        this->update();
    }
}

int VideoDisplay::nextPboIndex() const
{
    return this->isPboIndexValid( m_currentPboIndex )
           ? ( ( m_currentPboIndex + 1 ) % 2 )
           : 0;
}

void VideoDisplay::setVideoFrameSize( const QSize& aVideoFrameSize )
{
    if ( aVideoFrameSize == m_videoFrameSize )
    {
        return;
    }

    m_videoFrameSize = aVideoFrameSize;
    this->resizeGL( m_videoFrameSize.width(), m_videoFrameSize.height() );
}

int VideoDisplay::pixelBytesCount( const QVideoFrame::PixelFormat pixelFormat )
{
    switch ( pixelFormat )
    {
        case QVideoFrame::Format_ARGB32:
        case QVideoFrame::Format_ARGB32_Premultiplied:
        case QVideoFrame::Format_RGB32:
        case QVideoFrame::Format_BGRA32:
        case QVideoFrame::Format_BGR32:
        case QVideoFrame::Format_BGRA32_Premultiplied:
            return 4;

        case QVideoFrame::Format_RGB24:
        case QVideoFrame::Format_ARGB8565_Premultiplied:
        case QVideoFrame::Format_BGR24:
        case QVideoFrame::Format_BGRA5658_Premultiplied:
            return 3;

        case QVideoFrame::Format_RGB565:
        case QVideoFrame::Format_RGB555:
        case QVideoFrame::Format_BGR565:
        case QVideoFrame::Format_BGR555:
            return 2;
    }

    return 0;
}

void VideoDisplay::displayVideoFrameUsingNormalApproach( const QVideoFrame& videoFrame )
{
    if ( m_openGlFunctions == nullptr )
    {
        return;
    }

    QVideoFrame videoFrameExplicitCopy = videoFrame;
    if ( ! videoFrameExplicitCopy.map( QAbstractVideoBuffer::ReadOnly ) )
    {
        return;
    }

    const QSize videoFrameSize = videoFrame.size();
    this->setVideoFrameSize( videoFrameSize );
    this->setTexture( CommonEntities::TextureIndexVideoFrameColor,
                      videoFrame.bits(),
                      videoFrameSize.width(),
                      videoFrameSize.height(),
                      GL_RGB5_A1,
                      GL_BGRA,
                      GL_UNSIGNED_SHORT_1_5_5_5_REV,
                      GlUtilities::TextureFilter::Linear,
                      GlUtilities::TextureFilter::Linear,
                      GlUtilities::TextureWrapMode::Clamp,
                      GlUtilities::TextureWrapMode::Clamp,
                      true,
                      true );
    videoFrameExplicitCopy.unmap();
}

void VideoDisplay::displayVideoFrameUsingSinglePbo( const QVideoFrame& videoFrame )
{
    if ( m_openGlFunctions == nullptr )
    {
        return;
    }

    if ( m_pbos.isEmpty() )
    {
        return;
    }

    const GLuint pboId = m_pbos[ 0 ];
    if ( ! CommonEntities::isUnsignedGlValueValid( pboId ) )
    {
        return;
    }

    m_openGlFunctions->glBindBuffer( GL_PIXEL_UNPACK_BUFFER, pboId );
    LOG_OPENGL_ERROR();

    QVideoFrame videoFrameExplicitCopy = videoFrame;
    if ( videoFrameExplicitCopy.map( QAbstractVideoBuffer::ReadOnly ) )
    {
        m_openGlFunctions->glBufferData( GL_PIXEL_UNPACK_BUFFER,
                                         videoFrame.mappedBytes(),
                                         videoFrame.bits(),
                                         GL_STREAM_DRAW );
        LOG_OPENGL_ERROR();

        videoFrameExplicitCopy.unmap();
    }

    const QSize videoFrameSize = videoFrame.size();
    this->setVideoFrameSize( videoFrameSize );
    this->setTexture( CommonEntities::TextureIndexVideoFrameColor,
                      nullptr,
                      videoFrameSize.width(),
                      videoFrameSize.height(),
                      GL_RGB5_A1,
                      GL_BGRA,
                      GL_UNSIGNED_SHORT_1_5_5_5_REV,
                      GlUtilities::TextureFilter::Linear,
                      GlUtilities::TextureFilter::Linear,
                      GlUtilities::TextureWrapMode::Clamp,
                      GlUtilities::TextureWrapMode::Clamp,
                      true,
                      true );

    m_openGlFunctions->glBindBuffer( GL_PIXEL_UNPACK_BUFFER, CommonEntities::s_invalidUnsignedGlValue );
    LOG_OPENGL_ERROR();
}

void VideoDisplay::displayVideoFrameUsingSinglePbo2( const QVideoFrame& videoFrame )
{
    if ( m_openGlFunctions == nullptr )
    {
        return;
    }

    if ( m_pbos.isEmpty() )
    {
        return;
    }

    const GLuint pboId = m_pbos[ 0 ];
    if ( ! CommonEntities::isUnsignedGlValueValid( pboId ) )
    {
        return;
    }

    m_openGlFunctions->glBindBuffer( GL_PIXEL_UNPACK_BUFFER, pboId );
    LOG_OPENGL_ERROR();

    QVideoFrame videoFrameExplicitCopy = videoFrame;
    if ( videoFrameExplicitCopy.map( QAbstractVideoBuffer::ReadOnly ) )
    {
        const int mappedBytesCount = videoFrame.mappedBytes() * sizeof( uchar );
        m_openGlFunctions->glBufferData( GL_PIXEL_UNPACK_BUFFER,
                                         mappedBytesCount,
                                         nullptr,
                                         GL_STREAM_DRAW );
        LOG_OPENGL_ERROR();

        GLubyte* const bufferData = reinterpret_cast< GLubyte* > ( m_openGlFunctions->glMapBuffer( GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY ) );
        LOG_OPENGL_ERROR();
        if ( bufferData != nullptr )
        {
            memcpy( bufferData, videoFrame.bits(), mappedBytesCount );

            m_openGlFunctions->glUnmapBuffer( GL_PIXEL_UNPACK_BUFFER );
            LOG_OPENGL_ERROR();
        }

        videoFrameExplicitCopy.unmap();
    }

    const QSize videoFrameSize = videoFrame.size();
    this->setVideoFrameSize( videoFrameSize );
    this->setTexture( CommonEntities::TextureIndexVideoFrameColor,
                      nullptr,
                      videoFrameSize.width(),
                      videoFrameSize.height(),
                      GL_RGB5_A1,
                      GL_BGRA,
                      GL_UNSIGNED_SHORT_1_5_5_5_REV,
                      GlUtilities::TextureFilter::Linear,
                      GlUtilities::TextureFilter::Linear,
                      GlUtilities::TextureWrapMode::Clamp,
                      GlUtilities::TextureWrapMode::Clamp,
                      true,
                      true );

    m_openGlFunctions->glBindBuffer( GL_PIXEL_UNPACK_BUFFER, CommonEntities::s_invalidUnsignedGlValue );
    LOG_OPENGL_ERROR();
}

void VideoDisplay::displayVideoFrameUsingMultiplePbo( const QVideoFrame& videoFrame )
{
    if ( m_openGlFunctions == nullptr )
    {
        return;
    }

    if ( m_pbos.isEmpty() )
    {
        return;
    }

    const QSize videoFrameSize = videoFrame.size();
    const bool isCurrentPboIndexValid = this->isPboIndexValid( m_currentPboIndex );
    if ( isCurrentPboIndexValid )
    {
        // 1 -> display the content in the last filled PBO
        const GLuint pboId = m_pbos[ m_currentPboIndex ];
        if ( ! CommonEntities::isUnsignedGlValueValid( pboId ) )
        {
            return;
        }

        m_openGlFunctions->glBindBuffer( GL_PIXEL_UNPACK_BUFFER, pboId );
        LOG_OPENGL_ERROR();

        this->setVideoFrameSize( videoFrameSize );
        this->setTexture( CommonEntities::TextureIndexVideoFrameColor,
                          nullptr,
                          videoFrameSize.width(),
                          videoFrameSize.height(),
                          GL_RGB5_A1,
                          GL_BGRA,
                          GL_UNSIGNED_SHORT_1_5_5_5_REV,
                          GlUtilities::TextureFilter::Linear,
                          GlUtilities::TextureFilter::Linear,
                          GlUtilities::TextureWrapMode::Clamp,
                          GlUtilities::TextureWrapMode::Clamp,
                          true,
                          true );
    }

    // 2 -> load the content for the other PBO
    m_currentPboIndex = this->nextPboIndex();
    const GLuint nextPboId = m_pbos[ m_currentPboIndex ];
    m_openGlFunctions->glBindBuffer( GL_PIXEL_UNPACK_BUFFER, nextPboId );
    LOG_OPENGL_ERROR();

    QVideoFrame videoFrameExplicitCopy = videoFrame;
    if ( videoFrameExplicitCopy.map( QAbstractVideoBuffer::ReadOnly ) )
    {
        const int mappedBytesCount = videoFrame.mappedBytes() * sizeof( uchar );
        m_openGlFunctions->glBufferData( GL_PIXEL_UNPACK_BUFFER,
                                         mappedBytesCount,
                                         nullptr,
                                         GL_STREAM_DRAW );
        LOG_OPENGL_ERROR();

        GLubyte* const bufferData = reinterpret_cast< GLubyte* > ( m_openGlFunctions->glMapBuffer( GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY ) );
        LOG_OPENGL_ERROR();
        if ( bufferData != nullptr )
        {
            memcpy( bufferData, videoFrame.bits(), mappedBytesCount );

            m_openGlFunctions->glUnmapBuffer( GL_PIXEL_UNPACK_BUFFER );
            LOG_OPENGL_ERROR();
        }

        videoFrameExplicitCopy.unmap();
    }

    m_openGlFunctions->glBindBuffer( GL_PIXEL_UNPACK_BUFFER, CommonEntities::s_invalidUnsignedGlValue );
    LOG_OPENGL_ERROR();
}

QString VideoDisplay::modeDescription( const VideoFrameDisplayMode mode )
{
    switch ( mode )
    {
        case VideoFrameDisplayMode::Normal:
            return "normal";
        case VideoFrameDisplayMode::SinglePbo:
            return "single pbo";
        case VideoFrameDisplayMode::SinglePbo2:
            return "single pbo 2";
        case VideoFrameDisplayMode::MultiplePbo:
            return "multiple pbo";
    }

    return QString();
}

bool VideoDisplay::isPboIndexValid( const int pboIndex ) const
{
    return 0 <= pboIndex && pboIndex < m_pbos.size();
}

QRect VideoDisplay::viewport( const int w, const int h ) const
{
    const int sideSize = qMin( w, h );
    QRect targetRect( QPoint{}, QSize{ sideSize, sideSize } );
    targetRect.moveCenter( this->rect().center() );

    return targetRect;
}

QRect VideoDisplay::viewport() const
{
    return this->viewport( this->width(), this->height() );
}

QVector3D VideoDisplay::viewportToWaterMeshPt( const QPoint& viewportPt ) const
{
    const QRect vp = this->viewport();
    QPointF clickPos2 = viewportPt - vp.center();
    clickPos2.rx() /= vp.width() / 2.0;
    clickPos2.ry() /= - vp.height() / 2.0;

    QVector3D clickPos3{ clickPos2 };
    clickPos3.setZ( CommonEntities::s_drawingVolumeBoundsYMax * 2.0f );

    const QVector3D dir{ 0.0f, 0.0f, -1.0f };

    const QRectF waterRect = m_waterSurface->meshRect();

    const QMatrix4x4 mvpMatrix = m_projectionMatrix * m_viewMatrix * m_modelMatrix;

    QMatrix4x4 waterRectMvpMatrix;
    waterRectMvpMatrix.rotate( 180.0f, 1.0, 0.0 ); // OpenGL's Y direction is upwards, while Qt's one is downwards
    waterRectMvpMatrix = mvpMatrix * waterRectMvpMatrix;

    const QVector3D waterRectBottomLeft = waterRectMvpMatrix.map( QVector3D{ waterRect.bottomLeft() } );
    const QVector3D waterRectBottomRight = waterRectMvpMatrix.map( QVector3D{ waterRect.bottomRight() } );
    const QVector3D waterRectTopRight = waterRectMvpMatrix.map( QVector3D{ waterRect.topRight() } );
    const QVector3D waterRectTopLeft = waterRectMvpMatrix.map( QVector3D{ waterRect.topLeft() } );

    const Triangle3 t1{ waterRectBottomLeft,
                        waterRectBottomRight,
                        waterRectTopRight };
    const Triangle3 t2{ waterRectTopRight,
                        waterRectTopLeft,
                        waterRectBottomLeft };

    for ( const Triangle3& t : { t1, t2 } )
    {
        QVector3D intersectionPtBarycentric;
        if ( ! t.intersectsWithLine( clickPos3, dir, & intersectionPtBarycentric ) )
        {
            continue;
        }

        const QVector3D intersectionPt = t.fromBarycentricPt( intersectionPtBarycentric.y(), intersectionPtBarycentric.z() );
        const QVector3D intersectionPtFinal = mvpMatrix.inverted().map( intersectionPt );

        return intersectionPtFinal;
    }

    return QVector3D();
}

void VideoDisplay::scheduleWaterRippleAtViewportPt( const QPoint& viewportPt )
{
    const QVector3D meshPt = this->viewportToWaterMeshPt( viewportPt );
    m_waterSurface->rippleWaterAt( meshPt );
}

bool VideoDisplay::shouldHandleMouseEvent( QMouseEvent* const mouseEvent ) const
{
    const Qt::MouseButtons pressedMouseButtons = mouseEvent->buttons();
    const bool isRippleMode = pressedMouseButtons.testFlag( Qt::RightButton );
    if ( ! pressedMouseButtons.testFlag( Qt::LeftButton )
         && ! isRippleMode )
    {
        return false;
    }

    const QPoint mousePos = mouseEvent->pos();
    const QRect vp = this->viewport();
    if ( isRippleMode && ! vp.contains( mousePos ) )
    {
        return false;
    }

    return true;
}

void VideoDisplay::applyMeshSize()
{
    m_waterSurface->setMeshSize( m_waterSettings.meshSize() );
}

void VideoDisplay::applyShouldDrawAsWireframe()
{
    m_waterSurface->setShouldDrawAsWireframe( m_waterSettings.shouldDrawAsWireframe() );
}

void VideoDisplay::applyWavePropagationMethod()
{

}

void VideoDisplay::applyWaterRippleMagnitude()
{
    m_waterSurface->setWaterRippleMagnitude( m_waterSettings.waterRippleMagnitude() );
}

void VideoDisplay::applyWaterRippleAbsRadius()
{
    m_waterSurface->setWaterRippleAbsRadius( m_waterSettings.waterRippleAbsRadius() );
}

void VideoDisplay::applyWavePropagationSpeed()
{
    m_waterSurface->setWavePropagationSpeed( m_waterSettings.wavePropagationSpeed() );
}

void VideoDisplay::applyWaveHeightDampingOverTimeFactor()
{
    m_waterSurface->setWaveHeightDampingOverTimeFactor( m_waterSettings.waveHeightDampingOverTimeFactor() );
}

void VideoDisplay::applyWaveHeightClampingFactor()
{
    m_waterSurface->setWaveHeightClampingFactor( m_waterSettings.waveHeightClampingFactor() );
}

void VideoDisplay::applyShouldPropagateWaves()
{
    if ( m_waterSettings.shouldPropagateWaves() )
    {
        m_waterPropagationTimer->start();
    }
    else
    {
        m_waterPropagationTimer->stop();
    }
}

void VideoDisplay::applyShouldSimulateRain()
{
    if ( m_waterSettings.shouldSimulateRain() )
    {
        m_randomRippleTimer->start();
    }
    else
    {
        m_randomRippleTimer->stop();
    }
}

void VideoDisplay::applyRainDropsFallTimeInterval()
{
    m_randomRippleTimer->setInterval( m_waterSettings.rainDropsFallTimeInterval() );
}

void VideoDisplay::applyRainDropsCountPerFall()
{

}

void VideoDisplay::setFps( const qreal aFps )
{
    if ( ( aFps < 0.0 && ! qFuzzyIsNull( aFps ) )
         || qFuzzyCompare( aFps, m_fps ) )
    {
        return;
    }

    m_fps = aFps;
    emit fpsChanged( aFps );
}

void VideoDisplay::updateMouseMatrix( QMouseEvent* const mouseEvent )
{
    if ( mouseEvent == nullptr
         || ! m_hasMouseDragStarted )
    {
        return;
    }

    const Qt::MouseButtons pressedMouseButtons = mouseEvent->buttons();
    if ( ! pressedMouseButtons.testFlag( Qt::LeftButton )
         && ! pressedMouseButtons.testFlag( Qt::RightButton ) )
    {
        return;
    }

    const QPoint mouseMoveVector = mouseEvent->pos() - m_mousePressInitialPos;

    const float fullRotationAngle = 360.0f;
    const float yAngle = MathUtilities::to360DegreesAngle( ( float( mouseMoveVector.x() ) / this->width() ) * fullRotationAngle );
    const float xAngle = MathUtilities::to360DegreesAngle( ( float( mouseMoveVector.y() ) / this->height() ) * fullRotationAngle );

    m_mouseCacheMatrix.setToIdentity();
    m_mouseCacheMatrix.rotate( xAngle, 1, 0 );
    m_mouseCacheMatrix.rotate( yAngle, 0, 1 );

    this->applyAlteredMatrix();
}

void VideoDisplay::applyAlteredMatrix()
{
    this->applyMatrix( m_alteredMatrixType, this->completeAlteredMatrix(), true );
}

QMatrix4x4 VideoDisplay::completeMatrix( const CommonEntities::MatrixType matrixType ) const
{
    return m_mouseCacheMatrix * this->matrix( matrixType );
}

QMatrix4x4 VideoDisplay::completeAlteredMatrix() const
{
    return this->isAlteringAnyMatrix()
           ? this->completeMatrix( m_alteredMatrixType )
           : QMatrix4x4();
}

bool VideoDisplay::isAlteringAnyMatrix() const
{
    return m_alteredMatrixType != CommonEntities::MatrixType::Invalid;
}

QMatrix4x4 VideoDisplay::matrix( const CommonEntities::MatrixType matrixType ) const
{
    switch ( matrixType )
    {
        case CommonEntities::MatrixType::Model:
            return m_modelMatrix;
        case CommonEntities::MatrixType::View:
            return m_viewMatrix;
        case CommonEntities::MatrixType::Projection:
            return m_projectionMatrix;
        default:
            break;
    }

    return QMatrix4x4();
}

VideoDisplay::UniformVariableIndex VideoDisplay::matrixUniformVarIndexFromType( const CommonEntities::MatrixType matrixType )
{
    switch ( matrixType )
    {
        case CommonEntities::MatrixType::Model:
            return UniformVariableIndexModelMatrix;
        case CommonEntities::MatrixType::View:
            return UniformVariableIndexViewMatrix;
        case CommonEntities::MatrixType::Projection:
            return UniformVariableIndexProjectionMatrix;
        default:
            break;
    }

    return UniformVariableIndexInvalid;
}

void VideoDisplay::changeTexture()
{
    enum ColorComponentIndex
    {
        ColorComponentIndexAlpha = 0,
        ColorComponentIndexRed   = 1,
        ColorComponentIndexGreen = 2,
        ColorComponentIndexBlue  = 3,
        ColorComponentsCount     = 4
    };

    static int currentColorComponentIndex = 1;

    const qreal ratio = 16.0 / 9.0;
    const int hPixelsCount = 100;
    const int vPixelsCount = qRound( hPixelsCount / ratio );
    const int colorComponentsCountPerPixel = 4;
    const int bytesCount = hPixelsCount * vPixelsCount * colorComponentsCountPerPixel;
    QScopedArrayPointer< GLubyte > pixelColorComponents( new GLubyte[ bytesCount ] );
    // const int offset = qrand() % 256;
    for ( int i = 0; i < bytesCount; ++i )
    {
        GLubyte colorComponentIntensity = 0U;
        const int colorComponentIndex = i % 4;
        switch ( colorComponentIndex )
        {
            case 0: // RED
            case 1: // GREEN
            case 2: // BLUE
            {
                const int pixelIndex = i / 4;
                if ( ( (pixelIndex % 3) + 1 ) != colorComponentIndex )
                {
                    break;
                }
            }
            case 3: // ALPHA
                colorComponentIntensity = 255;
                break;
        }

//        currentColorComponentIndex = -1;
//        if ( colorComponentIndex != 0 )
//        {
//            if ( i < bytesCount / 3 )
//            {
//                currentColorComponentIndex = 1;
//            }
//            else if ( i < bytesCount * 2 / 3 )
//            {
//                currentColorComponentIndex = 2;
//            }
//            else
//            {
//                currentColorComponentIndex = 3;
//            }
//        }

//        if ( colorComponentIndex == currentColorComponentIndex )
//        {
//            colorComponentIntensity = 255;
//        }

        pixelColorComponents[ i ] = colorComponentIntensity;
    }

    QString colorDescription;
    switch ( currentColorComponentIndex )
    {
        case ColorComponentIndexRed:
            colorDescription = "red";
            break;
        case ColorComponentIndexGreen:
            colorDescription = "green";
            break;
        case ColorComponentIndexBlue:
            colorDescription = "blue";
            break;
    }

    qDebug() << "current color:" << colorDescription;

    const GLubyte* textureBytes = 0;
    int width = 0;
    int height = 0;
// #define USE_FILE_AS_IMAGE_SOURCE
#ifdef USE_FILE_AS_IMAGE_SOURCE
    QImage img( "C:/Users/PC/Desktop/dance_naturals_uomo_art116_comparison.png" );

    const QImage openGlImage = convertToGLFormat( img );
    textureBytes = openGlImage.constBits();
    width = openGlImage.width();
    height = openGlImage.height();
#else
    QImage img( pixelColorComponents.data(), hPixelsCount, vPixelsCount, QImage::Format_RGBA8888 );
    img.save( "C:/Users/PC/Desktop/a.png" );
    // QImage img( pixelColorComponents.data(), hPixelsCount, vPixelsCount, QImage::Format_RGBA8888 );
    textureBytes = const_cast< const GLubyte* >( pixelColorComponents.data() );
    width = hPixelsCount;
    height = vPixelsCount;
#endif

    this->setTexture( CommonEntities::TextureIndexVideoFrameColor,
                      textureBytes,
                      width,
                      height,
                      GL_RGBA,
                      GL_RGBA );

    currentColorComponentIndex = qBound( static_cast< int >( ColorComponentIndexRed ),
                                         ( currentColorComponentIndex + 1 ) % ColorComponentsCount,
                                         static_cast< int >( ColorComponentIndexBlue ) );

    this->update();
}

void VideoDisplay::calcFps()
{
    this->setFps( m_framesCount * qreal( 1000 ) / m_fpsTimer->interval() );
    m_framesCount = 0;
}

void VideoDisplay::scheduleWaterRippleAtRandomPoint()
{
    m_randomWaterRippleScheduled = true;
    this->update();
}

QList< CommonEntities::TextureIndex > VideoDisplay::vaoTextureIndices( const VideoDisplay::VaoIndex vaoIndex )
{
    QList< CommonEntities::TextureIndex > textureIndices;
    switch ( vaoIndex )
    {
        case VaoIndexScreen:
            textureIndices << CommonEntities::TextureIndexVideoFrameColor;
            break;
        default:
            break;
    }

    return textureIndices;
}

void VideoDisplay::bindTexturesForVaoIndex( const VideoDisplay::VaoIndex vaoIndex ) const
{
    const QList< CommonEntities::TextureIndex > textureIndices = VideoDisplay::vaoTextureIndices( vaoIndex );
    const int textureIndicesCount = textureIndices.size();
    for ( int i = 0; i < textureIndicesCount; ++ i )
    {
        this->bindTextureForTextureIndex( textureIndices[ i ] );
    }
}

bool VideoDisplay::bindTextureForTextureIndex( const CommonEntities::TextureIndex textureIndex ) const
{
    return this->bindTextureForTextureUnit( CommonEntities::textureUnitIndexFromTextureIndex( textureIndex ),
                                            this->textureId( textureIndex ) );
}

bool VideoDisplay::bindTextureForTextureUnit(
        const CommonEntities::TextureUnitIndex textureUnitIndex,
        const GLuint textureId ) const
{
    if ( ! CommonEntities::isTextureUnitIndexValid( textureUnitIndex )
         || ! this->isTextureIdValid( textureId ) )
    {
        return false;
    }

    m_openGlFunctions->glActiveTexture( CommonEntities::textureUnitOpenGlDescriptorFromTextureUnitIndex( textureUnitIndex ) );
    LOG_OPENGL_ERROR();

    m_openGlFunctions->glBindTexture( GL_TEXTURE_2D, textureId );
    LOG_OPENGL_ERROR();

    return true;
}

GLuint VideoDisplay::textureId( const CommonEntities::TextureIndex textureIndex ) const
{
    if ( ! CommonEntities::isTextureIndexValid( textureIndex ) )
    {
        return CommonEntities::s_invalidUnsignedGlValue;
    }

    return textureIndex == CommonEntities::TextureIndexNone
           || textureIndex >= m_textureIds.size()
           || textureIndex < 0
           ? CommonEntities::s_invalidUnsignedGlValue
           : m_textureIds[ textureIndex ];
}

bool VideoDisplay::isTextureIdValid( const GLuint textureId ) const
{
    return textureId == CommonEntities::s_invalidUnsignedGlValue
           || m_textureIds.contains( textureId );
}
