#ifndef WATERRIPPLESIMULATOR_WATERRIPPLESIMULATOR_NAMESPACE_H
#define WATERRIPPLESIMULATOR_WATERRIPPLESIMULATOR_NAMESPACE_H

#include <gl/GL.h>
#include <QString>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>

namespace WaterRippleSimulator {

class CommonEntities
{
public:
    enum TextureIndex
    {
        TextureIndexInvalid         = -2,
        TextureIndexNone            = -1,
        TextureIndexVideoFrameColor = 0,
        TexturesCount               = 1
    };

    enum TextureUnitIndex
    {
        TextureUnitIndexInvalid         = -2,
        TextureUnitIndexNone            = -1,
        TextureUnitIndexVideoFrameColor = 0,
        TextureUnitsCount               = 1
    };

    enum class MatrixType
    {
        Invalid,
        Model,
        Normal,
        View,
        Projection
    };

public:
    static const GLuint s_invalidUnsignedGlValue;
    static const GLint  s_invalidSignedGlValue;
    static const int    s_axesCount2D;
    static const int    s_axesCount3D;
    static const int    s_axesCount4D;

    static const float  s_drawingVolumeBoundsXMin;
    static const float  s_drawingVolumeBoundsXMax;
    static const float  s_drawingVolumeBoundsYMin;
    static const float  s_drawingVolumeBoundsYMax;
    static const float  s_drawingVolumeBoundsZMin;
    static const float  s_drawingVolumeBoundsZMax;

public:
    static QString vecToString( const QVector2D& v );
    static QString vecToString( const QVector3D& v );
    static QString vecToString( const QVector4D& v );

    static bool isTextureIndexValid( const TextureIndex textureIndex );
    static bool isTextureUnitIndexValid( const TextureUnitIndex textureUnitIndex );
    static TextureUnitIndex textureUnitIndexFromTextureIndex( const TextureIndex textureIndex );
    static GLenum textureUnitOpenGlDescriptorFromTextureUnitIndex( const TextureUnitIndex textureUnitIndex );
    static QString textureUnitGlSlNameFromTextureUnitIndex( const TextureUnitIndex textureUnitIndex );
    static QList< TextureIndex > textureIndicesFromTextureUnitIndex( const TextureUnitIndex textureUnitIndex );

    static GLenum invalidEnumGlValue();

    static bool isUnsignedGlValueValid( const GLuint unsignedGlValue );
    static bool isSignedGlValueValid( const GLint signedGlValue );
    static bool isEnumGlValueValid( const GLenum enumGlValue );

private:
    // do not define
    CommonEntities();
};

}

#endif // WATERRIPPLESIMULATOR_WATERRIPPLESIMULATOR_NAMESPACE_H
