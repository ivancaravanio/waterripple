#ifndef VIDEOEFFECTS_VIDEODISPLAY_H
#define VIDEOEFFECTS_VIDEODISPLAY_H

#include "Helpers/ShaderProgram.h"
#include "Data/ShaderInfo.h"
#include "Data/VaoData.h"
#include "Data/WaterSettings.h"
#include "Utilities/GlUtilities.h"
#include "VideoEffects_namespace.h"

#include <QByteArray>
#include <QColor>
#include <QList>
#include <QMatrix4x4>
#include <QOpenGLWidget>
#include <QScopedPointer>
#include <QSize>
#include <QVector>
#include <QVideoFrame>

QT_FORWARD_DECLARE_CLASS( QMouseEvent )
QT_FORWARD_DECLARE_CLASS( QTimer )

namespace VideoEffects {
namespace Helpers {

class CoordinateSystemIndicator;
class WaterPool;
class WaterSurfaceGpu;

}

using namespace VideoEffects::Data;
using namespace VideoEffects::Helpers;
using namespace VideoEffects::Utilities;

class VideoDisplay : public QOpenGLWidget
{
    Q_OBJECT

public:
    enum class VideoFrameDisplayMode : quint8
    {
        Normal,
        SinglePbo,
        SinglePbo2,
        MultiplePbo
    };

public:
    explicit VideoDisplay( QWidget* parent = 0, Qt::WindowFlags f = 0 );
    ~VideoDisplay() Q_DECL_OVERRIDE;

    void setAspectRatioMode( const Qt::AspectRatioMode aAspectRatioMode );
    Qt::AspectRatioMode aspectRatioMode() const;
    static bool isAspectRatioModeValid( const Qt::AspectRatioMode aAspectRatioMode );

    void setVideoFrameDisplayMode( const VideoFrameDisplayMode aVideoFrameDisplayMode );
    VideoFrameDisplayMode videoFrameDisplayMode() const;

    void setSceneBackgroundColor( const QColor& aSceneBackgroundColor );
    QColor sceneBackgroundColor() const;

    void setWaterSettings( const WaterSettings& aWaterSettings );
    WaterSettings waterSettings() const;

    qreal fps() const;

signals:
    void fpsChanged( const qreal fps );
    void shouldPropagateWaterChanged( const bool shouldPropagateWater );

public slots:
    void clear();
    void displayVideoFrame( const QVideoFrame& videoFrame );

protected:
    void initializeGL() Q_DECL_OVERRIDE;
    void paintGL() Q_DECL_OVERRIDE;
    void resizeGL( int w, int h ) Q_DECL_OVERRIDE;

    bool isGLInitialized() const;

    void keyPressEvent( QKeyEvent* event ) Q_DECL_OVERRIDE;

    void mousePressEvent( QMouseEvent* event ) Q_DECL_OVERRIDE;
    void mouseMoveEvent( QMouseEvent* event ) Q_DECL_OVERRIDE;
    void mouseReleaseEvent( QMouseEvent* event ) Q_DECL_OVERRIDE;
#ifndef QT_NO_WHEELEVENT
    void wheelEvent( QWheelEvent* event ) Q_DECL_OVERRIDE;
#endif

private:
    enum GenericVertexAttributeIndex
    {
        GenericVertexAttributeIndexInvalid        = -1,
        GenericVertexAttributeIndexVertexPos      = 0,
        GenericVertexAttributeIndexVertexTexCoord = 1,
        GenericVertexAttributesCount              = 2
    };

    enum FragmentOutputVariableIndex
    {
        FragmentOutputVariableIndexInvalid = -1,
        FragmentOutputVariableIndexColor   = 0,
        FragmentOutputVariablesCount       = 1
    };

    enum UniformVariableIndex
    {
        UniformVariableIndexInvalid          = -1,
        UniformVariableIndexModelMatrix      = 0,
        UniformVariableIndexViewMatrix       = 1,
        UniformVariableIndexProjectionMatrix = 2,
        UniformVariablesCount                = 3
    };

    enum VboIndex
    {
        VboIndexInvalid              = -1,
        VboIndexScreenVertexPos      = 0,
        VboIndexScreenVertexTexCoord = 1,
        VbosCount                    = 2
    };

    enum VaoIndex
    {
        VaoIndexInvalid = -1,
        VaoIndexScreen  = 0,
        VaosCount       = 1
    };

    enum PboIndex
    {
        PboIndexInvalid          = -1,
        PboIndexVideoFrameFirst  = 0,
        PboIndexVideoFrameSecond = 1,
        PbosCount                = 2
    };

private:
    static const QList< GenericVertexAttributeIndex > s_allOrderedGenericVertexAttributeIndices;
    static const QColor s_defaultClearColor;
    static const int s_randomRipplePeriodStep;

private:
    static VboIndex vboIndex( const VaoIndex vaoIndex, const GenericVertexAttributeIndex genericVertexAttributeIndex );
    static QList< CommonEntities::TextureIndex > vaoTextureIndices( const VaoIndex vaoIndex );
    void bindTexturesForVaoIndex( const VaoIndex vaoIndex ) const;
    bool bindTextureForTextureIndex( const CommonEntities::TextureIndex textureIndex ) const;
    bool bindTextureForTextureUnit( const CommonEntities::TextureUnitIndex textureUnitIndex, const GLuint textureId ) const;
    GLuint textureId( const CommonEntities::TextureIndex textureIndex ) const;
    bool isTextureIdValid( const GLuint textureId ) const;

    void clearGlData();

    void enableGenericVertexAttributeArrays( const bool enable );

    static QByteArray genericVertexAttributeName( const GenericVertexAttributeIndex index );
    static QByteArray fragmentOutputVariableName( const FragmentOutputVariableIndex index );
    static QByteArray uniformVariableName( const UniformVariableIndex index );

    void setTexture( const CommonEntities::TextureIndex textureIndex,
                     const GLubyte* const               pixelColorComponentValues,
                     const int                          pixelsPerRowCount,
                     const int                          pixelsPerColumnCount,
                     const GLint                        aDestinationFormat         = GlUtilities::s_textureDefaultDestinationFormat,
                     const GLenum                       aSourceFormat              = GlUtilities::s_textureDefaultSourceFormat,
                     const GLenum                       aSourcePixelComponentsSize = GlUtilities::s_textureDefaultSourcePixelComponentsSize,
                     const GlUtilities::TextureFilter   aMinificationFilter        = GlUtilities::s_textureDefaultMinificationFilter,
                     const GlUtilities::TextureFilter   aMagnificationFilter       = GlUtilities::s_textureDefaultMagnificationFilter,
                     const GlUtilities::TextureWrapMode aHorizontalWrapMode        = GlUtilities::s_textureDefaultHorizontalWrapMode,
                     const GlUtilities::TextureWrapMode aVerticalWrapMode          = GlUtilities::s_textureDefaultVerticalWrapMode,
                     const bool                         aApplyAnisotropicFiltering = GlUtilities::s_textureDefaultApplyAnisotropicFiltering,
                     const bool                         updateUi = false );

    void applyMatrix(
            const CommonEntities::MatrixType matrixType,
            const QMatrix4x4& matrix,
            const bool updateUi );

    void updateMouseMatrix( QMouseEvent* const mouseEvent );
    void applyAlteredMatrix();
    QMatrix4x4 completeMatrix( const CommonEntities::MatrixType matrixType ) const;
    QMatrix4x4 completeAlteredMatrix() const;
    bool isAlteringAnyMatrix() const;

    QMatrix4x4 matrix( const CommonEntities::MatrixType matrixType ) const;
    static UniformVariableIndex matrixUniformVarIndexFromType( const CommonEntities::MatrixType matrixType );

    int nextPboIndex() const;
    void setVideoFrameSize( const QSize& aVideoFrameSize );

    static int pixelBytesCount( const QVideoFrame::PixelFormat pixelFormat );
    void displayVideoFrameUsingNormalApproach( const QVideoFrame& videoFrame );
    void displayVideoFrameUsingSinglePbo( const QVideoFrame& videoFrame );
    void displayVideoFrameUsingSinglePbo2( const QVideoFrame& videoFrame );
    void displayVideoFrameUsingMultiplePbo( const QVideoFrame& videoFrame );
    static QString modeDescription( const VideoFrameDisplayMode mode );
    bool isPboIndexValid( const int pboIndex ) const;
    QRect viewport( const int w, const int h ) const;
    QRect viewport() const;
    QVector3D viewportToWaterMeshPt( const QPoint& viewportPt ) const;
    void scheduleWaterRippleAtViewportPt( const QPoint& viewportPt );
    bool shouldHandleMouseEvent( QMouseEvent* const mouseEvent ) const;

    void applyMeshSize();
    void applyShouldDrawAsWireframe();
    void applyWavePropagationMethod();
    void applyWaterRippleMagnitude();
    void applyWaterRippleAbsRadius();
    void applyWavePropagationSpeed();
    void applyWaveHeightDampingOverTimeFactor();
    void applyWaveHeightClampingFactor();
    void applyShouldPropagateWaves();
    void applyShouldSimulateRain();
    void applyRainDropsFallTimeInterval();
    void applyRainDropsCountPerFall();

    void setFps( const qreal aFps );

private slots:
    void changeTexture();
    void calcFps();
    void scheduleWaterRippleAtRandomPoint();

private:
    Qt::AspectRatioMode               m_aspectRatioMode;

    QOpenGLFunctions_3_3_Core*        m_openGlFunctions;
    ShaderProgram                     m_shaderProgram;
    QVector< GLuint >                 m_vaos;
    QVector< GLuint >                 m_vbos;
    QVector< GLuint >                 m_pbos;
    int                               m_currentPboIndex;
    QVector< GLint >                  m_uniformVariableLocations;

    QVector< GLuint >                 m_textureUnitGlSlIds;
    QVector< GLuint >                 m_textureIds;

    QList< VaoData >                  m_vaosData;

    QTimer*                           m_textureChangeTimer;

    QSize                             m_videoFrameSize;

    bool                              m_isPlayingVideo;
    VideoFrameDisplayMode             m_videoFrameDisplayMode;

    QMatrix4x4                        m_modelMatrix;
    QMatrix4x4                        m_viewMatrix;
    QMatrix4x4                        m_projectionMatrix;
    CommonEntities::MatrixType        m_alteredMatrixType;

    QMatrix4x4                        m_mouseCacheMatrix;

    bool                              m_hasMouseDragStarted;
    QPoint                            m_mousePressInitialPos;

    QScopedPointer< CoordinateSystemIndicator > m_coordinateSystemIndicator;
    QScopedPointer< WaterSurfaceGpu > m_waterSurface;
    QScopedPointer< WaterPool >       m_waterPool;
    QTimer*                           m_randomRippleTimer;
    QTimer*                           m_waterPropagationTimer;
    bool                              m_randomWaterRippleScheduled;
    QColor                            m_sceneBackgroundColor;
    WaterSettings                     m_waterSettings;

    int                               m_framesCount;
    QTimer*                           m_fpsTimer;
    qreal                             m_fps;
};

}

#endif // VIDEOEFFECTS_VIDEODISPLAY_H
