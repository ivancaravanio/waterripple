#ifndef WATERRIPPLESIMULATOR_MEDIAPLAYERCONTROL_H
#define WATERRIPPLESIMULATOR_MEDIAPLAYERCONTROL_H

// #define PERFORM_TESTS

// #define USE_PREVIEW_PROGRESS_OPACITY_ANIMATION

#include <QAbstractVideoSurface>
#include <QGraphicsVideoItem>
#include <QMediaPlayer>
#include <QPointer>
#include <QStringList>
#include <QStyle>
#include <QUrl>
#include <QVideoWidget>
#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QCheckBox)
QT_FORWARD_DECLARE_CLASS(QDoubleSpinBox)
QT_FORWARD_DECLARE_CLASS(QLabel)
QT_FORWARD_DECLARE_CLASS(QLineEdit)
QT_FORWARD_DECLARE_CLASS(QMediaPlayer)
QT_FORWARD_DECLARE_CLASS(QMimeData)
QT_FORWARD_DECLARE_CLASS(QPushButton)
QT_FORWARD_DECLARE_CLASS(QSlider)
QT_FORWARD_DECLARE_CLASS(QSpinBox)
QT_FORWARD_DECLARE_CLASS(QSplitter)
QT_FORWARD_DECLARE_CLASS(QTimer)

#ifdef USE_PREVIEW_PROGRESS_OPACITY_ANIMATION
    QT_FORWARD_DECLARE_CLASS(QPropertyAnimation)
#endif

namespace WaterRippleSimulator {

class MediaPlayerControl : public QWidget
{
    Q_OBJECT

public:
    static const int s_volumeLevelMin;
    static const int s_volumeLevelInitial;
    static const int s_volumeLevelMax;

    static const qreal s_playbackRateStep;
    static const qreal s_playbackRateMin;
    static const qreal s_playbackRateMax;

public:
    enum ProgressMeasurementMode
    {
        ProgressMeasurementModeElapsed,
        ProgressMeasurementModeRemaining
    };

public:
    explicit MediaPlayerControl( QWidget* parent = nullptr );
    ~MediaPlayerControl() Q_DECL_OVERRIDE;

    QUrl mediaUrl() const;
    bool isMediaValid() const;

    QMediaPlayer::State playerState() const;
    bool isPlaying() const;
    bool isPaused() const;
    bool isStopped() const;
    int isMuted() const;
    int volumeLevel() const;
    qreal playbackRate() const;
    ProgressMeasurementMode progressMeasurementMode() const;
    static bool isProgressMeasurementModeValid( const ProgressMeasurementMode progressMeasurementMode );

    void setVideoOutput( QVideoWidget* const aVideoOutput );
    void setVideoOutput( QGraphicsVideoItem* const aVideoOutput );
    void setVideoOutput( QAbstractVideoSurface* const aVideoOutput );

    virtual bool eventFilter( QObject* obj, QEvent* event );

    void retranslate();

signals:
    void playerStateChanged( const QMediaPlayer::State playerState );
    void startedPlaying();
    void paused();
    void stopped();

public slots:
    void setMediaUrl( const QUrl& aMediaUrl );
    void setMediaFilePath( const QString& aMediaFilePath );
    void clearMediaFilePath();
    void setPlayerState( const QMediaPlayer::State aPlayerState );
    void play();
    void pause();
    void stop();
    void setIsMuted( const bool aIsMuted );
    void setVolumeLevel( const int aVolumeLevel );
    void setPlaybackRate( const qreal& aPlaybackRate );
    void setProgressMeasurementMode( const ProgressMeasurementMode aProgressMeasurementMode );
    void switchProgressMeasurementMode();

protected:
    virtual void showEvent( QShowEvent* event );
    virtual void changeEvent( QEvent* event );

#ifndef QT_NO_DRAGANDDROP
    virtual void dragEnterEvent( QDragEnterEvent* event );
    virtual void dropEvent( QDropEvent* event );
#endif

private slots:
    void openMediaFileInteractively();

    void onMediaStatusChanged();
    void updateMediaDisplay();

    void onPlayerStateDataChanged( const QMediaPlayer::State aPlayerState );
    void updatePlayerStateData();
    void updatePlayerStateDisplay();

    void updateProgressData();
    void updateProgressDisplay();
    void stepBackward();
    void stepForward();

    void updateIsMutedData();
    void updateIsMutedDisplay();

    void updateVolumeLevelData();
    void updateVolumeLevelDisplay();

    void updatePlaybackRateData();
    void updatePlaybackRateDisplay();

    void updateDurationDisplay();
    void updateProgressAndDurationDisplay();

    void resetProgressEditorStepsCount();

    void updatePreviewProgressDisplay();
    void tryHidePreviewProgressDisplay();

    void onActionTriggered();

    void resizePartialPlaybackControlsOptimally();

#ifdef PERFORM_TESTS
    void setRandomAppFont();
#endif

private:
    void resetMediaPlayer();
    QPushButton* playerButtonFromPlayerState( const QMediaPlayer::State playerState ) const;
    QMediaPlayer::State playerStateFromPlayerButton( QPushButton* const playerButton ) const;
    static QString msecsToString( const qint64& msecs );
    void updateVolumeLevelDisplaySize();
    static int volumeLevelMaxDigitsCount();
    static QString volumeLevelDisplayFormat();

    void updateMediaUrlDisplay();
    void updateControlsEnablence();
    QString uiFriendlyMediaFilePath() const;
    QRect progressEditorSubControlRect( const QStyle::SubControl sliderSubControl ) const;
    int progressEditorStepsCountHint() const;
    qint64 mediaPlayerPositionFromProgress( const int progressEditorValue ) const;
    void setPreviewProgressDisplayVisibility( const bool shouldBeVisible );
    static QUrl urlFromMimeData( const QMimeData* const mimeData );

private:
    static const QList< QMediaPlayer::State > s_playerStates;
    static const QStringList s_allowedFileExtensions;

private:
    QLineEdit*                        m_mediaUrlDisplay;
    QPushButton*                      m_openMediaFileButton;
    QPushButton*                      m_closeMediaFileButton;
    QPushButton*                      m_playButton;
    QPushButton*                      m_pauseButton;
    QPushButton*                      m_stopButton;
    QSlider*                          m_progressEditor;
    ProgressMeasurementMode           m_progressMeasurementMode;
    QPushButton*                      m_progressDisplay;
    QLabel*                           m_progressDurationDelimiter;
    QLabel*                           m_durationDisplay;

    QPushButton*                      m_stepBackwardButton;
    QSpinBox*                         m_jumpIntervalEditor;
    QPushButton*                      m_stepForwardButton;
    QWidget*                          m_stepControlsContainer;

    QDoubleSpinBox*                   m_playbackRateEditor;

    QPushButton*                      m_muteEditor;
    QSlider*                          m_volumeLevelEditor;
    QLabel*                           m_volumeLevelDisplay;
    QWidget*                          m_volumeControlsContainer;

    QLabel*                           m_previewProgressDisplay;
    QSplitter*                        m_partialPlaybackControlsSplitter;
#ifdef USE_PREVIEW_PROGRESS_OPACITY_ANIMATION
    QPropertyAnimation*               m_previewProgressDisplayOpacityAnimation;
#endif

    QMediaPlayer*                     m_mediaPlayer;
    QPointer< QVideoWidget >          m_videoOutputVideoWidget;
    QPointer< QGraphicsVideoItem >    m_videoOutputGraphicsVideoItem;
    QPointer< QAbstractVideoSurface > m_videoOutputAbstractVideoSurface;

    QTimer*                           m_mediaStatusUpdateTimer;
    QTimer*                           m_progressDataUpdateTimer;
    QTimer*                           m_progressEditorStepsCountResetTimer;
    QTimer*                           m_previewProgressUpdateTimer;
    bool                              m_isProgressEditorPressed;
    bool                              m_isProgressEditorHovered;

#ifdef PERFORM_TESTS
    QTimer*                           m_pseudoUpdateTimer;
#endif
};

}

#endif // WATERRIPPLESIMULATOR_MEDIAPLAYERCONTROL_H
