#ifndef WATERRIPPLESIMULATOR_WATERSETTINGSEDITOR_H
#define WATERRIPPLESIMULATOR_WATERSETTINGSEDITOR_H

#include "Data/WaterSettings.h"
#include "WaterRippleSimulator_namespace.h"

#include <QString>
#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QGroupBox)
QT_FORWARD_DECLARE_CLASS(QCheckBox)
QT_FORWARD_DECLARE_CLASS(QComboBox)
QT_FORWARD_DECLARE_CLASS(QDoubleSpinBox)
QT_FORWARD_DECLARE_CLASS(QLabel)
QT_FORWARD_DECLARE_CLASS(QPushButton)
QT_FORWARD_DECLARE_CLASS(QSpinBox)

namespace WaterRippleSimulator {

using namespace WaterRippleSimulator::Data;

class WaterSettingsEditor : public QWidget
{
    Q_OBJECT

public:
    explicit WaterSettingsEditor( QWidget* parent = nullptr );
    ~WaterSettingsEditor() Q_DECL_OVERRIDE;

    void setMeshSize( const QSize& aMeshSize );
    QSize meshSize() const;

    void setShouldDrawAsWireframe( const bool aShouldDrawAsWireframe );
    bool shouldDrawAsWireframe() const;

    // wave propagation
    bool shouldPropagateWaves() const;

    void setWavePropagationMethod( const WaterSettings::WavePropagationMethod aWavePropagationMethod );
    WaterSettings::WavePropagationMethod wavePropagationMethod() const;

    void setWavePropagationSpeed( const float aWavePropagationSpeed );
    float wavePropagationSpeed() const;

    void setWaveHeightDampingOverTimeFactor( const float aWaveHeightDampingOverTimeFactor );
    float waveHeightDampingOverTimeFactor() const;

    void setWaveHeightClampingFactor( const float aWaveHeightClampingFactor );
    float waveHeightClampingFactor() const;

    // water ripple
    void setWaterRippleMethod( const WaterSettings::WaterRippleMethod aWaterRippleMethod );
    WaterSettings::WaterRippleMethod waterRippleMethod() const;

    void setWaterRippleMagnitude( const float aWaterRippleMagnitude );
    float waterRippleMagnitude() const;

    void setWaterRippleRadiusFactor( const QVector2D& aWaterRippleRadiusFactor );
    QVector2D waterRippleRadiusFactor() const;

    void setWaterRippleRotationAngle( const float aWaterRippleRotationAngle );
    float waterRippleRotationAngle() const;

    // rain
    void setShouldSimulateRain( const bool aShouldSimulateRain );
    bool shouldSimulateRain() const;

    void setRainDropsFallTimeInterval( const int aRainDropsFallTimeInterval );
    int rainDropsTimeFallInterval() const;

    void setRainDropsCountPerFall( const int aRainDropsCountPerFall );
    int rainDropsCountPerFall() const;

    void setShouldShowCoordinateSystemIndicator( const bool aShouldShowCoordinateSystemIndicator );
    bool shouldShowCoordinateSystemIndicator() const;

    qreal fps() const;

    void setWaterSettings( const WaterSettings& aWaterSettings );
    WaterSettings waterSettings() const;
    WaterSettings editedWaterSettings() const;

    void retranslate();

public slots:
    void setShouldPropagateWaves( const bool aShouldPropagateWaves );
    void setFps( const qreal& aFps );

signals:
    void dataChanged( const WaterRippleSimulator::Data::WaterSettings::WaterProperties changedWaterProperties );
    void calmWaterRequested();
    void resetOrientationRequested();
    void focusOpenGlSceneRequested();

private slots:
    void applyEditedData();
    void setDefaultSettingsInteractively();

private:
    void updateSettingsUi( const WaterSettings::WaterProperties waterProperties = WaterSettings::WaterPropertiesAll );
    void updateFpsDisplay();

    static QString wavePropagationMethodDescription( const int wavePropagationMethod );
    void repopulateWavePropagationMethodOptions();

    static QString waterRippleMethodDescription( const int waterRippleMethod );
    void repopulateWaterRippleMethodOptions();

    // settings
    // mesh
    void setMeshSize( const QSize& aMeshSize, const bool shouldUpdateData );
    void setShouldDrawAsWireframe( const bool aShouldDrawAsWireframe, const bool shouldUpdateData );

    // wave propagation
    void setShouldPropagateWaves( const bool aShouldPropagateWaves, const bool shouldUpdateData );
    void setWavePropagationMethod( const WaterSettings::WavePropagationMethod aWavePropagationMethod, const bool shouldUpdateData );
    void setWavePropagationSpeed( const float aWavePropagationSpeed, const bool shouldUpdateData );
    void setWaveHeightDampingOverTimeFactor( const float aWaveHeightDampingOverTimeFactor, const bool shouldUpdateData );
    void setWaveHeightClampingFactor( const float aWaveHeightClampingFactor, const bool shouldUpdateData );

    // water ripple
    void setWaterRippleMethod( const WaterSettings::WaterRippleMethod aWaterRippleMethod, const bool shouldUpdateData );
    void setWaterRippleMagnitude( const float aWaterRippleMagnitude, const bool shouldUpdateData );
    void setWaterRippleRadiusFactor( const QVector2D& aWaterRippleRadiusFactor, const bool shouldUpdateData );
    void setWaterRippleRotationAngle( const float aWaterRippleRotationAngle, const bool shouldUpdateData );

    // rain
    void setShouldSimulateRain( const bool aShouldSimulateRain, const bool shouldUpdateData );
    void setRainDropsFallTimeInterval( const int aRainDropsFallTimeInterval, const bool shouldUpdateData );
    void setRainDropsCountPerFall( const int aRainDropsCountPerFall, const bool shouldUpdateData );

    void setShouldShowCoordinateSystemIndicator( const bool aShouldShowCoordinateSystemIndicator, const bool shouldUpdateData );

    static QSpinBox* intNumberEditor( const int min,
                                      const int max );
    static QSpinBox* meshDimensionEditor();
    static QDoubleSpinBox* realNumberEditor( const float min = 0.0f,
                                             const float max = 1.0f,
                                             const float step = 1E-3f,
                                             const int decimalsCount = 3 );

    static QDoubleSpinBox* waterRippleRadiusEditor();

private:
    QGroupBox*      m_settingsGroupBox;
    QLabel*         m_meshSizeLabel;
    QSpinBox*       m_meshWidthEditor;
    QLabel*         m_xLabel;
    QSpinBox*       m_meshHeightEditor;

    QLabel*         m_drawAsWireframeLabel;
    QCheckBox*      m_drawAsWireframeEditor;

    QGroupBox*      m_waveSettingsGroupBox;

    QLabel*         m_shouldPropagateWavesLabel;
    QCheckBox*      m_shouldPropagateWavesEditor;

    QLabel*         m_wavePropagationMethodLabel;
    QComboBox*      m_wavePropagationMethodPicker;

    QLabel*         m_wavePropagationSpeedLabel;
    QDoubleSpinBox* m_wavePropagationSpeedEditor;

    // velocity scaling (see comments at the bottom)
    QLabel*         m_waveHeightDampingOverTimeFactorLabel;
    QDoubleSpinBox* m_waveHeightDampingOverTimeFactorEditor;

    // clamping (see comments at the bottom)
    QLabel*         m_waveHeightClampingFactorLabel;
    QDoubleSpinBox* m_waveHeightClampingFactorEditor;

    QGroupBox*      m_waterRippleSettingsGroupBox;

    QLabel*         m_waterRippleMethodLabel;
    QComboBox*      m_waterRippleMethodPicker;

    QLabel*         m_waterRippleMagnitudeLabel;
    QDoubleSpinBox* m_waterRippleMagnitudeEditor;

    QLabel*         m_waterRippleXRadiusFactorLabel;
    QDoubleSpinBox* m_waterRippleXRadiusFactorEditor;
    QLabel*         m_waterRippleYRadiusFactorLabel;
    QDoubleSpinBox* m_waterRippleYRadiusFactorEditor;

    QLabel*         m_waterRippleRotationAngleLabel;
    QDoubleSpinBox* m_waterRippleRotationAngleEditor;

    QGroupBox*      m_rainSettingsGroupBox;

    QLabel*         m_shouldSimulateRainLabel;
    QCheckBox*      m_shouldSimulateRainEditor;

    QLabel*         m_rainDropsFallTimeIntervalLabel;
    QSpinBox*       m_rainDropsFallTimeIntervalEditor;

    QLabel*         m_rainDropsCountPerFallLabel;
    QSpinBox*       m_rainDropsCountPerFallEditor;

    QLabel*         m_shouldShowCoordSysIndicatorLabel;
    QCheckBox*      m_shouldShowCoordSysIndicatorEditor;

    QPushButton*    m_applyChangesButton;
    QPushButton*    m_setDefaultSettingsButton;

    QLabel*         m_fpsLabel;
    QLabel*         m_fpsDisplay;
    qreal           m_fps;
    QPushButton*    m_calmWaterButton;
    QPushButton*    m_resetOrientationButton;
    QPushButton*    m_focusOpenGlSceneButton;

    /*
     * Damping force (physically "correct")
     * f = -k⋅v
     * But how to choose k? No stability guarantees
     *
     * Scaling (unphysical, more direct control)
     * v = s⋅v // s < 1, smaller time step -> stronger effect
     *
     * Clamping (unphysical, direct control)
     * offset = (   u[i+1,j]
     *            + u[i-1,j]
     *            + u[i,j+1]
     *            + u[i,j-1] ) / 4 – u[i,j]
     * maxOffset = maxSlope * h // independence of resolution h:
     * if (offset > maxOffset) u[i,j] += offset – maxOffset
     * if (offset < -maxOffset) u[i,j] += offset + maxOffset
     *
     */

    WaterSettings m_waterSettings;
};

}

#endif // WATERRIPPLESIMULATOR_WATERSETTINGSEDITOR_H
