#ifndef WATERRIPPLESIMULATOR_MAINCONTROLLER_H
#define WATERRIPPLESIMULATOR_MAINCONTROLLER_H

#include <QObject>
#include <QScopedPointer>
#include <Qt>
#include <QtGlobal>

namespace WaterRippleSimulator {

class MainWindow;

class MainController : public QObject
{
    Q_OBJECT

public:
    explicit MainController( QObject* parent = nullptr );
    ~MainController() Q_DECL_OVERRIDE;

    void initialize();
    void setMediaFilePath( const QString& aMediaFilePath );

private:
    static void loadStyleSheet();

private:
    QScopedPointer< MainWindow > m_mainWindow;
};

}

#endif // WATERRIPPLESIMULATOR_MAINCONTROLLER_H
