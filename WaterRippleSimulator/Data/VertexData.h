#ifndef WATERRIPPLESIMULATOR_DATA_VERTEXDATA_H
#define WATERRIPPLESIMULATOR_DATA_VERTEXDATA_H

#include <QVector2D>
#include <QVector3D>

#include <qopengl.h>

namespace WaterRippleSimulator {
namespace Data {

struct VertexData
{
public:
    VertexData();
    explicit VertexData( const QVector2D& aPos );
    ~VertexData();

    inline float& height( const bool shouldProvideFirst )
    {
        return shouldProvideFirst
                ? height0
                : height1;
    }

public:
    QVector2D pos;
    float     velocity;
    float     height0;
    float     height1;
    QVector3D normal;
};

}
}

#endif // WATERRIPPLESIMULATOR_DATA_VERTEXDATA_H
