#ifndef WATERRIPPLESIMULATOR_DATA_WATERSETTINGS_H
#define WATERRIPPLESIMULATOR_DATA_WATERSETTINGS_H

#include "../WaterRippleSimulator_namespace.h"

#include <QFlags>
#include <QSize>

namespace WaterRippleSimulator {
namespace Data {

using namespace WaterRippleSimulator;

class WaterSettings
{
public:
    enum WaterProperty
    {
        // mesh
        WaterPropertyNone                                = 0x0,
        WaterPropertyMeshSize                            = 0x1 << 0,
        WaterPropertyShouldDrawAsWireframe               = 0x1 << 1,

        // water propagation
        WaterPropertyShouldPropagateWaves                = 0x1 << 2,
        WaterPropertyWavePropagationMethod               = 0x1 << 3,
        WaterPropertyWavePropagationSpeed                = 0x1 << 4,
        WaterPropertyWaveHeightDampingOverTimeFactor     = 0x1 << 5,
        WaterPropertyWaveHeightClampingFactor            = 0x1 << 7,

        // water ripple
        WaterPropertyWaterRippleMethod                   = 0x1 << 8,
        WaterPropertyWaterRippleMagnitude                = 0x1 << 9,
        WaterPropertyWaterRippleRadiusFactor             = 0x1 << 10,
        WaterPropertyWaterRippleRotationAngle            = 0x1 << 11,

        WaterPropertyShouldSimulateRain                  = 0x1 << 12,
        WaterPropertyRainDropsFallTimeInterval           = 0x1 << 13,
        WaterPropertyRainDropsCountPerFall               = 0x1 << 14,

        WaterPropertyShouldShowCoordinateSystemIndicator = 0x1 << 15,

        WaterPropertiesAll                               = WaterPropertyMeshSize
                                                           | WaterPropertyShouldDrawAsWireframe

                                                           // water propagation
                                                           | WaterPropertyShouldPropagateWaves
                                                           | WaterPropertyWavePropagationMethod
                                                           | WaterPropertyWavePropagationSpeed
                                                           | WaterPropertyWaveHeightDampingOverTimeFactor
                                                           | WaterPropertyWaveHeightClampingFactor

                                                           // water ripple
                                                           | WaterPropertyWaterRippleMethod
                                                           | WaterPropertyWaterRippleMagnitude
                                                           | WaterPropertyWaterRippleRadiusFactor
                                                           | WaterPropertyWaterRippleRotationAngle

                                                           | WaterPropertyShouldSimulateRain
                                                           | WaterPropertyRainDropsFallTimeInterval
                                                           | WaterPropertyRainDropsCountPerFall
                                                           | WaterPropertyShouldShowCoordinateSystemIndicator
    };

    Q_DECLARE_FLAGS( WaterProperties, WaterProperty )

    enum WavePropagationMethod
    {
        WavePropagationMethodInvalid                       = -1,
        WavePropagationMethodCpu                           = 0,
        WavePropagationMethodGpu                           = 1,
        WavePropagationMethodGpuPrecomputedNormalsVertices = 2,
        WavePropagationMethodsCount                        = 3
    };

    enum WaterRippleMethod
    {
        WaterRippleMethodInvalid               = -1,
        WaterRippleMethodCircularTrigonometric = 0,
        WaterRippleMethodEllipticTrigonometric = 1,
        WaterRippleMethodEllipticGaussian      = 2,
        WaterRippleMethodsCount                = 3
    };

public:
    // mesh
    static const int s_meshDimensionMin;
    static const int s_meshDimensionDefault;
    static const int s_meshDimensionMax;

    static const bool s_shouldDrawAsWireframeDefault;

    // wave propagation
    static const bool s_shouldPropagateWavesDefault;
    static const WavePropagationMethod s_wavePropagationMethodDefault;

    static const float s_wavePropagationSpeedMin;
    static const float s_wavePropagationSpeedDefault;
    static const float s_wavePropagationSpeedMax;

    static const float s_waveHeightDampingOverTimeFactorMin;
    static const float s_waveHeightDampingOverTimeFactorDefault;
    static const float s_waveHeightDampingOverTimeFactorMax;

    static const float s_waveHeightClampingFactorMin;
    static const float s_waveHeightClampingFactorDefault;
    static const float s_waveHeightClampingFactorMax;

    // water ripple
    static const WaterRippleMethod s_waterRippleMethodDefault;

    static const float s_waterRippleMagnitudeMin;
    static const float s_waterRippleMagnitudeDefault;
    static const float s_waterRippleMagnitudeMax;

    static const float s_waterRippleRadiusFactorMin;
    static const float s_waterRippleRadiusFactorDefault;
    static const float s_waterRippleRadiusFactorMax;

    static const QVector2D s_waterRipple2DRadiusFactorDefault;

    static const float s_waterRippleRotationAngleMin;
    static const float s_waterRippleRotationAngleDefault;
    static const float s_waterRippleRotationAngleMax;

    // rain
    static const bool s_shouldSimulateRainDefault;

    static const int s_rainDropsFallTimeIntervalMin;
    static const int s_rainDropsFallTimeIntervalDefault;
    static const int s_rainDropsFallTimeIntervalMax;

    static const int s_rainDropsCountPerFallMin;
    static const int s_rainDropsCountPerFallDefault;
    static const int s_rainDropsCountPerFallMax;

    static const bool s_shouldShowCoordinateSystemIndicatorDefault;

public:
    WaterSettings();
    WaterSettings(
            const QSize&     aMeshSize,
            const bool       aShouldDrawAsWireframe,

            // wave propagation
            const bool       aShouldPropagateWaves,
            const WavePropagationMethod aWavePropagationMethod,
            const float      aWavePropagationSpeed,
            const float      aWaveHeightDampingOverTimeFactor,
            const float      aWaveHeightClampingFactor,

            // water ripple
            const WaterRippleMethod aWaterRippleMethod,
            const float      aWaterRippleMagnitude,
            const QVector2D& aWaterRippleRadiusFactor,
            const float      aWaterRippleRotationAngle,

            // rain
            const bool       aShouldSimulateRain,
            const int        aRainDropsFallTimeInterval,
            const int        aRainDropsCountPerFall,

            const bool       aShouldShowCoordinateSystemIndicator );
    ~WaterSettings();


    bool operator==( const WaterSettings& other ) const;
    bool operator!=( const WaterSettings& other ) const;

    WaterProperties diff( const WaterSettings& other ) const;

    static bool isMeshDimensionValid( const int dimension );
    static bool isMeshSizeValid( const QSize& m_meshSize );

    static bool isWavePropagationMethodValid( const int wavePropagationMethod );
    static bool isWavePropagationSpeedValid( const float waterPropagationSpeed );
    static bool isWaveHeightDampingOverTimeFactorValid( const float factor );
    static bool isWaveHeightClampingFactorValid( const float factor );

    static bool isWaterRippleMethodValid( const int waterRippleMethod );
    static bool isWaterRippleMagnitudeValid( const float waterRippleMagnitude );
    static bool isWaterRippleRadiusFactorValid( const float waterRippleRadiusFactor );
    static bool isWaterRippleRadiusFactorValid( const QVector2D& waterRippleRadiusFactor );
    static bool isWaterRippleRotationAngleValid( const float waterRippleRotationAngle );

    static bool isRainDropsFallTimeIntervalValid( const int rainDropsTimeFallInterval );
    static bool isRainDropsCountPerFallValid( const int rainDropsCountPerFall );

    void setMeshSize( const QSize& aMeshSize );
    QSize meshSize() const;

    void setShouldDrawAsWireframe( const bool aShouldDrawAsWireframe );
    bool shouldDrawAsWireframe() const;

    // wave propagation
    void setShouldPropagateWaves( const bool aShouldPropagateWaves );
    bool shouldPropagateWaves() const;

    void setWavePropagationMethod( const WavePropagationMethod aWavePropagationMethod );
    WavePropagationMethod wavePropagationMethod() const;

    void setWavePropagationSpeed( const float aWavePropagationSpeed );
    float wavePropagationSpeed() const;

    void setWaveHeightDampingOverTimeFactor( const float aWaveHeightDampingOverTimeFactor );
    float waveHeightDampingOverTimeFactor() const;

    void setWaveHeightClampingFactor( const float aWaveHeightClampingFactor );
    float waveHeightClampingFactor() const;

    // water ripple
    void setWaterRippleMethod( const WaterRippleMethod aWaterRippleMethod );
    WaterRippleMethod waterRippleMethod() const;

    void setWaterRippleMagnitude( const float aWaterRippleMagnitude );
    float waterRippleMagnitude() const;

    void setWaterRippleRadiusFactor( const QVector2D& aWaterRippleRadiusFactor );
    const QVector2D& waterRippleRadiusFactor() const;

    void setWaterRippleRotationAngle( const float aWaterRippleRotationAngle );
    float waterRippleRotationAngle() const;

    // rain
    void setShouldSimulateRain( const bool aShouldSimulateRain );
    bool shouldSimulateRain() const;

    void setRainDropsFallTimeInterval( const int aRainDropsFallTimeInterval );
    int rainDropsFallTimeInterval() const;

    void setRainDropsCountPerFall( const int aRainDropsCountPerFall );
    int rainDropsCountPerFall() const;


    void setShouldShowCoordinateSystemIndicator( const bool aShouldShowCoordinateSystemIndicator );
    bool shouldShowCoordinateSystemIndicator() const;

public:
    QSize     m_meshSize;
    bool      m_shouldDrawAsWireframe;

    bool      m_shouldPropagateWaves;
    WavePropagationMethod m_wavePropagationMethod;
    float     m_wavePropagationSpeed;
    float     m_waveHeightDampingOverTimeFactor;
    float     m_waveHeightClampingFactor;

    WaterRippleMethod m_waterRippleMethod;
    float     m_waterRippleMagnitude;
    QVector2D m_waterRippleRadiusFactor;
    float     m_waterRippleRotationAngle;

    bool      m_shouldSimulateRain;
    int       m_rainDropsFallTimeInterval;
    int       m_rainDropsCountPerFall;
    bool      m_shouldShowCoordinateSystemIndicator;
};

}
}

#endif // WATERRIPPLESIMULATOR_DATA_WATERSETTINGS_H
