#ifndef WATERRIPPLESIMULATOR_DATA_SHADERINFO_H
#define WATERRIPPLESIMULATOR_DATA_SHADERINFO_H

#include <qopengl.h>

#include <QByteArray>
#include <QDebug>
#include <QMap>

namespace WaterRippleSimulator {
namespace Data {

class ShaderInfo
{
public:
    enum ShaderType
    {
        ShaderTypeInvalid,
        ShaderTypeVertex,
        ShaderTypeFragment,
        ShaderTypeGeometry,
        ShaderTypeTessellationEvaluation,
        ShaderTypeTessellationControl,
        ShaderTypeCompute
    };

public:
    ShaderInfo();
    ShaderInfo( const ShaderType aType, const QByteArray& aSourceCode );
    ~ShaderInfo();

    void setType( const ShaderType aType );
    ShaderType type() const;

    static bool isShaderTypeValid( const ShaderType shaderType );
    static GLuint openGlShaderTypeFromShaderType( const ShaderType shaderType );

    GLuint openGlShaderType() const;

public:
    QByteArray sourceCode;

    // https://www.opengl.org/wiki/GLSL_Object
    // =======================================
    // Pre-link setup
    // There are a number of operations that may need to be performed on programs before linking.
    //
    // Vertex Attributes for a Vertex Shader (if present in the program object) can be manually assigned an attribute index.
    // Obviously, if no vertex shader is in the program, you do not need to assign attributes manually.
    // Note that it is still best to assign them explicitly in the shader, where possible.

    // Fragment shader bindings specified in the shader source will be used if specified,
    // regardless of whether a location was specified using one of these functions.

    // use only if [generic vertex attribute]/[fragment output variables]'s locations
    // aren't explicitly specified in the [vertex]/[fragment] shader's source

    QMap< GLuint, QByteArray > variableLocationNames;

private:
    ShaderType m_type;
};

}
}

QDebug operator<<( QDebug d, const WaterRippleSimulator::Data::ShaderInfo& shaderInfo );

#endif // WATERRIPPLESIMULATOR_DATA_SHADERINFO_H
