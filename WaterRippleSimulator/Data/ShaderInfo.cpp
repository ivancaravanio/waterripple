#include "ShaderInfo.h"

#include "../WaterRippleSimulator_namespace.h"

using namespace WaterRippleSimulator::Data;

using namespace WaterRippleSimulator;

ShaderInfo::ShaderInfo()
    : m_type( ShaderTypeInvalid )
{
}

ShaderInfo::ShaderInfo(
        const ShaderInfo::ShaderType aType,
        const QByteArray& aSourceCode )
    : sourceCode( aSourceCode )
    , m_type( ShaderTypeInvalid )

{
    this->setType( aType );
}

ShaderInfo::~ShaderInfo()
{
}

void ShaderInfo::setType( const ShaderInfo::ShaderType aType )
{
    if ( ! ShaderInfo::isShaderTypeValid( aType ) )
    {
        return;
    }

    m_type = aType;
}

ShaderInfo::ShaderType ShaderInfo::type() const
{
    return m_type;
}

bool ShaderInfo::isShaderTypeValid( const ShaderInfo::ShaderType shaderType )
{
    return    shaderType == ShaderTypeVertex
           || shaderType == ShaderTypeFragment
           || shaderType == ShaderTypeGeometry
           || shaderType == ShaderTypeTessellationEvaluation
           || shaderType == ShaderTypeTessellationControl
           || shaderType == ShaderTypeCompute;
}

GLuint ShaderInfo::openGlShaderTypeFromShaderType( const ShaderType shaderType )
{
    switch ( shaderType )
    {
        case ShaderTypeVertex:
            return GL_VERTEX_SHADER;
        case ShaderTypeFragment:
            return GL_FRAGMENT_SHADER;
        case ShaderTypeGeometry:
            return GL_GEOMETRY_SHADER;
        case ShaderTypeTessellationEvaluation:
            return GL_TESS_EVALUATION_SHADER;
        case ShaderTypeTessellationControl:
            return GL_TESS_CONTROL_SHADER;
        case ShaderTypeCompute:
            return GL_COMPUTE_SHADER;
        default:
            break;
    }

    return CommonEntities::s_invalidUnsignedGlValue;
}

GLuint ShaderInfo::openGlShaderType() const
{
    return ShaderInfo::openGlShaderTypeFromShaderType( m_type );
}

QDebug operator<<( QDebug d, const ShaderInfo& shaderInfo )
{
    QString shaderTypeDescription;
    switch ( shaderInfo.type() )
    {
        case ShaderInfo::ShaderTypeVertex:
            shaderTypeDescription = "vertex";
            break;
        case ShaderInfo::ShaderTypeFragment:
            shaderTypeDescription = "fragment";
            break;
        case ShaderInfo::ShaderTypeGeometry:
            shaderTypeDescription = "geometry";
            break;
        case ShaderInfo::ShaderTypeTessellationEvaluation:
            shaderTypeDescription = "tessellation evaluation";
            break;
        case ShaderInfo::ShaderTypeTessellationControl:
            shaderTypeDescription = "tessellation control";
            break;
        case ShaderInfo::ShaderTypeCompute:
            shaderTypeDescription = "compute";
            break;
        default:
            break;
    }

    d << QT_STRINGIFY2( ShaderInfo ) << endl
      << '{' << endl
      << "\ttype:" << shaderTypeDescription << endl
      << "\tsource:" << endl
      << shaderInfo.sourceCode << endl
      << '}';

    return d;
}
