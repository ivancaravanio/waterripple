#ifndef WATERRIPPLESIMULATOR_DATA_TRIANGLE2_H
#define WATERRIPPLESIMULATOR_DATA_TRIANGLE2_H

#include <QList>
#include <QString>
#include <QVector2D>

namespace WaterRippleSimulator {
namespace Data {

class Triangle2
{
public:
    static const int s_verticesCount;

public:
    explicit Triangle2(
            const QVector2D& aV0 = QVector2D(),
            const QVector2D& aV1 = QVector2D(),
            const QVector2D& aV2 = QVector2D() );
    ~Triangle2();

    void setVertices( const QVector2D& aV0,
                      const QVector2D& aV1,
                      const QVector2D& aV2 );
    const QList< QVector2D >& vertices() const;

    void setVertexAtIndex( const int index, const QVector2D& v );
    QVector2D vertexAtIndex( const int index ) const;

    void setV0( const QVector2D& aV0 );
    QVector2D v0() const;

    void setV1( const QVector2D& aV1 );
    QVector2D v1() const;

    void setV2( const QVector2D& aV2 );
    QVector2D v2() const;

    QVector2D edge10() const;
    QVector2D edge20() const;
    QVector2D edge21() const;

    void reverse();

    int indexOfVertex( const QVector2D& v ) const;
    bool isVertex( const QVector2D& v ) const;
    bool contains( const QVector2D& v ) const;
    bool contains( const Triangle2& t ) const;

    float angleAtVertex( const QVector2D& v ) const;

    float area() const;

    bool operator==( const Triangle2& other ) const;
    bool operator!=( const Triangle2& other ) const;

    QString toString() const;

private:
    int cyclicVertexIndex( const int vertexIndex ) const;

    static float sign(
            const QVector2D& v0,
            const QVector2D& v1,
            const QVector2D& v2 );

private:
    QList< QVector2D > m_vertices;
};

}
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug qdebug, const WaterRippleSimulator::Data::Triangle2& t );
#endif

#endif // WATERRIPPLESIMULATOR_DATA_TRIANGLE2_H
