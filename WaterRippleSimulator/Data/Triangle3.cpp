#include "Triangle3.h"

#include "../Utilities/MathUtilities.h"
#include "../WaterRippleSimulator_namespace.h"

#include <QTextStream>
#include <QtGlobal>

using namespace WaterRippleSimulator::Data;

using namespace WaterRippleSimulator::Utilities;

const int Triangle3::s_verticesCount = 3;

Triangle3::Triangle3( const QVector3D& aV0,
                      const QVector3D& aV1,
                      const QVector3D& aV2 )
{
    m_vertices.reserve( s_verticesCount );
    m_vertices << aV0
               << aV1
               << aV2;
}

Triangle3::~Triangle3()
{
}

void Triangle3::setVertices(
        const QVector3D& aV0,
        const QVector3D& aV1,
        const QVector3D& aV2 )
{
    this->setV0( aV0 );
    this->setV1( aV1 );
    this->setV2( aV2 );
}

const QList< QVector3D >& Triangle3::vertices() const
{
    return m_vertices;
}

void Triangle3::setVertexAtIndex( const int index, const QVector3D& v )
{
    m_vertices[ Triangle3::cyclicVertexIndex( index ) ] = v;
}

QVector3D Triangle3::vertexAtIndex( const int index ) const
{
    return m_vertices[ Triangle3::cyclicVertexIndex( index ) ];
}

void Triangle3::setV0( const QVector3D& aV0 )
{
    this->setVertexAtIndex( 0, aV0 );
}

QVector3D Triangle3::v0() const
{
    return this->vertexAtIndex( 0 );
}

void Triangle3::setV1( const QVector3D& aV1 )
{
    this->setVertexAtIndex( 1, aV1 );
}

QVector3D Triangle3::v1() const
{
    return this->vertexAtIndex( 1 );
}

void Triangle3::setV2( const QVector3D& aV2 )
{
    this->setVertexAtIndex( 2, aV2 );
}

QVector3D Triangle3::v2() const
{
    return this->vertexAtIndex( 2 );
}

QVector3D Triangle3::edge10() const
{
    return this->v1() - this->v0();
}

QVector3D Triangle3::edge20() const
{
    return this->v2() - this->v0();
}

QVector3D Triangle3::edge21() const
{
    return this->v2() - this->v1();
}

void Triangle3::reverse()
{
    qSwap( m_vertices[ 0 ], m_vertices[ 2 ] );
}

QVector3D Triangle3::normal() const
{
    const QVector3D v0 = this->v0();
    return QVector3D::crossProduct( this->v1() - v0,
                                    this->v2() - v0 ).normalized();
}

int Triangle3::indexOfVertex( const QVector3D& v ) const
{
    return MathUtilities::vertexIndex( m_vertices, v );
}

bool Triangle3::isVertex( const QVector3D& v ) const
{
    return this->indexOfVertex( v ) >= 0;
}

bool Triangle3::contains( const QVector3D& v ) const
{
    const QVector3D v0 = this->v0();
    const QVector3D v1 = this->v1();
    const QVector3D v2 = this->v2();

    const bool b0 = Triangle3::sign( v, v0, v1 ) < 0.0f;
    const bool b1 = Triangle3::sign( v, v1, v2 ) < 0.0f;
    const bool b2 = Triangle3::sign( v, v2, v0 ) < 0.0f;

    return ( b0 == b1 ) && ( b1 == b2 );
}

bool Triangle3::contains( const Triangle3& t ) const
{
    const int otherTriangleVerticesCount = t.vertices().size();
    for ( int i = 0; i < otherTriangleVerticesCount; ++ i )
    {
        if ( ! this->contains( t.vertexAtIndex( i ) ) )
        {
            return false;
        }
    }

    return true;
}

float Triangle3::angleAtVertex( const QVector3D& v ) const
{
    const int vIndex = this->indexOfVertex( v );
    if ( vIndex < 0 )
    {
        return 0.0f;
    }

    return MathUtilities::angle( this->vertexAtIndex( vIndex - 1 ) - v,
                                 this->vertexAtIndex( vIndex + 1 ) - v );
}

float Triangle3::area() const
{
    const QVector3D v0 = this->v0();
    return QVector3D::crossProduct(
                this->v1() - v0,
                this->v2() - v0 ).length() / 2.0f;
}

bool Triangle3::operator==( const Triangle3& other ) const
{
    const int verticesCount = m_vertices.size();
    if ( verticesCount != other.vertices().size() )
    {
        return false;
    }

    for ( int i = 0; i < verticesCount; ++ i )
    {
        if ( ! MathUtilities::epsilonEqual(
                 this->vertexAtIndex( i ),
                 other.vertexAtIndex( i ) ) )
        {
            return false;
        }
    }

    return true;
}

bool Triangle3::operator!=( const Triangle3& other ) const
{
    return ! ( *this == other );
}

QString Triangle3::toString() const
{
    QString s;

    QTextStream ts( &s );

    ts << QT_STRINGIFY2( Triangle3 ) << '(' << endl
       << "\tv0: " << CommonEntities::vecToString( this->v0() ) << endl
       << "\tv1: " << CommonEntities::vecToString( this->v1() ) << endl
       << "\tv2: " << CommonEntities::vecToString( this->v2() ) << " )";

    return s;
}

bool Triangle3::intersectsWithLine(
        const QVector3D& origin,
        const QVector3D& dir,
        QVector3D* const intersectionPt ) const
{
    QVector3D crossPt;

    const float eps = std::numeric_limits< float >::epsilon();

    const QVector3D v0 = this->v0();
    const QVector3D v1 = this->v1();
    const QVector3D v2 = this->v2();

    const QVector3D edge1 = v1 - v0;
    const QVector3D edge2 = v2 - v0;

    const QVector3D pvec = QVector3D::crossProduct( dir, edge2 );

    const float det = QVector3D::dotProduct( edge1, pvec );

    if ( det > -eps && det < eps )
    {
        return false;
    }

    const float inverseDet = 1.0f / det;

    const QVector3D tvec = origin - v0;

    crossPt.setY( QVector3D::dotProduct( tvec, pvec ) * inverseDet );
    if ( crossPt.y() < 0.0f || crossPt.y() > 1.0f )
    {
        return false;
    }

    const QVector3D qvec = QVector3D::crossProduct( tvec, edge1 );

    crossPt.setZ( QVector3D::dotProduct( dir, qvec ) * inverseDet );
    if ( crossPt.z() < 0.0f || crossPt.y() + crossPt.z() > 1.0f )
    {
        return false;
    }

    crossPt.setX( QVector3D::dotProduct( edge2, qvec ) * inverseDet );

    if ( intersectionPt != nullptr )
    {
        *intersectionPt = crossPt;
    }

    return true;
}

bool Triangle3::intersectsWithRay(
        const QVector3D& origin,
        const QVector3D& dir,
        QVector3D* const barycentricIntersectionPt ) const
{
    QVector3D barycentricCrossPt;

    const QVector3D v0 = this->v0();
    const QVector3D v1 = this->v1();
    const QVector3D v2 = this->v2();

    const QVector3D e1 = v1 - v0;
    const QVector3D e2 = v2 - v0;

    const QVector3D p = QVector3D::crossProduct( dir, e2 );

    const float a = QVector3D::dotProduct( e1, p );

    const float eps = std::numeric_limits< float >::epsilon();
    if ( a < eps )
    {
        return false;
    }

    const float f = 1.0f / a;

    const QVector3D s = origin - v0;
    barycentricCrossPt.setX( f * QVector3D::dotProduct( s, p ) );
    if ( barycentricCrossPt.x() < 0.0f
         || barycentricCrossPt.x() > 1.0f )
    {
        return false;
    }

    const QVector3D q = QVector3D::crossProduct( s, e1 );
    barycentricCrossPt.setY( f * QVector3D::dotProduct( dir, q ) );
    if ( barycentricCrossPt.y() < 0.0f
         || barycentricCrossPt.y() + barycentricCrossPt.x() > 1.0f )
    {
        return false;
    }

    barycentricCrossPt.setZ( f * QVector3D::dotProduct( e2, q ) );

    if ( barycentricIntersectionPt != nullptr )
    {
        *barycentricIntersectionPt = barycentricCrossPt;
    }

    return barycentricCrossPt.z() >= 0.0f;
}

QVector3D Triangle3::fromBarycentricPt( const float u, const float v ) const
{
    const QVector3D v0 = this->v0();
    return u * ( this->v1() - v0 )
           + v * ( this->v2() - v0 )
           + v0;
}

int Triangle3::cyclicVertexIndex( const int vertexIndex ) const
{
    const int verticesCount = m_vertices.size();
    return vertexIndex >= 0
           ? vertexIndex % verticesCount
           : ( verticesCount - qAbs( vertexIndex ) % verticesCount );
}

float Triangle3::sign(
        const QVector3D& v0,
        const QVector3D& v1,
        const QVector3D& v2 )
{
    return   ( v0.x() - v2.x() ) * ( v1.y() - v2.y() )
           - ( v1.x() - v2.x() ) * ( v0.y() - v2.y() );
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug qdebug, const Triangle3& t )
{
    return qdebug << t.toString();
}
#endif
