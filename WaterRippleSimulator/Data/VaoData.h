#ifndef WATERRIPPLESIMULATOR_DATA_VAODATA_H
#define WATERRIPPLESIMULATOR_DATA_VAODATA_H

#include "Triangle2.h"
#include "Triangle3.h"

#include <QList>
#include <qopengl.h>

namespace WaterRippleSimulator {
namespace Data {

struct VaoData
{
public:
    VaoData();
    ~VaoData();

    void clear();

    QList< Triangle3 > geometry;
    QList< Triangle3 > color;
    QList< Triangle2 > textureCoordinates;
};

}
}

#endif // WATERRIPPLESIMULATOR_DATA_VAODATA_H
