#include "WaterSettings.h"

#include "../Utilities/MathUtilities.h"

#include <limits>

using namespace WaterRippleSimulator::Data;

using namespace WaterRippleSimulator::Utilities;

// mesh
const int WaterSettings::s_meshDimensionMin     = 1;
const int WaterSettings::s_meshDimensionDefault = 64;
const int WaterSettings::s_meshDimensionMax     = 2048;

const bool WaterSettings::s_shouldDrawAsWireframeDefault = false;

// wave propagation
const bool WaterSettings::s_shouldPropagateWavesDefault = true;

const WaterSettings::WavePropagationMethod WaterSettings::s_wavePropagationMethodDefault = WaterSettings::WavePropagationMethodCpu;
// const WaterSettings::WavePropagationMethod WaterSettings::s_wavePropagationMethodDefault = WaterSettings::WavePropagationMethodGpu;

const float WaterSettings::s_wavePropagationSpeedMin     = 0.0f;
const float WaterSettings::s_wavePropagationSpeedDefault = 10.0f;
const float WaterSettings::s_wavePropagationSpeedMax     = 1000.0f;

const float WaterSettings::s_waveHeightDampingOverTimeFactorMin     = 0.0f;
const float WaterSettings::s_waveHeightDampingOverTimeFactorDefault = 0.9990f;
const float WaterSettings::s_waveHeightDampingOverTimeFactorMax     = 1.0f;

const float WaterSettings::s_waveHeightClampingFactorMin     = 0.0f;
const float WaterSettings::s_waveHeightClampingFactorDefault = 0.9f;
const float WaterSettings::s_waveHeightClampingFactorMax     = 1.0f;

// water ripple
const WaterSettings::WaterRippleMethod WaterSettings::s_waterRippleMethodDefault = WaterSettings::WaterRippleMethodCircularTrigonometric;

const float WaterSettings::s_waterRippleMagnitudeMin     = -16.0f;
const float WaterSettings::s_waterRippleMagnitudeDefault =  0.5f;
const float WaterSettings::s_waterRippleMagnitudeMax     =  16.0f;

const float WaterSettings::s_waterRippleRadiusFactorMin     = 0.0f;
const float WaterSettings::s_waterRippleRadiusFactorDefault = 0.05f;
const float WaterSettings::s_waterRippleRadiusFactorMax     = 1.0f;

const QVector2D WaterSettings::s_waterRipple2DRadiusFactorDefault{ WaterSettings::s_waterRippleRadiusFactorDefault,
                                                                   WaterSettings::s_waterRippleRadiusFactorDefault };

const float WaterSettings::s_waterRippleRotationAngleMin     = 0.0f;
//const float WaterSettings::s_waterRippleRotationAngleDefault = WaterSettings::s_waterRippleRotationAngleMin;
const float WaterSettings::s_waterRippleRotationAngleDefault = 30.0f;
const float WaterSettings::s_waterRippleRotationAngleMax     = 360.0f;

// rain
const bool WaterSettings::s_shouldSimulateRainDefault   = true;

const int WaterSettings::s_rainDropsFallTimeIntervalMin     = 200;
const int WaterSettings::s_rainDropsFallTimeIntervalDefault = 1300;
const int WaterSettings::s_rainDropsFallTimeIntervalMax     = 5000;

const int WaterSettings::s_rainDropsCountPerFallMin     = 0;
const int WaterSettings::s_rainDropsCountPerFallDefault = 1;
const int WaterSettings::s_rainDropsCountPerFallMax     =
        static_cast< int >( qMin( static_cast< qint64 >( std::numeric_limits< int >::max() ),
                                  static_cast< qint64 >( WaterSettings::s_meshDimensionMax
                                                         * WaterSettings::s_meshDimensionMax ) ) );

const bool WaterSettings::s_shouldShowCoordinateSystemIndicatorDefault = false;

WaterSettings::WaterSettings()
    // mesh
    : m_meshSize{ s_meshDimensionDefault, s_meshDimensionDefault }
    , m_shouldDrawAsWireframe{ s_shouldDrawAsWireframeDefault }

    // wave propagation
    , m_shouldPropagateWaves{ s_shouldPropagateWavesDefault }
    , m_wavePropagationMethod{ s_wavePropagationMethodDefault }
    , m_wavePropagationSpeed{ s_wavePropagationSpeedDefault }
    , m_waveHeightDampingOverTimeFactor{ s_waveHeightDampingOverTimeFactorDefault }
    , m_waveHeightClampingFactor{ s_waveHeightClampingFactorDefault }

    // water ripple
    , m_waterRippleMethod{ s_waterRippleMethodDefault }
    , m_waterRippleMagnitude{ s_waterRippleMagnitudeDefault }
    , m_waterRippleRadiusFactor{ s_waterRipple2DRadiusFactorDefault }
    , m_waterRippleRotationAngle{ s_waterRippleRotationAngleDefault }

    // rain
    , m_shouldSimulateRain{ s_shouldSimulateRainDefault }
    , m_rainDropsFallTimeInterval{ s_rainDropsFallTimeIntervalDefault }
    , m_rainDropsCountPerFall{ s_rainDropsCountPerFallDefault }
    , m_shouldShowCoordinateSystemIndicator{ s_shouldShowCoordinateSystemIndicatorDefault }
{
}

WaterSettings::WaterSettings(
        const QSize& aMeshSize,
        const bool aShouldDrawAsWireframe,

        const bool aShouldPropagateWaves,
        const WaterSettings::WavePropagationMethod aWavePropagationMethod,
        const float aWavePropagationSpeed,
        const float aWaveHeightDampingOverTimeFactor,
        const float aWaveHeightClampingFactor,

        const WaterSettings::WaterRippleMethod aWaterRippleMethod,
        const float aWaterRippleMagnitude,
        const QVector2D& aWaterRippleRadiusFactor,
        const float aWaterRippleRotationAngle,

        const bool aShouldSimulateRain,
        const int aRainDropsFallTimeInterval,
        const int aRainDropsCountPerFall,

        const bool aShouldShowCoordinateSystemIndicator )
    : m_meshSize{ s_meshDimensionDefault, s_meshDimensionDefault }
    , m_shouldDrawAsWireframe{ s_shouldDrawAsWireframeDefault }

    , m_shouldPropagateWaves{ s_shouldPropagateWavesDefault }
    , m_wavePropagationMethod{ s_wavePropagationMethodDefault }
    , m_wavePropagationSpeed{ s_wavePropagationSpeedDefault }
    , m_waveHeightDampingOverTimeFactor{ s_waveHeightDampingOverTimeFactorDefault }
    , m_waveHeightClampingFactor{ s_waveHeightClampingFactorDefault }

    , m_waterRippleMethod{ s_waterRippleMethodDefault }
    , m_waterRippleMagnitude{ s_waterRippleMagnitudeDefault }
    , m_waterRippleRadiusFactor{ s_waterRipple2DRadiusFactorDefault }
    , m_waterRippleRotationAngle{ s_waterRippleRotationAngleDefault }

    , m_shouldSimulateRain{ s_shouldSimulateRainDefault }
    , m_rainDropsFallTimeInterval{ s_rainDropsFallTimeIntervalDefault }
    , m_rainDropsCountPerFall{ s_rainDropsCountPerFallDefault }
    , m_shouldShowCoordinateSystemIndicator{ s_shouldShowCoordinateSystemIndicatorDefault }
{
    this->setMeshSize( aMeshSize );
    this->setShouldDrawAsWireframe( aShouldDrawAsWireframe );

    this->setShouldPropagateWaves( aShouldPropagateWaves );
    this->setWavePropagationMethod( aWavePropagationMethod );
    this->setWavePropagationSpeed( aWavePropagationSpeed );
    this->setWaveHeightDampingOverTimeFactor( aWaveHeightDampingOverTimeFactor );
    this->setWaveHeightClampingFactor( aWaveHeightClampingFactor );

    this->setWaterRippleMethod( aWaterRippleMethod );
    this->setWaterRippleMagnitude( aWaterRippleMagnitude );
    this->setWaterRippleRadiusFactor( aWaterRippleRadiusFactor );
    this->setWaterRippleRotationAngle( aWaterRippleRotationAngle );

    this->setShouldSimulateRain( aShouldSimulateRain );
    this->setRainDropsFallTimeInterval( aRainDropsFallTimeInterval );
    this->setRainDropsCountPerFall( aRainDropsCountPerFall );

    this->setShouldShowCoordinateSystemIndicator( aShouldShowCoordinateSystemIndicator );
}

WaterSettings::~WaterSettings()
{
}

bool WaterSettings::operator==( const WaterSettings& other ) const
{
    return    m_meshSize == other.m_meshSize
           && m_shouldDrawAsWireframe == other.m_shouldDrawAsWireframe

           && m_shouldPropagateWaves == other.m_shouldPropagateWaves
           && m_wavePropagationMethod == other.m_wavePropagationMethod
           && qFuzzyCompare( m_wavePropagationSpeed, other.m_wavePropagationSpeed )
           && qFuzzyCompare( m_waveHeightDampingOverTimeFactor, other.m_waveHeightDampingOverTimeFactor )
           && qFuzzyCompare( m_waveHeightClampingFactor, other.m_waveHeightClampingFactor )

           && m_waterRippleMethod == other.m_waterRippleMethod
           && qFuzzyCompare( m_waterRippleMagnitude, other.m_waterRippleMagnitude )
           && qFuzzyCompare( m_waterRippleRadiusFactor, other.m_waterRippleRadiusFactor )
           && qFuzzyCompare( m_waterRippleRotationAngle, other.m_waterRippleRotationAngle )

           && m_shouldSimulateRain == other.m_shouldSimulateRain
           && m_rainDropsFallTimeInterval == other.m_rainDropsFallTimeInterval
           && m_rainDropsCountPerFall == other.m_rainDropsCountPerFall

           && m_shouldShowCoordinateSystemIndicator == other.m_shouldShowCoordinateSystemIndicator;
}

bool WaterSettings::operator!=( const WaterSettings& other ) const
{
    return ! ( *this == other );
}

WaterSettings::WaterProperties WaterSettings::diff( const WaterSettings& other ) const
{
    WaterProperties changedProperties;

    // mesh
    if ( m_meshSize != other.m_meshSize )
    {
        changedProperties |= WaterPropertyMeshSize;
    }

    if ( m_shouldDrawAsWireframe != other.m_shouldDrawAsWireframe )
    {
        changedProperties |= WaterPropertyShouldDrawAsWireframe;
    }

    // wave propagation
    if ( m_shouldPropagateWaves != other.m_shouldPropagateWaves )
    {
        changedProperties |= WaterPropertyShouldPropagateWaves;
    }

    if ( m_wavePropagationMethod != other.m_wavePropagationMethod )
    {
        changedProperties |= WaterPropertyWavePropagationMethod;
    }

    if ( ! qFuzzyCompare( m_wavePropagationSpeed, other.m_wavePropagationSpeed ) )
    {
        changedProperties |= WaterPropertyWavePropagationSpeed;
    }

    if ( ! qFuzzyCompare( m_waveHeightDampingOverTimeFactor, other.m_waveHeightDampingOverTimeFactor ) )
    {
        changedProperties |= WaterPropertyWaveHeightDampingOverTimeFactor;
    }

    if ( ! qFuzzyCompare( m_waveHeightClampingFactor, other.m_waveHeightClampingFactor ) )
    {
        changedProperties |= WaterPropertyWaveHeightClampingFactor;
    }

    // water ripple
    if ( m_waterRippleMethod != other.m_waterRippleMethod )
    {
        changedProperties |= WaterPropertyWaterRippleMethod;
    }

    if ( ! qFuzzyCompare( m_waterRippleMagnitude, other.m_waterRippleMagnitude ) )
    {
        changedProperties |= WaterPropertyWaterRippleMagnitude;
    }

    if ( ! qFuzzyCompare( m_waterRippleRadiusFactor, other.m_waterRippleRadiusFactor ) )
    {
        changedProperties |= WaterPropertyWaterRippleRadiusFactor;
    }

    if ( ! qFuzzyCompare( m_waterRippleRotationAngle, other.m_waterRippleRotationAngle ) )
    {
        changedProperties |= WaterPropertyWaterRippleRotationAngle;
    }

    // rain
    if ( m_shouldSimulateRain != other.m_shouldSimulateRain )
    {
        changedProperties |= WaterPropertyShouldSimulateRain;
    }

    if ( m_rainDropsFallTimeInterval != other.m_rainDropsFallTimeInterval )
    {
        changedProperties |= WaterPropertyRainDropsFallTimeInterval;
    }

    if ( m_rainDropsCountPerFall != other.m_rainDropsCountPerFall )
    {
        changedProperties |= WaterPropertyRainDropsCountPerFall;
    }


    if ( m_shouldShowCoordinateSystemIndicator != other.m_shouldShowCoordinateSystemIndicator )
    {
        changedProperties |= WaterPropertyShouldShowCoordinateSystemIndicator;
    }

    return changedProperties;
}

bool WaterSettings::isMeshDimensionValid( const int dimension )
{
    return WaterSettings::s_meshDimensionMin <= dimension
           && dimension <= WaterSettings::s_meshDimensionMax;
}

bool WaterSettings::isMeshSizeValid( const QSize& meshSize )
{
    return WaterSettings::isMeshDimensionValid( meshSize.width() )
           && WaterSettings::isMeshDimensionValid( meshSize.height() );
}
bool WaterSettings::isWavePropagationSpeedValid( const float wavePropagationSpeed )
{
    return MathUtilities::fuzzyContainedInRegion(
                WaterSettings::s_wavePropagationSpeedMin,
                wavePropagationSpeed,
                WaterSettings::s_wavePropagationSpeedMax );
}

bool WaterSettings::isWaterRippleMagnitudeValid( const float waterRippleMagnitude )
{
    return MathUtilities::fuzzyContainedInRegion(
                WaterSettings::s_waterRippleMagnitudeMin,
                waterRippleMagnitude,
                WaterSettings::s_waterRippleMagnitudeMax );
}

bool WaterSettings::isWaterRippleRadiusFactorValid( const float waterRippleRadiusFactor )
{
    return MathUtilities::fuzzyContainedInRegion(
                WaterSettings::s_waterRippleRadiusFactorMin,
                waterRippleRadiusFactor,
                WaterSettings::s_waterRippleRadiusFactorMax );
}

bool WaterSettings::isWaterRippleRadiusFactorValid( const QVector2D& waterRippleRadiusFactor )
{
    return WaterSettings::isWaterRippleRadiusFactorValid( waterRippleRadiusFactor.x() )
           && WaterSettings::isWaterRippleRadiusFactorValid( waterRippleRadiusFactor.y() );
}

bool WaterSettings::isWaterRippleRotationAngleValid( const float waterRippleRotationAngle )
{
    return MathUtilities::fuzzyContainedInRegion(
                WaterSettings::s_waterRippleRotationAngleMin,
                waterRippleRotationAngle,
                WaterSettings::s_waterRippleRotationAngleMax );
}

bool WaterSettings::isWaveHeightDampingOverTimeFactorValid( const float factor )
{
    return MathUtilities::fuzzyContainedInRegion(
                WaterSettings::s_waveHeightDampingOverTimeFactorMin,
                factor,
                WaterSettings::s_waveHeightDampingOverTimeFactorMax );
}

bool WaterSettings::isWaveHeightClampingFactorValid( const float factor )
{
    return MathUtilities::fuzzyContainedInRegion(
                WaterSettings::s_waveHeightClampingFactorMin,
                factor,
                WaterSettings::s_waveHeightClampingFactorMax );
}

bool WaterSettings::isRainDropsFallTimeIntervalValid( const int rainDropsTimeFallInterval )
{
    return WaterSettings::s_rainDropsFallTimeIntervalMin <= rainDropsTimeFallInterval
           && rainDropsTimeFallInterval <= WaterSettings::s_rainDropsFallTimeIntervalMax;
}

bool WaterSettings::isRainDropsCountPerFallValid( const int rainDropsCountPerFall )
{
    return WaterSettings::s_rainDropsCountPerFallMin <= rainDropsCountPerFall
           && rainDropsCountPerFall <= WaterSettings::s_rainDropsCountPerFallMax;
}

int WaterSettings::rainDropsFallTimeInterval() const
{
    return m_rainDropsFallTimeInterval;
}

void WaterSettings::setRainDropsFallTimeInterval( const int aRainDropsFallTimeInterval )
{
    if ( WaterSettings::isRainDropsFallTimeIntervalValid( aRainDropsFallTimeInterval ) )
    {
        m_rainDropsFallTimeInterval = aRainDropsFallTimeInterval;
    }
}

int WaterSettings::rainDropsCountPerFall() const
{
    return m_rainDropsCountPerFall;
}

void WaterSettings::setRainDropsCountPerFall( const int aRainDropsCountPerFall )
{
    if ( WaterSettings::isRainDropsCountPerFallValid( aRainDropsCountPerFall ) )
    {
        m_rainDropsCountPerFall = aRainDropsCountPerFall;
    }
}

bool WaterSettings::shouldShowCoordinateSystemIndicator() const
{
    return m_shouldShowCoordinateSystemIndicator;
}

float WaterSettings::waterRippleRotationAngle() const
{
    return m_waterRippleRotationAngle;
}

void WaterSettings::setWaterRippleRotationAngle( const float aWaterRippleRotationAngle )
{
    const float normalizedAngle = MathUtilities::to360DegreesAngle( aWaterRippleRotationAngle );
    if ( WaterSettings::isWaterRippleRotationAngleValid( normalizedAngle ) )
    {
        m_waterRippleRotationAngle = normalizedAngle;
    }
}

void WaterSettings::setShouldShowCoordinateSystemIndicator( const bool aShouldShowCoordinateSystemIndicator )
{
    m_shouldShowCoordinateSystemIndicator = aShouldShowCoordinateSystemIndicator;
}

void WaterSettings::setShouldDrawAsWireframe( const bool aShouldDrawAsWireframe )
{
    m_shouldDrawAsWireframe = aShouldDrawAsWireframe;
}

bool WaterSettings::shouldDrawAsWireframe() const
{
    return m_shouldDrawAsWireframe;
}

float WaterSettings::waterRippleMagnitude() const
{
    return m_waterRippleMagnitude;
}

void WaterSettings::setWaterRippleMethod(
        const WaterSettings::WaterRippleMethod aWaterRippleMethod )
{
    if ( ! WaterSettings::isWaterRippleMethodValid( aWaterRippleMethod )
         || aWaterRippleMethod == m_waterRippleMethod )
    {
        return;
    }

    m_waterRippleMethod = aWaterRippleMethod;
}

WaterSettings::WaterRippleMethod WaterSettings::waterRippleMethod() const
{
    return m_waterRippleMethod;
}

void WaterSettings::setWaterRippleMagnitude( const float aWaterRippleMagnitude )
{
    if ( WaterSettings::isWaterRippleMagnitudeValid( aWaterRippleMagnitude ) )
    {
        m_waterRippleMagnitude = aWaterRippleMagnitude;
    }
}

const QVector2D& WaterSettings::waterRippleRadiusFactor() const
{
    return m_waterRippleRadiusFactor;
}

void WaterSettings::setWaterRippleRadiusFactor( const QVector2D& aWaterRippleRadiusFactor )
{
    if ( WaterSettings::isWaterRippleRadiusFactorValid( aWaterRippleRadiusFactor ) )
    {
        m_waterRippleRadiusFactor = aWaterRippleRadiusFactor;
    }
}

bool WaterSettings::shouldSimulateRain() const
{
    return m_shouldSimulateRain;
}

void WaterSettings::setShouldSimulateRain( const bool aShouldSimulateRain )
{
    m_shouldSimulateRain = aShouldSimulateRain;
}

bool WaterSettings::shouldPropagateWaves() const
{
    return m_shouldPropagateWaves;
}

void WaterSettings::setShouldPropagateWaves( const bool aShouldPropagateWaves )
{
    m_shouldPropagateWaves = aShouldPropagateWaves;
}

float WaterSettings::waveHeightClampingFactor() const
{
    return m_waveHeightClampingFactor;
}

void WaterSettings::setWaveHeightClampingFactor( const float aWaveHeightClampingFactor )
{
    if ( WaterSettings::isWaveHeightClampingFactorValid( aWaveHeightClampingFactor ) )
    {
        m_waveHeightClampingFactor = aWaveHeightClampingFactor;
    }
}

float WaterSettings::waveHeightDampingOverTimeFactor() const
{
    return m_waveHeightDampingOverTimeFactor;
}

void WaterSettings::setWaveHeightDampingOverTimeFactor( const float aWaveHeightDampingOverTimeFactor )
{
    if ( WaterSettings::isWaveHeightDampingOverTimeFactorValid( aWaveHeightDampingOverTimeFactor ) )
    {
        m_waveHeightDampingOverTimeFactor = aWaveHeightDampingOverTimeFactor;
    }
}

float WaterSettings::wavePropagationSpeed() const
{
    return m_wavePropagationSpeed;
}

void WaterSettings::setWavePropagationSpeed( const float aWavePropagationSpeed )
{
    if ( WaterSettings::isWavePropagationSpeedValid( aWavePropagationSpeed ) )
    {
        m_wavePropagationSpeed = aWavePropagationSpeed;
    }
}

bool WaterSettings::isWaterRippleMethodValid( const int waterRippleMethod )
{
    return    waterRippleMethod == WaterRippleMethodCircularTrigonometric
           || waterRippleMethod == WaterRippleMethodEllipticTrigonometric
           || waterRippleMethod == WaterRippleMethodEllipticGaussian;
}

WaterSettings::WavePropagationMethod WaterSettings::wavePropagationMethod() const
{
    return m_wavePropagationMethod;
}

bool WaterSettings::isWavePropagationMethodValid( const int wavePropagationMethod )
{
    return    wavePropagationMethod == WavePropagationMethodCpu
           || wavePropagationMethod == WavePropagationMethodGpu
           || wavePropagationMethod == WavePropagationMethodGpuPrecomputedNormalsVertices;
}

void WaterSettings::setWavePropagationMethod( const WaterSettings::WavePropagationMethod aWavePropagationMethod )
{
    if ( WaterSettings::isWavePropagationMethodValid( aWavePropagationMethod ) )
    {
        m_wavePropagationMethod = aWavePropagationMethod;
    }
}

QSize WaterSettings::meshSize() const
{
    return m_meshSize;
}

void WaterSettings::setMeshSize( const QSize& aMeshSize )
{
    if ( WaterSettings::isMeshSizeValid( aMeshSize ) )
    {
        m_meshSize = aMeshSize;
    }
}

