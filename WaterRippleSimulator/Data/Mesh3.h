#ifndef WATERRIPPLESIMULATOR_DATA_MESH3_H
#define WATERRIPPLESIMULATOR_DATA_MESH3_H

#include "Triangle2.h"
#include "Triangle3.h"

#include <QList>
#include <QPair>
#include <QVector3D>
#include <QVector4D>

namespace WaterRippleSimulator {
namespace Data {

class Mesh3
{
public:
    explicit Mesh3( const QList< QPair< Triangle3, Triangle2 > >& aTriangles = QList< QPair< Triangle3, Triangle2 > >() );
    ~Mesh3();

    void setTriangles( const QList< QPair< Triangle3, Triangle2 > >& aTriangles );
    const QList< QPair< Triangle3, Triangle2 > >& triangles() const;

    QList< QPair< Triangle3, Triangle2 > > trianglesSharingVertex( const QVector3D& v ) const;
    QVector3D normalAtVertex( const QVector3D& v ) const;
    QVector4D tangentAtVertex( const QVector3D& v ) const;
    QVector4D bitangentAtVertex( const QVector3D& v ) const;

    bool isVertex( const QVector3D v ) const;
    bool contains( const Triangle3& t ) const;

    float area() const;

    bool operator==( const Mesh3& other ) const;
    bool operator!=( const Mesh3& other ) const;

private:
    QVector4D tangentOrBitangentAtVertex(
            const QVector3D& v,
            const bool requestTangent,
            const bool shouldCalculateManually ) const;

private:
    QList< QPair< Triangle3, Triangle2 > > m_triangles;
};

}
}

#endif // WATERRIPPLESIMULATOR_DATA_MESH3_H
