#ifndef WATERRIPPLESIMULATOR_DATA_TRIANGLE3_H
#define WATERRIPPLESIMULATOR_DATA_TRIANGLE3_H

#include <QList>
#include <QString>
#include <QVector3D>

namespace WaterRippleSimulator {
namespace Data {

class Triangle3
{
public:
    static const int s_verticesCount;

public:
    explicit Triangle3(
            const QVector3D& aV0 = QVector3D(),
            const QVector3D& aV1 = QVector3D(),
            const QVector3D& aV2 = QVector3D() );
    ~Triangle3();

    void setVertices( const QVector3D& aV0,
                      const QVector3D& aV1,
                      const QVector3D& aV2 );
    const QList< QVector3D >& vertices() const;

    void setVertexAtIndex( const int index, const QVector3D& v );
    QVector3D vertexAtIndex( const int index ) const;

    void setV0( const QVector3D& aV0 );
    QVector3D v0() const;

    void setV1( const QVector3D& aV1 );
    QVector3D v1() const;

    void setV2( const QVector3D& aV2 );
    QVector3D v2() const;

    QVector3D edge10() const;
    QVector3D edge20() const;
    QVector3D edge21() const;

    void reverse();

    QVector3D normal() const;

    int indexOfVertex( const QVector3D& v ) const;
    bool isVertex( const QVector3D& v ) const;
    bool contains( const QVector3D& v ) const;
    bool contains( const Triangle3& t ) const;

    float angleAtVertex( const QVector3D& v ) const;

    float area() const;

    bool operator==( const Triangle3& other ) const;
    bool operator!=( const Triangle3& other ) const;

    QString toString() const;

    bool intersectsWithLine(
            const QVector3D& origin,
            const QVector3D& dir,
            QVector3D* const intersectionPt = nullptr ) const;
    bool intersectsWithRay(
            const QVector3D& origin,
            const QVector3D& dir,
            QVector3D* const barycentricIntersectionPt = nullptr ) const;
    QVector3D fromBarycentricPt( const float u, const float v ) const;

private:
    int cyclicVertexIndex( const int vertexIndex ) const;

    static float sign(
            const QVector3D& v0,
            const QVector3D& v1,
            const QVector3D& v2 );

private:
    QList< QVector3D > m_vertices;
};

}
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug qdebug, const WaterRippleSimulator::Data::Triangle3& t );
#endif

#endif // WATERRIPPLESIMULATOR_DATA_TRIANGLE3_H
