#ifndef WATERRIPPLESIMULATOR_DATA_MATERIAL_H
#define WATERRIPPLESIMULATOR_DATA_MATERIAL_H

#include <QColor>
#include <QVector3D>

namespace WaterRippleSimulator {
namespace Data {

struct MaterialReflectance
{
    bool operator==( const MaterialReflectance& other ) const;
    bool operator!=( const MaterialReflectance& other ) const;

    QVector3D emissive;
    QVector3D ambient;
    QVector3D diffuse;
    QVector3D specular;
};

struct BaseMaterial
{
public:
    BaseMaterial();
    virtual ~BaseMaterial();

    bool operator==( const BaseMaterial& other ) const;
    bool operator!=( const BaseMaterial& other ) const;

public:
    MaterialReflectance reflectance;
    float shininess;
};

struct Material : public BaseMaterial
{
public:
    Material();
    ~Material() Q_DECL_OVERRIDE;

    bool operator==( const Material& other ) const;
    bool operator!=( const Material& other ) const;

public:
    QColor color;
};

}
}

#endif // WATERRIPPLESIMULATOR_DATA_MATERIAL_H
