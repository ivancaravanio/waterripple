#ifndef WATERRIPPLESIMULATOR_DATA_LIGHT_H
#define WATERRIPPLESIMULATOR_DATA_LIGHT_H

#include <QColor>
#include <QVector3D>

namespace WaterRippleSimulator {
namespace Data {

struct LightAttenuation
{
public:
    LightAttenuation();
    ~LightAttenuation();

    bool operator==( const LightAttenuation& other ) const;
    bool operator!=( const LightAttenuation& other ) const;

public:
    float constant;
    float linear;
    float quadratic;
};

struct Light
{
public:
    enum class Type
    {
        Point
    };

public:
    Light();
    ~Light();

    bool operator==( const Light& other ) const;
    bool operator!=( const Light& other ) const;

public:
    Type             type;
    QVector3D        pos;
    QColor           color;
    LightAttenuation attenuation;
    float            intensity;
};

}
}

#endif // WATERRIPPLESIMULATOR_DATA_LIGHT_H
