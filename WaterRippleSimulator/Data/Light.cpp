#include "Light.h"

using namespace WaterRippleSimulator::Data;

// http://www.ogre3d.org/tikiwiki/tiki-index.php?page=-Point+Light+Attenuation

LightAttenuation::LightAttenuation()
    : constant{ 0.0f }
    , linear{ 0.0f }
    , quadratic{ 0.0f }
{
}

LightAttenuation::~LightAttenuation()
{
}

bool LightAttenuation::operator==( const LightAttenuation& other ) const
{
    return    qFuzzyCompare( constant,  other.constant )
           && qFuzzyCompare( linear,    other.linear )
           && qFuzzyCompare( quadratic, other.quadratic );
}

bool LightAttenuation::operator!=(const LightAttenuation& other) const
{
    return ! ( *this == other );
}

Light::Light()
   : type{ Type::Point }
   , color{ Qt::white }
   , intensity{ 1.0f }
{
}

Light::~Light()
{
}

bool Light::operator==( const Light& other ) const
{
    return    type == other.type
           && qFuzzyCompare( pos, other.pos )
           && color == other.color
           && attenuation == other.attenuation
           && qFuzzyCompare( intensity, other.intensity );
}

bool Light::operator!=( const Light& other ) const
{
    return ! ( *this == other );
}

