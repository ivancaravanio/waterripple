#include "Material.h"

using namespace WaterRippleSimulator::Data;

Material::Material()
   : color{ Qt::black }
{
}

Material::~Material()
{
}

bool Material::operator==( const Material& other ) const
{
    return    BaseMaterial::operator ==( other )
           && color == other.color;
}

bool Material::operator!=( const Material& other ) const
{
    return ! ( *this == other );
}


BaseMaterial::BaseMaterial()
   : shininess{ 0.0f }
{
}

BaseMaterial::~BaseMaterial()
{
}

bool BaseMaterial::operator==( const BaseMaterial& other ) const
{
    return    reflectance == other.reflectance
           && qFuzzyCompare( shininess, other.shininess );
}

bool BaseMaterial::operator!=( const BaseMaterial& other ) const
{
    return ! ( *this == other );
}


bool MaterialReflectance::operator==( const MaterialReflectance& other ) const
{
    return    qFuzzyCompare( emissive, other.emissive )
           && qFuzzyCompare( ambient,  other.ambient  )
           && qFuzzyCompare( diffuse,  other.diffuse  )
           && qFuzzyCompare( specular, other.specular );
}

bool MaterialReflectance::operator!=( const MaterialReflectance& other ) const
{
    return ! ( *this == other );
}
