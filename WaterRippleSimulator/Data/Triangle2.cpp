#include "Triangle2.h"

#include "../Utilities/MathUtilities.h"
#include "../WaterRippleSimulator_namespace.h"

#include <QTextStream>
#include <QtGlobal>

using namespace WaterRippleSimulator::Data;

using namespace WaterRippleSimulator::Utilities;

const int Triangle2::s_verticesCount = 3;

Triangle2::Triangle2( const QVector2D& aV0,
                      const QVector2D& aV1,
                      const QVector2D& aV2 )
{
    m_vertices.reserve( s_verticesCount );
    m_vertices << aV0
               << aV1
               << aV2;
}

Triangle2::~Triangle2()
{
}

void Triangle2::setVertices(
        const QVector2D& aV0,
        const QVector2D& aV1,
        const QVector2D& aV2 )
{
    this->setV0( aV0 );
    this->setV1( aV1 );
    this->setV2( aV2 );
}

const QList< QVector2D >& Triangle2::vertices() const
{
    return m_vertices;
}

void Triangle2::setVertexAtIndex( const int index, const QVector2D& v )
{
    m_vertices[ Triangle2::cyclicVertexIndex( index ) ] = v;
}

QVector2D Triangle2::vertexAtIndex( const int index ) const
{
    return m_vertices[ Triangle2::cyclicVertexIndex( index ) ];
}

void Triangle2::setV0( const QVector2D& aV0 )
{
    this->setVertexAtIndex( 0, aV0 );
}

QVector2D Triangle2::v0() const
{
    return this->vertexAtIndex( 0 );
}

void Triangle2::setV1( const QVector2D& aV1 )
{
    this->setVertexAtIndex( 1, aV1 );
}

QVector2D Triangle2::v1() const
{
    return this->vertexAtIndex( 1 );
}

void Triangle2::setV2( const QVector2D& aV2 )
{
    this->setVertexAtIndex( 2, aV2 );
}

QVector2D Triangle2::v2() const
{
    return this->vertexAtIndex( 2 );
}

QVector2D Triangle2::edge10() const
{
    return this->v1() - this->v0();
}

QVector2D Triangle2::edge20() const
{
    return this->v2() - this->v0();
}

QVector2D Triangle2::edge21() const
{
    return this->v2() - this->v1();
}

void Triangle2::reverse()
{
    qSwap( m_vertices[ 0 ], m_vertices[ 2 ] );
}

int Triangle2::indexOfVertex( const QVector2D& v ) const
{
    return MathUtilities::vertexIndex( m_vertices, v );
}

bool Triangle2::isVertex( const QVector2D& v ) const
{
    return this->indexOfVertex( v ) >= 0;
}

bool Triangle2::contains( const QVector2D& v ) const
{
    const QVector2D v0 = this->v0();
    const QVector2D v1 = this->v1();
    const QVector2D v2 = this->v2();

    const bool b0 = Triangle2::sign( v, v0, v1 ) < 0.0f;
    const bool b1 = Triangle2::sign( v, v1, v2 ) < 0.0f;
    const bool b2 = Triangle2::sign( v, v2, v0 ) < 0.0f;

    return ( b0 == b1 ) && ( b1 == b2 );
}

bool Triangle2::contains( const Triangle2& t ) const
{
    const int otherTriangleVerticesCount = t.vertices().size();
    for ( int i = 0; i < otherTriangleVerticesCount; ++ i )
    {
        if ( ! this->contains( t.vertexAtIndex( i ) ) )
        {
            return false;
        }
    }

    return true;
}

float Triangle2::angleAtVertex( const QVector2D& v ) const
{
    const int vIndex = this->indexOfVertex( v );
    if ( vIndex < 0 )
    {
        return 0.0f;
    }

    return MathUtilities::angle( this->vertexAtIndex( vIndex - 1 ) - v,
                                 this->vertexAtIndex( vIndex + 1 ) - v );
}

float Triangle2::area() const
{
    const QVector2D v0 = this->v0();
    const QVector2D v1 = this->v1();
    const QVector2D v2 = this->v2();

    const QVector2D edge10 = v1 - v0;
    const QVector2D edge20 = v2 - v0;
    const float edgesLengthProduct = edge10.length() * edge20.length();
    const float sinAngle = sqrtf( 1.0f - powf( QVector2D::dotProduct( edge10, edge20 ) / edgesLengthProduct, 2.0f ) );

    return sinAngle * edgesLengthProduct / 2.0f;
}

bool Triangle2::operator==( const Triangle2& other ) const
{
    const int verticesCount = m_vertices.size();
    if ( verticesCount != other.vertices().size() )
    {
        return false;
    }

    for ( int i = 0; i < verticesCount; ++ i )
    {
        if ( ! MathUtilities::epsilonEqual(
                 this->vertexAtIndex( i ),
                 other.vertexAtIndex( i ) ) )
        {
            return false;
        }
    }

    return true;
}

bool Triangle2::operator!=( const Triangle2& other ) const
{
    return ! ( *this == other );
}

QString Triangle2::toString() const
{
    QString s;

    QTextStream ts( &s );

    ts << QT_STRINGIFY2( Triangle2 ) << '(' << endl
       << "\tv0: " << CommonEntities::vecToString( this->v0() ) << endl
       << "\tv1: " << CommonEntities::vecToString( this->v1() ) << endl
       << "\tv2: " << CommonEntities::vecToString( this->v2() ) << " )";

    return s;
}

int Triangle2::cyclicVertexIndex( const int vertexIndex ) const
{
    const int verticesCount = m_vertices.size();
    return vertexIndex >= 0
           ? vertexIndex % verticesCount
           : ( verticesCount - qAbs( vertexIndex ) % verticesCount );
}

float Triangle2::sign(
        const QVector2D& v0,
        const QVector2D& v1,
        const QVector2D& v2 )
{
    return   ( v0.x() - v2.x() ) * ( v1.y() - v2.y() )
           - ( v1.x() - v2.x() ) * ( v0.y() - v2.y() );
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug qdebug, const Triangle2& t )
{
    return qdebug << t.toString();
}
#endif
