#ifndef WATERRIPPLESIMULATOR_DATA_VERTEXDATAGPU_H
#define WATERRIPPLESIMULATOR_DATA_VERTEXDATAGPU_H

#include <QPoint>

namespace WaterRippleSimulator {
namespace Data {

struct VertexDataGpu
{
    QPoint texCoords;
};

}
}

#endif // WATERRIPPLESIMULATOR_DATA_VERTEXDATAGPU_H
