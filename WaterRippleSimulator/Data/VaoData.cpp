#include "VaoData.h"

using namespace WaterRippleSimulator::Data;

VaoData::VaoData()
{
}

VaoData::~VaoData()
{
}

void VaoData::clear()
{
    geometry.clear();
    color.clear();
    textureCoordinates.clear();
}
