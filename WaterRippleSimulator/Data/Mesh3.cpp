#include "Mesh3.h"

#include "../Utilities/MathUtilities.h"

#include <glm/mat2x2.hpp>
#include <glm/mat2x3.hpp>
#include <glm/mat3x2.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/matrix.hpp>
#include <glm/vec3.hpp>

#include <QGenericMatrix>
#include <QMatrix4x4>

#include <algorithm>

using namespace WaterRippleSimulator::Data;

using namespace WaterRippleSimulator::Utilities;

Mesh3::Mesh3( const QList< QPair< Triangle3, Triangle2 > >& aTriangles )
    : m_triangles( aTriangles )
{
}

Mesh3::~Mesh3()
{
}

void Mesh3::setTriangles( const QList< QPair< Triangle3, Triangle2 > >& aTriangles )
{
    m_triangles = aTriangles;
}

const QList< QPair< Triangle3, Triangle2 > >& Mesh3::triangles() const
{
    return m_triangles;
}

QList< QPair< Triangle3, Triangle2 > > Mesh3::trianglesSharingVertex( const QVector3D& v ) const
{
    QList< QPair< Triangle3, Triangle2 > > triangles;
    const int trianglesCount = m_triangles.size();
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        const QPair< Triangle3, Triangle2 >& triangle = m_triangles[ i ];
        if ( triangle.first.isVertex( v ) )
        {
            triangles.append( triangle );
        }
    }

    return triangles;
}

QVector3D Mesh3::normalAtVertex( const QVector3D& v ) const
{
    const QList< QPair< Triangle3, Triangle2 > > triangles = this->trianglesSharingVertex( v );
    QVector3D vn;

    const int trianglesCount = triangles.size();
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        const Triangle3& triangle = triangles[ i ].first;
        vn += triangle.normal()
              * triangle.area()
              * triangle.angleAtVertex( v );
    }

    return vn.normalized();
}

QVector4D Mesh3::tangentAtVertex( const QVector3D& v ) const
{
    return this->tangentOrBitangentAtVertex( v, true, false );
}

QVector4D Mesh3::bitangentAtVertex( const QVector3D& v ) const
{
    return this->tangentOrBitangentAtVertex( v, false, false );
}

bool Mesh3::isVertex( const QVector3D v ) const
{
    const int trianglesCount = m_triangles.size();
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        if ( m_triangles[ i ].first.isVertex( v ) )
        {
            return true;
        }
    }

    return false;
}

bool Mesh3::contains( const Triangle3& t ) const
{
    const int trianglesCount = m_triangles.size();
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        if ( m_triangles[ i ].first == t )
        {
            return true;
        }
    }

    return false;
}

float Mesh3::area() const
{
    float area = 0.0f;
    const int trianglesCount = m_triangles.size();
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        area += m_triangles[ i ].first.area();
    }

    return area;
}

bool Mesh3::operator==( const Mesh3& other ) const
{
    const QList< QPair< Triangle3, Triangle2 > >& otherTriangles = other.triangles();
    if ( m_triangles.size() != otherTriangles.size() )
    {
        return false;
    }

    return std::equal( m_triangles.begin(),
                       m_triangles.end(),
                       otherTriangles.begin() );
}

bool Mesh3::operator!=( const Mesh3& other ) const
{
    return ! ( *this == other );
}

QVector4D Mesh3::tangentOrBitangentAtVertex(
        const QVector3D& v,
        const bool requestTangent,
        const bool shouldCalculateManually ) const
{
#define TB_CALCULATION_MATRIX 0
#define TB_CALCULATION_VECTOR_1 1
#define TB_CALCULATION_VECTOR_2 2
#define TB_CALCULATION_APPROACH TB_CALCULATION_MATRIX

    const QVector3D vn = this->normalAtVertex( v );
    const QList< QPair< Triangle3, Triangle2 > > triangles = this->trianglesSharingVertex( v );

#ifdef THOROUGH_LOGGING_ENABLED
//    qDebug() << "==========================" << endl
//             << "    v(" << v << ')' << endl
//             << "    n(" << vn << ')' << endl
//             << "------------------------";
#endif

#if TB_CALCULATION_APPROACH == TB_CALCULATION_MATRIX
    Q_UNUSED( shouldCalculateManually )

    QVector3D vt;
    QVector3D vb;
#elif TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_1
    QVector3D vtManual;
    QVector3D vbManual;
#elif TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_2
    QVector3D vtManual2;
    QVector3D vbManual2;
#endif

    const int trianglesCount = triangles.size();
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        const QPair< Triangle3, Triangle2 >& triangle = triangles[ i ];
        const Triangle3& triangleGeometry      = triangle.first;
        const Triangle2& triangleTextureCoords = triangle.second;

        const QVector3D v10 = triangleGeometry.edge10();
        const QVector3D v20 = triangleGeometry.edge20();

        const QVector2D vt10 = triangleTextureCoords.edge10();
        const QVector2D vt20 = triangleTextureCoords.edge20();

        const float weightFactor =
                triangleGeometry.area()
                * triangleGeometry.angleAtVertex( v );

#if TB_CALCULATION_APPROACH == TB_CALCULATION_MATRIX
        // matrix approach
        const glm::mat2x3 geometryMat(
                    MathUtilities::glmFromQtVec( v10 ),
                    MathUtilities::glmFromQtVec( v20 ) );

        const glm::mat2x2 textureCoordsMat = glm::transpose(
                                                 glm::mat2x2(
                                                     MathUtilities::glmFromQtVec( vt10 ),
                                                     MathUtilities::glmFromQtVec( vt20 ) ) );

        const glm::mat2x3 tbMat = glm::transpose( glm::inverse( textureCoordsMat )
                                                  * glm::transpose( geometryMat ) );

        vt += MathUtilities::qtFromGlmVec( tbMat[ 0 ] ) * weightFactor;
        vb += MathUtilities::qtFromGlmVec( tbMat[ 1 ] ) * weightFactor;

#elif TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_1
        // vector approach 1
        const float r = 1.0f / ( vt10.x * vt20.y - vt20.x * vt10.y );

        QVector3D sdir( vt20.y * v10.x - vt10.y * v20.x,
                        vt20.y * vt10.y - vt10.y * v20.y,
                        vt20.y * v10.z - vt10.y * v20.z );
        sdir *= r;
        vtManual += sdir * weightFactor;

        QVector3D tdir( vt10.x * v20.x - vt20.x * v10.x,
                        vt10.x * v20.y - vt20.x * v10.y,
                        vt10.x * v20.z - vt20.x * v10.z );
        tdir *= r;
        vbManual += tdir * weightFactor;

#elif TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_2
        // vector approach 2
        const float r = 1.0f / ( vt10.x * vt20.y - vt20.x * vt10.y );

        const QVector3D tangent   = ( v10 * vt20.y - v20 * vt10.y ) * r;
        const QVector3D bitangent = ( v20 * vt10.x - v10 * vt20.x ) * r;

        vtManual2 += tangent   * weightFactor;
        vbManual2 += bitangent * weightFactor;
#endif
    }

    // Gram-Schmidt orthogonalization matrix approach
#if TB_CALCULATION_APPROACH == TB_CALCULATION_MATRIX
    const QList< QVector3D > grammSchmidtOrthonormalized =
            MathUtilities::gramSchmidtOrthogonalization( QList< QVector3D >()
                                                         << vn
                                                         << vt
                                                         << vb );
    const QVector3D vtOrtho   = grammSchmidtOrthonormalized[ 1 ];
    const QVector3D vbOrtho   = grammSchmidtOrthonormalized[ 2 ];
    const QVector3D vnOrtho   = grammSchmidtOrthonormalized[ 0 ];

//    const QVector3D vtOrtho1  =  vt - vn * glm::dot( vn, vt );
//    const QVector3D vbOrtho1  =  vb - vn * glm::dot( vn, vb ) - vtOrtho1 * glm::dot( vtOrtho1, vb ) / glm::dot( vtOrtho1, vtOrtho1 );
//    const QVector3D vnOrtho1  =  vn;

//    const QVector3D vtOrtho2  =  vt - glm::dot( vt, vn ) / ( glm::length( vn ) * glm::length( vn ) ) * vn;
//    const QVector3D vbOrtho21 =  vb - glm::dot( vb, vn ) / ( glm::length( vn ) * glm::length( vn ) ) * vn - glm::dot( vb, vtOrtho2 ) / ( glm::length( vtOrtho2 ) * glm::length( vtOrtho2 ) ) * vtOrtho2;
//    const QVector3D vbOrtho22 =  vb - glm::dot( vb, vn ) / ( glm::dot( vn, vn ) ) * vn - glm::dot( vb, vtOrtho2 ) / ( glm::dot( vtOrtho2, vtOrtho2 ) ) * vtOrtho2;
//    const QVector3D vnOrtho2  =  vn;

#ifdef THOROUGH_LOGGING_ENABLED
//    qDebug() << endl
//             << "vtOrtho   =" << vtOrtho << endl
//             << "vbOrtho   =" << vbOrtho << endl
//             << "vnOrtho   =" << vnOrtho << endl
//             << "-----------" << endl
//             << "vtOrtho1  =" << vtOrtho1 << endl
//             << "vbOrtho1  =" << vbOrtho1 << endl
//             << "vnOrtho1  =" << vnOrtho1 << endl
//             << "-----------" << endl
//             << "vtOrtho2  =" << vtOrtho2 << endl
//             << "vbOrtho21 =" << vbOrtho21 << endl
//             << "vbOrtho22 =" << vbOrtho22 << endl
//             << "vnOrtho2  =" << vnOrtho2;
#endif

    const glm::vec3 vtOrthoNorm = MathUtilities::safeNormalize( MathUtilities::glmFromQtVec( vtOrtho ) );
    const glm::vec3 vbOrthoNorm = MathUtilities::safeNormalize( MathUtilities::glmFromQtVec( vbOrtho ) );
    const glm::vec3 vnOrthoNorm = MathUtilities::safeNormalize( MathUtilities::glmFromQtVec( vnOrtho ) );

    const glm::vec3::value_type handedness = glm::determinant( glm::transpose( glm::mat3x3( vtOrthoNorm, vbOrthoNorm, vnOrthoNorm ) ) );
    const glm::vec3 vTorBOrthoNorm = MathUtilities::safeNormalize( requestTangent ? vtOrthoNorm : vbOrthoNorm );
    const QVector4D vTorBOrthoNormHand = MathUtilities::qtFromGlmVec( glm::vec4(
                                                                          vTorBOrthoNorm.x,
                                                                          vTorBOrthoNorm.y,
                                                                          vTorBOrthoNorm.z,
                                                                          handedness ) );
// #define USE_FAKE_TANGENTS
#ifdef USE_FAKE_TANGENTS
    static QList< QPair< QVector3D, QVector3D > > vertexPositionsTangents;
    if ( vertexPositionsTangents.isEmpty() )
    {
        vertexPositionsTangents
                << QPair< QVector3D, QVector3D >( QVector3D( -0.5, -0.5,  0.5 ), QVector3D(  0.5,  0.0,  0.5 ) )
                << QPair< QVector3D, QVector3D >( QVector3D(  0.5, -0.5,  0.5 ), QVector3D(  0.5,  0.0, -0.5 ) )
                << QPair< QVector3D, QVector3D >( QVector3D(  0.5, -0.5, -0.5 ), QVector3D( -0.5,  0.0, -0.5 ) )
                << QPair< QVector3D, QVector3D >( QVector3D( -0.5, -0.5, -0.5 ), QVector3D( -0.5,  0.0,  0.5 ) )

                << QPair< QVector3D, QVector3D >( QVector3D( -0.5, 0.5,  0.5 ),  QVector3D(  0.5,  0.0,  0.5 ) )
                << QPair< QVector3D, QVector3D >( QVector3D(  0.5, 0.5,  0.5 ),  QVector3D(  0.5,  0.0, -0.5 ) )
                << QPair< QVector3D, QVector3D >( QVector3D(  0.5, 0.5, -0.5 ),  QVector3D( -0.5,  0.0, -0.5 ) )
                << QPair< QVector3D, QVector3D >( QVector3D( -0.5, 0.5, -0.5 ),  QVector3D( -0.5,  0.0,  0.5 ) )

                << QPair< QVector3D, QVector3D >( QVector3D( 0.0, 0.75, 0.0 ),   QVector3D( 0.5,  0.0,  0.0 ) );
    }

    const int vertexPositionsTangentsCount = vertexPositionsTangents.size();
    for ( int i = 0; i < vertexPositionsTangentsCount; ++ i )
    {
        const QPair< QVector3D, QVector3D >& vertexPositionTangent = vertexPositionsTangents[ i ];
        if ( MathUtilities::epsilonEqual( vertexPositionTangent.first, v ) )
        {
            return QVector4D( vertexPositionTangent.second.x,
                              vertexPositionTangent.second.y,
                              vertexPositionTangent.second.z,
                              1.0 );
        }
    }

    return QVector4D( 0.0 );
#endif

#ifdef THOROUGH_LOGGING_ENABLED
    qDebug() << "========================" << endl
             << "    v(" << v << ')' << endl
             << "m:  t(" << vtOrthoNorm << ')' << endl
             << "m:  b(" << vbOrthoNorm << ')' << endl
             << "m:  h(" << handedness << ')' << endl
             << "------------------------";
#endif

    return vTorBOrthoNormHand;

#elif TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_1
    // Gram-Schmidt orthogonalization vector approach 1
    const QVector3D vtManualOrtho     = vtManual - vn * glm::dot( vn, vtManual );
    const QVector3D vbManualOrtho     = vbManual - vn * glm::dot( vn, vbManual ) - vtManualOrtho * glm::dot( vtManualOrtho, vbManual ) / glm::dot( vtManualOrtho, vtManualOrtho );

    const QVector3D vtManualOrthoNorm = MathUtilities::safeNormalize( vtManualOrtho );
    const QVector3D vbManualOrthoNorm = MathUtilities::safeNormalize( vbManualOrtho );

#elif TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_2
    // Gram-Schmidt orthogonalization vector approach 2
    const QVector3D vtManual2Ortho     = vtManual2 - vn * glm::dot( vn, vtManual2 );
    const QVector3D vbManual2Ortho     = vbManual2 - vn * glm::dot( vn, vbManual2 ) - vtManual2Ortho * glm::dot( vtManual2Ortho, vbManual2 ) / glm::dot( vtManual2Ortho, vtManual2Ortho );

    const QVector3D vtManual2OrthoNorm = MathUtilities::safeNormalize( vtManual2Ortho );
    const QVector3D vbManual2OrthoNorm = MathUtilities::safeNormalize( vbManual2Ortho );
#endif

#if TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_1
    // vector approach 1: calculate handedness
    const float handednessManual = ( glm::dot( glm::cross( vn, vtManualOrthoNorm ), vbManualOrthoNorm ) < 0.0f ) ? -1.0f : 1.0f;

    const QVector3D& vTorBManualOrthoNorm = requestTangent ? vtManualOrthoNorm : vbManualOrthoNorm;
    const QVector4D vTorBManualOrthoNormHand = glm::normalize( QVector4D(
                                                                   vTorBManualOrthoNorm.x,
                                                                   vTorBManualOrthoNorm.y,
                                                                   vTorBManualOrthoNorm.z,
                                                                   handednessManual ) );
#ifdef THOROUGH_LOGGING_ENABLED
    qDebug() << "v1: t(" << vtManualOrthoNorm << ')' << endl
             << "v1: b(" << vbManualOrthoNorm << ')' << endl
             << "v1: h(" << handednessManual << ')' << endl
             << "------------------------";
#endif

    return vTorBManualOrthoNormHand;

#elif TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_2
    // vector approach 2: calculate handedness
    const float handednessManual2 = ( glm::dot( glm::cross( vn, vtManual2OrthoNorm ), vbManual2OrthoNorm ) < 0.0f ) ? -1.0f : 1.0f;
    const QVector3D& vTorBManual2OrthoNorm = requestTangent ? vtManual2OrthoNorm : vbManual2OrthoNorm;
    const QVector4D vTorBManual2OrthoNormHand = glm::normalize( QVector4D(
                                                                    vTorBManual2OrthoNorm.x,
                                                                    vTorBManual2OrthoNorm.y,
                                                                    vTorBManual2OrthoNorm.z,
                                                                    handednessManual2 ) );
#ifdef THOROUGH_LOGGING_ENABLED
    qDebug() << "v2: t(" << vtManual2OrthoNorm << ')' << endl
             << "v2: b(" << vbManual2OrthoNorm << ')' << endl
             << "v2: h(" << handednessManual2 << ')' << endl
             << "--------------------------";
#endif

    return vTorBManual2OrthoNormHand;
#endif
}

#ifdef TEST_ORG_ALGORITHM
static const QList< QVector3D > allVertices =
        QList< QVector3D >()
        /* 0 */ << QVector3D( -0.5, -0.5,  0.5 ) // front bottom left
        /* 1 */ << QVector3D(  0.5, -0.5,  0.5 ) // front bottom right
        /* 2 */ << QVector3D(  0.5, -0.5, -0.5 ) // back bottom right
        /* 3 */ << QVector3D( -0.5, -0.5, -0.5 ) // back bottom left

        /* 4 */ << QVector3D( -0.5, 0.5,  0.5 ) // front top left
        /* 5 */ << QVector3D(  0.5, 0.5,  0.5 ) // front top right
        /* 6 */ << QVector3D(  0.5, 0.5, -0.5 ) // back top right
        /* 7 */ << QVector3D( -0.5, 0.5, -0.5 ) // back top left

        /* 8 */ << QVector3D( 0.0, 0.75, 0.0 ); // peak

static const QList< QVector2D > allVertices =
        QList< QVector2D >()
        /* 0 */ << QVector2D( -0.5, -0.5,  0.5 ) // front bottom left
        /* 1 */ << QVector2D(  0.5, -0.5,  0.5 ) // front bottom right
        /* 2 */ << QVector2D(  0.5, -0.5, -0.5 ) // back bottom right
        /* 3 */ << QVector2D( -0.5, -0.5, -0.5 ) // back bottom left

        /* 4 */ << QVector2D( -0.5, 0.5,  0.5 ) // front top left
        /* 5 */ << QVector2D(  0.5, 0.5,  0.5 ) // front top right
        /* 6 */ << QVector2D(  0.5, 0.5, -0.5 ) // back top right
        /* 7 */ << QVector2D( -0.5, 0.5, -0.5 ) // back top left

        /* 8 */ << QVector2D( 0.0, 0.75, 0.0 ); // peak


static const QList< glm::ivec3 > triangles =
        QList< glm::ivec3 >()
        // bottom
        << glm::ivec3( 3, 2, 0 )
        << glm::ivec3( 0, 2, 1 )

        // front
        << glm::ivec3( 0, 1, 4 )
        << glm::ivec3( 4, 1, 5 )

        // back
        << glm::ivec3( 2, 3, 6 )
        << glm::ivec3( 6, 3, 7 )

        // left
        << glm::ivec3( 3, 0, 7 )
        << glm::ivec3( 7, 0, 4 )

        // right
        << glm::ivec3( 1, 2, 5 )
        << glm::ivec3( 5, 2, 6 )

        // roof
        << glm::ivec3( 4, 5, 8 )
        << glm::ivec3( 5, 6, 8 )
        << glm::ivec3( 6, 7, 8 )
        << glm::ivec3( 7, 4, 8 );

struct Triangle
{
    ushort index[ 3 ];
};

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

QList< QVector4D > calculateTangents(
        const QList< QVector3D >& vertices,
        const QList< QVector3D >& normals,
        const QList< QVector2D >& texcoords,
        const QList< glm::ivec3 >& triangles )
{
    const int vertexCount = vertices.size();
    QList< QVector3D > tan1;
    QList< QVector3D > tan2;
    tan1.reserve( vertexCount );
    tan2.reserve( vertexCount );
    for ( int i = 0; i < vertexCount; ++ i )
    {
        tan1.append( QVector3D( 0.0f ) );
        tan2.append( QVector3D( 0.0f ) );
    }

    const int triangleCount = triangles.size();
    for ( int i = 0; i < triangleCount; ++ i )
    {
        const glm::ivec3& triangle = triangles[ i ];
        int i1 = triangle.x;
        int i2 = triangle.y;
        int i3 = triangle.z;

        const QVector3D& v1 = vertices[i1];
        const QVector3D& v2 = vertices[i2];
        const QVector3D& v3 = vertices[i3];

        const QVector2D& w1 = texcoords[i1];
        const QVector2D& w2 = texcoords[i2];
        const QVector2D& w3 = texcoords[i3];

        float x1 = v2.x - v1.x;
        float x2 = v3.x - v1.x;
        float y1 = v2.y - v1.y;
        float y2 = v3.y - v1.y;
        float z1 = v2.z - v1.z;
        float z2 = v3.z - v1.z;

        float s1 = w2.x - w1.x;
        float s2 = w3.x - w1.x;
        float t1 = w2.y - w1.y;
        float t2 = w3.y - w1.y;

        float r = 1.0F / (s1 * t2 - s2 * t1);
        QVector3D sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r,
                (t2 * z1 - t1 * z2) * r);
        QVector3D tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r,
                (s1 * z2 - s2 * z1) * r);

        tan1[i1] += sdir;
        tan1[i2] += sdir;
        tan1[i3] += sdir;

        tan2[i1] += tdir;
        tan2[i2] += tdir;
        tan2[i3] += tdir;
    }

    QList< QVector4D > tangents;
    for ( int i = 0; i < vertexCount; ++ i )
    {
        const QVector3D& n = normals[i];
        const QVector3D& t = tan1[i];

        // Gram-Schmidt orthogonalize
        const QVector3D tangentGeom = glm::normalize(t - n * glm::dot(n, t));

        // Calculate handedness
        const float handedness = (glm::dot(glm::cross(n, t), tan2[i]) < 0.0F) ? -1.0F : 1.0F;
        const QVector4D tangent( tangentGeom.x, tangentGeom.x, tangentGeom.x, handedness );
        tangents.append( tangent );
    }

    return tangents;
}
#endif
