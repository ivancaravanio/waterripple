#include "VertexData.h"

using namespace WaterRippleSimulator::Data;

VertexData::VertexData()
    : velocity{ 0.0f }
    , height0{ 0.0f }
    , height1{ 0.0f }
{
}

VertexData::VertexData( const QVector2D& aPos )
    : pos{ aPos }
    , velocity{ 0.0f }
    , height0{ 0.0f }
    , height1{ 0.0f }
{
}

VertexData::~VertexData()
{
}
