#include "MainWindow.h"

#include "MediaPlayerControl.h"
#include "VideoSurface.h"
#include "WaterRipplePresenter.h"
#include "WaterSettingsEditor.h"

#include <QApplication>
#include <QGridLayout>
#include <QPushButton>
#include <QSplitter>
#include <QVBoxLayout>
#include <QWidget>

using namespace WaterRippleSimulator;

MainWindow::MainWindow( QWidget* parent )
    : QWidget{ parent }
    , m_videoSurface{ nullptr }
    , m_waterDisplay{ nullptr }
    , m_waterSettingsEditor{ nullptr }
    , m_mediaPlayerControl{ nullptr }
{
    m_waterDisplay = new WaterRipplePresenter;

    m_videoSurface = new VideoSurface( this );
    connect( m_videoSurface,
             SIGNAL(frameReceived(QVideoFrame)),
             m_waterDisplay,
             SLOT(displayVideoFrame(QVideoFrame)) );

    m_mediaPlayerControl = new MediaPlayerControl;
    connect( m_mediaPlayerControl,
             SIGNAL(stopped()),
             m_waterDisplay,
             SLOT(clearVideoDisplay()) );

    m_mediaPlayerControl->setVideoOutput( m_videoSurface );

    m_waterSettingsEditor = new WaterSettingsEditor;
    m_waterSettingsEditor->layout()->setContentsMargins( QMargins() );
    connect( m_waterSettingsEditor,
             SIGNAL(dataChanged(WaterRippleSimulator::Data::WaterSettings::WaterProperties)),
             this,
             SLOT(applyWaterSettings()) );
    connect( m_waterSettingsEditor,
             SIGNAL(calmWaterRequested()),
             m_waterDisplay,
             SLOT(calmWater()) );
    connect( m_waterSettingsEditor,
             SIGNAL(resetOrientationRequested()),
             m_waterDisplay,
             SLOT(resetOrientation()) );
    connect( m_waterSettingsEditor,
             SIGNAL(focusOpenGlSceneRequested()),
             m_waterDisplay,
             SLOT(setFocus()) );

    QWidget* const settingsPaneContainer = new QWidget;
    QVBoxLayout* const settingsPaneContainerLayout = new QVBoxLayout( settingsPaneContainer );
    settingsPaneContainerLayout->setContentsMargins( QMargins() );
    settingsPaneContainerLayout->addWidget( m_waterSettingsEditor, 1 );

    QSplitter* const waterDisplaySplitter = new QSplitter( Qt::Vertical );
    waterDisplaySplitter->addWidget( m_waterDisplay );
    waterDisplaySplitter->addWidget( m_mediaPlayerControl );
    waterDisplaySplitter->setStretchFactor( 0, 1 );
    waterDisplaySplitter->setStretchFactor( 1, 0 );

    QWidget* const waterSettingsAlignmentHelper = new QWidget;
    QVBoxLayout* const waterSettingsAlignmentHelperLayout = new QVBoxLayout( waterSettingsAlignmentHelper );
    waterSettingsAlignmentHelperLayout->setContentsMargins( QMargins() );
    waterSettingsAlignmentHelperLayout->addWidget( settingsPaneContainer, 0, Qt::AlignTop );

    QSplitter* const mainSplitter = new QSplitter( Qt::Horizontal );
    mainSplitter->addWidget( waterDisplaySplitter );
    mainSplitter->addWidget( waterSettingsAlignmentHelper );
    mainSplitter->setStretchFactor( 0, 1 );
    mainSplitter->setStretchFactor( 1, 0 );

    QGridLayout* const mainLayout = new QGridLayout( this );
    mainLayout->addWidget( mainSplitter );

    connect( qApp, SIGNAL(paletteChanged(QPalette)), this, SLOT(resetSceneBackgroundColor()) );
    connect( m_waterDisplay,
             SIGNAL(fpsChanged(qreal)),
             m_waterSettingsEditor,
             SLOT(setFps(qreal)),
             Qt::QueuedConnection );
    connect( m_waterDisplay,
             SIGNAL(shouldPropagateWaterChanged(bool)),
             m_waterSettingsEditor,
             SLOT(setShouldPropagateWaves(bool)),
             Qt::QueuedConnection );
    this->resetSceneBackgroundColor();

    this->retranslate();
}

MainWindow::~MainWindow()
{
    m_mediaPlayerControl->setVideoOutput( static_cast< QAbstractVideoSurface* >( nullptr ) );
}

void MainWindow::setMediaUrl( const QUrl& aMediaUrl )
{
    m_mediaPlayerControl->setMediaUrl( aMediaUrl );
}

void MainWindow::setMediaFilePath( const QString& aMediaFilePath )
{
    m_mediaPlayerControl->setMediaFilePath( aMediaFilePath );
}

void MainWindow::retranslate()
{
    m_mediaPlayerControl->retranslate();
    m_waterSettingsEditor->retranslate();
}

void MainWindow::resetSceneBackgroundColor()
{
    const QPalette appPalette = QApplication::palette();
    m_waterDisplay->setSceneBackgroundColor( appPalette.color( QPalette::Window ) );
}

void MainWindow::applyWaterSettings()
{
    m_waterDisplay->setWaterSettings( m_waterSettingsEditor->waterSettings() );
}
