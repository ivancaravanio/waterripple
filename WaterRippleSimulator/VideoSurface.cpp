#include "VideoSurface.h"

#include <QString>
#include <QVideoSurfaceFormat>
#include <QApplication>

using namespace WaterRippleSimulator;

VideoSurface::VideoSurface( QObject* parent )
    : QAbstractVideoSurface( parent )
{
}

VideoSurface::~VideoSurface()
{
}

QList< QVideoFrame::PixelFormat > VideoSurface::supportedPixelFormats( QAbstractVideoBuffer::HandleType handleType ) const
{
    if ( handleType == QAbstractVideoBuffer::NoHandle )
    {
        return QList< QVideoFrame::PixelFormat >()
               << QVideoFrame::Format_RGB32
               << QVideoFrame::Format_ARGB32
               << QVideoFrame::Format_ARGB32_Premultiplied
               << QVideoFrame::Format_RGB565
               << QVideoFrame::Format_RGB555

               << QVideoFrame::Format_BGRA32
               << QVideoFrame::Format_BGRA32_Premultiplied
               << QVideoFrame::Format_BGR32
               << QVideoFrame::Format_BGR565
               << QVideoFrame::Format_BGR555;
    }

    return QList< QVideoFrame::PixelFormat >();
}

bool VideoSurface::isFormatSupported( const QVideoSurfaceFormat& format ) const
{
    if ( format.handleType() != QAbstractVideoBuffer::NoHandle
         || format.frameSize().isEmpty() )
    {
        return false;
    }

    qDebug() << "format.pixelFormat() =" << format.pixelFormat();

    const QImage::Format imageFormat = QVideoFrame::imageFormatFromPixelFormat( format.pixelFormat() );
    if ( imageFormat == QImage::Format_Invalid )
    {
        return false;
    }

    return true;
}

bool VideoSurface::start( const QVideoSurfaceFormat& format )
{
    if ( ! this->isFormatSupported( format ) )
    {
        return false;
    }

    return QAbstractVideoSurface::start( format );
}

void VideoSurface::stop()
{
    QAbstractVideoSurface::stop();
}

bool VideoSurface::present( const QVideoFrame& frame )
{
    const QVideoSurfaceFormat sf = this->surfaceFormat();
    if ( sf.pixelFormat() != frame.pixelFormat()
         || sf.frameSize() != frame.size() )
    {
        this->setError( QAbstractVideoSurface::IncorrectFormatError );
        this->stop();

        return false;
    }

    emit frameReceived( frame );

    return true;
}
