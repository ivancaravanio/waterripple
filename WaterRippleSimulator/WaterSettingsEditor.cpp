#include "WaterSettingsEditor.h"

#include "Utilities/MathUtilities.h"

#include <QCheckBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QFormLayout>
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>

using namespace WaterRippleSimulator;

using namespace WaterRippleSimulator::Utilities;

WaterSettingsEditor::WaterSettingsEditor( QWidget* parent )
    : QWidget{ parent }
    , m_meshSizeLabel{ nullptr }
    , m_meshWidthEditor{ nullptr }
    , m_xLabel{ nullptr }
    , m_meshHeightEditor{ nullptr }

    , m_drawAsWireframeLabel{ nullptr }
    , m_drawAsWireframeEditor{ nullptr }

    // wave propagation
    , m_waveSettingsGroupBox{ nullptr }

    , m_shouldPropagateWavesLabel{ nullptr }
    , m_shouldPropagateWavesEditor{ nullptr }

    , m_wavePropagationMethodLabel{ nullptr }
    , m_wavePropagationMethodPicker{ nullptr }

    , m_wavePropagationSpeedLabel{ nullptr }
    , m_wavePropagationSpeedEditor{ nullptr }

    // velocity scaling {see comments at the bottom}
    , m_waveHeightDampingOverTimeFactorLabel{ nullptr }
    , m_waveHeightDampingOverTimeFactorEditor{ nullptr }

    // clamping {see comments at the bottom}
    , m_waveHeightClampingFactorLabel{ nullptr }
    , m_waveHeightClampingFactorEditor{ nullptr }

    // water ripple
    , m_waterRippleSettingsGroupBox{ nullptr }

    , m_waterRippleMethodLabel{ nullptr }
    , m_waterRippleMethodPicker{ nullptr }

    , m_waterRippleMagnitudeLabel{ nullptr }
    , m_waterRippleMagnitudeEditor{ nullptr }

    , m_waterRippleXRadiusFactorLabel{ nullptr }
    , m_waterRippleXRadiusFactorEditor{ nullptr }
    , m_waterRippleYRadiusFactorLabel{ nullptr }
    , m_waterRippleYRadiusFactorEditor{ nullptr }

    , m_waterRippleRotationAngleLabel{ nullptr }
    , m_waterRippleRotationAngleEditor{ nullptr }

    // rain
    , m_rainSettingsGroupBox{ nullptr }

    , m_shouldSimulateRainLabel{ nullptr }
    , m_shouldSimulateRainEditor{ nullptr }

    , m_rainDropsFallTimeIntervalLabel{ nullptr }
    , m_rainDropsFallTimeIntervalEditor{ nullptr }

    , m_rainDropsCountPerFallLabel{ nullptr }
    , m_rainDropsCountPerFallEditor{ nullptr }

    , m_shouldShowCoordSysIndicatorLabel{ nullptr }
    , m_shouldShowCoordSysIndicatorEditor{ nullptr }

    , m_applyChangesButton{ nullptr }
    , m_setDefaultSettingsButton{ nullptr }

    , m_fpsLabel{ nullptr }
    , m_fpsDisplay{ nullptr }
    , m_fps{ 0.0 }

    , m_calmWaterButton{ nullptr }
    , m_resetOrientationButton{ nullptr }
    , m_focusOpenGlSceneButton{ nullptr }
{
    m_settingsGroupBox = new QGroupBox;

    m_meshSizeLabel = new QLabel;
    m_meshWidthEditor = WaterSettingsEditor::meshDimensionEditor();

    m_xLabel = new QLabel;
    m_xLabel->setAlignment( Qt::AlignCenter );

    m_meshHeightEditor = WaterSettingsEditor::meshDimensionEditor();

    m_drawAsWireframeLabel = new QLabel;
    m_drawAsWireframeEditor = new QCheckBox;

    m_waveSettingsGroupBox = new QGroupBox;

    m_shouldPropagateWavesLabel = new QLabel;
    m_shouldPropagateWavesEditor = new QCheckBox;

    m_wavePropagationMethodLabel = new QLabel;
    m_wavePropagationMethodPicker = new QComboBox;
    this->repopulateWavePropagationMethodOptions();

    m_wavePropagationSpeedLabel = new QLabel;
    m_wavePropagationSpeedEditor = WaterSettingsEditor::realNumberEditor( WaterSettings::s_wavePropagationSpeedMin,
                                                                          WaterSettings::s_wavePropagationSpeedMax );

    m_waveHeightDampingOverTimeFactorLabel = new QLabel;
    m_waveHeightDampingOverTimeFactorEditor = WaterSettingsEditor::realNumberEditor( WaterSettings::s_waveHeightDampingOverTimeFactorMin,
                                                                                     WaterSettings::s_waveHeightDampingOverTimeFactorMax,
                                                                                     1E-6f,
                                                                                     6 );

    m_waveHeightClampingFactorLabel = new QLabel;
    m_waveHeightClampingFactorEditor = WaterSettingsEditor::realNumberEditor( WaterSettings::s_waveHeightClampingFactorMin,
                                                                              WaterSettings::s_waveHeightClampingFactorMax );

    m_waterRippleSettingsGroupBox = new QGroupBox;

    m_waterRippleMethodLabel = new QLabel;
    m_waterRippleMethodPicker = new QComboBox;
    this->repopulateWaterRippleMethodOptions();

    m_waterRippleMagnitudeLabel = new QLabel;
    m_waterRippleMagnitudeEditor = WaterSettingsEditor::realNumberEditor( WaterSettings::s_waterRippleMagnitudeMin,
                                                                          WaterSettings::s_waterRippleMagnitudeMax );

    m_waterRippleXRadiusFactorLabel = new QLabel;
    m_waterRippleXRadiusFactorEditor = WaterSettingsEditor::waterRippleRadiusEditor();
    m_waterRippleYRadiusFactorLabel = new QLabel;
    m_waterRippleYRadiusFactorEditor = WaterSettingsEditor::waterRippleRadiusEditor();

    m_waterRippleRotationAngleLabel = new QLabel;
    m_waterRippleRotationAngleEditor = WaterSettingsEditor::realNumberEditor( 0.0f,
                                                                              360.0f,
                                                                              1E-1f,
                                                                              1 );

    m_rainSettingsGroupBox = new QGroupBox;

    m_shouldSimulateRainLabel = new QLabel;
    m_shouldSimulateRainEditor = new QCheckBox;

    m_rainDropsFallTimeIntervalLabel = new QLabel;
    m_rainDropsFallTimeIntervalEditor = WaterSettingsEditor::intNumberEditor(
                                            WaterSettings::s_rainDropsFallTimeIntervalMin,
                                            WaterSettings::s_rainDropsFallTimeIntervalMax );

    m_rainDropsCountPerFallLabel = new QLabel;
    m_rainDropsCountPerFallEditor = WaterSettingsEditor::intNumberEditor(
                                        WaterSettings::s_rainDropsCountPerFallMin,
                                        WaterSettings::s_rainDropsCountPerFallMax );

    m_shouldShowCoordSysIndicatorLabel = new QLabel;
    m_shouldShowCoordSysIndicatorEditor = new QCheckBox;

    m_applyChangesButton = new QPushButton;
    connect( m_applyChangesButton, SIGNAL(clicked()), this, SLOT(applyEditedData()) );

    m_setDefaultSettingsButton = new QPushButton;
    connect( m_setDefaultSettingsButton, SIGNAL(clicked()), this, SLOT(setDefaultSettingsInteractively()) );

    m_fpsLabel = new QLabel;
    m_fpsDisplay = new QLabel;

    m_calmWaterButton = new QPushButton;
    connect( m_calmWaterButton, SIGNAL(clicked()), this, SIGNAL(calmWaterRequested()) );

    m_resetOrientationButton = new QPushButton;
    connect( m_resetOrientationButton, SIGNAL(clicked()), this, SIGNAL(resetOrientationRequested()) );

    m_focusOpenGlSceneButton = new QPushButton;
    connect( m_focusOpenGlSceneButton, SIGNAL(clicked()), this, SIGNAL(focusOpenGlSceneRequested()) );

    int layoutRowIndex = 0;

    QGridLayout* const settingsGroupBoxLayout = new QGridLayout( m_settingsGroupBox );
    settingsGroupBoxLayout->addWidget( m_meshSizeLabel,    layoutRowIndex, 0, 1, 1 );
    settingsGroupBoxLayout->addWidget( m_meshWidthEditor,  layoutRowIndex, 1, 1, 1 );
    settingsGroupBoxLayout->addWidget( m_xLabel,           layoutRowIndex, 2, 1, 1, Qt::AlignCenter );
    settingsGroupBoxLayout->addWidget( m_meshHeightEditor, layoutRowIndex, 3, 1, 1 );

    ++ layoutRowIndex;

    settingsGroupBoxLayout->addWidget( m_drawAsWireframeLabel,  layoutRowIndex, 0 );
    settingsGroupBoxLayout->addWidget( m_drawAsWireframeEditor, layoutRowIndex, 1, 1, 3 );

    QFormLayout* const waveSettingsGroupBoxLayout = new QFormLayout( m_waveSettingsGroupBox );
    waveSettingsGroupBoxLayout->addRow( m_shouldPropagateWavesLabel, m_shouldPropagateWavesEditor );
    waveSettingsGroupBoxLayout->addRow( m_wavePropagationMethodLabel, m_wavePropagationMethodPicker );
    waveSettingsGroupBoxLayout->addRow( m_wavePropagationSpeedLabel, m_wavePropagationSpeedEditor );
    waveSettingsGroupBoxLayout->addRow( m_waveHeightDampingOverTimeFactorLabel, m_waveHeightDampingOverTimeFactorEditor );
    waveSettingsGroupBoxLayout->addRow( m_waveHeightClampingFactorLabel, m_waveHeightClampingFactorEditor );

    QFormLayout* const waterRippleSettingsGroupBoxLayout = new QFormLayout( m_waterRippleSettingsGroupBox );
    waterRippleSettingsGroupBoxLayout->addRow( m_waterRippleMethodLabel, m_waterRippleMethodPicker );
    waterRippleSettingsGroupBoxLayout->addRow( m_waterRippleMagnitudeLabel, m_waterRippleMagnitudeEditor );
    waterRippleSettingsGroupBoxLayout->addRow( m_waterRippleXRadiusFactorLabel, m_waterRippleXRadiusFactorEditor );
    waterRippleSettingsGroupBoxLayout->addRow( m_waterRippleYRadiusFactorLabel, m_waterRippleYRadiusFactorEditor );
    waterRippleSettingsGroupBoxLayout->addRow( m_waterRippleRotationAngleLabel, m_waterRippleRotationAngleEditor );

    QFormLayout* const rainSettingsGroupBoxLayout = new QFormLayout( m_rainSettingsGroupBox );
    rainSettingsGroupBoxLayout->addRow( m_shouldSimulateRainLabel, m_shouldSimulateRainEditor );
    rainSettingsGroupBoxLayout->addRow( m_rainDropsFallTimeIntervalLabel, m_rainDropsFallTimeIntervalEditor );
    rainSettingsGroupBoxLayout->addRow( m_rainDropsCountPerFallLabel, m_rainDropsCountPerFallEditor );

    settingsGroupBoxLayout->addWidget( m_waveSettingsGroupBox,              ++ layoutRowIndex, 0, 1, 4 );
    settingsGroupBoxLayout->addWidget( m_waterRippleSettingsGroupBox,       ++ layoutRowIndex, 0, 1, 4 );
    settingsGroupBoxLayout->addWidget( m_rainSettingsGroupBox,              ++ layoutRowIndex, 0, 1, 4 );
    settingsGroupBoxLayout->addWidget( m_shouldShowCoordSysIndicatorLabel,  ++ layoutRowIndex, 0, 1, 1 );
    settingsGroupBoxLayout->addWidget( m_shouldShowCoordSysIndicatorEditor,    layoutRowIndex, 1, 1, 3 );

    ++ layoutRowIndex;

    settingsGroupBoxLayout->addWidget( m_applyChangesButton, layoutRowIndex, 0, 1, 4 );

    ++ layoutRowIndex;

    settingsGroupBoxLayout->addWidget( m_setDefaultSettingsButton, layoutRowIndex, 0, 1, 4 );

    settingsGroupBoxLayout->setColumnStretch( 0, 0 );
    settingsGroupBoxLayout->setColumnStretch( 1, 1 );
    settingsGroupBoxLayout->setColumnStretch( 2, 0 );
    settingsGroupBoxLayout->setColumnStretch( 3, 1 );

    layoutRowIndex = 0;
    QGridLayout* const mainLayout = new QGridLayout( this );
    mainLayout->addWidget( m_settingsGroupBox, layoutRowIndex, 0, 1, 2 );

    ++ layoutRowIndex;

    mainLayout->addWidget( m_fpsLabel,   layoutRowIndex, 0 );
    mainLayout->addWidget( m_fpsDisplay, layoutRowIndex, 1 );

    ++ layoutRowIndex;

    mainLayout->addWidget( m_calmWaterButton, layoutRowIndex, 0, 1, 2 );

    ++ layoutRowIndex;

    mainLayout->addWidget( m_resetOrientationButton, layoutRowIndex, 0, 1, 2 );

    ++ layoutRowIndex;

    mainLayout->addWidget( m_focusOpenGlSceneButton, layoutRowIndex, 0, 1, 2 );

    this->retranslate();

    this->updateFpsDisplay();
    this->updateSettingsUi();
}

WaterSettingsEditor::~WaterSettingsEditor()
{
}

void WaterSettingsEditor::setMeshSize( const QSize& aMeshSize )
{
    this->setMeshSize( aMeshSize, true );
}

QSize WaterSettingsEditor::meshSize() const
{
    return QSize( m_meshWidthEditor->value(), m_meshHeightEditor->value() );
}

void WaterSettingsEditor::setShouldDrawAsWireframe( const bool aShouldDrawAsWireframe )
{
    this->setShouldDrawAsWireframe( aShouldDrawAsWireframe, true );
}

bool WaterSettingsEditor::shouldDrawAsWireframe() const
{
    return m_drawAsWireframeEditor->isChecked();
}

void WaterSettingsEditor::setWavePropagationMethod(
        const WaterSettings::WavePropagationMethod aWavePropagationMethod )
{
    this->setWavePropagationMethod( aWavePropagationMethod, true );
}

WaterSettings::WavePropagationMethod WaterSettingsEditor::wavePropagationMethod() const
{
    const int methodIndex = m_wavePropagationMethodPicker->currentIndex();
    if ( methodIndex < 0
         || methodIndex >= m_wavePropagationMethodPicker->count() )
    {
        return WaterSettings::WavePropagationMethodInvalid;
    }

    bool converted = false;
    const int method = m_wavePropagationMethodPicker->itemData( methodIndex ).toInt( & converted );
    return converted
           && WaterSettings::isWavePropagationMethodValid( method )
           ? static_cast< WaterSettings::WavePropagationMethod >( method )
           : WaterSettings::WavePropagationMethodInvalid;
}

void WaterSettingsEditor::setWaterRippleMagnitude( const float aWaterRippleMagnitude )
{
    this->setWaterRippleMagnitude( aWaterRippleMagnitude, true );
}

float WaterSettingsEditor::waterRippleMagnitude() const
{
    return m_waterRippleMagnitudeEditor->value();
}

void WaterSettingsEditor::setWaterRippleRadiusFactor( const QVector2D& aWaterRippleRadiusFactor )
{
    this->setWaterRippleRadiusFactor( aWaterRippleRadiusFactor, true );
}

QVector2D WaterSettingsEditor::waterRippleRadiusFactor() const
{
    return QVector2D{
                static_cast< float >( m_waterRippleXRadiusFactorEditor->value() ),
                static_cast< float >( m_waterRippleYRadiusFactorEditor->value() ) };
}

void WaterSettingsEditor::setWaterRippleRotationAngle( const float aWaterRippleRotationAngle )
{
    this->setWaterRippleRotationAngle( aWaterRippleRotationAngle, true );
}

float WaterSettingsEditor::waterRippleRotationAngle() const
{
    return m_waterRippleRotationAngleEditor->value();
}

void WaterSettingsEditor::setWavePropagationSpeed( const float aWavePropagationSpeed )
{
    this->setWavePropagationSpeed( aWavePropagationSpeed, true );
}

float WaterSettingsEditor::wavePropagationSpeed() const
{
    return m_wavePropagationSpeedEditor->value();
}

void WaterSettingsEditor::setFps( const qreal& aFps )
{
    if ( ( aFps < 0.0 && ! qFuzzyIsNull( aFps ) )
         || qFuzzyCompare( aFps, m_fps ) )
    {
        return;
    }

    m_fps = aFps;
    this->updateFpsDisplay();
}

qreal WaterSettingsEditor::fps() const
{
    return m_fps;
}

void WaterSettingsEditor::setWaterSettings( const WaterSettings& aWaterSettings )
{
    m_waterSettings = aWaterSettings;
    this->updateSettingsUi();
}

WaterSettings WaterSettingsEditor::waterSettings() const
{
    return m_waterSettings;
}

void WaterSettingsEditor::setWaveHeightDampingOverTimeFactor( const float aWaveHeightDampingOverTimeFactor )
{
    this->setWaveHeightDampingOverTimeFactor( aWaveHeightDampingOverTimeFactor, true );
}

float WaterSettingsEditor::waveHeightDampingOverTimeFactor() const
{
    return m_waveHeightDampingOverTimeFactorEditor->value();
}

void WaterSettingsEditor::setWaveHeightClampingFactor( const float aWaveHeightClampingFactor )
{
    this->setWaveHeightClampingFactor( aWaveHeightClampingFactor, true );
}

float WaterSettingsEditor::waveHeightClampingFactor() const
{
    return m_waveHeightClampingFactorEditor->value();
}

void WaterSettingsEditor::setWaterRippleMethod( const WaterSettings::WaterRippleMethod aWaterRippleMethod )
{
    this->setWaterRippleMethod( aWaterRippleMethod, true );
}

void WaterSettingsEditor::setWaterRippleMethod( const WaterSettings::WaterRippleMethod aWaterRippleMethod, const bool shouldUpdateData )
{
    if ( ! WaterSettings::isWaterRippleMethodValid( aWaterRippleMethod )
         || aWaterRippleMethod == this->waterRippleMethod() )
    {
        return;
    }

    const int methodIndex = m_waterRippleMethodPicker->findData( static_cast< int >( aWaterRippleMethod ) );
    if ( methodIndex < 0 )
    {
        return;
    }

    m_waterRippleMethodPicker->setCurrentIndex( methodIndex );

    if ( shouldUpdateData )
    {
        m_waterSettings.setWaterRippleMethod( aWaterRippleMethod );
    }
}

WaterSettings::WaterRippleMethod WaterSettingsEditor::waterRippleMethod() const
{
    const int methodIndex = m_waterRippleMethodPicker->currentIndex();
    if ( methodIndex < 0
         || methodIndex >= m_waterRippleMethodPicker->count() )
    {
        return WaterSettings::WaterRippleMethodInvalid;
    }

    bool converted = false;
    const int method = m_waterRippleMethodPicker->itemData( methodIndex ).toInt( & converted );
    return converted
           && WaterSettings::isWaterRippleMethodValid( method )
           ? static_cast< WaterSettings::WaterRippleMethod >( method )
           : WaterSettings::WaterRippleMethodInvalid;
}

void WaterSettingsEditor::setShouldPropagateWaves( const bool aShouldPropagateWaves )
{
    this->setShouldPropagateWaves( aShouldPropagateWaves, true );
}

bool WaterSettingsEditor::shouldPropagateWaves() const
{
    return m_shouldPropagateWavesEditor->isChecked();
}

void WaterSettingsEditor::setShouldSimulateRain( const bool aShouldSimulateRain )
{
    this->setShouldSimulateRain( aShouldSimulateRain, true );
}

bool WaterSettingsEditor::shouldSimulateRain() const
{
    return m_shouldSimulateRainEditor->isChecked();
}

void WaterSettingsEditor::setRainDropsFallTimeInterval( const int aRainDropsFallTimeInterval )
{
    this->setRainDropsFallTimeInterval( aRainDropsFallTimeInterval, true );
}

int WaterSettingsEditor::rainDropsTimeFallInterval() const
{
    return m_rainDropsFallTimeIntervalEditor->value();
}

void WaterSettingsEditor::setRainDropsCountPerFall( const int aRainDropsCountPerFall )
{
    this->setRainDropsCountPerFall( aRainDropsCountPerFall, true );
}

int WaterSettingsEditor::rainDropsCountPerFall() const
{
    return m_rainDropsCountPerFallEditor->value();
}

void WaterSettingsEditor::setShouldShowCoordinateSystemIndicator( const bool aShouldShowCoordinateSystemIndicator )
{
    this->setShouldShowCoordinateSystemIndicator( aShouldShowCoordinateSystemIndicator, true );
}

bool WaterSettingsEditor::shouldShowCoordinateSystemIndicator() const
{
    return m_shouldShowCoordSysIndicatorEditor->isChecked();
}

void WaterSettingsEditor::retranslate()
{
    m_settingsGroupBox->setTitle( tr( "Settings" ) );

    // mesh
    m_meshSizeLabel->setText( tr( "Mesh size:" ) );
    m_xLabel->setText( tr( "x" ) );
    m_drawAsWireframeLabel->setText( tr( "Draw as wireframe:" ) );

    // wave propagation
    m_waveSettingsGroupBox->setTitle( tr( "Wave" ) );

    m_shouldPropagateWavesLabel->setText( tr( "Propagate:" ) );

    m_wavePropagationMethodLabel->setText( tr( "Method:" ) );
    this->repopulateWavePropagationMethodOptions();

    m_wavePropagationSpeedLabel->setText( tr( "Speed:" ) );
    m_waveHeightDampingOverTimeFactorLabel->setText( tr( "Height damping over time factor:" ) );
    m_waveHeightClampingFactorLabel->setText( tr( "Height clamping factor:" ) );

    // water ripple
    m_waterRippleSettingsGroupBox->setTitle( tr( "Water Ripple" ) );

    m_waterRippleMethodLabel->setText( tr( "Method:" ) );
    this->repopulateWaterRippleMethodOptions();

    m_waterRippleMagnitudeLabel->setText( tr( "Magnitude:" ) );
    m_waterRippleXRadiusFactorLabel->setText( tr( "X Radius:" ) );
    m_waterRippleYRadiusFactorLabel->setText( tr( "Y Radius:" ) );
    m_waterRippleRotationAngleLabel->setText( tr( "Angle:" ) );

    // rain
    m_rainSettingsGroupBox->setTitle( tr( "Rain" ) );
    m_shouldSimulateRainLabel->setText( tr( "Simulate:" ) );
    m_rainDropsFallTimeIntervalLabel->setText( tr( "Drops time interval:" ) );
    m_rainDropsCountPerFallLabel->setText( tr( "Drops count per fall:" ) );

    m_shouldShowCoordSysIndicatorLabel->setText( tr( "Show coordinate system:" ) );
    m_fpsLabel->setText( tr( "FPS:" ) );
    m_applyChangesButton->setText( tr( "Apply" ) );
    m_setDefaultSettingsButton->setText( tr( "Set Defaults" ) );
    m_calmWaterButton->setText( tr( "Calm Water" ) );
    m_resetOrientationButton->setText( tr( "Reset Orientation" ) );
    m_focusOpenGlSceneButton->setText( tr( "Focus OpenGL Scene" ) );
}

void WaterSettingsEditor::applyEditedData()
{
    const WaterSettings editedWaterSettings = this->editedWaterSettings();
    const WaterSettings::WaterProperties changedWaterProperties = editedWaterSettings.diff( m_waterSettings );
    if ( changedWaterProperties == WaterSettings::WaterPropertyNone )
    {
        return;
    }

    m_waterSettings = editedWaterSettings;

    emit dataChanged( changedWaterProperties );
}

void WaterSettingsEditor::setDefaultSettingsInteractively()
{
    const WaterSettings defaultWaterSettings;
    const WaterSettings::WaterProperties modifiedWaterProperties = m_waterSettings.diff( defaultWaterSettings );
    this->setWaterSettings( defaultWaterSettings );

    emit dataChanged( modifiedWaterProperties );
}

QString WaterSettingsEditor::waterRippleMethodDescription( const int waterRippleMethod )
{
    switch ( waterRippleMethod )
    {
        case WaterSettings::WaterRippleMethodCircularTrigonometric:
            return tr( "Circular Trigonometric" );
        case WaterSettings::WaterRippleMethodEllipticTrigonometric:
            return tr( "Elliptic Trigonometric" );
        case WaterSettings::WaterRippleMethodEllipticGaussian:
            return tr( "Elliptic Gaussian" );
    }

    return QString();
}

QString WaterSettingsEditor::wavePropagationMethodDescription( const int wavePropagationMethod )
{
    switch ( wavePropagationMethod )
    {
        case WaterSettings::WavePropagationMethodCpu:
            return tr( "CPU" );
        case WaterSettings::WavePropagationMethodGpu:
            return tr( "GPU triple pass" );
        case WaterSettings::WavePropagationMethodGpuPrecomputedNormalsVertices:
            return tr( "GPU quadruple pass" );
    }

    return QString();
}

void WaterSettingsEditor::updateSettingsUi( const WaterSettings::WaterProperties waterProperties )
{
    if ( waterProperties.testFlag( WaterSettings::WaterPropertyMeshSize ) )
    {
        this->setMeshSize( m_waterSettings.meshSize(), false );
    }

    if ( waterProperties.testFlag( WaterSettings::WaterPropertyShouldDrawAsWireframe ) )
    {
        this->setShouldDrawAsWireframe( m_waterSettings.shouldDrawAsWireframe(), false );
    }

    // wave propagation
    if ( waterProperties.testFlag( WaterSettings::WaterPropertyShouldPropagateWaves ) )
    {
        this->setShouldPropagateWaves( m_waterSettings.shouldPropagateWaves(), false );
    }

    if ( waterProperties.testFlag( WaterSettings::WaterPropertyWavePropagationMethod ) )
    {
        this->setWavePropagationMethod( m_waterSettings.wavePropagationMethod(), false );
    }

    if ( waterProperties.testFlag( WaterSettings::WaterPropertyWavePropagationSpeed ) )
    {
        this->setWavePropagationSpeed( m_waterSettings.wavePropagationSpeed(), false );
    }

    if ( waterProperties.testFlag( WaterSettings::WaterPropertyWaveHeightDampingOverTimeFactor ) )
    {
        this->setWaveHeightDampingOverTimeFactor( m_waterSettings.waveHeightDampingOverTimeFactor(), false );
    }

    if ( waterProperties.testFlag( WaterSettings::WaterPropertyWaveHeightClampingFactor ) )
    {
        this->setWaveHeightClampingFactor( m_waterSettings.waveHeightClampingFactor(), false );
    }

    // water ripple
    if ( waterProperties.testFlag( WaterSettings::WaterPropertyWaterRippleMethod ) )
    {
        this->setWaterRippleMethod( m_waterSettings.waterRippleMethod(), false );
    }

    if ( waterProperties.testFlag( WaterSettings::WaterPropertyWaterRippleMagnitude ) )
    {
        this->setWaterRippleMagnitude( m_waterSettings.waterRippleMagnitude(), false );
    }

    if ( waterProperties.testFlag( WaterSettings::WaterPropertyWaterRippleRadiusFactor ) )
    {
        this->setWaterRippleRadiusFactor( m_waterSettings.waterRippleRadiusFactor(), false );
    }

    if ( waterProperties.testFlag( WaterSettings::WaterPropertyWaterRippleRotationAngle ) )
    {
        this->setWaterRippleRotationAngle( m_waterSettings.waterRippleRotationAngle(), false );
    }

    // rain
    if ( waterProperties.testFlag( WaterSettings::WaterPropertyShouldSimulateRain ) )
    {
        this->setShouldSimulateRain( m_waterSettings.shouldSimulateRain(), false );
    }

    if ( waterProperties.testFlag( WaterSettings::WaterPropertyRainDropsFallTimeInterval ) )
    {
        this->setRainDropsFallTimeInterval( m_waterSettings.rainDropsFallTimeInterval(), false );
    }

    if ( waterProperties.testFlag( WaterSettings::WaterPropertyRainDropsCountPerFall ) )
    {
        this->setRainDropsCountPerFall( m_waterSettings.rainDropsCountPerFall(), false );
    }

    // other
    if ( waterProperties.testFlag( WaterSettings::WaterPropertyShouldShowCoordinateSystemIndicator ) )
    {
        this->setShouldShowCoordinateSystemIndicator( m_waterSettings.shouldShowCoordinateSystemIndicator(), false );
    }
}

void WaterSettingsEditor::updateFpsDisplay()
{
    m_fpsDisplay->setText( QString::number( m_fps, 'f', 2 ) );
}

void WaterSettingsEditor::repopulateWaterRippleMethodOptions()
{
    if ( m_wavePropagationMethodPicker == nullptr )
    {
        return;
    }

    m_waterRippleMethodPicker->clear();
    for ( int i = 0; i < WaterSettings::WaterRippleMethodsCount; ++ i )
    {
        m_waterRippleMethodPicker->addItem( WaterSettingsEditor::waterRippleMethodDescription( i ), i );
    }

    this->updateSettingsUi( WaterSettings::WaterPropertyWaterRippleMethod );
}

void WaterSettingsEditor::repopulateWavePropagationMethodOptions()
{
    if ( m_wavePropagationMethodPicker == nullptr )
    {
        return;
    }

    m_wavePropagationMethodPicker->clear();
    for ( int i = 0; i < WaterSettings::WavePropagationMethodsCount; ++ i )
    {
        m_wavePropagationMethodPicker->addItem( WaterSettingsEditor::wavePropagationMethodDescription( i ), i );
    }

    this->updateSettingsUi( WaterSettings::WaterPropertyWavePropagationMethod );
}

void WaterSettingsEditor::setMeshSize( const QSize& aMeshSize, const bool shouldUpdateData )
{
    if ( ! WaterSettings::isMeshSizeValid( aMeshSize )
         || aMeshSize == this->meshSize() )
    {
        return;
    }

    const int w = aMeshSize.width();
    if ( w != m_meshWidthEditor->value() )
    {
        m_meshWidthEditor->setValue( w );
    }

    const int h = aMeshSize.width();
    if ( h != m_meshHeightEditor->value() )
    {
        m_meshHeightEditor->setValue( h );
    }

    if ( shouldUpdateData )
    {
        m_waterSettings.setMeshSize( aMeshSize );
    }
}

void WaterSettingsEditor::setShouldDrawAsWireframe( const bool aShouldDrawAsWireframe, const bool shouldUpdateData )
{
    m_drawAsWireframeEditor->setChecked( aShouldDrawAsWireframe );

    if ( shouldUpdateData )
    {
        m_waterSettings.setShouldDrawAsWireframe( aShouldDrawAsWireframe );
    }
}

void WaterSettingsEditor::setWavePropagationMethod( const WaterSettings::WavePropagationMethod aWavePropagationMethod, const bool shouldUpdateData )
{
    if ( ! WaterSettings::isWavePropagationMethodValid( aWavePropagationMethod )
         || aWavePropagationMethod == this->wavePropagationMethod() )
    {
        return;
    }

    const int methodIndex = m_wavePropagationMethodPicker->findData( static_cast< int >( aWavePropagationMethod ) );
    if ( methodIndex < 0 )
    {
        return;
    }

    m_wavePropagationMethodPicker->setCurrentIndex( methodIndex );

    if ( shouldUpdateData )
    {
        m_waterSettings.setWavePropagationMethod( aWavePropagationMethod );
    }
}

void WaterSettingsEditor::setWaterRippleMagnitude( const float aWaterRippleMagnitude, const bool shouldUpdateData )
{
    if ( ! WaterSettings::isWaterRippleMagnitudeValid( aWaterRippleMagnitude )
         || qFuzzyCompare( static_cast< double >( aWaterRippleMagnitude ), m_waterRippleMagnitudeEditor->value() ) )
    {
        return;
    }

    m_waterRippleMagnitudeEditor->setValue( aWaterRippleMagnitude );

    if ( shouldUpdateData )
    {
        m_waterSettings.setWaterRippleMagnitude( aWaterRippleMagnitude );
    }
}

void WaterSettingsEditor::setWaterRippleRadiusFactor( const QVector2D& aWaterRippleRadiusFactor, const bool shouldUpdateData )
{
    if ( ! WaterSettings::isWaterRippleRadiusFactorValid( aWaterRippleRadiusFactor ) )
    {
        return;
    }

    bool isDifferent = false;
    const QVector2D waterRippleRadiusFactor = this->waterRippleRadiusFactor();
    if ( ! qFuzzyCompare( aWaterRippleRadiusFactor.x(), waterRippleRadiusFactor.x() ) )
    {
        m_waterRippleXRadiusFactorEditor->setValue( aWaterRippleRadiusFactor.x() );
        isDifferent = true;
    }

    if ( ! qFuzzyCompare( aWaterRippleRadiusFactor.y(), waterRippleRadiusFactor.y() ) )
    {
        m_waterRippleYRadiusFactorEditor->setValue( aWaterRippleRadiusFactor.y() );
        isDifferent = true;
    }

    if ( isDifferent && shouldUpdateData )
    {
        m_waterSettings.setWaterRippleRadiusFactor( aWaterRippleRadiusFactor );
    }
}

void WaterSettingsEditor::setWaterRippleRotationAngle( const float aWaterRippleRotationAngle, const bool shouldUpdateData )
{
    const float normalizedAngle = MathUtilities::to360DegreesAngle( aWaterRippleRotationAngle );
    if ( ! WaterSettings::isWaterRippleRotationAngleValid( normalizedAngle )
         || qFuzzyCompare( static_cast< double >( normalizedAngle ), m_waterRippleRotationAngleEditor->value() ) )
    {
        return;
    }

    m_waterRippleRotationAngleEditor->setValue( normalizedAngle );
    if ( shouldUpdateData )
    {
        m_waterSettings.setWaterRippleRotationAngle( normalizedAngle );
    }
}

void WaterSettingsEditor::setWavePropagationSpeed( const float aWavePropagationSpeed, const bool shouldUpdateData )
{
    if ( ! WaterSettings::isWavePropagationSpeedValid( aWavePropagationSpeed )
         || qFuzzyCompare( static_cast< double >( aWavePropagationSpeed ), m_wavePropagationSpeedEditor->value() ) )
    {
        return;
    }

    m_wavePropagationSpeedEditor->setValue( aWavePropagationSpeed );
    if ( shouldUpdateData )
    {
        m_waterSettings.setWavePropagationSpeed( aWavePropagationSpeed );
    }
}

void WaterSettingsEditor::setWaveHeightDampingOverTimeFactor( const float aWaveHeightDampingOverTimeFactor, const bool shouldUpdateData )
{
    if ( ! WaterSettings::isWaveHeightDampingOverTimeFactorValid( aWaveHeightDampingOverTimeFactor )
         || qFuzzyCompare( static_cast< double >( aWaveHeightDampingOverTimeFactor ),
                           m_waveHeightDampingOverTimeFactorEditor->value() ) )
    {
        return;
    }

    m_waveHeightDampingOverTimeFactorEditor->setValue( aWaveHeightDampingOverTimeFactor );
    if ( shouldUpdateData )
    {
        m_waterSettings.setWaveHeightDampingOverTimeFactor( aWaveHeightDampingOverTimeFactor );
    }
}

void WaterSettingsEditor::setWaveHeightClampingFactor( const float aWaveHeightClampingFactor, const bool shouldUpdateData )
{
    if ( ! WaterSettings::isWaveHeightClampingFactorValid( aWaveHeightClampingFactor )
         || qFuzzyCompare( static_cast< double >( aWaveHeightClampingFactor ),
                           m_waveHeightClampingFactorEditor->value() ) )
    {
        return;
    }

    m_waveHeightClampingFactorEditor->setValue( aWaveHeightClampingFactor );
    if ( shouldUpdateData )
    {
        m_waterSettings.setWaveHeightClampingFactor( aWaveHeightClampingFactor );
    }
}

void WaterSettingsEditor::setShouldPropagateWaves( const bool aShouldPropagateWaves, const bool shouldUpdateData )
{
    m_shouldPropagateWavesEditor->setChecked( aShouldPropagateWaves );
    if ( shouldUpdateData )
    {
        m_waterSettings.setShouldPropagateWaves( aShouldPropagateWaves );
    }
}

void WaterSettingsEditor::setShouldSimulateRain( const bool aShouldSimulateRain, const bool shouldUpdateData )
{
    m_shouldSimulateRainEditor->setChecked( aShouldSimulateRain );
    if ( shouldUpdateData )
    {
        m_waterSettings.setShouldSimulateRain( aShouldSimulateRain );
    }
}

void WaterSettingsEditor::setRainDropsFallTimeInterval( const int aRainDropsFallTimeInterval, const bool shouldUpdateData )
{
    if ( ! WaterSettings::isRainDropsFallTimeIntervalValid( aRainDropsFallTimeInterval )
         || aRainDropsFallTimeInterval == m_rainDropsFallTimeIntervalEditor->value() )
    {
        return;
    }

    m_rainDropsFallTimeIntervalEditor->setValue( aRainDropsFallTimeInterval );
    if ( shouldUpdateData )
    {
        m_waterSettings.setRainDropsFallTimeInterval( aRainDropsFallTimeInterval );
    }
}

void WaterSettingsEditor::setRainDropsCountPerFall( const int aRainDropsCountPerFall, const bool shouldUpdateData )
{
    if ( ! WaterSettings::isRainDropsCountPerFallValid( aRainDropsCountPerFall )
         || aRainDropsCountPerFall == m_rainDropsCountPerFallEditor->value() )
    {
        return;
    }

    m_rainDropsCountPerFallEditor->setValue( aRainDropsCountPerFall );
    if ( shouldUpdateData )
    {
        m_waterSettings.setRainDropsCountPerFall( aRainDropsCountPerFall );
    }
}

void WaterSettingsEditor::setShouldShowCoordinateSystemIndicator( const bool aShouldShowCoordinateSystemIndicator, const bool shouldUpdateData )
{
    m_shouldShowCoordSysIndicatorEditor->setChecked( aShouldShowCoordinateSystemIndicator );
    if ( shouldUpdateData )
    {
        m_waterSettings.setShouldShowCoordinateSystemIndicator( aShouldShowCoordinateSystemIndicator );
    }
}

QSpinBox* WaterSettingsEditor::intNumberEditor(
        const int min,
        const int max )
{
    QSpinBox* const editor = new QSpinBox;
    editor->setAlignment( Qt::AlignRight );
    editor->setRange( min, max );

    return editor;
}

QSpinBox* WaterSettingsEditor::meshDimensionEditor()
{
    return WaterSettingsEditor::intNumberEditor( WaterSettings::s_meshDimensionMin,
                                                 WaterSettings::s_meshDimensionMax );
}

QDoubleSpinBox* WaterSettingsEditor::realNumberEditor(
        const float min,
        const float max,
        const float step,
        const int decimalsCount )
{
    QDoubleSpinBox* const editor = new QDoubleSpinBox;
    editor->setAlignment( Qt::AlignRight );
    editor->setRange( min, max );
    editor->setSingleStep( step );
    editor->setDecimals( decimalsCount );

    return editor;
}

QDoubleSpinBox* WaterSettingsEditor::waterRippleRadiusEditor()
{
    return WaterSettingsEditor::realNumberEditor( WaterSettings::s_waterRippleRadiusFactorMin,
                                                  WaterSettings::s_waterRippleRadiusFactorMax );
}

WaterSettings WaterSettingsEditor::editedWaterSettings() const
{
    return WaterSettings
    {
        // mesh
        this->meshSize(),
        this->shouldDrawAsWireframe(),

        // wave propagation
        this->shouldPropagateWaves(),
        this->wavePropagationMethod(),
        this->wavePropagationSpeed(),
        this->waveHeightDampingOverTimeFactor(),
        this->waveHeightClampingFactor(),

        // water ripple
        this->waterRippleMethod(),
        this->waterRippleMagnitude(),
        this->waterRippleRadiusFactor(),
        this->waterRippleRotationAngle(),

        // rain
        this->shouldSimulateRain(),
        this->rainDropsTimeFallInterval(),
        this->rainDropsCountPerFall(),

        this->shouldShowCoordinateSystemIndicator()
    };
}
