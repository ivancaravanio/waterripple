#ifndef WATERRIPPLESIMULATOR_MEDIAPLAYERMANAGER_H
#define WATERRIPPLESIMULATOR_MEDIAPLAYERMANAGER_H

#include <QAbstractVideoSurface>
#include <QGraphicsVideoItem>
#include <QMediaPlayer>
#include <QPointer>
#include <QStringList>
#include <QStyle>
#include <QUrl>
#include <QVideoFrame>
#include <QVideoWidget>

QT_FORWARD_DECLARE_CLASS(QMediaPlayer)
QT_FORWARD_DECLARE_CLASS(QMimeData)
QT_FORWARD_DECLARE_CLASS(QTimer)

namespace WaterRippleSimulator {

class MediaPlayerManager : public QObject
{
    Q_OBJECT

public:
    static const int s_volumeLevelMin;
    static const int s_volumeLevelInitial;
    static const int s_volumeLevelMax;

    static const qreal s_playbackRateStep;
    static const qreal s_playbackRateMin;
    static const qreal s_playbackRateMax;

public:
    explicit MediaPlayerManager( QObject* parent = 0 );
    ~MediaPlayerManager();

    QUrl mediaUrl() const;
    bool isMediaValid() const;

    QMediaPlayer::State playerState() const;
    bool isPlaying() const;
    bool isPaused() const;
    bool isStopped() const;
    int isMuted() const;
    int volumeLevel() const;
    qreal playbackRate() const;
    // in milliseconds
    void step( const qint64& stepSize );

signals:
    void playerStateChanged( const QMediaPlayer::State playerState );
    void startedPlaying();
    void paused();
    void stopped();
    void frameReceived( const QVideoFrame& frame );

public slots:
    void setMediaUrl( const QUrl& aMediaUrl );
    void setMediaFilePath( const QString& aMediaFilePath );
    void clearMediaFilePath();
    void setPlayerState( const QMediaPlayer::State aPlayerState );
    void play();
    void pause();
    void stop();
    void setIsMuted( const bool aIsMuted );
    void setVolumeLevel( const int aVolumeLevel );
    void setPlaybackRate( const qreal& aPlaybackRate );

private slots:
    void onMediaStatusChanged();
    void onPlayerStateDataChanged( const QMediaPlayer::State aPlayerState );
    void changePlaybackRate();

#ifdef PERFORM_TESTS
    void setRandomAppFont();
#endif

private:
    void resetMediaPlayer();
    void resetVideoOutput();
    void setVideoOutput( QVideoWidget* const aVideoOutput );
    void setVideoOutput( QGraphicsVideoItem* const aVideoOutput );
    void setVideoOutput( QAbstractVideoSurface* const aVideoOutput );

    static QUrl urlFromMimeData( const QMimeData* const mimeData );

private:
    static const QList< QMediaPlayer::State > s_playerStates;
    static const QStringList s_allowedFileExtensions;

private:
    QMediaPlayer*                     m_mediaPlayer;
    QPointer< QVideoWidget >          m_videoOutputVideoWidget;
    QPointer< QGraphicsVideoItem >    m_videoOutputGraphicsVideoItem;
    QPointer< QAbstractVideoSurface > m_videoOutputAbstractVideoSurface;
    QTimer*                           m_mediaStatusUpdateTimer;
};

}

#endif // WATERRIPPLESIMULATOR_MEDIAPLAYERMANAGER_H
